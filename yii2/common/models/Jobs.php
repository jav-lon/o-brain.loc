<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jobs".
 *
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property string $time время затраченное на упражн
 * @property string $date дата выполнения
 * @property int $errors
 * @property int $rating
 */
class Jobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'name', 'time', 'date', 'errors'], 'required'],
            [['id_user', 'errors', 'rating'], 'integer'],
            [['name', 'date'], 'string', 'max' => 100],
            [['time'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'name' => 'Name',
            'time' => 'Time',
            'date' => 'Date',
            'errors' => 'Errors',
            'rating' => 'Rating',
        ];
    }
}
