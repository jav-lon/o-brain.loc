<?php

namespace frontend\controllers;

use frontend\components\filters\AccessUserFilter;
use frontend\models\Olimp;
use frontend\models\OlimpExerciseResult;
use frontend\models\OlimpStage;
use frontend\models\olympiad\Olympiad;
use frontend\models\SimulatorsResults;
use frontend\models\User;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ResultController extends Controller
{
    public function behaviors()
    {
        return [
            'access-user' => [
                'class' => AccessUserFilter::className(),
            ]
        ];
    }

    public function actionIndex($type)
    {
        $user_id = Yii::$app->session->get('login_user');
        $user = User::findOne($user_id);

        if ($type == 'olimp') {
            $olimps = null;
            if ($user->type == 'admin') {
                $olimps = Olimp::find()->all();
            } elseif ($user->type == 'partner') {
                $mineTeacherIDs = User::find()->select('id')->where(['id_who_learn' => $user_id])->column();

                $olimps = Olimp::find()->where(['id_user' => $mineTeacherIDs])->orWhere(['id_user' => $user_id])->all();

            } elseif ($user->type == 'teacher') {
                $olimps = Olimp::find()->where(['id_user' => $user_id])->all();
            } elseif ($user->type == 'student') {
                $ownTeacherID = User::find()->select('id_who_learn')->where(['id' => $user_id])->scalar();
                $olimps = Olimp::find()->where(['id_user' => $ownTeacherID])->all();
            }

            return $this->render('index', [
                'olimps' => $olimps,
            ]);
        } else if ($type == 'olympiad') {

            $olympiads = null;
            if ($user->type == 'admin') {
                $olympiads = Olympiad::find()->all();
            } elseif ($user->type == 'partner') {
                $mineTeacherIDs = User::find()->select('id')->where(['id_who_learn' => $user_id])->column();

                if ($mineTeacherIDs) {
                    // olimpiadi v prepodavatelya
                    $olympiads = Olympiad::find()->where(['id_user' => $mineTeacherIDs])->all();
                }
            } elseif ($user->type == 'teacher') {
                $olympiads = Olympiad::find()->where(['id_user' => $user_id])->all();
            } elseif ($user->type == 'student') {
                $ownTeacherID = User::find()->select('id_who_learn')->where(['id' => $user_id])->scalar();

                $olympiads = Olympiad::find()
                    ->where(['id_user' => $ownTeacherID, 'is_public' => 1])
                    ->orWhere(['id_user' => 15, 'is_public' => 1])
                    ->with(['exercises', 'author'])
                    ->all();
            }

            Yii::debug($olympiads, 'olympiad');

            return $this->render('index', [
                'olympiads' => $olympiads,
                'type' => $type,
                'user' => $user,
            ]);
        } else if($type == 'course') {
            $courses = Olimp::findAll(['is_public' => 1, 'is_end' => 0]);

            return $this->render('index', [
                'courses' => $courses,
                'type' => $type,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /*== Tablitsa reytinga olimpiadi ==*/
    public function actionOlympiadHighScoreTable($id_olympiad) {
        $user = User::findOne(Yii::$app->session->get('login_user'));

        // estli dostup polzovatelya admin, partner, teacher, student
        $is_public_olympiad = false;
        if ($user->type == 'admin') $is_public_olympiad = true;
        elseif ($user->type == 'partner') {
            // mogut smotret svoy prepodavateli i ego olimpiadi
            $mineTeacherIDs = User::find()->select('id')->where(['id_who_learn' => $user->id])->column();

            if ($mineTeacherIDs != null && Olympiad::find()->where(['id_user' => $mineTeacherIDs, 'id' => $id_olympiad])->one() !== null) {
                $is_public_olympiad = true;
            }
        }
        elseif ($user->type == 'teacher') {
            // mogut smotret svoy olimpiadi
            if (Olympiad::find()->where(['id_user' => $user->id, 'id' => $id_olympiad])->one() !== null) $is_public_olympiad = true;
        }
        elseif ($user->type == 'student') {
            // mogut smotret svoy prepodavatelnie olimpiadi
            if (Olympiad::isPublic($id_olympiad, $user->id)) $is_public_olympiad = true;
        }

        //proverayem dostup est?
        if ($is_public_olympiad) {
            $olympiad = Olympiad::find()->where(['id' => $id_olympiad])->one();

            $olympiad_high_score_table = $olympiad->getResults()
                ->select(['sum(count_true_answers) AS all_true_answers', 'sum(time) AS all_time', 'id_user'])
                ->groupBy(['id_user'])
                ->with(['student'])
                ->asArray()
                ->orderBY([
                    'all_true_answers' => SORT_DESC,
                    'all_time' => SORT_ASC,
                ])
                ->all();

            Yii::debug($olympiad_high_score_table);

            return $this->render('high-score-table', [
                'olympiad' => $olympiad,
                'olympiad_high_score_table' => $olympiad_high_score_table,
            ]);
        } else {
            Yii::$app->session->setFlash('error', 'Нет доступа!');
            return $this->goBack(); // vozvrashayem nazad
        }

    }

    public function actionListCourseMembers($id_olimp){
        $user_id = Yii::$app->session->get('login_user');
        $user = User::findOne($user_id);

        $teachers = null;
        $partners = null;
        $students = null;
        Yii::debug($user->type);
        if ($user->type == 'admin') {
            $teachers = User::findAll(['id_who_learn' => '-', 'type' => 'teacher']);
            $partners = User::findAll(['type' => 'partner']);

        } elseif ($user->type == 'partner') {
            $teachers = User::findAll(['id_who_learn' => $user_id, 'type' => 'teacher']);
        } elseif ($user->type == 'teacher') {
            $students = User::findAll(['id_who_learn' => $user_id, 'type' => 'student']);
        } elseif ($user->type == 'student') {
            return $this->redirect(['/result/course-result', 'id_olimp' => $id_olimp, 'id_user' => $user->id]);
        } else {
            throw new NotFoundHttpException();
        }

        return $this->render('list-course-members', [
            'user' => $user,
            'id_olimp' => $id_olimp,
            'students' => $students,
            'teachers' => $teachers,
            'partners' => $partners,
        ]);
    }

    public function actionCourseResult($id_olimp, $id_user){
        $course = Olimp::find()->where(['id' => $id_olimp, 'is_public' => 1])->with('stages.lessons.exercises')->one();

        Yii::debug($course);

        return $this->render('course-result', [
            'course' => $course,
            'id_olimp' => $id_olimp,
            'id_user' => $id_user,
        ]);
    }

    public function actionMember($olimp_id) {
        $authorOlimpID = Olimp::find()->select('id_user')->where(['id' => $olimp_id])->scalar();


        return $this->render('member', [

        ]);
    }

    public function actionSmartyUserResults($id) {
        $this->layout = 'pa/pa';

        $session = Yii::$app->session;
        $user = User::findOne($session->get('login_user'));

        $smarty_results = null;
        $query = User::find();
        $query->with([
            'smartyResults' => function($query) {
                $query->orderBy('created_at DESC');
            },
        ]);

        /*=== uznaem tip polzovatelya ====*/
        if ($user->type == 'teacher') {
            $query->andWhere(['id_who_learn' => $user->id]);// rezultati ucheniki
            $query->orWhere(['id' => $user->id]);//svoi rezultati
        } elseif ($user->type == 'student') {
            $query->andWhere(['id' => $user->id]); // tolko svoy rezultati
        } elseif ($user->type == 'partner') {
            $teacherIDs = User::find()->select('id')->where(['id_who_learn' => $user->id])->column();
            $query->andWhere(['id_who_learn' => $teacherIDs]); // rezultati vse ucheniki
            $query->orWhere(['id' => $user->id]); //svoy rezultati
        }

        //tolko rezultati studenta
        //$query->where(['id' => $id]);

        $smarty_results = $query->asArray()->all();


        return $this->render('smarty-user-results', [
            'smarty_results' => $smarty_results,
            'id' => $id,
        ]);
    }

    public function actionSmartyResult(){
        $session = Yii::$app->session;
        $user = User::findOne($session->get('login_user'));

        $smarty_results = null;
        /*$query = *//*SimulatorsResults::find()->*/
        if ($user->type == 'admin') {

        }
    }

    /*==ajax==*/
    public function actionGetMoreInfoSmarty(){
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost && isset($_POST['id_smarty_results'])) {
            $id_smarty_results = (int) Yii::$app->request->post('id_smarty_results');
            //Yii::debug($id_smarty_results);
            $moreInfo = SimulatorsResults::findOne($id_smarty_results);
            $smarty = $moreInfo->smarty;
            $result = [];
            $result['author'] = $smarty != null ? $smarty->user->fio : 'deleted';
            $result['name'] = $smarty != null ? $smarty->name : 'deleted';
            $result['qa'] = unserialize($moreInfo->qa);

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $result;

        } else {
            throw new NotFoundHttpException();
        }
    }

    /*==ajax==*/
    public function actionGetExerciseResult() {
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost && isset($_POST['id_exercise'], $_POST['id_user'])) {
            $id_user = (int) $request->post('id_user');
            $id_exercise = (int) $request->post('id_exercise');

            $exerciseResult = OlimpExerciseResult::findOne(['id_user' => $id_user, 'id_exercise' => $id_exercise]);

            $result = [];
            $result['id_simulator'] = $exerciseResult->simulator->id;
            $result['simulator_name'] = $exerciseResult->simulator->name;
            $result['time'] = $exerciseResult->simulator->id == 9 ? $exerciseResult->time/1000 : $exerciseResult->time;
            $result['errors'] = $exerciseResult->errors;
            $result['created_at'] = date('d.m.y', $exerciseResult->created_at);
            $result['qa'] = unserialize($exerciseResult->qa);

            Yii::debug($result['qa']);
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $result;
        } else {
            throw new NotFoundHttpException();
        }
    }

}
