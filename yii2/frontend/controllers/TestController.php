<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 23.08.2018
 * Time: 16:12
 */

namespace frontend\controllers;


use frontend\models\OlimpExerciseResult;
use frontend\models\OlimpExerciseStatus;
use frontend\models\OlimpLessonStatus;
use frontend\models\OlimpStage;
use frontend\models\OlimpStageStatus;
use frontend\models\OlimpUserProperties;
use yii\web\Controller;

class TestController extends Controller {

    public function actionIndex() {

        var_dump(null == '');
    }

    public function actionTruncateOlimpStatus() {
        $es = OlimpExerciseStatus::find()->all();
        foreach ($es as $e) {
            $e->delete();
        }

        $ls = OlimpLessonStatus::find()->all();
        foreach ( $ls as $l) {
            $l->delete();
        }

        $ss = OlimpStageStatus::find()->all();
        foreach($ss as $s) {
            $s->delete();
        }


        return $this->redirect(['/olimp']);

    }

    public function actionTruncateAllOlimpResult(){
        $or = OlimpExerciseResult::find()->all();
        foreach ($or as $r) {
            $r->delete();
        }

        return $this->redirect(['/olimp']);
    }

    public function actionTruncateAllUserProperties(){
        $or = OlimpUserProperties::find()->all();
        foreach ($or as $r) {
            $r->delete();
        }

        return $this->redirect(['/olimp']);
    }


}