<?php

namespace frontend\controllers;

use frontend\components\filters\AccessAdminFilter;
use frontend\models\CreateNewOlimpForm;
use frontend\models\Olimp;
use frontend\models\OlimpCreateExerciseForm;
use frontend\models\OlimpExercise;
use frontend\models\OlimpExerciseResult;
use frontend\models\OlimpExerciseStatus;
use frontend\models\OlimpLesson;
use frontend\models\OlimpLessonStatus;
use frontend\models\OlimpStage;
use frontend\models\OlimpStageStatus;
use frontend\models\OlimpUserProperties;
use frontend\models\Simulators;
use frontend\models\Stage;
use frontend\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use frontend\components\filters\AccessUserFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OlimpController extends Controller
{
    public function behaviors()
    {
        $behaviors = [
            /**  filter dlya, polzovatel avtorizovan ili net. Esli ne avtorizovan dostup zapreshen  */
            'access_user' => [
                'class' => AccessUserFilter::class,
            ],
            /** filter dlya, polzovtel admin,partner,teacher */
            'access-admin' => [
                'class' => AccessAdminFilter::class,
                'only' => ['create-olimp', 'create-stage', 'create-lesson', 'create-exercise', 'create-new-olimp']
            ]
        ];
        return $behaviors;
    }

    public function actions()
    {
        return [
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => Yii::$app->request->baseUrl . '/uploads/olimp/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/olimp/', // Or absolute path to directory where files are stored.
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::$app->request->baseUrl . '/uploads/olimp/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/olimp/', // Or absolute path to directory where files are stored.
            ],
            'file-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Yii::$app->request->baseUrl . '/uploads/olimp/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/olimp/', // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionCreateNewOlimp() {
        if (Olimp::find()->count() == 1) {
            Yii::$app->session->setFlash('error', 'Вы можете создать только один курс!');
            return $this->redirect(['/crud/olimp']);
        }
        $createNewOlimpForm = new CreateNewOlimpForm();

        if ($createNewOlimpForm->load(Yii::$app->request->post())) {
            if ($createNewOlimpForm->saveOlimp()) {

                Yii::$app->session->addFlash('success', "Курс создань.");
                return $this->redirect(['/crud/olimp']);
            }
        }

        return $this->render('create-new-olimp', [
            'createNewOlimpForm' => $createNewOlimpForm,
        ]);
    }

    public function actionCreateStage($id)
    {
        $id_olimp = (int) $id;
        $session = Yii::$app->session; // otkrit sessiya

        /* Polzovatele est takoy olimpiada? */
        if ( !Olimp::hasOlimp($id_olimp) ) {
            throw new NotFoundHttpException('Нет доступа такой курс!');
        }

        $newStage = new OlimpStage();

        if ($newStage->load(Yii::$app->request->post())) {
            $newStage->id_user = $session->get('login_user'); // kto sozdal stupen
            $newStage->id_olimp = $id_olimp;
            $newStage->stage_number = 1; // beforeSave izmenitsya

            if ($newStage->validate()) {
                $newStage->save(false);
                $session->setFlash('success', "Ступен создан!");

                return $this->redirect(['/olimp/create-exercise', 'id' => $id_olimp]);
            }
        }

        return $this->render('create-stage', [
            'model' => $newStage,
        ]);
    }

    public function actionCreateLesson($id)
    {
        $id_olimp = (int) $id;
        $session = Yii::$app->session;

        /* Polzovatele est takoy olimpiada? */
        if ( !Olimp::hasOlimp($id_olimp) ) {
            throw new NotFoundHttpException('Нет доступа такой курс!');
        }

        /**  stupen sozdan ili net. Esli ne sozdan, snachala sozdat stupen */
        if ( OlimpStage::find()->where(['id_olimp' => $id_olimp])->count() == 0 ) {
            $session->setFlash('error', 'Сначала создайте ступень.');

            return $this->redirect(['/crud/olimp']);
        }

        $newLesson = new OlimpLesson();

        if ($newLesson->load(Yii::$app->request->post())) {
            $newLesson->id_user = $session->get('login_user');
            $newLesson->id_olimp = $id_olimp;
            $newLesson->lesson_number = 1; // beforeSave izmenitsya

            if ($newLesson->validate()) {
                $newLesson->save(false);
                $session->setFlash('success', "Урок создан!");

                return $this->redirect(['/olimp/create-exercise', 'id' => $id_olimp]);
            }
        }

        /** @var  $stages dlya dropdownList*/
        $stages = OlimpStage::find()->select(['name', 'id'])->where(['id_olimp' => $id_olimp])->indexBy('id')->column();

        return $this->render('create-lesson', [
            'model' => $newLesson,
            'stages' => $stages,
        ]);
    }

    public function actionCreateExercise($id) {
        $id_olimp = (int) $id;
        $session = Yii::$app->session;
        $user = User::findOne($session->get('login_user'));

        /* Polzovatele est takoy olimpiada? */
        if ( !Olimp::hasOlimp($id_olimp) ) {
            throw new NotFoundHttpException('Нет доступа такой курс!');
        }

        $olimpExercise = new OlimpCreateExerciseForm();
        if ($olimpExercise->load(Yii::$app->request->post())) {
            $olimpExercise->settingsValues = Yii::$app->request->post('Settings', []);
            $olimpExercise->id_olimp = $id_olimp;

            if ($olimpExercise->saveExercise()) {
                Yii::$app->session->setFlash('success', 'Задание сохранен.');
                return $this->redirect('/crud/olimp');
            }
            $olimpExercise->id_simulators = '';

            Yii::debug($olimpExercise->errors, 'olimpExercise_errors');
        }

        /* Stupeni */
        $stages = OlimpStage::find()->select(['name', 'id'])->where(['id_olimp' => $id_olimp])->indexBy('id')->column();

        /* Trenajori */
        $simulators = Simulators::find()->select(['name', 'id'])->indexBy('id')->column();

        return $this->render('create-exercise', [
            'olimpExercise' => $olimpExercise,
            'stages' => $stages,
            'simulators' => $simulators,
            'id_olimp' => $id_olimp,
            //'settings' => $settings,
        ]);
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;
        $user = User::findOne($session->get('login_user'));
        $admins = User::find()->select('id')->where(['type' => 'admin'])->column();

        $olimps = null;
        if ( !$olimps = Olimp::find()->where(['id_user' => $user->id_who_learn, 'is_public' => '1'])->orWhere(['id_user' => $admins, 'is_public' => '1'])->all() ) {
            $session->setFlash('info', '<strong>Пока курсов нет.</strong>');
        }

        return $this->render('index', [
            'olimps' => $olimps,
        ]);
    }

    public function actionStage($id_olimp)
    {
        if (!$this->isAllowedOlimp($id_olimp)) {
            throw new ForbiddenHttpException('Нет доступа такой курс');
        }

        /** v baze dannixe (olimp_user_properties) dobavim noviy */
        OlimpUserProperties::initUserProperties($id_olimp);

        /* sozdat nesushestvuyushie stagestatus polzovatelya */
        OlimpStageStatus::initUserStageStatus($id_olimp);

        /* tekushie OlimpStageStatus dat dostup (is_public => '1') */
        OlimpStageStatus::setCurrentStageStatusAllow($id_olimp);

        /* vse stupeni */
        $stages = OlimpStage::getOlimpStage($id_olimp)->orderBy(['stage_number' => SORT_ASC])->all();

        /* vse sostoyani stupeni */
        $stageStatus = OlimpStageStatus::getUserStageStatus($id_olimp)->asArray()->indexBy('id_stage')->all();

        return $this->render('stage', [
            'stages' => $stages,
            'stageStatus' => $stageStatus,
        ]);
    }

    public function actionLesson( $id_olimp, $id_stage ) {
        if ( !$this->isAllowedStage($id_olimp, $id_stage) ) {
            throw new ForbiddenHttpException('Нет доступа такой ступен.');
        }

        /** v baze dannixe (olimp_user_properties) dobavim noviy */
        OlimpUserProperties::initUserProperties($id_olimp, $id_stage);

        /* sozdat nesushestvuyushie lessonStatus polzovatelya */
        OlimpLessonStatus::initUserLessonStatus($id_olimp, $id_stage);

        /* tekushie OlimpLessonStatus dat dostup (is_public => '1') */
        OlimpLessonStatus::setCurrentLessonStatusAllow($id_olimp, $id_stage);

        /* vse uroki */
        $lessons = OlimpLesson::getOlimpLesson($id_olimp, $id_stage)->orderBy(['lesson_number' => SORT_ASC])->all();

        /* vse sostoayni uroki */
        $lessonStatus = OlimpLessonStatus::getUserLessonStatus($id_olimp, $id_stage)->asArray()->indexBy('id_lesson')->all();

        return $this->render('lesson', [
            'lessons' => $lessons,
            'lessonStatus' => $lessonStatus,
        ]);
    }

    public function actionExercise ( $id_olimp, $id_stage, $id_lesson ) {
        if ( !$this->isAllowedLesson($id_olimp, $id_stage, $id_lesson) ) {
            throw new ForbiddenHttpException('Нет доступа такой урок.');
        }

        /** v baze dannixe (olimp_user_properties) dobavim noviy */
        OlimpUserProperties::initUserProperties($id_olimp, $id_stage, $id_lesson);

        /* sozdat nesushestvuyushie exerciseStatus polzovatelya */
        OlimpExerciseStatus::initUserExerciseStatus($id_olimp, $id_stage, $id_lesson);

        /* tekushie OlimpExerciseStatus dat dostup (is_public => '1') */
        OlimpExerciseStatus::setCurrentExerciseStatusAllow($id_olimp, $id_stage, $id_lesson);

        /* vse zadanie */
        $exercises = OlimpExercise::getOlimpExercise($id_olimp, $id_stage, $id_lesson)->orderBy(['exercise_number' => SORT_ASC])->all();

        /* vse sostoayni zadanie */
        $exerciseStatus = OlimpExerciseStatus::getUserExerciseStatus($id_olimp, $id_stage, $id_lesson)->asArray()->indexBy('id_exercise')->all();

        return $this->render('exercise', [
            'exercises' => $exercises,
            'exerciseStatus' => $exerciseStatus,
        ]);
    }

    public function actionResult ( $id_exercise ) {
        $result = OlimpExerciseResult::findOne(['id_exercise' => $id_exercise, 'id_user' => Yii::$app->session->get('login_user')]);

        if ( !$result ) {
            throw new NotFoundHttpException();
        }

        return $this->render('result', [
            'result' => $result,
        ]);

    }

    /* ajax */
    public function actionExerciseSaveResult() {
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            if (
                isset($_POST['id_user']) &&
                isset($_POST['id_exercise']) &&
                isset($_POST['number_job']) &&
                isset($_POST['time_job']) &&
                isset($_POST['errors']) &&
                isset($_POST['rating']) &&
                isset($_POST['arr_answers'])
            ) {
                $result = '...';

                $id_user = (int) $request->post('id_user');
                $id_exercise = (int) $request->post('id_exercise');
                $id_simulators = (int) $request->post('number_job');
                $work_time = (int) $request->post('time_job');
                $errors = (int) $request->post('errors');
                $rating = (float) $request->post('rating');
                $arr_answers = $request->post('arr_answers');


                /* proverka: polzovatel i current_exercise pravilno*/
                $session = Yii::$app->session;
                $user_id = $session->get('login_user');

                $olimpExerciseStatus = OlimpExerciseStatus::findOne(['id_user' => $user_id, 'id_exercise' => $id_exercise]);
                if ($user_id == $id_user && $olimpExerciseStatus->is_public == 1 && $olimpExerciseStatus->is_end == 0 && OlimpExerciseResult::find()->where(['id_user' => $user_id, 'id_exercise' => $id_exercise])->count() == 0) {
                    /* soxranenie rezultata */
                    $newExerciseResult = new OlimpExerciseResult();
                    $newExerciseResult->id_user = $user_id;
                    $newExerciseResult->id_exercise = $id_exercise;
                    $newExerciseResult->id_simulators = $id_simulators;
                    $newExerciseResult->time = $work_time;
                    $newExerciseResult->errors = $errors;
                    $newExerciseResult->rating = $rating;
                    $newExerciseResult->qa = serialize($arr_answers);
                    if ($newExerciseResult->save()) {
                        $result = 'OK';

                        OlimpUserProperties::initNextExercise($olimpExerciseStatus->id_olimp, $id_exercise);
                    }
                } else {
                    $result = 'no id_user | no cur_exercise | result exists';
                    throw new NotFoundHttpException('exists');
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'result' => $result,
                    'id_user' => $id_user,
                    'id_exercise' => $id_exercise,
                    'id_simulators' => $id_simulators,
                    'work_time' => $work_time,
                    'errors' => $errors,
                    'rating' => $rating,
                    'arr_answers' => $arr_answers,
                ];
            } else {
                throw new NotFoundHttpException('no parameters');
            }
        } else {
            throw new NotFoundHttpException('404');
        }
    }

    /* polzovatele est dostup takoy olimpiadu */
    protected function isAllowedOlimp($id_olimp){
        if ( !( $olimp = Olimp::find()->select(['is_end', 'is_public'])->where(['id' => $id_olimp])->one() ) || $olimp->is_end == 1 || $olimp->is_public == 0) {
            return false;
        }

        $session = Yii::$app->session;
        $user = User::findOne(['id' => $session->get('login_user')]);

        $authorIdOlimp = Olimp::find()->select('id_user')->where(['id' => $id_olimp])->scalar();

        if (User::find()->select('type')->where(['id' => $authorIdOlimp])->scalar() == 'admin') {

            return true;
        }

        if ($user->id_who_learn == $authorIdOlimp) {

            return true;
        }

        return false;
    }

    /* polzovatele est dostup takoy stupen */
    protected function isAllowedStage($id_olimp, $id_stage) {
        $userStageStatus = OlimpStageStatus::getUserStageStatus($id_olimp)->andWhere(['id_stage' => $id_stage])->one();
        if ( $userStageStatus && $userStageStatus->is_public == 1 ) {

            return true;
        }

        return false;
    }

    /* polzovatele est dostup takoy uroke */
    protected function isAllowedLesson($id_olimp, $id_stage, $id_lesson) {
        $userLessonStatus = OlimpLessonStatus::getUserLessonStatus($id_olimp, $id_stage)->andWhere(['id_lesson' => $id_lesson])->one();
        if ( $userLessonStatus && $userLessonStatus->is_public == 1 ) {

            return true;
        }

        return false;
    }

    /* ajax, dlya DepDrop, vivodit subkategoriya */
    public function actionSubcat() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $stage_id = $parents[0];
                $out = self::getSubCatList($stage_id);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                Yii::debug($out);
                return Json::encode(['output'=> $out, 'selected'=>'']);
                //return;
            }
        }
        return Json::encode(['output'=>'', 'selected'=>'']);
    }

    /*dlya Subcat, return Lesson list*/
    public static function getSubCatList($stage_id) {

        $lessons = OlimpLesson::find()->select(['id', 'name'])->where(['id_stage' => $stage_id])->all();
        $data = ArrayHelper::toArray($lessons, [
            'frontend\models\Lesson' => [
                'id',
                'name',
            ]
        ]);
        return $data;
    }

}