<?php

namespace frontend\controllers;

use frontend\components\filters\AccessAdminFilter;
use frontend\components\filters\AccessUserFilter;
use frontend\models\CourseResult;
use frontend\models\ExerciseResult;
use frontend\models\Setting;
use frontend\models\Simulators;
use frontend\models\User;
use Yii;
use frontend\models\Stage;
use frontend\models\Lesson;
use frontend\models\Exercise;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CourseController extends \yii\web\Controller
{
    public $isCourseEnd = false;

    public function behaviors()
    {
        return [
            /**  filter dlya, polzovatel avtorizovan ili net. Esli ne avtorizovan dostup zapreshen  */
            'access_user' => [
                'class' => AccessUserFilter::class,
            ],
            /** filter dlya, polzovtel admin */
            'access-admin' => [
                'class' => AccessAdminFilter::class,
                'only' => ['create-course', 'create-stage', 'create-lesson', 'create-exercise', 'get-settings-list']
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => Yii::$app->request->baseUrl . '/uploads/imperavi/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/imperavi/', // Or absolute path to directory where files are stored.
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::$app->request->baseUrl . '/uploads/imperavi/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/imperavi/', // Or absolute path to directory where files are stored.
            ],
            'file-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Yii::$app->request->baseUrl . '/uploads/imperavi/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/imperavi/', // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionCreateStage()
    {
        $model = new Stage();

        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session; // otkrit sessiya
            $model->id_user = $session->get('login_user'); // kto sozdal stupen
            if ($model->validate()) {
                $model->save(false);
                $session->setFlash('stageCreated', "Ступен создан!");

                return $this->redirect(['/course/create-course']);
            }
        }

        return $this->render('create-stage', [
            'model' => $model,
        ]);
    }

    public function actionCreateLesson()
    {
        $session = Yii::$app->session;

        /**  stupen sozdan ili net. Esli ne sozdan, snachala sozdat stupen */
        if ( Stage::find()->count() == 0 ) {
            $session->setFlash('createStage', 'Сначала создайте ступень.');

            return $this->redirect(['/course/create-course']);
        }

        $user = User::findOne($session->get('login_user'));

        if ( $user->type == 'admin' ) {
            $model = new Lesson(['scenario' => 'admin']);
        } else {
            $model = new Lesson();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->id_user = $session->get('login_user');

            /* esli  */
            if ( $model->scenario == 'admin' ) {
                if ( !$model->plan ) {
                    $model->is_public = '0';
                    $model->is_draft = '1';
                } else {
                    $model->is_public = '0';
                    $model->is_draft = '0';
                }
            }

            if ($model->validate()) {
                $model->save(false);
                $session->setFlash('lessonCreated', "Урок создан!");

                return $this->redirect(['/course/create-course']);
            }
        }

        /** @var  $stages dlya dropdownList*/
        $stages = Stage::find()->select(['title', 'id'])->indexBy('id')->column();

        return $this->render('create-lesson', [
            'model' => $model,
            'stages' => $stages,
        ]);
    }

    public function actionCreateExercise()
    {
        $session = Yii::$app->session;

        /** esli ne sozdan urok */
        if ( Lesson::find()->count() == 0 ) {
            $session->setFlash('createExercise', 'Сначала создайте урок.');

            return $this->redirect(['/course/create-course']);
        }

        $user = User::findOne($session->get('login_user'));

        /**
         * esli polzovatel admin, to mojno propustit opisanie zadanie
         */
        if ( $user->type == 'admin' ) {
            $newExercise = new Exercise(['scenario' => 'admin']);
        } else {
            $newExercise = new Exercise();
        }
        $settings = new Setting();

        $settingsValues = Yii::$app->request->post('Settings', []);
        Yii::debug($settingsValues);

        if ($newExercise->load(Yii::$app->request->post())) {

            /*$settings->level = $settingsValues['level'];
            $settings->rows = $settingsValues['rows'];
            $settings->operation = $settingsValues['operation'];
            $settings->voice = $settingsValues['voice'];
            $settings->rules = $settingsValues['rules'];
            $settings->id_rules = $settingsValues['id_rules'];
            $settings->delay = $settingsValues['delay'];*/
            $settings->attributes = $settingsValues;
            $settings->id_job = $newExercise->id_simulators;

            Yii::debug($settings->attributes, 'pre');


            /** esli opisanie zadanie ne vvodil */
            if ( $newExercise->description == '' ) {
                $newExercise->is_draft = '1';
                $newExercise->is_public = '0';
            } else {
                $newExercise->is_draft = '0';
                $newExercise->is_public = '1';
            }

            $settings->id_user = $newExercise->id_user = $session->get('login_user');

            $settings::$exercise = $newExercise;
            $settings->initAttributes();

            $lesson = Lesson::findOne(['id' => $newExercise->id_lesson]);
            if ( $newExercise->is_draft == '0' ) {
                $lesson->is_public = '1';
            }

            /** nachat tranzaksiyu */
            $transaction = Yii::$app->db->beginTransaction();

            try {
                $settings->save();

                Yii::debug($settings->attributes, 'post');

                $newExercise->id_settings = $settings->id;
                if ( !$newExercise->save() || !$lesson->save()) {
                    throw new Exception("tranzaksii ne srabotal");
                }

                $transaction->commit();

                if ( $newExercise->is_draft ) {
                    $session->setFlash('createdExercise', 'Задание сохранен в папке черновик');
                } else {
                    $session->setFlash('createdExercise', 'Задание создан.');
                }

            } catch (\Exception $e) {
                $transaction->rollBack();

                $session->setFlash('createdExercise', "Error! Задание не сохранен!");
                Yii::debug([$settings->errors, $newExercise->errors, $lesson->errors], 'errors');
            } catch (\Throwable $e) {
                $transaction->rollBack();

                $session->setFlash('createdExercise', "Error! Задание не сохранен!");
                Yii::debug([$settings->errors, $newExercise->errors, $lesson->errors], 'errors');
            }

            return $this->redirect('/course/create-course');
        }

        /* Stupeni */
        $stages = Stage::find()->select(['title', 'id'])->indexBy('id')->column();

        /*var_dump($stages);
        exit;*/
        /* Trenajori */
        $simulators = Simulators::find()->select(['name', 'id'])->indexBy('id')->column();
        /*var_dump($simulators);
                exit;*/
        return $this->render('create-exercise', [
            'newExercise' => $newExercise,
            'stages' => $stages,
            'simulators' => $simulators,
            'settings' => $settings,
        ]);
    }

    public function actionCreateCourse()
    {
        return $this->render('create-course');
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;

        /** v baze dannixe (course_results) est takoy polzovatel */
        $courseResult = null;
        if ( !( $courseResult = CourseResult::findOne(['id_user' => $session->get('login_user')]) ) ) {
            $courseResult = new CourseResult();
            $courseResult->id_user = $session->get('login_user');
            $courseResult->save();
        }

        /* Vse publichnie id stupeni */
        $stage_object = Stage::getPublicStages();
        $stages = null;
        if ($stage_object) {
            //$stages = $stage_object->all();
        }

        /** nado znat tekushiy stupen */
        if ( !$stages ) {

            $session->setFlash('hasStages', 'Данный момент Курс нет!');
        } else {

            // esli polzovatel tolko chto nachal kurs
            if ( $courseResult->id_current_stage === null ) {
                $courseResult = CourseResult::findOne(['id_user' => $session->get('login_user')]);
                $stages = Stage::find()->limit(1)->one();
                $courseResult->id_current_stage = $stages->id;
                //Yii::debug($courseResult->id_current_stage);
                $courseResult->save();
            }
        }


        return $this->render('index', [
            'stages' => $stages,
            'courseResult' => $courseResult,
            'session' => $session,
        ]);
    }

    public function actionLesson($id_stage) {
        $session = Yii::$app->session;

        /**
         * nado sdelat proverka polzovatelya
         */

        $courseResult = CourseResult::findOne(['id_user' => $session->get('login_user')]);

        /**
         * esli polzovatel ne sotvestvuet id_current_stage = $id_stage ili netu resultata, otpravlayem v stupen
         */
        if ( !$courseResult || $courseResult->id_current_stage != $id_stage ) {

            throw new NotFoundHttpException('Такой урок нет!');
        }

        /* vse uroki */
        $lessons = Lesson::find()->where(['id_stage' => $id_stage, 'is_public' => '1'])->all();

        if ( !$lessons ) {
            Yii::debug('setFlash', 'app');
            $session->setFlash('hasLessons', 'Данный момент Уроки нет!');
        } else {

            /* esli polzovatel tolko chto nachal stupen */
            if ( $courseResult->id_current_lesson == null ) {
                $lesson = Lesson::findOne(['id_stage' => $id_stage, 'is_public' => '1']);
                $courseResult->id_current_lesson = $lesson->id;
                $courseResult->save();
            }
        }

        return $this->render('lesson', [
            'lessons' => $lessons,
            'courseResult' => $courseResult,
        ]);
    }

    public function actionExercise($id_lesson) {
        $session = Yii::$app->session;

        $courseResult = CourseResult::findOne(['id_user' => $session->get('login_user')]);

        /**
         * esli polzovatel ne sotvestvuet id_current_stage = $id_stage ili netu resultata, otpravlayem v stupen
         */
        if ( !$courseResult || $courseResult->id_current_lesson != $id_lesson ) {

            throw new NotFoundHttpException('Такой урок нет!');
        }

        /* vse КР */
        $classwork = Exercise::find()->where(['id_lesson' => $id_lesson, 'work_type' => 'classwork', 'is_public' => '1'])->all();

        /* vse ДР */
        $homework = Exercise::find()->where(['id_lesson' => $id_lesson, 'work_type' => 'homework', 'is_public' => '1'])->all();


        if ( !$classwork || !$homework ) {
            Yii::debug('setFlash', 'app');
            $session->setFlash('hasExercise', 'Данный момент задании нет!');
        } else {

            /* esli polzovatel tolko chto nachal urok */
            if ( !$courseResult->id_current_exercise ) {
                $exercise = Exercise::findOne(['id_lesson' => $id_lesson, 'is_public' => '1', 'work_type' => 'classwork']);
                $courseResult->id_current_exercise = $exercise->id;
                $courseResult->save();
            }
        }

        return $this->render('exercise', [
            'classwork' => $classwork,
            'homework' => $homework,
            'courseResult' => $courseResult,
        ]);
    }

    /* ajax */
    public function actionExerciseSaveResult() {
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            if (
                isset($_POST['id_user']) &&
                isset($_POST['id_exercise']) &&
                isset($_POST['number_job']) &&
                isset($_POST['time_job']) &&
                isset($_POST['errors']) &&
                isset($_POST['rating']) &&
                isset($_POST['arr_answers'])
            ) {
                $result = '...';

                $id_user = (int) $request->post('id_user');
                $id_exercise = (int) $request->post('id_exercise');
                $id_simulators = (int) $request->post('number_job');
                $work_time = (int) $request->post('time_job');
                $errors = (int) $request->post('errors');
                $rating = (float) $request->post('rating');
                $arr_answers = $request->post('arr_answers');


                /* proverka: polzovatel i current_exercise pravilno*/
                $session = Yii::$app->session;
                $user_id = $session->get('login_user');
                $current_exercise_id = CourseResult::find()->select(['id_current_exercise'])->where(['id_user' => $user_id])->scalar();

                if ($user_id == $id_user && $id_exercise == $current_exercise_id && ExerciseResult::find()->where(['id_user' => $user_id, 'id_exercise' => $current_exercise_id])->count() == 0) {
                    /* soxranenie rezultata */
                    $newExerciseResult = new ExerciseResult();
                    $newExerciseResult->id_user = $user_id;
                    $newExerciseResult->id_exercise = $current_exercise_id;
                    $newExerciseResult->id_simulators = $id_simulators;
                    $newExerciseResult->time = $work_time;
                    $newExerciseResult->errors = $errors;
                    $newExerciseResult->rating = $rating;
                    if ($newExerciseResult->save()) {
                        $result = 'OK';

                        $this->initNextExercise();
                    }
                } else {
                    $result = 'no id_user | no cur_exercise | result exists';
                    throw new NotFoundHttpException('exists');
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'result' => $result,
                    'id_user' => $id_user,
                    'id_exercise' => $id_exercise,
                    'id_simulators' => $id_simulators,
                    'work_time' => $work_time,
                    'errors' => $errors,
                    'rating' => $rating,
                    'arr_answers' => $arr_answers,
                ];
            } else {
                throw new NotFoundHttpException('no parameters');
            }
        } else {
            throw new NotFoundHttpException('404');
        }
    }

    public function actionExerciseResult($id_exercise) {
        $session = Yii::$app->session;
        $isResult = false;

        $result = ExerciseResult::findOne(['id_user' => $session->get('login_user'), 'id_exercise' => $id_exercise]);
        $exercise = Exercise::findOne(['id' => $id_exercise]);
        $lesson_title = Lesson::find()->select(['title'])->where(['id' => $exercise->id_lesson])->scalar();
        /* esli zadanie proyden */
        if ($result) {
            $isResult = true;

            return $this->render('exercise-result', [
                'isResult' => $isResult,
                'result' => $result,
                'exercise' => $exercise,
                'lesson_title' => $lesson_title,
            ]);
        } else { // zadanie ne proyden

            return $this->render('exercise-result', [
                'isResult' => $isResult,
                'exercise' => $exercise,
                'lesson_title' => $lesson_title,
            ]);
        }
    }

    /* ajax, dlya DepDrop, vivodit subkategoriya */
    public function actionSubcat() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $stage_id = $parents[0];
                $out = self::getSubCatList($stage_id);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                Yii::debug($out);
                return Json::encode(['output'=> $out, 'selected'=>'']);
                //return;
            }
        }
        return Json::encode(['output'=>'', 'selected'=>'']);
    }

    /*dlya Subcat, return Lesson list*/
    public static function getSubCatList($stage_id) {

        $lessons = Lesson::find()->select(['id', 'title'])->where(['id_stage' => $stage_id, 'is_draft' => '0'])->all();
        $data = ArrayHelper::toArray($lessons, [
            'frontend\models\Lesson' => [
                'id',
                'name' => 'title',
            ]
        ]);
        return $data;
    }

    /**  ajax, return: vozvratit nujiniy nastroyki dla sozdanie zadanie */
    public function actionGetSettingsList() {
        if ( Yii::$app->request->isAjax ) {
            $id_simulators = Yii::$app->request->post('id_simulators');
            if ( $id_simulators ) {

                return $this->renderAjax('get-settings-list', [
                    'id_simulators' => $id_simulators,
                ]);
            } else {
                return '';
            }
        }
    }


    /**
     * privatnie funksiyii dlya upravlenie kursa
     */

    private function initNextExercise() {
        $courseResult = CourseResult::findOne(['id_user' => Yii::$app->session->get('login_user')]);

        $next_exercise_id = $this->getNextExercise();
        $next_lesson_id = null;
        Yii::debug($next_exercise_id, '$next_exercise_id');
        if ($next_exercise_id == false) {
            $next_lesson_id = $this->getNextLesson();
            Yii::debug($next_lesson_id, '$next_lesson_id');
            if ( $next_lesson_id == false ) {
                $next_stage_id = $this->getNextStage();
                Yii::debug($next_stage_id, '$next_stage_id');
                if ( $next_stage_id == false ) {
                    $courseResult->id_current_exercise = 0;
                    $courseResult->id_current_lesson = 0;
                    $courseResult->id_current_stage = 0;
                    $courseResult->save();

                    $this->isCourseEnd = true; //kurs okonchen
                } else {
                    $courseResult->id_current_exercise = null;
                    $courseResult->id_current_lesson = null;
                    $courseResult->id_current_stage = $next_stage_id;
                    $courseResult->save();
                }
            } else {
                $courseResult->id_current_exercise = null;
                $courseResult->id_current_lesson = $next_lesson_id;
                $courseResult->save();
            }
        } else {
            Yii::debug($next_exercise_id);
            $courseResult->id_current_exercise = $next_exercise_id;
            $courseResult->save();
        }
    }

    private function getNextExercise() {
        $user_id = Yii::$app->session->get('login_user');
        $userCourseResult = CourseResult::findOne(['id_user' => $user_id]);

        $work_type = Exercise::find()->select(['work_type'])->where(['id' => $userCourseResult->id_current_exercise])->scalar();


        $next_exercise_id = Exercise::find()
            ->select(['id'])
            ->where(['>', 'id', $userCourseResult->id_current_exercise])
            ->andWhere(['id_lesson' => $userCourseResult->id_current_lesson, 'is_public' => '1', 'work_type' => $work_type])
            ->limit(1)
            ->scalar();

        /* esli ne nayden klassnie raboti*/
        if ($next_exercise_id == null && $work_type == 'classwork') {
            $next_exercise_id = Exercise::find()
                ->select(['id'])
                ->where(['>', 'id', $userCourseResult->id_current_exercise])
                ->andWhere(['id_lesson' => $userCourseResult->id_current_lesson, 'is_public' => '1', 'work_type' => 'homework'])
                ->limit(1)
                ->scalar();
        }

        /* esli ne nayden domashnie zadanie, to ukazat sledushuyu urok */
        /*if ( $next_exercise_id == null ) {
            $this->getNextLesson($userCourseResult->id_current_lesson);
        }*/

        return $next_exercise_id;
    }

    private function getNextLesson() {
        $user_id = Yii::$app->session->get('login_user');
        $userCourseResult = CourseResult::findOne(['id_user' => $user_id]);

        $next_lesson_id = Lesson::find()
            ->select('id')
            ->where(['>', 'id', $userCourseResult->id_current_lesson])
            ->andWhere(['id_stage' => $userCourseResult->id_current_stage, 'is_public' => '1'])
            ->limit(1)
            ->scalar();

        return $next_lesson_id;
    }

    private function getNextStage() {
        $user_id = Yii::$app->session->get('login_user');
        $userCourseResult = CourseResult::findOne(['id_user' => $user_id]);

        $public_stages = Stage::getPublicStages()->select(['id'])->asArray()->all();
        $public_stages = ArrayHelper::getColumn($public_stages, 'id');

        $next_stage_id = Stage::find()
            ->select('id')
            ->where(['>', 'id', $userCourseResult->id_current_stage])
            ->andWhere(['id' => $public_stages])
            ->limit(1)
            ->scalar();

        return $next_stage_id;
    }

}
