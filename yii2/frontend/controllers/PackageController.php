<?php

namespace frontend\controllers;

use frontend\components\filters\AccessAdminFilter;
use frontend\components\filters\AccessUserFilter;
use frontend\models\package\PackageTheme;
use frontend\models\package\SelectUser;
use frontend\models\User;
use Yii;
use frontend\models\package\Package;
use frontend\models\package\PackageSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access-user' => [
                'class' => AccessUserFilter::class,
                'only' => ['franchise', 'partner'],
            ],
            'access-admin' => [
                'class' => AccessAdminFilter::class,
                'only' => ['index', 'view', 'delete', 'update', 'select-user']
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //dlya sozdaniy tematika dlya paketa
    public function actionCreatePackageTheme() {
        $newTheme = new PackageTheme();

        if ($newTheme->load(Yii::$app->request->post()) && $newTheme->validate()) {
            if ($newTheme->save(false)) {

                Yii::$app->session->setFlash('success', 'Тема создан.');

                return $this->goBack();
            }
        }

        return $this->render('create-package-theme', [
            'newTheme' => $newTheme,
        ]);
    }

    public function actionSelectUser() {
        $model = new SelectUser();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->addUserInPackage();

                Yii::$app->session->addFlash('success', '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Сделано!');
            return $this->redirect('index');
        }

        $userIDs = User::find()->select('fio')->indexBy('id')->column();
//        var_dump($userIDs);
//        exit;

        return $this->render('select-user', [
            'model' => $model,
            'userIDs' => $userIDs,
        ]);
    }

    public function actionFranchise(){
        $franchises = Package::find()
            ->where(['type' => 'franchise'])
            ->orderBy([
                'theme' => SORT_ASC,
            ])
            ->all();
        $user_id = Yii::$app->session->get('login_user');

        $buttons = [];
        foreach ($franchises as $franchise) {
            $publicIDs = unserialize($franchise->public_users);
            if (in_array($user_id, $publicIDs)) {
                $buttons[] = $franchise;
            }
        }

        return $this->render('franchise', [
            'buttons' => $buttons,
        ]);
    }

    public function actionPartner(){
        $partners = Package::find()
            ->where(['type' => 'partner'])
            ->orderBy([
                'theme' => SORT_ASC,
            ])
            ->all();
        $user_id = Yii::$app->session->get('login_user');

        $buttons = [];
        foreach ($partners as $partner) {
            $publicIDs = unserialize($partner->public_users);
            if (in_array($user_id, $publicIDs)) {
                $buttons[] = $partner;
            }
        }

        return $this->render('partner', [
            'buttons' => $buttons,
        ]);
    }

    /**
     * Lists all Package models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Package model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Package model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Package();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->saveAll()) {

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $userIDs = User::find()->select(['fio'])->asArray()->indexBy('id')->column();

        return $this->render('create', [
            'model' => $model,
            'userIDs' => $userIDs
        ]);
    }

    /**
     * Updates an existing Package model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->saveAll()) {

                //Yii::$app->session->addFlash('success', 'Сделано');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $userIDs = User::find()->select(['fio'])->asArray()->indexBy('id')->column();

        $package = Package::findOne($id);
        $publicIDs = unserialize($package->public_users);

        $selected_IDs = [];
        foreach ( $publicIDs as $publicID) {
            $selected_IDs[$publicID] = ['selected' => true];
        }

        return $this->render('update', [
            'model' => $model,
            'userIDs' => $userIDs,
            'selected_IDs' => $selected_IDs,
        ]);
    }

    /**
     * Deletes an existing Package model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Package model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Package the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /* ajax, dlya DepDrop, vivodit subkategoriya */
    public function actionGetThemes() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $package_name = $parents[0];
                $selected = '';
                if (isset($_POST['depdrop_all_params']['model-theme-id'])) {
                    $selected = $_POST['depdrop_all_params']['model-theme-id'];
                }
                $out = self::getGetThemeList($package_name);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]

                //$selected = '';//self::getDefaultThemeName($package_theme_id);
                // the getDefaultSubCat function will query the database
                // and return the default sub cat for the cat_id
                //Yii::debug($out);
                return Json::encode(['output'=> $out, 'selected'=>$selected]);
                //return;
            }
        }
        return Json::encode(['output'=>'', 'selected'=>'']);
    }

    /*dlya Subcat, return Lesson list*/
    public static function getGetThemeList($package_name) {

        $themes = PackageTheme::find()->select(['id', 'name'])->where(['package_name' => $package_name])->all();
        $data = ArrayHelper::toArray($themes, [
            'frontend\models\package\PackageTheme' => [
                'id',
                'name',
            ]
        ]);
        return $data;
    }

    /*dlya subkategoriy, */
    public static function getDefaultThemeName($package_theme_id) {


    }
}
