<?php

namespace frontend\controllers;

use frontend\components\filters\AccessAdminTeacherFilter;
use frontend\components\filters\AccessUserFilter;
use frontend\models\olympiad\OlympiadExercise;
use frontend\models\olympiad\OlympiadResult;
use frontend\models\Smarty;
use frontend\models\SmartyExample;
use frontend\models\User;
use Mpdf\Tag\Ol;
use Yii;
use frontend\models\olympiad\Olympiad;
use frontend\models\olympiad\OlympiadSearch;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OlympiadController implements the CRUD actions for Olympiad model.
 */
class OlympiadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'save-result' => ['POST'],
                ],
            ],
            'access-admin-teacher' => [
                'class' => AccessAdminTeacherFilter::className(),
                'only' => ['index', 'view', 'create', 'delete', 'publish-olympiad', 'hide-olympiad']
            ],
            'access-user' => [
                'class' => AccessUserFilter::className(),
                'only' => ['list', 'save-result', 'result']
            ]
        ];
    }


    public function actionList() {
        $user = User::findOne(Yii::$app->session->get('login_user'));

        $time = time() - ((24*60*60) - 60);
        $olympiads = Olympiad::find()
            ->where([
                'and',
                [
                    'or',
                    ['=', 'id_user', $user->id_who_learn],
                    ['=', 'id_user', 15]
                ],
                [
                    'and',
                    ['>', 'date', $time],
                    ['=', 'is_public', 1]
                ]
            ])
            ->with('author')
            ->all();

        //Yii::debug($olympiads);

        return $this->render('list', [
            'olympiads' => $olympiads,
            'user' => $user,
        ]);
    }

    public function actionExerciseList($id_olympiad) {
        $user = User::findOne(Yii::$app->session->get('login_user'));

        /*olympiada aktivna i polzovatele est dostup*/
        $is_active = Olympiad::isActive($id_olympiad);
        $is_public = Olympiad::isPublic($id_olympiad, $user->id);
        Yii::debug($is_active, 'is_active');
        Yii::debug($is_public, 'is_public');
        if (!$is_active || !$is_public) {
            Yii::$app->session->setFlash('error', 'Нет доступа олимпиаду!');
            return $this->redirect(['list']);
        }

        /*olimpiada*/
        $olympiad = Olympiad::findOne($id_olympiad);

        return $this->render('exercise-list', [
            'olympiad' => $olympiad,
        ]);
    }

    /*ajax*/
    public function actionSaveResult() {
        $request = Yii::$app->request;

        if ($request->isPost && $request->isAjax) { // ajax i post request
            if (isset(
                $_POST['id_user'],
                $_POST['id_exercise'],
                $_POST['time'],
                $_POST['count_true_answers'],
                $_POST['count_errors'],
                $_POST['simulator_id'],
                $_POST['id_smarty'],
                $_POST['qa']
            )) {
                /*== vse POST peremennie proveryayem ==*/
                $new_olympiad_id_user            = (int) $request->post('id_user');
                $new_olympiad_id_exercise        = (int) $request->post('id_exercise');
                $new_olympiad_time               = (int) $request->post('time');
                $new_olympiad_count_true_answers = (int) $request->post('count_true_answers');
                $new_olympiad_count_errors       = (int) $request->post('count_errors');
                $new_olympiad_simulator_id       = (int) $request->post('simulator_id'); //ne ispolzuetsya
                $new_olympiad_id_smarty          = (int) $request->post('id_smarty');//ne ispolzuetsya
                $new_olympiad_qa                 = $request->post('qa');

                $user = User::findOne(Yii::$app->session->get('login_user'));

                $exercise = OlympiadExercise::findOne($new_olympiad_id_exercise);
                if ($exercise === null || $user->id != $new_olympiad_id_user) throw new NotFoundHttpException(); // esli zadanie net ili polzovatel nepravilniy

                /*== Polzovatele est dostup olimpiade i olimpiade net takoy rezultat zadanie ==*/
                if (
                    Olympiad::isActive($exercise->id_olympiad) &&
                    Olympiad::isPublic($exercise->id_olympiad, $user->id) &&
                    OlympiadResult::findOne(['id_user' => $user->id, 'id_exercise' => $exercise->id]) === null
                ) {
                    /*== Soxranenie rezultata ==*/
                    $newOlympiadResult = new OlympiadResult();
                    $newOlympiadResult->id_user = $user->id;
                    $newOlympiadResult->id_olympiad = $exercise->id_olympiad;
                    $newOlympiadResult->id_exercise = $new_olympiad_id_exercise;
                    $newOlympiadResult->time = $new_olympiad_time;
                    $newOlympiadResult->qa = serialize($new_olympiad_qa);
                    $newOlympiadResult->count_true_answers = $new_olympiad_count_true_answers;
                    $newOlympiadResult->count_errors = $new_olympiad_count_errors;
                    if (!$newOlympiadResult->save()) {
                        throw new Exception('Error!', $newOlympiadResult->errors);
                    }

                    /*== return result link ==*/
                    return '/olympiad/exercise-result?id_olympiad_result=' . $newOlympiadResult->primaryKey;
                }
            }
        }

        throw new NotFoundHttpException();
    }

    public function actionExerciseResult($id_olympiad_result) {

        /**
         * ====Nujno realizovat dostupi========
         */

        $olympiadResult = OlympiadResult::findOne($id_olympiad_result);

        return $this->render('result', [
            'olympiadResult' => $olympiadResult,
        ]);
    }

    /**
     * Lists all Olympiad models.
     * =====Olimpiadu mojno sozdat tolko admin i prepodavateli==========
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OlympiadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /*==dlya publikatsii==*/
    public function actionPublishOlympiad($id_olympiad) {
        $user = User::findOne(Yii::$app->session->get('login_user'));

        $olympiad = Olympiad::findOne(['id' => $id_olympiad]);
        $olympiad->scenario = Olympiad::SCENARIO_PUBLISH;

        // prepodavatel ne mogut opublikovat drugoe olimpiadu
        if ($user->type == 'teacher' && $olympiad !== null && $olympiad->id_user != $user->id) {
            Yii::$app->session->setFlash('error','Нет доступа!');
            return $this->redirect(['index']);
        }
        Yii::debug($olympiad->exercises, 'exercises');
        if ( $olympiad->exercises == null ) {
            Yii::$app->session->setFlash('error','Сначала добавьте задание!');
            return $this->redirect(['index']);
        }
        $olympiad->is_public = 1; // Opublikuem
        if ($olympiad->save()) {

            Yii::$app->session->setFlash('success','Олимпиада успешно опубликован.');
            return $this->redirect(['index']);
        } else {
            Yii::debug($olympiad->errors, 'error db');
            Yii::$app->session->setFlash('warning','Ошибка!');
            return $this->redirect(['index']);
        }
    }

    /*skrit olimpiadu*/
    public function actionHideOlympiad($id_olympiad){
        $user = User::findOne(Yii::$app->session->get('login_user'));

        $olympiad = Olympiad::findOne(['id' => $id_olympiad]);
        $olympiad->scenario = Olympiad::SCENARIO_PUBLISH;

        // prepodavatel ne mogut skrit drugoe olimpiadu
        if ($user->type == 'teacher' && $olympiad !== null && $olympiad->id_user != $user->id) {
            Yii::$app->session->setFlash('error','Нет доступа!');
            return $this->redirect(['index']);
        }
        $olympiad->is_public = 0; // skrivaem
        if ($olympiad->save()) {

            Yii::$app->session->setFlash('info','Операция успешно завершена.');
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('warning','Ошибка!');
            return $this->redirect(['index']);
        }
    }

    /**
     * Displays a single Olympiad model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $olympiadExercises = OlympiadExercise::find()->where(['id_olympiad' => $id])->with(['smarty'])->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'olympiadExercises' => $olympiadExercises,
        ]);
    }

    /**
     * Creates a new Olympiad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = User::findOne(Yii::$app->session->get('login_user'));
        $newOlympiad = new Olympiad();

        if ($newOlympiad->load(Yii::$app->request->post())) {
            $newOlympiad->id_user = $user->id;
            $newOlympiad->is_public = 0;

            if ($newOlympiad->save()) {
                return $this->redirect(['view', 'id' => $newOlympiad->id]);
            }

            Yii::debug($newOlympiad->errors);
        }

        $students_IDs = null;
        if ($user->type == 'admin') {
            $students_IDs = User::find()->select('fio')->where(['type' => 'student'])->indexBy('id')->column();
        } elseif ($user->type == 'teacher') {
            $students_IDs = User::find()->select('fio')->where(['type' => 'student', 'id_who_learn' => $user->id])->indexBy('id')->column();;
        }

        Yii::debug($students_IDs);


        return $this->render('create', [
            'model' => $newOlympiad,
            'students_IDs' => $students_IDs,
        ]);
    }

    /**
     * Updates an existing Olympiad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $user = User::findOne(Yii::$app->session->get('login_user'));
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $students_IDs = null;
        if ($user->type == 'admin') {
            $students_IDs = User::find()->select('fio')->where(['type' => 'student'])->asArray()->indexBy('id')->column();
        } elseif ($user->type == 'teacher') {
            $students_IDs = User::find()->select('fio')->where(['type' => 'student', 'id_who_learn' => $user->id])->indexBy('id')->column();
        }

        $model->tmp_date = date('d.m.Y', $model->date);
        $publicIDs = unserialize($model->public_users_ids);

        $selected_IDs = [];
        foreach ( $publicIDs as $publicID) {
            $selected_IDs[$publicID] = ['selected' => true];
        }

        return $this->render('update', [
            'model' => $model,
            'students_IDs' => $students_IDs,
            'selected_IDs' => $selected_IDs,
        ]);
    }

    /**
     * Deletes an existing Olympiad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $olympiadExercise = OlympiadExercise::findAll(['id_olympiad' => $id]);
            if ($olympiadExercise) {
                foreach ($olympiadExercise as $exercise) {
                    Smarty::deleteAll(['id' => $exercise->id_smarty, 'type' => 'olympiad']); // udalenie smarty
                    SmartyExample::deleteAll(['id_smarty' => $exercise->id_smarty]); // udalenie primeri smarty
                    $exercise->delete(); // udalenie zadanie olimpiadi
                }
            }
            OlympiadResult::deleteAll(['id_olympiad' => $id]);
            if ($this->findModel($id)->delete() === false) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Не удалено!');
            } else {
                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Удалено!');
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Olympiad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Olympiad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Olympiad::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
