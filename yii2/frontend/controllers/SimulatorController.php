<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 15.08.2018
 * Time: 15:46
 */

namespace frontend\controllers;

use frontend\components\filters\AccessAdminFilter;
use frontend\models\CourseResult;
use frontend\models\Exercise;
use frontend\models\Mentalmap;
use frontend\models\OlimpExercise;
use frontend\models\OlimpExerciseStatus;
use frontend\models\olympiad\Olympiad;
use frontend\models\olympiad\OlympiadExercise;
use frontend\models\olympiad\OlympiadResult;
use frontend\models\Setting;
use frontend\models\Simulators;
use frontend\models\SimulatorsResults;
use frontend\models\Smarty;
use frontend\models\User;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use frontend\components\filters\AccessUserFilter;
use yii\web\JsonResponseFormatter;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use common\models\Jobs;
use yii\web\Response;

class SimulatorController extends Controller {

    public function beforeAction($action)
    {
        if ($action->id == 'save-result') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            /**  filter dlya, polzovatel avtorizovan ili net. Esli ne avtorizovan dostup zapreshen  */
            'access_user' => [
                'class' => AccessUserFilter::class,
            ],
            /* filter dlya tolko admina */
            'access_admin' => [
                'class' => AccessAdminFilter::class,
                'only' => ['create-smarty'],
            ]
        ];
    }


    public function actionWork ( $id_exercise, $t = 'course') {
        $this->layout = 'work';
        $session = Yii::$app->session;
        if ( $t == 'course' ) {

            if (CourseResult::findOne(['id_user' => $session->get('login_user')])->id_current_exercise != $id_exercise) {
                throw new NotFoundHttpException('Неверний задания');
            }

            /* zadanie */
            $exercise = Exercise::findOne(['id' => $id_exercise]);

            /* nastroyki */
            $settings = Setting::findOne(['id' => $exercise->id_settings]);

            return $this->render('work-start', [
                'settings' => $settings,
                'id_exercise' => $exercise->id,
            ]);
        } elseif ($t == 'olimp') {
            $olimpExerciseStatus = OlimpExerciseStatus::find()->select('is_public')->where(['id_exercise' => $id_exercise, 'id_user' => $session->get('login_user')])->one();

            if ( $olimpExerciseStatus->is_public == 0 || $olimpExerciseStatus->is_end == 1) {
                throw new NotFoundHttpException();
            }

            /* zadanie */
            $exercise = OlimpExercise::findOne(['id' => $id_exercise]);

            if ($exercise->id_simulators == 9) {
                $id_smarty = (int)Setting::find()->select('id_rules')->where(['id' => $exercise->id_settings])->scalar();

                return $this->redirect(['/simulator/smarty', 'id_smarty' => $id_smarty, 'id_exercise' => $exercise->id, 't' => 'olimp']);
            }
            /* nastroyki */
            $settings = Setting::findOne(['id' => $exercise->id_settings]);

            return $this->render('work-start', [
                'settings' => $settings,
                'id_exercise' => $exercise->id,
                'course_type' => $t,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function actionCreateSmarty() {
        $smarty = new \frontend\models\CreateSmartyForm();

        if ($smarty->load(Yii::$app->request->post())) {

            if ($smarty->saveSmarty()) {

                Yii::$app->session->setFlash('success', 'Примеры Smarty сохранен.');

                return $this->redirect(['/smarty-manager']);

            } else {
                Yii::$app->session->addFlash('error', 'Ошибка в валидации.');
            }
        }

        $smarty->name = '';
        $smarty->rows = 2;
        $smarty->quantity = 1;
        $smarty->examples = null;

        return $this->render('create-smarty', [
            'smarty' => $smarty,
        ]);
    }

    public function actionSettings($id_simulators) {

        //poka tolko est nastroyki Smarty (id = 9)
        if ($id_simulators != 9) {
            throw new NotFoundHttpException('Настройка не найдена.');
        }

        // vse primeri trenajor Smarty
        $user = User::findOne(Yii::$app->session->get('login_user'));
        $settings_query = Smarty::find()->where(['id_user' => 15, 'type' => 'simulator']);
        if ($user->type == 'student') {
            $teacher_id = $user->id_who_learn;
            $settings_query->orWhere(['id_user' => $teacher_id, 'type' => 'simulator']);
        }
        if ($user->type == 'teacher') {
            $settings_query->orWhere(['id_user' => $user->id, 'type' => 'simulator']);
        }
        $smarty_settings = $settings_query->with('user')->asArray()->all();
        $settings = ArrayHelper::map($smarty_settings, 'id', 'name', 'user.fio' );
        Yii::debug($settings);
        if (!$settings) {
            Yii::$app->session->setFlash('error', "<strong>Пока примеров нет.</strong>");
        }

        return $this->render('settings', [
            'settings' => $settings,
        ]);
    }

    public function actionSmarty($id_smarty, $t = 'simulator', $id_exercise = 0) {
        $smarty_id = (int)$id_smarty;

        /* esli get parametr ne int, to stranitsa ne naydena */
        if (!is_integer($smarty_id)) {
            throw new NotFoundHttpException('Страница не найдена.');
        }

        /*== esli rezultate takoy zadanie est ili net dostupa==*/
        if ($t == 'olympiad') {
            $user = User::findOne(Yii::$app->session->get('login_user'));
            $exercise = OlympiadExercise::findOne($id_exercise);
            if ( ($olympiadResult = OlympiadResult::findOne(['id_user' => $user->id, 'id_exercise' => $id_exercise])) !== null || $exercise === null || !Olympiad::isPublic($exercise->id_olympiad, $user->id) || !Olympiad::isActive($exercise->id_olympiad)) {
                Yii::$app->session->setFlash('error', 'Нет доступа или задание уже пройден!');
                return $this->redirect(['/olympiad/list']);
            }
        }

        $smarty_examples = Smarty::find()->where(['id' => $smarty_id])->with('examples')->one();

        $examplesArray = [];
        $examplesAnswersArray = [];
        $cnt = 0;
        foreach ($smarty_examples->examples as $ex) {
            $examplesAnswersArray[$cnt] = $ex->answer;
            $examplesArray[$cnt++] = explode('|', $ex->example);
        }

        Yii::debug($examplesAnswersArray);

        Yii::debug(count($smarty_examples->examples));

        Yii::debug($examplesArray);

        return $this->render('smarty', [
            'smarty_examples' => $smarty_examples,
            'examplesArray' => $examplesArray,
            'examplesAnswersArray' => $examplesAnswersArray,
            't' => $t,
            'id_exercise' => $id_exercise,
        ]);
    }

    /*pechat primeri Smarty*/
    public function actionExercisePrint($id_smarty) {
        $this->layout = '';
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_exerciseView', ['smarty_id' => $id_smarty]);

        $cssInline =<<<CSS
.kv-heading-1{font-size:18px} .smarty {
    background: rgba(255, 255, 255, 0.7);
}
.smarty td {
    text-align: center;
    font-weight: normal;
    font-size: 1.2em;
}
.smarty td input {
    width:2px !important;
    padding: 0 5px;
    font-size: 1em;
}

.page-header p{
    font-size: 18px;
}
CSS;

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => $cssInline,
            // set mPDF properties on the fly
            'options' => [
                'title' => 'Тренажор Smarty',
                'showWatermarkText' => true,
                "autoScriptToLang" => true,
                "autoLangToFont" => true,
                /*
                'allow_charset_conversion' => true,
                'charset_in' => 'utf-8',*/
            ],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>['Примеры'],
                'SetFooter'=>['{PAGENO}'],
                'SetWatermarkText' => ['Obrain', 0.3],

            ],
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionResult($id) {
        $session = Yii::$app->session;

        $sr = SimulatorsResults::findOne(['id' => $id]);

        if (!$sr || $sr->id_user != $session->get('login_user')) {
            throw new NotFoundHttpException('Неверний ползователь');
        }

        return $this->render('result', [
            'simulators_results' => $sr,
        ]);
    }

    /* ajax request */
    public function actionSaveResult() {
        $request = Yii::$app->request;
        $is_isset = false;

        //smarty
        if ($request->post('simulator_id') == 9) {
            $is_isset = isset($_POST['time'])
                && isset($_POST['count_true_answers'])
                && isset($_POST['count_errors'])
                && isset($_POST['simulator_id'])
                && isset($_POST['id_smarty'])
                && isset($_POST['qa']);

            if ( $is_isset && $request->isAjax && $request->isPost) {
                $time               = (int) $request->post('time');
                $count_true_answers = (int) $request->post('count_true_answers');
                $count_errors       = (int) $request->post('count_errors');
                $simulator_id       = (int) $request->post('simulator_id');
                $id_smarty          = (int) $request->post('id_smarty');
                $qa                 = $request->post('qa');

                $sr = new SimulatorsResults();
                $sr->id_user = Yii::$app->session->get('login_user');
                $sr->id_simulator = $simulator_id;
                $sr->true_answers = $count_true_answers;
                $sr->time = $time;
                $sr->count_errors = $count_errors;
                $sr->id_smarty = $id_smarty;
                $sr->qa = serialize($qa);
                //reyting
                $sr->rating = ((float)$count_true_answers)/10;

                if ($sr->save()) {
                    return '/simulator/result?id=' . $sr->id;
                }
                //Yii::debug($sr->errors, 'errorModel');
            }
        } else {
            $is_isset = isset($_POST['time'])
                && isset($_POST['arr_answers'])
                && isset($_POST['count_errors'])
                && isset($_POST['simulator_id'])
                && isset($_POST['id_user'])
                && isset($_POST['rating']);


            if ( $is_isset && $request->isAjax && $request->isPost) {
                if (isset($_POST['id_user']) && ( (int)$_POST['id_user'] != Yii::$app->session->get('login_user') ) ) {
                    throw new NotFoundHttpException("Неверный ползователь");
                }
                $id_user            = (int) $request->post('id_user');
                $time               = (int) $request->post('time');
                $count_errors       = (int) $request->post('count_errors');
                $simulator_id       = (int) $request->post('simulator_id');
                $rating             = (float) $request->post('rating');
                $arr_answers        = $request->post('arr_answers');

                $count_true_answers = 0;
                Yii::debug($arr_answers, 'arr_answers');
                foreach ($arr_answers as $answer) {
                    Yii::debug($answer, 'answer');
                    if ( $answer['question'] == $answer['answer'] ) $count_true_answers++;
                }

                //reyting
                $rating = ((float)$count_true_answers)/10;

                $name               = Simulators::findOne(['id' => $simulator_id])->name;

                /*========== Chto to sdelat otvetami ============*/

                $sr = new SimulatorsResults();
                $sr->id_user      = Yii::$app->session->get('login_user');
                $sr->id_simulator = $simulator_id;
                $sr->true_answers = $count_true_answers;
                $sr->time         = (int) ($time * 1000);
                $sr->count_errors = $count_errors;
                $sr->rating       = $rating;
                $sr->true_answers = $count_true_answers;
                $sr->id_smarty = 0;
                $sr->qa = serialize($arr_answers);

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    //$time /= 1000;
                    $date = time();
                    $executeJob = Yii::$app->db->createCommand()->insert('jobs', [
                        'id_user' => $id_user,
                        'name' => $name,
                        'time' => $time,
                        'date' => $date,
                        'errors' => $count_errors,
                        'rating' => $rating,
                    ])->execute();

                    //sozranit arr_anwers
                    $id_job = Yii::$app->db->getLastInsertID();
                    $job_detailed_values = [];
                    foreach ($arr_answers as $arr_answer) {
                        $job_detailed_values[] = [$id_job, $arr_answer['nomer_job'], $arr_answer['question'], $arr_answer['answer']];
                    }
                    $executeJobDetailed = Yii::$app->db->createCommand()->batchInsert('job_detailed', [
                        'id_job',
                        'num',
                        'question',
                        'answer',
                    ], $job_detailed_values)->execute();

                    if (!$sr->save() || !$executeJob || !$executeJobDetailed) {
                        throw new Exception('Ошибка транзакции!');
                    }
                    $transaction->commit();
                } catch (Exception $e) {
                    Yii::debug($sr->errors, 'tr');
                    $transaction->rollBack();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                }
                Yii::debug($sr->attributes, 'save_attributes');
                //Yii::debug($sr->errors, 'errorModel');
                return '/simulator/result?id=' . $sr->id;
            }
        }


        throw new NotFoundHttpException('Проблема Ajax запроса.');
    }

    /* high-score */
    public function actionHighScore($id_simulator, $oneError = false) {
        $this->layout = 'pa/pa';

        $high_scores = null;
        $errorMessage = $oneError == false ? 'Без ошибок' : 'С 1-ой ошибкой';
        if ($id_simulator == 9) {
            $query = SimulatorsResults::find()->with('user')->where(['id_simulator' => 9 ])->orderBy('time');
            if ( $oneError ) {
                $query->andWhere(['count_errors' => 1]);
            } else {
                $query->andWhere(['count_errors' => 0]);
            }
            $high_scores = $query->all();
        }


        return $this->render('high-score', [
             'high_scores' => $high_scores,
            'errorMessage' => $errorMessage,
        ]);
    }

    public function actionMentalMap() {
        $this->layout = 'simulator/main';


        $user = User::findOne(Yii::$app->session->get('login_user'));


        $settings_query = Mentalmap::find()
            ->where([
                'id_user' => 15, //admin
                'type' => 'simulator'
            ]);

        if ($user->type == 'student') {
            $teacher_id = $user->id_who_learn;
            $settings_query->orWhere(['id_user' => $teacher_id, 'type' => 'simulator']);
        }

        if ($user->type == 'teacher') {
            $settings_query->orWhere(['id_user' => $user->id, 'type' => 'simulator']);
        }

        $mentalmap_settings = $settings_query->with('user')->asArray()->all();

        $public_exercises = [];
        foreach ($mentalmap_settings as $mentalmap_setting) {
            $publicIDs = unserialize($mentalmap_setting['public_users']);
            if (in_array($user->id, $publicIDs)) {
                $public_exercises[] = $mentalmap_setting;
            }
        }

        $public_exercises = ArrayHelper::map($public_exercises, 'id', 'name', 'user.fio' );

        Yii::debug($public_exercises, 'mental-map');

        return $this->render('mental-map', [
            'public_exercises' => $public_exercises,
        ]);
    }

    /* ajax, post, dlya poluchenie zadanie */
    public function actionGetExerciseMentalMap() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax) {

            $id_mentalmap = (int) Yii::$app->request->post('id_mentalmap');

            $mentalmap_exercise = Mentalmap::find()->where(['id' => $id_mentalmap])->with('examples')->one();

            $examplesArray = [];
            $examplesAnswersArray = [];
            $cnt = 0;
            if ($mentalmap_exercise) {
                foreach ($mentalmap_exercise->examples as $ex) {
                    $examplesAnswersArray[$cnt] = $ex->answer;
                    $examplesArray[$cnt++] = explode('|', $ex->example);
                }

                Yii::debug($examplesAnswersArray);

                Yii::debug(count($mentalmap_exercise->examples));

                return [
                    'exercise' => [
                        'examples' => $examplesArray,
                        'answers' => $examplesAnswersArray,
                    ],
                    'count' => $cnt,
                    'id_user' => Yii::$app->session->get('login_user'),
                    'id_mentalmap' => $mentalmap_exercise->id,
                ];
            }

            Yii::$app->response->statusCode = 404;
            return 'No exercises';
        }
    }
}