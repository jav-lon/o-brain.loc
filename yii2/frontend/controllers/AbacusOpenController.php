<?php

namespace frontend\controllers;

use yii\web\Controller;

class AbacusOpenController extends Controller
{
    public $layout = 'abacus-open/main';

    public function actionIndex()
    {
        return $this->render('index');
    }

}
