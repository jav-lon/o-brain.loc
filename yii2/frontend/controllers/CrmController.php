<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 17.12.2018
 * Time: 22:16
 */

namespace frontend\controllers;


use frontend\components\filters\AccessUserFilter;
use yii\web\Controller;

class CrmController extends Controller
{
    public function behaviors()
    {
        return [
            'access-admin' => [
                'class' => AccessUserFilter::class,
            ]
        ];
    }

    public function actionSettings() {

        return $this->render('settings');
    }
}