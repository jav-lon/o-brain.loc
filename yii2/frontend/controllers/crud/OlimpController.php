<?php

namespace frontend\controllers\crud;

use frontend\components\filters\AccessAdminFilter;
use frontend\models\OlimpExercise;
use frontend\models\OlimpLesson;
use frontend\models\OlimpStage;
use frontend\models\User;
use Yii;
use frontend\models\Olimp;
use frontend\models\OlimpSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OlimpController implements the CRUD actions for Olimp model.
 */
class OlimpController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access-admin' => [
                'class' => AccessAdminFilter::className(),
            ]
        ];
    }

    /**
     * Lists all Olimp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OlimpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Olimp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $courseItems = Olimp::find()->where(['id' => $id])->with('stages.lessons.exercises')->one();
        Yii::debug($courseItems, 'courseItems');

        return $this->render('view', [
            'model' => $this->findModel($id),
            'courseItems' => $courseItems,
        ]);
    }

    /**
     * Creates a new Olimp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Olimp();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Olimp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Olimp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            OlimpStage::deleteAll(['id_olimp' => $id]);
            OlimpLesson::deleteAll(['id_olimp' => $id]);
            OlimpExercise::deleteAll(['id_olimp' => $id]);
            if ($this->findModel($id)->delete() === false) {
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        Yii::$app->session->setFlash('info', 'Курс удален.');

        return $this->redirect(['index']);
    }

    public function actionDeleteItem($item, $id) {
        if ($item == 'exercise') {

            $exercise = OlimpExercise::findOne(['id' => $id]);
            if ($exercise !== null && $exercise->delete()) {
                Yii::$app->session->setFlash('info', 'Задание удалено.');
                return $this->redirect(Url::previous('course-view'));
            } else {
                Yii::$app->session->setFlash('error', 'Задание не удалено.');
                return $this->redirect(Url::previous('course-view'));
            }
        } elseif ($item == 'lesson') {
            $lesson = OlimpLesson::findOne(['id' => $id]);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($lesson !== null) {
                    OlimpExercise::deleteAll(['id_lesson' =>$lesson->id]);
                }
                if ($lesson !== null && $lesson->delete()) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('info', 'Урок удалено.');
                    return $this->redirect(Url::previous('course-view'));
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Урок не удалено.');
                    return $this->redirect(Url::previous('course-view'));
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        } elseif ($item == 'stage') {
            $stage = OlimpStage::findOne(['id' => $id]);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($stage !== null) {
                    OlimpLesson::deleteAll(['id_stage' => $stage->id]);
                    OlimpExercise::deleteAll(['id_stage' =>$stage->id]);
                }
                if ($stage !== null && $stage->delete()) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('info', 'Ступен удалено.');

                    return $this->redirect(Url::previous('course-view'));
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Ступен не удалено.');

                    return $this->redirect(Url::previous('course-view'));
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * Finds the Olimp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Olimp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $user = User::findOne(Yii::$app->session->get('login_user'));
        $model = null;
        if ($user->type == 'admin') {
            $model = Olimp::findOne(['id' => $id]);
        } else {
            $model = Olimp::findOne(['id' => $id, 'id_user' => Yii::$app->session->get('login_user')]);
        }
        if ( $model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}