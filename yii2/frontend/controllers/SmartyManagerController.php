<?php

namespace frontend\controllers;


use frontend\components\filters\AccessAdminTeacherFilter;
use frontend\models\CreateSmartyForm;
use frontend\models\SmartyExample;
use Yii;
use frontend\models\Smarty;
use frontend\models\smartymanager\SmartySearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmartyManagerController implements the CRUD actions for Smarty model.
 */
class SmartyManagerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access-admin' => [
                'class' => AccessAdminTeacherFilter::class,
                'only' => ['index', 'view', 'delete', 'update', 'create'],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Smarty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmartySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Smarty model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Smarty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $type
     * @param int $id
     * @return mixed
     */
    public function actionCreate($type = 'simulator', $id = 0)
    {
        $smarty = new \frontend\models\CreateSmartyForm();

        if ($smarty->load(Yii::$app->request->post())) {

            if ($smarty->saveSmarty($type, $id)) {

                if ($type == 'olympiad') {
                    Yii::$app->session->setFlash('success', 'Задание Smarty добавлен.');
                    return $this->redirect(['/olympiad']);

                }
                Yii::$app->session->setFlash('success', 'Примеры Smarty сохранен.');
                return $this->redirect(['/smarty-manager']);

            } else {
                Yii::$app->session->addFlash('error', 'Ошибка в валидации.');
            }
        }

        $smarty->name = '';
        $smarty->rows = 2;
        $smarty->quantity = 1;
        $smarty->examples = null;

        return $this->render('create', [
            'smarty' => $smarty,
        ]);
    }

    /**
     * Updates an existing Smarty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $smarty = new CreateSmartyForm();
        $smarty->findSmarty($id);

        if ($smarty->load(Yii::$app->request->post()) && $smarty->updateSmarty()) {
            return $this->redirect(['view', 'id' => $smarty->_smarty->id]);
        }

        return $this->render('update', [
            'smarty' => $smarty,
        ]);
    }

    /**
     * Deletes an existing Smarty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $isDelete = SmartyExample::deleteAll(['id_smarty' => $id]);

            $isDelete = $isDelete && $this->findModel($id)->delete();

            if ( !$isDelete ) {
                throw new Exception('oshibka v tranzaksiyi');
            }

            $transaction->commit();

            Yii::$app->session->setFlash('success', 'Smarty deleted');
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger', 'Error. Smarty not deleted');
        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger', 'Error. Smarty not deleted');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Smarty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Smarty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Smarty::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
