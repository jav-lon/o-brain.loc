<?php

namespace frontend\controllers;

use frontend\models\Product;

class ShopController extends \yii\web\Controller
{
    public $layout = 'pa/pa';

    public function actionIndex()
    {
        $products = Product::find()->all();

        return $this->render('index', [
            'products' => $products,
        ]);
    }

    public function actionOffer() {

        return $this->render('offer', [

        ]);
    }

    public function actionOrderComplete() {

        return $this->render('order-complete', [

        ]);
    }

}
