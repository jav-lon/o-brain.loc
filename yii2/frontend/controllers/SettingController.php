<?php

namespace frontend\controllers;


use yii\helpers\FileHelper;
use yii\web\Controller;

class SettingController extends Controller
{
    public function actionTest() {
        return \Yii::getAlias('@vendor/dvizh/yii2-cart/src/migrations');
//        return FileHelper::filterPath('@vendor/dvizh/yii2-cart/src/migrations',[]);
    }

    public function actionMigrateUp()
    {
        // https://github.com/yiisoft/yii2/issues/1764#issuecomment-42436905
        $oldApp = \Yii::$app;
        new \yii\console\Application([
            'id'            => 'Command runner',
            'basePath'      => '@frontend',
            'components'    => [
                'db' => $oldApp->db,
                'authManager' => $oldApp->authManager,
            ],
            'controllerMap' => [
                'migrate' => [
                    'class' => 'yii\console\controllers\MigrateController',
//                    'migrationPath' => \Yii::getAlias('@vendor') . '/dvizh/yii2-cart/src/migrations',
//                    'migrationPath' => \Yii::getAlias('@console') . '/migrations',
                ],
            ],
        ]);

        \Yii::debug(\Yii::getAlias('@vendor'), 'console');

        \Yii::$app->runAction('migrate/up', [
//            'migrationPath' => '@vendor/dvizh/yii2-cart/src/migrations',
            'migrationPath' => '@console/migrations/',
            'interactive' => false
        ]);

        \Yii::$app = $oldApp;
    }

}