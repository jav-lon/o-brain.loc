<?php

namespace frontend\controllers;

use frontend\models\PrivacyPolicyAgreement;

class PrivacyPolicyController extends \yii\web\Controller
{

    public $layout = 'privacy-policy/main';

    public function actionIndex()
    {
        $privacy_policy = PrivacyPolicyAgreement::find()->asArray()->all();

        return $this->render('index', [
            'privacy_policy' => $privacy_policy,
        ]);
    }

}
