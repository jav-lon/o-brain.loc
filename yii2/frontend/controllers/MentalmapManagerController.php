<?php

namespace frontend\controllers;


use frontend\components\filters\AccessAdminTeacherFilter;
use frontend\models\CreateMentalmapForm;
use frontend\models\MentalmapExample;
use frontend\models\User;
use Yii;
use frontend\models\Mentalmap;
use frontend\models\mentalmapmanager\MentalmapSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmartyManagerController implements the CRUD actions for Smarty model.
 */
class MentalmapManagerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access-admin' => [
                'class' => AccessAdminTeacherFilter::class,
                'only' => ['index', 'view', 'delete', 'update', 'create']
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Smarty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MentalmapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Smarty model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Smarty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $type
     * @param int $id
     * @return mixed
     */
    public function actionCreate($type = 'simulator', $id = 0)
    {
        $mentalmap = new \frontend\models\CreateMentalmapForm();

        if ($mentalmap->load(Yii::$app->request->post())) {

            if ($mentalmap->saveMentalmap($type, $id)) {

                if ($type == 'olympiad') {
                    Yii::$app->session->setFlash('success', 'Задание Mentalmap добавлен.');
                    return $this->redirect(['/olympiad']);

                }
                Yii::$app->session->setFlash('success', 'Примеры Mentalmap сохранен.');
                return $this->redirect(['/mentalmap-manager']);

            } else {
                Yii::$app->session->addFlash('error', 'Ошибка в валидации.');
            }
        }

        $mentalmap->name = '';
        $mentalmap->rows = 2;
        $mentalmap->quantity = 1;
        $mentalmap->examples = null;

        //$userIDs = User::find()->select(['fio'])->asArray()->indexBy('id')->column();

        $user = User::findOne(Yii::$app->session->get('login_user'));
        $userIDs = null;
        if ($user->type == 'admin') {
            $userIDs = User::find()->select('fio')/*->where(['type' => 'student'])*/->indexBy('id')->column();
        } elseif ($user->type == 'teacher') {
            $userIDs = User::find()->select('fio')->where(['type' => 'student', 'id_who_learn' => $user->id])->orWhere(['id' => $user->id])->indexBy('id')->column();;
        }

        return $this->render('create', [
            'mentalmap' => $mentalmap,
            'userIDs' => $userIDs,
        ]);
    }

    /**
     * Updates an existing Smarty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $mentalmap = new CreateMentalmapForm(['scenario' => CreateMentalmapForm::SCENARIO_UPDATE]);
        $mentalmap->findMentalmap($id);

        if ($mentalmap->load(Yii::$app->request->post()) && $mentalmap->updateMentalmap()) {
            return $this->redirect(['view', 'id' => $mentalmap->_mentalmap->id]);
        }

        //$userIDs = User::find()->select(['fio'])->asArray()->indexBy('id')->column();

        $user = User::findOne(Yii::$app->session->get('login_user'));
        $userIDs = null;
        if ($user->type == 'admin') {
            $userIDs = User::find()->select('fio')/*->where(['type' => 'student'])*/->indexBy('id')->column();
        } elseif ($user->type == 'teacher') {
            $userIDs = User::find()->select('fio')->where(['type' => 'student', 'id_who_learn' => $user->id])->orWhere(['id' => $user->id])->indexBy('id')->column();;
        }

        $publicIDs = unserialize($mentalmap->public_users);

        $selected_IDs = [];
        foreach ( $publicIDs as $publicID) {
            $selected_IDs[$publicID] = ['selected' => true];
        }

        return $this->render('update', [
            'mentalmap' => $mentalmap,
            'userIDs' => $userIDs,
            'selected_IDs' => $selected_IDs,
        ]);
    }

    /**
     * Deletes an existing Smarty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $isDelete = MentalmapExample::deleteAll(['id_mentalmap' => $id]);

            $isDelete = $isDelete && $this->findModel($id)->delete();

            if ( !$isDelete ) {
                throw new Exception('oshibka v tranzaksiyi');
            }

            $transaction->commit();

            Yii::$app->session->setFlash('success', 'Mentalmap deleted');
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger', 'Error. Mentalmap not deleted');
        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger', 'Error. Mentalmap not deleted');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Smarty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Smarty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mentalmap::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
