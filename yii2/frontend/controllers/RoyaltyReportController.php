<?php

namespace frontend\controllers;

use frontend\components\filters\AccessPartnerFilter;
use Yii;
use frontend\components\filters\AccessAdminFilter;
use frontend\models\RoyaltyReport;
use frontend\models\User;

class RoyaltyReportController extends \yii\web\Controller
{
    public $layout = 'pa/pa';

    public function behaviors()
    {
        return [
            'access-admin' => [
                'class' => AccessAdminFilter::class,
                'only' => ['index', 'report'],
            ],
            'access-partner' => [
                'class' => AccessPartnerFilter::class,
                'only' => ['partner-report'],
            ],
        ];
    }

    public function actionIndex()
    {
        $partners = User::find()->where(['type' => 'partner'])->all();

        return $this->render('index', [
            'partners' => $partners,
        ]);
    }

    public function actionReport($id_partner) {

        if (null === User::findOne(['id' => $id_partner, 'type' => 'partner'])) {
            return $this->redirect(['/lk/reports']);
        }

//        $royalty_reports = RoyaltyReport::findAll(['id_partner' => $id_partner]);
        $royalty_reports = RoyaltyReport::find()
            ->andWhere(['id_partner' => $id_partner])
            ->andWhere(['>=', 'payment_schedule', mktime(0,0,0, 1, 1)])
            ->all();

        /* esli ne dobavlen tekushiy godovoy tablitsa to dobavim */
        if ( empty( $royalty_reports ) ) {
            for ($i = 1; $i <= 12; $i++) {
                $new_royalty_report = new RoyaltyReport();
                $new_royalty_report->id_partner = $id_partner;
                $new_royalty_report->payment_schedule = mktime(0, 0 , 0, $i, 5, date('Y'));
                $new_royalty_report->save();
            }

            $royalty_reports = RoyaltyReport::findAll(['id_partner' => $id_partner]);
        }

        // Check if there is an Editable ajax request
        if (isset($_POST['hasEditable'])) {
            // use Yii's response format to encode output as JSON
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $editable_royalty_report_id = Yii::$app->request->post('royalty_report_id');
            $editable_royalty_report_attribute = Yii::$app->request->post('royalty_report_editable_attribute');

            $editable_royalty_report = RoyaltyReport::findOne(['id' => $editable_royalty_report_id]);

            if ($editable_royalty_report->load($_POST) && $editable_royalty_report->save()) {

                $value = '';
                if ($editable_royalty_report_attribute == 'actual_payment_date') {
                    $value = $editable_royalty_report->actual_payment_date;
                } elseif ($editable_royalty_report_attribute == 'actual_payment_amount') {
                    $value = $editable_royalty_report->is_debt ? 'Долг' : $editable_royalty_report->actual_payment_amount;
                }

                return [
                    'output' => $value,
                    'message' => '',//$editable_royalty_report->getFirstError('actual_payment_date')
                ];
            }
            else {
                return ['output'=>'', 'message'=>''];
            }

        }

        return $this->render('report', [
            'royalty_reports' => $royalty_reports,
        ]);
    }

    public function actionPartnerReport() {
        $user = User::findOne(Yii::$app->session->get('login_user'));

        $royalty_reports = RoyaltyReport::find()->where(['id_partner' => $user->id])->all();

        return $this->render('partner-report', [
            'royalty_reports' => $royalty_reports,
            'user' => $user,
        ]);
    }

    /**
     * STEP 1: SETUP YOUR CONTROLLER ACTION
     */
    /*public function actionEditableDemo() {
        $model = new Demo; // your model can be loaded here

        // Check if there is an Editable ajax request
        if (isset($_POST['hasEditable'])) {
            // use Yii's response format to encode output as JSON
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            // read your posted model attributes
            if ($model->load($_POST)) {
                // read or convert your posted information
                $value = $model->name;

                // return JSON encoded output in the below format
                return ['output'=>$value, 'message'=>''];

                // alternatively you can return a validation error
                // return ['output'=>'', 'message'=>'Validation error'];
            }
            // else if nothing to do always return an empty JSON encoded output
            else {
                return ['output'=>'', 'message'=>''];
            }
        }

        // Else return to rendering a normal view
        return $this->render('view', ['model'=>$model]);
    }*/

}
