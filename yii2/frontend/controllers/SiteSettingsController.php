<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 19.02.2019
 * Time: 0:15
 */

namespace frontend\controllers;

use frontend\components\filters\AccessAdminFilter;
use frontend\models\PrivacyPolicyAgreement;
use Yii;
use yii\web\Controller;

class SiteSettingsController extends Controller
{
    public $layout = 'pa/pa';

    public function behaviors()
    {
        return [
            'access-admin' => [
                'class' => AccessAdminFilter::class,
            ]
        ];
    }

    public function actionIndex() {

        return $this->render('index');
    }

    public function actionPrivacyPolicyAgreement() {
        $ppa = PrivacyPolicyAgreement::find()->all();

        if (!$ppa) {
            $ppa = new PrivacyPolicyAgreement();
        }
        else {
            $ppa = $ppa[0];
        }

        if ($ppa->load(Yii::$app->request->post())) {
            if ($ppa->save()) {

                return $this->redirect(['/site-settings']);
            }
        }

        return $this->render('privacy-policy-agreement',[
            'ppa' => $ppa,
        ]);
    }
}