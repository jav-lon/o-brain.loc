<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'name' => 'O-brain', // nazvanie sayta
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'dvizh\order\Bootstrap'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'cart' => [
            'class' => 'dvizh\cart\Module',
        ],
        'order' => [
            'class' => 'dvizh\order\Module',
            'layoutPath' => 'frontend\views\layouts',
            'successUrl' => '/shop/order-complete', //Страница, куда попадает пользователь после успешного заказа
//            'adminNotificationEmail' => 'test@yandex.ru', //Мыло для отправки заказов
            'as order_filling' => '\common\aspects\OrderFilling',
            'userModel' => \frontend\models\User::class,
        ],
    ],
    'components' => [
        'cart' => [
            'class' => 'dvizh\cart\Cart',
            'currency' => 'р.', //Валюта
            'currencyPosition' => 'after', //after или before (позиция значка валюты относительно цены)
            'priceFormat' => [2,'.', ''], //Форма цены
        ],
        'errorHandler' => [
            'errorAction' => '/course/error',
        ],
//        'db' => [
//            'class' => 'yii\db\Connection',
//            'dsn' => $params['dsn'],
//            'username' => $params['username'],
//            'password' => $params['password'],
//
//            // local
//            /*'dsn' => 'mysql:host=localhost;dbname=p-101_obrain',
//            'username' => 'root',
//            'password' => 'data2222',*/
//
//            // noviy hosting
//            /*'dsn' => 'mysql:host=localhost;dbname=p-15213_obrain',
//            'username' => 'p-152_obrain',
//            'password' => 'hlGn13!0G5bKP5',*/
//
//            // old hosting
//            /*'username' => 'p-101_obrain',
//            'password' => 'hlGn13!0G5bKP5',*/
//            'charset' => 'utf8',
//        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '-2TZ4UEbtaeAVWMyMnbgInFcRhTr43LS',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/course/work' => '/simulator/work'
            ],
        ],

    ],
    'params' => $params,
];
