<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 30.08.2018
 * Time: 16:33
 *
 * @var $this \yii\web\View
 * @var
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Участники';

?>
<div class="page-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
