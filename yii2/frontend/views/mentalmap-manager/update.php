<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $mentalmap frontend\models\CreateMentalmapForm */
/* @var $userIDs array */
/* @var $selected_IDs array */

$cssInputExamples = <<<CSS
.ex_value, .ex_answer{
    padding-left: 3px;
    padding-right: 3px;
}
CSS;

\rmrevin\yii\fontawesome\AssetBundle::register($this);
$this->registerCss($cssInputExamples);
$this->registerJsFile('/js/mentalmapUpdateSettings.js', ['depends' => [\yii\web\JqueryAsset::class, \kartik\touchspin\TouchSpinAsset::class]]);


$this->title = "Изменить Mentalmap: {$mentalmap->name}";
$this->params['breadcrumbs'][] = ['label' => 'Все Mentalmap', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $mentalmap->name, 'url' => ['view', 'id' => $mentalmap->_mentalmap->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>

<div id="mentalmap_rows" data-val="<?=$mentalmap->rows?>"></div>
<div id="mentalmap_quantity" data-val="<?=$mentalmap->quantity?>"></div>
<div id="mentalmap_examples" data-val='<?=json_encode($mentalmap->examples)?>'></div>

<div class="mentalmap-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_create-mentalmap-form', [
        'mentalmap' => $mentalmap,
        'userIDs' => $userIDs,
        'selected_IDs' => $selected_IDs,
    ]) ?>

</div>
