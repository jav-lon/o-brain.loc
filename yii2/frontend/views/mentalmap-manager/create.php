<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $mentalmap frontend\models\CreateSmartyForm */
/* @var $userIDs array */

$cssInputExamples = <<<CSS
.ex_value, .ex_answer{
    padding-left: 3px;
    padding-right: 3px;
}
CSS;

\rmrevin\yii\fontawesome\AssetBundle::register($this);
$this->registerCss($cssInputExamples);
$this->registerJsFile('/js/mentalmapSettings.js', ['depends' => [\yii\web\JqueryAsset::class, \kartik\touchspin\TouchSpinAsset::class]]);

$this->title = 'Создать "Mentalmap"';
$this->params['breadcrumbs'][] = ['label' => 'Все Mentalmap', 'url' => ['/mentalmap-manager']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> </h1>
</div>

<div class="simulators-create-mentalmap col-sm-12 col-md-11">
    <?= $this->render('_create-mentalmap-form', [
        'mentalmap' => $mentalmap,
        'userIDs' => $userIDs,
    ]) ?>
</div>
