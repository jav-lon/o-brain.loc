<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 01.09.2018
 * Time: 19:02
 */

/* @var $this yii\web\View */
/* @var $mentalmap frontend\models\CreateMentalmapForm */
/* @var $form \yii\widgets\ActiveForm */
/* @var $userIDs array */
/* @var $selected_IDs array */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;

$selected_IDs = isset($selected_IDs) ? $selected_IDs : [];

?>

<div class="smarty-create">
    <?php $form = ActiveForm::begin([
        'id' => 'mentalmapForm',
        'fieldConfig' => [
            'options' => ['class' => 'col-md-7']
        ]
    ]); ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($mentalmap, 'name') ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($mentalmap, 'public_users')->widget(Select2::class,[
                'name' => 'kv-state-240',
                'data' => $userIDs,
                'theme' => Select2::THEME_KRAJEE,
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => 'Выберите ...',
                    'multiple' => true,
                    'options' => $selected_IDs
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

        </div>
    </div>

    <div class="row well">
        <?= $form->field($mentalmap, 'rows')->widget(\kartik\touchspin\TouchSpin::class,[
            'name' => 't6',
            //'readonly' => true,
            'pluginOptions' => [
                'verticalbuttons' => true,
                'buttonup_txt' => '<i class="fas fa-caret-up"></i>',
                'buttondown_txt' => '<i class="fas fa-caret-down"></i>',
                'initval' => 2,
                'min' => 2,
                'max' => 20,
            ],
            'pluginEvents' => [
                //"touchspin.on.startspin " => "function() { console.log('touchspin.on.startspin' + '->' + $(this).val()); }",
                //"touchspin.on.startupspin" => "function() { console.log('touchspin.on.startupspin'); }",
            ]
        ]) ?>
        <?= $form->field($mentalmap, 'quantity')->widget(\kartik\touchspin\TouchSpin::class,[
            'name' => 't6',
            //'readonly' => true,
            'pluginOptions' => [
                'verticalbuttons' => true,
                'buttonup_txt' => '<i class="fas fa-plus"></i>',
                'buttondown_txt' => '<i class="fas fa-minus"></i>',
                'initval' => 1,
                'min' => 1,
                'max' => 40,
                //'step' => 1,
                'boostat' => 5,
                'maxboostedstep' => 10,
            ],
            'pluginEvents' => [
                //"touchspin.on.startspin " => "function() { console.log('touchspin.on.startspin'); }",
                //"touchspin.on.startupspin" => "function() { console.log('touchspin.on.startupspin'); }",
            ]
        ]) ?>
        <?php if ($mentalmap->scenario !== 'update'): ?>
        <div class="col-sm-12">
            <label for="chekboxList">Сложность</label>
            <div id="checkboxList">
                <label class="checkbox-inline">
                    <input type="checkbox" id="onedigits" value="" checked>
                    однозначные
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" id="twodigits" value="">
                    двухзначные
                </label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <?= Html::buttonInput('Генерация', ['class' => 'btn btn-success', 'id' => 'generate_button']) ?>
            </div>
        </div>
        <?php endif; ?>

    </div>

    <div class="row">
        <div class="form-group col-sm-12 well">
            <label class="control-label" for="wrap_examples">Все примеры</label>
            <div id="wrap_examples"></div>
        </div>
    </div>

    <div class="form-group col-sm-7">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'id' => 'mentalmap_save_button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
