<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Olimp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="olimp-form col-md-5">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_public')->checkbox() ?>

    <?= $form->field($model, 'is_end')->radioList([0 => 'Запущен', 1 => 'Завершен']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
