<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Olimp */

$this->title = 'Create Olimp';
$this->params['breadcrumbs'][] = ['label' => 'Создание олипиада', 'url' => ['/olimp/create-olimp']];
$this->params['breadcrumbs'][] = ['label' => 'Olimps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olimp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
