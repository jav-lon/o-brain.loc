<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Olimp */
/* @var $courseItems frontend\models\Olimp */

Url::remember(Url::current(), 'course-view');

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olimp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            //'is_public',
            ['attribute' => 'is_public', 'format' => 'html', 'value' => function($model) { return $model->is_public == 1 ? '<span class="label label-success" style="font-size: 14px">ДА</span>' : '<span class="label label-danger"  style="font-size: 14px">НЕТ</span>'; }],
            //'is_end',
            ['attribute' => 'is_end', 'format' => 'html', 'value' => function($model) { return $model->is_end == 1 ? '<span class="label label-warning" style="font-size: 14px">Завершен</span>' : '<span class="label label-info"  style="font-size: 14px">Запущен</span>'; }],
            //'created_at',
            ['attribute' => 'created_at', 'value' => function($model) { return Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.yyyy HH:mm'); }],
        ],
    ]) ?>


    <?php
    $count_stage = $count_lesson = $count_exercise = 1;
    ?>

    <h3 class="page-header">
        Предметы курса
    </h3>
    <?php if($courseItems): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-group" id="accordion_stage" role="tablist" aria-multiselectable="true">
                    <?php foreach($courseItems->stages as $stage): ?>
                        <!--Stupeni-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_stage_<?=$count_stage?>">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion_stage" href="#collapse_stage_<?=$count_stage?>" aria-expanded="false" aria-controls="collapse_stage_<?=$count_stage?>">
                                        <?= $stage->name ?>
                                    </a>

                                    <?=Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/crud/olimp/delete-item', 'item' => 'stage', 'id' => $stage->id]), [
                                        'class' => 'text-primary',
                                        'title' => 'Удалить'
                                    ])?>
                                    <span class="badge">ступень</span>
                                    <span class="badge pull-right">Кол-во: <?=count($stage->lessons)?></span>
                                </h4>
                            </div>
                            <div id="collapse_stage_<?=$count_stage?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_stage_<?=$count_stage++?>">
                                <div class="panel-body">
                                </div>
                                <?php foreach($stage->lessons as $lesson): ?>
                                    <!--Lesson Collapse-->
                                    <div class="row">
                                        <div class="col-md-11 col-md-offset-1">
                                            <div class="panel-group" id="accordion_lesson" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="heading_lesson_<?=$count_lesson?>">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion_lesson" href="#collapse_lesson_<?=$count_lesson?>" aria-expanded="false" aria-controls="collapse_lesson_<?=$count_lesson?>">
                                                                <?=$lesson->name?>
                                                            </a>
                                                            <?=Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/crud/olimp/delete-item', 'item' => 'lesson', 'id' => $lesson->id]), [
                                                                'class' => 'text-primary',
                                                                'title' => 'Удалить'
                                                            ])?>
                                                            <span class="badge">урок</span>
                                                            <span class="badge pull-right">Кол-во: <?=count($lesson->exercises)?></span>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_lesson_<?=$count_lesson?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_lesson_<?=$count_lesson++?>">
                                                        <!--<div class="panel-body">

                                                        </div>-->
                                                        <?php if ($lesson->exercises): //esli est zadanie?>
                                                            <!--Exercise Table-->
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered">
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Тип задание</th>
                                                                        <th>Тренажер</th>
                                                                        <th>Действие</th>
                                                                    </tr>
                                                                    <?php foreach ($lesson->exercises as $exercise): ?>
                                                                        <tr>
                                                                            <td><?=$exercise->exercise_number?></td>
                                                                            <td><?=$exercise->work_type == 'classwork' ? 'Классная работа' : 'Домашняя работа'?></td>
                                                                            <td><?=$exercise->simulatorName?></td>
                                                                            <td>
                                                                                    <?=Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/crud/olimp/delete-item', 'item' => 'exercise', 'id' => $exercise->id]), [
                                                                                        //'class' => 'btn btn-primary btn-xs'
                                                                                        'title' => 'Удалить'
                                                                                    ])?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                </table>
                                                            </div><!--Exercise Table-->
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div><!--Lesson Collapse-->

                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div><!--Stupeni-->
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(!$courseItems->stages): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="well">
                    <strong>Пусто!</strong>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
