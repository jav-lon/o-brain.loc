<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OlimpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olimp-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать курс', ['/olimp/create-new-olimp'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            //'is_public:boolean',
            ['attribute' => 'is_public', 'format' => 'html', 'value' => function($model) { return $model->is_public == 1 ? '<span class="label label-success" style="font-size: 14px">ДА</span>' : '<span class="label label-danger"  style="font-size: 14px">НЕТ</span>'; }],
            //['attribute' => 'is_end', 'format' => 'html', 'value' => function($model) { return $model->is_end == 1 ? '<span class="label label-warning" style="font-size: 14px">Завершен</span>' : '<span class="label label-info"  style="font-size: 14px">Запущен</span>'; }],
            //'is_end:boolean',
            ['attribute' => 'created_at', 'value' => function($model) { return Yii::$app->formatter->asDatetime($model->created_at, 'dd.MM.yyyy HH:mm'); }],
            //'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {add-exercise}',
                'buttons' => [
                    'add-exercise' => function($url, $model, $key) {
                        return Html::a('Добавить задание', ['/olimp/create-exercise', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs', 'data-pjax' => '0']);
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
