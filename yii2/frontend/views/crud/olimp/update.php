<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Olimp */

$this->title = "Изменение олимпиады: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Создание олипиада', 'url' => ['/olimp/create-olimp']];
$this->params['breadcrumbs'][] = ['label' => 'Все олипиады', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="olimp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
