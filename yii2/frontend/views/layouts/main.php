<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\models\User;
use frontend\assets\GoogleFontsAsset;

$cssFont = <<<CSS
body {
font-family: 'Open Sans', sans-serif;
/*font-size: 16px;*/
}
CSS;

$this->registerCss($cssFont);
GoogleFontsAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php

        /** dlya sozdanie button vixod */
        $session = Yii::$app->session;
        $user = null;
        if ( $session != '' ) {
            $user = User::findOne($session->get('login_user'));
        }

        /** dlya sozdanie dropdown button Kurs */
        $dropdownCourse = [
            ['label' => 'Пройти',  'url' => '/olimp']
        ];
        if ( $user->type == 'admin' ) { // esli polzovatel admin, to prekleplyayem
            $dropdownCourse[] = ['label' => 'Создать',  'url' => '/crud/olimp'];
        }
        $dropdownCourse[] = '<li class="divider"></li>';
        $dropdownCourse[] = ['label' => 'Результаты', 'url' => Url::to(['/result', 'type' => 'course'])];

        /** dropdown Button dlya olimiada */
        $dropdownOlimp = [
            ['label' => 'Пройти',  'url' => '/olympiad/list']
        ];
        if ( $user->type == 'admin' || $user->type == 'teacher' ) { // esli polzovatel admin, to prekleplyayem
            $dropdownOlimp[] = ['label' => 'Создать',  'url' => '/olympiad'];
        }
        $dropdownOlimp[] = '<li class="divider"></li>';
        $dropdownOlimp[] = ['label' => 'Результаты', 'url' => Url::to(['/result', 'type' => 'olympiad'])];

        /*====================*/
        /*$dropdownDebug = [];
        $dropdownDebug[] = ['label' => 'Truncate Olimp Status', 'url' => '/test/truncate-olimp-status' ];
        $dropdownDebug[] = ['label' => 'Truncate Olimp Result', 'url' => '/test/truncate-all-olimp-result' ];
        $dropdownDebug[] = ['label' => 'Truncate Olimp User Properties', 'url' => '/test/truncate-all-user-properties' ];*/

    ?>
    <?php
    NavBar::begin([
        'brandLabel' => 'Панель управления - На главную',
        'brandUrl' => '/lk/index',
        'renderInnerContainer' => false,
        'options' => [
            'class' => 'navbar-default',
        ],
    ]);
    $menuItems = [
        ['label' => 'Начать решение', 'url' => ['/jobs/training']],
        [
            'label' => 'Курс',
            'items' => $dropdownCourse,
        ],
        [
            'label' => 'Олимпиада',
            'items' => $dropdownOlimp,
        ],
    ];
    /*if (YII_ENV_DEV) {
        $menuItems[] = [
            'label' => 'Settings',
            'items' => $dropdownDebug,
        ];
    }*/
    $actionItems = [];
    if ($user == '') {
        $actionItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $actionItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        if ($user->type == 'admin' || $user->type == 'teacher') {
            $menuItems[] = ['label' => 'Создать Smarty', 'url' => ['/smarty-manager']];
        }
        $actionItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . $user->fio . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menuItems,
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $actionItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?/*= Yii::powered() */?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
