<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\WorkStartAsset;
use frontend\models\User;

WorkStartAsset::register($this);

$cssJqueryUI = <<<CSS
.ui-state-error, .ui-widget-content .ui-state-error, .ui-widget-header .ui-state-error {
border: none;
background-color: transparent;
}
CSS;

$this->registerCss($cssJqueryUI);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style='background-color:#e3e3e3;'>
<?php $this->beginBody() ?>

<!--<div class="wrap">
    <div class="container">-->
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <div id="id_user" data-val="<?=Yii::$app->session->get('login_user');?>"></div>
        <p id="Excellent" style="display:none;">Excellent!</p>
        <p id="Very_nice" style="display:none;">Very nice</p>
        <p id="Well_done" style="display:none;">Well done</p>
        <p id="Try_more" style="display:none;">Try more</p>

        <p id="used_rules"></p>

        <?= $content ?>

        <div class="clear"></div>
<!--    </div>
</div>-->

<!--<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?/*= Html::encode(Yii::$app->name) */?> <?/*= date('Y') */?></p>

        <p class="pull-right"><?/*= Yii::powered() */?></p>
    </div>
</footer>
-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
