<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\PAAsset;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

PAAsset::register($this);

$session = Yii::$app->session;
$user_id = $session->get('login_user');

$user = \frontend\models\User::findOne(['id' => $user_id]);

$count_orders = 0;
if ($user !== null && $user->type == 'admin') {
    //chislo zakazov
    $count_orders = (int) \dvizh\order\models\Order::find()->where(['status' => 'new', 'is_deleted' => 0])->count();
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>



<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Url::to(['/lk/index']) ?>">Панель управления - На главную</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php if ($user != null): ?>
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="/jobs/training"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Начать решение</a></li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Курс <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/olimp">Пройти</a></li>
                            <?php if ( $user->type == 'admin' ): //tolko admin mogut sozdat kurs ?>
                                <li><a href="/crud/olimp">Создать</a></li>
                            <?php endif; ?>
                            <li role="separator" class="divider"></li>
                            <li><a href="/result?type=course">Результаты</a></li>
                        </ul>
                    </li><!-- kurs -->
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Олимпиада <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/olympiad/list">Пройти</a></li>
                            <?php if ( $user->type == 'admin' || $user->type == 'teacher' ): ?>
                                <li><a href="/olympiad">Создать</a></li>
                            <?php endif; ?>
                            <li role="separator" class="divider"></li>
                            <li><a href="/result?type=olympiad">Результаты</a></li>
                        </ul>
                    </li><!-- olimpiadi -->
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Пакеты <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <!--Vsem dostupno eti knopki-->
                            <li>
                                <a href="/package/franchise"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> Франчайзинговый пакет</a>
                            </li>
                            <li>
                                <a href="/package/partner"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> Партнерский пакет</a>
                            </li>
                            <?php if( $user->type == 'admin' ): //tolko admin mogut sozdat paket?>
                                <li>
                                    <div class="divider"></div>
                                </li>
                                <li>
                                    <a href="/package"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Создать пакет</a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li><!-- paketi -->

                    <?php if ( $user->type == 'admin'  || $user->type == 'teacher'): //tolko admin mogut sozdat kurs ?>
                        <li><a href="/smarty-manager"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Создать Smarty</a></li>
                    <?php endif; ?>

                </ul>
            <?php endif; ?>
            <ul class="nav navbar-nav navbar-right">
                <?php if ($user): ?>
                    <li><a href="<?= Url::to(['/lk/logout']); ?>">Выход (<?= $user->fio; ?>)</a></li>
                <?php else: ?>
                    <li><a href="<?= Url::to(['/lk']); ?>">Вход</a></li>
                    <li><a href="<?= Url::to(['/lk/registration']); ?>">Регистрация</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>

<div id="wrapper">

    <div class="overlay"></div>

    <?php if ($user): ?>
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        Abacus Open
                    </a>
                </li>
                <?php if ($user->type == 'admin' || $user->type == 'partner'): ?><!-- admin i partneri -->
                    <?php if ($user->type == 'admin'): ?>
                        <li><a href="<?= Url::to(['/lk/partners']); ?>">Партнеры</a></li>
                    <?php endif; ?>
                    <li><a href="<?= Url::to(['/lk/teachers']); ?>">Преподаватели</a></li>
                    <li><a href="<?= Url::to(['/lk/students']); ?>">Ученики</a></li>
                    <li><a href="<?= Url::to(['/lk/reports']); ?>">Отчеты</a></li>
                    <li>
                        <a href="<?= Url::to(['/shop']); ?>">
                            Заказы
                            <?php if ($count_orders > 0 && $user->type == 'admin'): ?>
                                <span class="badge"><?= $count_orders ?></span>
                            <?php endif; ?>
                        </a>
                    </li>
                <?php if ($user->type == 'admin'): ?>
                    <li><a href="<?= Url::to(['/lk/rules']); ?>">Примеры Mental Map</a></li>
                    <li><a href="<?= Url::to(['/site-settings']); ?>">Настройки сайта</a></li>
                <?php endif; ?>
                <?php endif; ?>

                <!-- dostupno vsem avtorizovannie polzovatelyam -->
                <li><a href="<?= Url::to(['/lk/changePassword']); ?>">Сменить пароль</a></li>
                <li><a href="<?= Url::to(['/lk/records']); ?>">Рекорды</a></li>
                <li><a href="<?= Url::to(['/lk/news']); ?>">Новости</a></li>
                <li><a href="<?= Url::to(['/lk/journal']); ?>">Журнал</a></li>

                <?php if( $user->type != 'admin' && $user->type != 'partner' ): ?><!-- Profil tolko est perpodavatelya i studenta -->
                    <li><a href="<?= Url::to(['/lk/profile']); ?>">Профиль</a></li>
                <?php endif; ?>

                <?php if( $user->type != 'student' ): ?>
                    <li><a href="<?= Url::to(['/lk/filter']); ?>">Фильтр</a></li>
                    <li><a href="<?= Url::to(['/lk/sendEmail']); ?>">Рассылка писем</a></li>
                <?php endif; ?>

                <?php if( $user->type == 'student' ): ?>
                    <li><a href="<?= Url::to(['/lk/payment', 'user' => $user->id]); ?>">Оплата</a></li>
                <?php endif; ?>

                <li><a href="<?= Url::to(['/lk/index']); ?>">Упражнения, результаты, истории посещений</a></li>
                <!--                    <li><a href="--><?//= Url::to(['/result/smarty-user-results']); ?><!--"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Результаты Smarty</a></li>-->
            </ul>
        </nav>
        <!-- /#sidebar-wrapper -->
    <?php endif; ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
        <div class="container-fluid">
            <div class="row">
                <div class=" main">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>