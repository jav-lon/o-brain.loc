<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 03.09.2018
 * Time: 15:24
 *
 * @var $this \yii\web\View
 * @var $high_scores \frontend\models\SimulatorsResults;
 * @var $errorMessage string
 */

use yii\helpers\Html;

$this->title = 'Smarty';
$this->params['breadcrumbs'][] = ['label' => 'Рекорды', 'url' => \yii\helpers\Url::to(['lk/records'])];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> <small>Рекордсмены упражнения: Smarty (<?=$errorMessage?>)</small></h1>
</div>
<?php if($high_scores):?>
    <div class="responsive-table">
        <table class="table table-bordered">
            <tr>
                <th>Место</th>
                <th>ФИО</th>
                <th>Город</th>
                <th>Дата</th>
                <th>Результат</th>
            </tr>
            <?php
            $place = 1;
            ?>
            <?php foreach ($high_scores as $high_score): ?>
                <tr>
                    <?php
                    $color = '#fff';
                    if ($place == 1) $color = '#ffd700';
                    if ($place == 2) $color = '#b5b5bd';
                    if ($place == 3) $color = '#8c8752';
                    ?>
                    <td>
                        <?php if($place == 1 || $place == 2 || $place == 3): ?>
                            <span class="label" style="background-color: <?=$color?>"><?=$place++?></span>
                        <?php else: ?>
                            <?=$place++?>
                        <?php endif; ?>
                    </td>
                    <td><?=$high_score->user->fio?></td>
                    <td><?=$high_score->user->city?></td>
                    <td><?=Yii::$app->formatter->asDate($high_score->created_at, 'php: d.m.y')?></td>
                    <td><?=$high_score->time/1000?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
<?php endif; ?>
