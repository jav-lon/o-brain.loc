<div id="wrap_abacus">
	<div id="abacus">
	
		<?php //if( ($_GET["lev"] == 1) || ($_GET["lev"] == 3) ) { ?>  <?php //} ?>
		<div id="itogo">0</div>
		
		<div id="reset_btn"></div>

		<div id="bead_up_1" class="bead upper_beads row_up col1 dragElement ui-widget ui-corner-all ui-state-error" data-val="5000"></div>
		<div id="bead_up_2" class="bead upper_beads row_up col2 dragElement ui-widget ui-corner-all ui-state-error" data-val="500"></div>
		<div id="bead_up_3" class="bead upper_beads row_up col3 dragElement ui-widget ui-corner-all ui-state-error" data-val="50"></div>
		<div id="bead_up_4" class="bead upper_beads row_up col4 dragElement ui-widget ui-corner-all ui-state-error" data-val="5"></div>

		<!-- beam -->
		<div id="bead_1_1" class="bead lower_beads row1 col1 dragElement ui-widget ui-corner-all ui-state-error" data-val="1000"></div>
		<div id="bead_1_2" class="bead lower_beads row1 col2 dragElement ui-widget ui-corner-all ui-state-error" data-val="100"></div>
		<div id="bead_1_3" class="bead lower_beads row1 col3 dragElement ui-widget ui-corner-all ui-state-error" data-val="10"></div>
		<div id="bead_1_4" class="bead lower_beads row1 col4 dragElement ui-widget ui-corner-all ui-state-error" data-val="1"></div>
		
		<div id="bead_2_1" class="bead lower_beads row2 col1 dragElement ui-widget ui-corner-all ui-state-error" data-val="1000"></div>
		<div id="bead_2_2" class="bead lower_beads row2 col2 dragElement ui-widget ui-corner-all ui-state-error" data-val="100"></div>
		<div id="bead_2_3" class="bead lower_beads row2 col3 dragElement ui-widget ui-corner-all ui-state-error" data-val="10"></div>
		<div id="bead_2_4" class="bead lower_beads row2 col4 dragElement ui-widget ui-corner-all ui-state-error" data-val="1"></div>
		
		<div id="bead_3_1" class="bead lower_beads row3 col1 dragElement ui-widget ui-corner-all ui-state-error" data-val="1000"></div>
		<div id="bead_3_2" class="bead lower_beads row3 col2 dragElement ui-widget ui-corner-all ui-state-error" data-val="100"></div>
		<div id="bead_3_3" class="bead lower_beads row3 col3 dragElement ui-widget ui-corner-all ui-state-error" data-val="10"></div>
		<div id="bead_3_4" class="bead lower_beads row3 col4 dragElement ui-widget ui-corner-all ui-state-error" data-val="1"></div>
		
		<div id="bead_4_1" class="bead lower_beads row4 col1 dragElement ui-widget ui-corner-all ui-state-error" data-val="1000"></div>
		<div id="bead_4_2" class="bead lower_beads row4 col2 dragElement ui-widget ui-corner-all ui-state-error" data-val="100"></div>
		<div id="bead_4_3" class="bead lower_beads row4 col3 dragElement ui-widget ui-corner-all ui-state-error" data-val="10"></div>
		<div id="bead_4_4" class="bead lower_beads row4 col4 dragElement ui-widget ui-corner-all ui-state-error" data-val="1"></div>
		
		<!--
		<div class="container_bead">
			<div id="up_bead_1" class="bead upper_beads row_up col1" data-val="5000"></div>
			<div class="bead lower_beads row1 col1" data-val="1000"></div>
			<div class="bead lower_beads row2 col1" data-val="1000"></div>
			<div class="bead lower_beads row3 col1" data-val="1000"></div>
			<div class="bead lower_beads row4 col1" data-val="1000"></div>
		</div>
		
		<div class="container_bead">
			<div class="bead upper_beads row_up col2" data-val="500"></div>
			<div class="bead lower_beads row1 col2" data-val="100"></div>
			<div class="bead lower_beads row2 col2" data-val="100"></div>
			<div class="bead lower_beads row3 col2" data-val="100"></div>
			<div class="bead lower_beads row4 col2" data-val="100"></div>
		</div>
		
		<div class="container_bead">
			<div class="bead upper_beads row_up col3" data-val="50"></div>
			<div class="bead lower_beads row1 col3" data-val="10"></div>
			<div class="bead lower_beads row2 col3" data-val="10"></div>
			<div class="bead lower_beads row3 col3" data-val="10"></div>
			<div class="bead lower_beads row4 col3" data-val="10"></div>
		</div>
		
		<div class="container_bead">
			<div class="bead upper_beads row_up col4" data-val="5"></div>
			<div class="bead lower_beads row1 col4" data-val="1"></div>
			<div class="bead lower_beads row2 col4" data-val="1"></div>
			<div class="bead lower_beads row3 col4" data-val="1"></div>
			<div class="bead lower_beads row4 col4" data-val="1"></div>
		</div>
		-->
		
	</div>
</div>