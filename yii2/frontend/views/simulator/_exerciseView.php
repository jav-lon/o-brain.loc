<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 22.08.2018
 * Time: 14:26
 *
 * @var $this \yii\web\View
 * @var $smarty_id int
 */

use frontend\models\Smarty;
use yii\helpers\ArrayHelper;

$smarty_examples = Smarty::find()->where(['id' => $smarty_id])->with('examples')->one();
$examplesArray = [];
$examplesAnswersArray = [];
$cnt = 0;
foreach ($smarty_examples->examples as $ex) {
    $examplesAnswersArray[$cnt] = $ex->answer;
    $examplesArray[$cnt++] = explode('|', $ex->example);
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--    <title>Document</title>-->
</head>
<body>
<div class="page-header">
    <h2>Тренажер Smarty <small>Правила: <?= $smarty_examples->name ?></small></h2>
</div>
<?php
$manyArOne = [];
$tmpArray = array_chunk($examplesArray, 20);

$cnt = 0;
$answer_id = 0;
$cntTmpArray = count($tmpArray);
for ($j = 0; $j < $cntTmpArray; $j++) {
    for ($i = 0; $i < $smarty_examples->rows; $i++) {
        $manyArOne[$cnt++] = '<tr><td>' . implode('</td><td>', ArrayHelper::getColumn($tmpArray[$j], $i)) . '</td></tr>';
    }

    $tmpCount = count($tmpArray[$j]);
    $helperString = '';
    for ($k = 0; $k < $tmpCount; $k++) {
        $helperString .= '<td>' . '<div class="form-group"><input type="text" data-answerid="'. $answer_id++ .'" size="5" class="form-control example-answer"></div>' . '</td>';
    }
    $manyArOne[$cnt++] = '<tr>' . $helperString . '</tr>';
}
?>

<?php
$cntArray = count($tmpArray);
?>
<?php for($i = 1; $i <= $cntArray; $i++): ?>
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="center-block">
                <div class="table-responsive">
                    <form action="" class="form-inline">
                        <table class="smarty table table-bordered">
                                <?php $cntEx = $smarty_examples->rows + 1?>
                                <?php foreach ($manyArOne as $k => $tr): ?>
                                    <?= $tr ?>
                                    <?php
                                    unset($manyArOne[$k]);
                                    if (--$cntEx == 0) {
                                        break;
                                    }
                                    ?>
                                <?php endforeach; ?>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php if ($i != $cntArray && $smarty_examples->rows > 10): ?>
        <pagebreak />
    <?php endif; ?>
<?php endfor; ?>
