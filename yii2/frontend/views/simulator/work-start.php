<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 15.08.2018
 * Time: 15:52
 *
 * @var $this \yii\web\View
 * @var $settings \frontend\models\Setting
 * @var $id_exercise integer
 * @var $course_type string
 */
use frontend\models\Rule;

$arr_examples = '';
if ($settings->id_job == 3 || $settings->id_job == 4 || $settings->id_job == 5 || $settings->id_job == 8) {
    if ($settings->id_rules != '') {
        $id_rules = substr($settings->id_rules, 0, -1); // удалим последнее ;
        $list_rules = str_replace(';', ',', $id_rules);

        if (
            $examples = Yii::$app->db
                ->createCommand('SELECT * FROM `examples` WHERE id_rules IN (:list_rules) AND rows = :rows  ORDER BY `id` ASC')
                ->bindValue(':list_rules', $list_rules)
                ->bindValue(':rows', $settings->rows)
                ->queryAll()
        ) {
            foreach ($examples as $example) {
                $rulesAr = Rule::findOne($example['id_rules']);
                $arr_examples = $arr_examples . $example['example'] . '=' . $example['answer'] . '#' . '<a href=' . $rulesAr->link_video . ' target=blanc title=Видео-инструкция>' . $rulesAr->descr . '</a>' . '&';
            }
            $arr_examples = substr($arr_examples, 0, -1); // удалим последнее &
        }

        /*if ($examples = ExamplesAr::model()->findAllBySql("SELECT * FROM `examples` WHERE id_rules IN ({$list_rules}) AND rows = " . $settings->rows . " ORDER BY `id` ASC")) {
            foreach ($examples as $example) {
                $rulesAr = RulesAr::model()->findByPk($example->id_rules);
                $arr_examples = $arr_examples . $example->example . '=' . $example->answer . '#' . '<a href=' . $rulesAr->link_video . ' target=blanc title=Видео-инструкция>' . $rulesAr->descr . '</a>' . '&';
            }
            $arr_examples = substr($arr_examples, 0, -1); // удалим последнее &
        }*/
    }
}

?>
<div id="loading" style="display:none;"><img src="/images/load.gif" /></div>
<a id="a_home" href="#"></a>

<div>
    <div id="id_exercise" data-val="<?= $id_exercise ?>"></div>
    <div id="course_type" data-val="<?= $course_type ?>"></div>

    <div id="level" data-val="<?= substr($settings->level, 0, -1); ?>"></div>
    <div id="rows" data-val="<?= ($settings->rows != '0') ? substr($settings->rows, 0, -1) : '0' ?>"></div>
    <div id="operation" data-val="<?=$settings->operation;?>"></div>
    <div id="voice" data-val="<?=$settings->voice;?>"></div>
    <div id="rules" data-val="<?=$settings->rules;?>"></div>
    <div id="delay" data-val="<?=$settings->delay;?>"></div>
    <div id="number_job" data-val="<?= $settings->id_job; ?>"></div>
    <div id="id_rules" data-val="<?=$arr_examples;?>"></div>


    <?php if ($settings->id_job == 1): ?>
        <div id="wrap_numbers" style="float:left;margin-left: 20px;">
            <!--<a href="/jobs/training/?jobSetting=numbers" style="margin:15px 0 20px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
            <p class="numbers"><span id="source_number"></span></p>
            <button id="next_number" style="" type="button" class="btn btn-info">NEXT</button>
            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
            <?php //echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

            <div id="info_job">
                <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                <p id="back_time"></p>
            </div>

            <br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
        </div>
    <?php elseif ($settings->id_job == 2): ?>
        <div style="float:left;margin-left: 20px;">

            <div style="display:inline-block;vertical-align: top;">
                <a href="/jobs/training/?jobSetting=map" style="margin:15px 0 20px 0;" class="btn btn-danger">НАСТРОЙКИ</a>
                <!--<button id="btn_settingsOpen" class="btn btn-danger btn_settingsOpen_from_job" style="margin:15px 0 20px 0;" type="button">SETTINGS</button>-->
                <br>
                <form role="form" id="form_next_number">
                    <input id="j2_input_number" type="text" class="form-control">
                </form>
                <br>
                <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
            </div>

            <div style="display:inline-block;vertical-align: top;">
                <br><br>
                <div id="info_job" style="margin: 0px 0px 0px 30px;">
                    <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                    <p id="back_time"></p>
                </div>
                <br>
                <button id="next_number" style="margin: 0px 0px 0px 30px;" type="button" class="btn btn-info">NEXT</button>
            </div>

        <br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
    </div>
    <?php elseif ($settings->id_job == 3): ?>
        <div id="wrap_numbers" style="float:left;margin-left: 20px;">

            <div style="display:inline-block;vertical-align: top;">
                <!--<a href="/jobs/training/?jobSetting=slalom" style="margin:15px 0 20px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
                <br>
                <button id="next_number" style="" type="button" class="btn btn-info">NEXT</button>
                <div id="info_job">
                    <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                    <p id="back_time"></p>
                </div>
                <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
            </div>

            <div style="display:inline-block;">
                <p class="numbers_salom"><span id="source_number"></span></p>
            </div>

            <!-- for test -->
            <!--<span id="for_test">0</span> <span>-->
            <br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
        </div>
    <?php elseif ($settings->id_job == 4): ?>
        <div id="wrap_numbers" style="float:left;margin-left: 20px;">

            <div style="display:inline-block;vertical-align: top;">
                <!--<a href="/jobs/training/?jobSetting=multiply" style="margin:15px 0 20px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
                <br>
                <button id="next_number" style="" type="button" class="btn btn-info">NEXT</button>
                <div id="info_job">
                    <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                    <p id="back_time"></p>
                </div>
                <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
            </div>

            <div style="display:inline-block;">
                <p class="numbers_salom"><span id="source_number"></span></p>
            </div>

            <br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
        </div>
    <?php elseif ($settings->id_job == 5): ?>
        <div id="wrap_numbers" style="float:left;margin-left: 20px;">

            <div style="display:inline-block;vertical-align: top;">
                <!--<a href="/jobs/training/?jobSetting=division" style="margin:15px 0 20px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
                <br>
                <button id="next_number" style="" type="button" class="btn btn-info">NEXT</button>
                <div id="info_job">
                    <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                    <p id="back_time"></p>
                </div>
                <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
            </div>

            <div style="display:inline-block;">
                <p class="numbers_salom"><span id="source_number"></span></p>
            </div>

            <!-- for test -->
            <!--<span id="for_test">0</span> <span>-->
            <br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
        </div>
    <?php elseif ($settings->id_job == 6): ?>
        <div id="wrap_numbers" style="float:left;margin-left: 20px;">
            <!--<a href="/jobs/training/?jobSetting=shulteNumbers" style="margin:15px 0 0px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
            <p class="numbers"><span id="source_number"></span></p>
            <button id="next_number" style="" type="button" class="btn btn-info">NEXT</button>
            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
            <?php //echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

            <div id="info_job">
                <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                <p id="back_time"></p>
            </div>

            <br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
        </div>
    <?php elseif ($settings->id_job == 7): ?>
        <div id="wrap_numbers" style="float:left;margin-left: 20px;">
            <!--<a href="/jobs/training/?jobSetting=shulteAbacus" style="margin:15px 0 0px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
            <p class="numbers"><span id="source_number"></span></p>
            <button id="next_number" style="" type="button" class="btn btn-info">NEXT</button>
            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
            <?php //echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

            <div id="info_job">
                <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                <p id="back_time"></p>
            </div>

            <br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
        </div>
    <?php elseif ($settings->id_job == 8): ?>
        <div style="float:left;margin-left: 20px;">

            <div style="display:inline-block;vertical-align: top;">
                <br><br>
                <form role="form" id="form_next_number">
                    <input id="j2_input_number" type="text" class="form-control">
                </form>
                <br>
                <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;margin-bottom:20px;" class="btn btn-info">Try again</a>
            </div>

            <div style="display:inline-block;vertical-align: top;">
                <br><br>
                <div id="info_job" style="margin: 0px 0px 0px 30px;">
                    <span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
                    <p id="back_time"></p>
                </div>
                <br>
                <button id="next_number" style="margin: 0px 0px 0px 30px;" type="button" class="btn btn-info">NEXT</button>
                <br><button id="answer_number" style="margin: 15px 0px 0px 30px;font-size: 30px;" type="button" class="btn btn-info">ANSWER</button>
            </div>

            <!--<br><div id="btn_zvuk" class="zvuk_on"></div>-->
        </div>
    <?php endif; ?>



    <?php if ($settings->id_job == 6 || $settings->id_job == 7): ?>
        <div id="shulteNumbers" style="float:right;margin-right: 20px;">
            <?= $this->render('shulte_html.php'); ?>
        </div>
    <?php elseif ($settings->id_job == 8): ?>
        <div id="wrap_job8" style=""></div>
    <?php else: ?>
        <div style="float:right;margin-right: 20px;">
            <?= $this->render('abacus_html', [
                'settings' => $settings,
            ]); ?>
        </div>
    <?php endif; ?>

</div>
