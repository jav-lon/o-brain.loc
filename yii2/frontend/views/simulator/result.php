<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 22.08.2018
 * Time: 12:06
 *
 * @var $this \yii\web\View
 * @var $simulators_results \frontend\models\SimulatorsResults
 */

use kartik\rating\StarRating;

$this->title = 'Резултать задание';
?>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="center-block">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Результаты тренажер <?= $simulators_results->simulator->name ?></h3>
                </div>
                <div class="panel-body text-center">
                    <?= StarRating::widget([
                        'name' => 'rating_result',
                        'value' => $simulators_results->ratingStar,
                        'pluginOptions' => ['displayOnly' => true]
                    ]); ?>
                    <div><strong>Рейтинг:</strong><?= $simulators_results->rating?> </div>
                    <div><strong>Правильно:</strong><span class="text-success"> <?= $simulators_results->true_answers?></span> </div>
                    <div><strong>Ошибки:</strong><span class="text-danger"> <?= $simulators_results->count_errors?></span> </div>
                    <span><strong>Потрачено: </strong><?= Yii::$app->formatter->asDuration($simulators_results->time/1000)?>, <?=$simulators_results->time%1000?> миллисекунд</span>
                </div>
                <div class="panel-footer">
                    <nav aria-label="...">
                        <ul class="pager">
                            <?php
                            //nujno utochnit jobSetting
                            $jobSettingValue = '';
                            switch ($simulators_results->id_simulator) {
                                case 1: $jobSettingValue = 'numbers'; break;
                                case 2: $jobSettingValue = 'map'; break;
                                case 3: $jobSettingValue = 'slalom'; break;
                                case 4: $jobSettingValue = 'multiply'; break;
                                case 5: $jobSettingValue = 'division'; break;
                                case 6: $jobSettingValue = 'shulteNumbers'; break;
                                case 7: $jobSettingValue = 'shulteAbacus'; break;
                                case 8: $jobSettingValue = 'MentalMap'; break;
                            }
                            ?>
                            <?php if ($simulators_results->id_simulator == 9): ?>
                                <li><a href="<?= \yii\helpers\Url::to(['/simulator/settings', 'id_simulators' => 9]) ?>">Новое задание</a></li>
                            <?php else: ?>
                                <li><a href="/jobs/training?jobSetting=<?=$jobSettingValue?>">Новое задание</a></li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
