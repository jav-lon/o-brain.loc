<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 20.08.2018
 * Time: 23:50
 *
 * @var $this \yii\web\View
 * @var $settings \frontend\models\Smarty
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;

$this->title = 'Настройки тренажера - Smarty';


$selectJS = <<<JS

if ($(this).val() != '') {
    $('#start_button').attr('href', '/simulator/smarty?id_smarty=' + $(this).val());
}

return false;
JS;

$startButtonJS = <<<JS
//console.log(typeof $('[name=smarty-settings]').val());
if ($('[name=smarty-settings]').val() == '') {
   
    return false;
}
JS;


?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= \common\widgets\Alert::widget()?>

<?php if ($settings): ?>
    <nav aria-label="...">
        <ul class="pager">
            <li class="previous"><a href="<?= Url::to(['/jobs/training'])?>"><span aria-hidden="true">&larr;</span> Списки тренажеров</a></li>
            <!--<li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>-->
        </ul>
    </nav>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Настройки</h3>
                </div>
                <div class="panel-body">
                    <?= Select2::widget([
                        'name' => 'smarty-settings',
                        'data' => $settings,
                        'options' => [
                            'placeholder' => 'Выберите правила ...',
                            'onchange' => $selectJS,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]);?>

                </div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!--<li><a href="#">Previous</a></li>-->
                        <li><a href="#" id="start_button" onclick="<?= $startButtonJS ?>">Начать <span aria-hidden="true">&rarr;</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
<?php endif; ?>
