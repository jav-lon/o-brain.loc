<?php
/* @var $this yii\web\View */
/* @var $public_exercises array */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\assets\MentalMapAsset;

$this->title = 'Настройки Mental Map';

MentalMapAsset::register($this);
?>

<div class="wrapper">

    <!-- Settings Container -->
    <div class="container" id="m_settings_form">
        <div class="row">
            <div class="col m12">
                <div class="card-panel">
                    <div class="row">
                        <div class="col s2">
                            <a href="<?=Url::to(['/jobs/training'])?>" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">home</i></a>
                        </div>
                        <div class="col s8 center-align">
                            <h4>Настройки</h4>
                        </div>
                    </div>
                    <?php if ($public_exercises): ?>
                        <!-- settings -->
                        <div class="row">
                            <div class="col s12 m12 l6 offset-l3">
                                <div class="input-field">

                                    <select name="m_exercise_elem">
                                        <option value="" disabled selected>Выберите</option>
                                        <?php foreach ($public_exercises as $author => $exercise): ?>
                                            <optgroup label="Автор: <?=$author?>">
                                                <?php foreach ($exercise as $id => $name): ?>
                                                    <option value="<?=$id?>">Название задания: <?=Html::encode($name)?></option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    </select>

                                    <label>Задание</label>
                                </div>
                            </div>
                            <div class="col s12 m12 l6 offset-l3">
                                <h6>Задержка</h6>
                                <p class="range-field">
                                    <input type="range" id="m_delay_range_elem" min="0.3" max="2.0" step="0.1" value="1.9" />
                                </p>
                            </div>
                            <div class="col s12 m12 l6 offset-l3">
                                <h6>Озвучка</h6>
                                <div class="switch">
                                    <label>
                                        Выкл
                                        <input type="checkbox" id="m_voice_elem" name="m_voice">
                                        <span class="lever"></span>
                                        Вкл
                                    </label>
                                </div>
                            </div>
                            <div id="m_voice_lang_column" class="col s12 m12 l6 offset-l3 scale-transition scale-out">
                                <p id="voice-lang-radio">
                                <h6>Язык</h6>
                                <label>
                                    <input class="with-gap" name="m_voice_lang" type="radio" value="ru-RU"  checked/>
                                    <span>Русский</span>
                                </label>
                                <label>
                                    <input class="with-gap" name="m_voice_lang" type="radio" value="en-US"  />
                                    <span>Английский</span>
                                </label>
                                </p>
                            </div>
                            <div class="col s12 center-align">
                                <a href="#" class="btn btn-large waves-effect waves-light" id="m_send_button">Начать</a>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="card-panel yellow red-text center-align">
                            <h6>Пока примеров нет.</h6>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Mental Map Container -->
    <div class="container hide" id="m_mentalmap_container">
        <!-- Stroka sostoyaniya -->
        <div class="row">
            <!-- actions: setting button, ... -->
            <div class="col s3">
                <a href="<?=Url::to(['/jobs/training'])?>" class="btn-floating waves-effect waves-light"><i class="material-icons">home</i></a>
                <a href="<?=Url::to(['/simulator/mental-map'])?>" class="btn-floating waves-effect waves-light"><i class="material-icons">settings</i></a>
            </div>
            <!-- timer -->
            <div class="col s3">
                <div class="chip z-depth-1" id="back_time">
                    00:00
                </div>
            </div>
            <!-- scores -->
            <div class="col s6">
                <div class="chip z-depth-1">
                    <span id="count_job">0</span> <span> / </span> <span id="m_count_examples_DOM">20</span> <span> / </span> <span id="errors">0</span>
                </div>
            </div>
        </div>
        <!-- input, next, answer, displey -->
        <div class="row">
            <!-- answer input, next button, answer button -->
            <div class="col m4 s12">
                <div class="row">
                    <div class="col s12">
                        <form role="form" id="form_next_number">
                            <div class="input-field col s8">
                                <i class="material-icons prefix">border_color<!--arrow_forward--></i>
                                <input id="j2_input_number" type="text" class="validate" autofocus autocomplete="off">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col s6 m12 m_next_buttons">
                        <button id="next_number" class="waves-effect waves-light btn btn-large active-button">NEXT</button>
                    </div>

                    <div class="col s6 m12 m_next_buttons">
                        <button id="answer_number" class="active-button waves-effect waves-light btn btn-large">ANSWER</button>
                    </div>
                </div>
            </div>
            <!-- displey -->
            <div class="col m8 s12">
                <div class="card-panel valign-wrapper z-depth-2" id="m_number_panel">
                    <div class="center-block" id="wrap_job8"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Result -->
<div class="result-bg valign-wrapper scale-transition scale-out" id="m_result_bg">
    <div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m3">
                <div class="card-panel">
                    <div class="row">
                        <div class="col s12 center-align">
                            <h4>Результаты</h4>
                        </div>
                    </div>
                    <!-- stars -->
                    <div class="row">
                        <div class="col s12 center-align">
                            <div id="m_stars">
                                <div class="disabled-star scale-transition scale-out"><i class="material-icons">star</i></div>
                                <div class="disabled-star scale-transition scale-out"><i class="material-icons">star</i></div>
                                <div class="disabled-star scale-transition scale-out"><i class="material-icons">star</i></div>
                                <div class="disabled-star scale-transition scale-out"><i class="material-icons">star</i></div>
                                <div class="disabled-star scale-transition scale-out"><i class="material-icons">star</i></div>
                            </div>
                        </div>
                    </div>
                    <!-- tablitsa rezultatov -->
                    <div class="row">
                        <div class="col s12 m6 offset-m3">
                            <table>
                                <tbody>
                                <tr>
                                    <td>Правильно: </td>
                                    <td><span id="m_cnt_true_answer_render"></span></td>
                                </tr>
                                <tr>
                                    <td>Ошибки: </td>
                                    <td><span id="m_cnt_error_answer_render"></span></td>
                                </tr>
                                <tr>
                                    <td>Потрачено: </td>
                                    <td><span id="m_time_render"></span> сек.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- button -->
                    <div class="row">
                        <div class="col s12 center-align">
                            <a href="<?=Url::to(['/simulator/mental-map'])?>" class="btn waves-effect waves-light" id="new_exercise_btn">Новое задание</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- preloader -->
<div class="preloader-bg valign-wrapper scale-transition scale-out" id="m_preloader">
    <div class="preloader-wrapper big active center-block">
        <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

<!-- countdown -->
<div class="countdown-bg valign-wrapper hide scale-transition" id="m_countdown">
    <div class="countdown-wrapper center-block">
        <div id="m_countdownText" class="scale-transition scale-out"></div>
    </div>
</div>
