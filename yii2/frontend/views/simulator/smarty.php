<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 21.08.2018
 * Time: 3:04
 * @var $this \yii\web\View
 * @var $smarty_examples \frontend\models\Smarty
 * @var $examplesArray array
 * @var $examplesAnswersArray array
 * @var $t string
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\assets\SmartyAsset;

SmartyAsset::register($this);

$this->title = 'Тренажер - Smarty';

$cssSmarty = <<<CSS
td, th{
border: 2px solid black;
}
CSS;

$this->registerCss($cssSmarty);


?>
<nav aria-label="...">
    <ul class="pager">
        <?php if ( $t == 'simulator' ): ?>
            <li class="next"><a href="<?= Url::to(['/simulator/exercise-print', 'id_smarty' => $smarty_examples->id])?>"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print</a></li>
        <?php endif; ?>
    </ul>
</nav>

<div class="page-header">
    <h2>Тренажер Smarty <small>Задание: <?= $smarty_examples->name ?></small></h2>
    <p>
        <strong>Потрачено: </strong><span id="durationElem" class="label label-info"></span>
    </p>
</div>

<?php
$manyArOne = [];
    $tmpArray = array_chunk($examplesArray, 20);

    $cnt = 0;
    $answer_id = 0;
    $cntTmpArray = count($tmpArray);
    for ($j = 0; $j < $cntTmpArray; $j++) {
        for ($i = 0; $i < $smarty_examples->rows; $i++) {
            $manyArOne[$cnt++] = '<tr><td>' . implode('</td><td>', ArrayHelper::getColumn($tmpArray[$j], $i)) . '</td></tr>';
        }

        $tmpCount = count($tmpArray[$j]);
        $helperString = '';
        for ($k = 0; $k < $tmpCount; $k++) {
            $helperString .= '<td>' . '<div class="form-group"><input type="text" data-answerid="'. $answer_id++ .'" class="form-control example-answer"></div>' . '</td>';
        }
        $manyArOne[$cnt++] = '<tr>' . $helperString . '</tr>';
    }
?>
<div class="row">
    <div class="col-md-12">
        <div id="smartyTable" class="center-block" style="display: none;">
            <div class="table-responsive">
                <table class="smarty table table-bordered">
                    <?php foreach ($manyArOne as $tr): ?>
                        <?= $tr ?>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="center-block">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary center-block', 'id' => 'smartySendButton']) ?>
                <div class="row">
                    <div class="col-md-2 col-md-offset-5">
                        <div class="progress"  style="display: none">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">45% Complete</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal-->
<div class="modal" id="startModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <h4 class="modal-title">Нажмите кнопку "START"</h4>
            </div>-->
            <div class="modal-body">
                <span id="countdownText"></span>
            </div><!--
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="id_user" data-val="<?=Yii::$app->session->get('login_user')?>"></div>
<div id="id_exercise" data-val="<?=$id_exercise?>"></div>
<div id="course_type" data-val="<?=$t?>"></div>
<div id="examplesAnswers" data-val="<?= json_encode($examplesAnswersArray)?>"></div>
<div id="id_smarty" data-val="<?= $smarty_examples->id?>"></div>