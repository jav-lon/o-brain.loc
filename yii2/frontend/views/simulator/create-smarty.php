<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $smarty frontend\models\CreateSmartyForm */
/* @var $form ActiveForm */

$cssInputExamples = <<<CSS
.ex_value, .ex_answer{
    padding-left: 3px;
    padding-right: 3px;
}
CSS;

\rmrevin\yii\fontawesome\AssetBundle::register($this);
$this->registerCss($cssInputExamples);
$this->registerJsFile('/js/smartySettings.js', ['depends' => [\yii\web\JqueryAsset::class, \kartik\touchspin\TouchSpinAsset::class]]);

$this->title = 'Создать "Smarty"';
$this->params['breadcrumbs'][] = ['label' => 'Все Smarty', 'url' => ['/smarty-manager']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> </h1>
</div>

<?= $this->render('_create-smarty-form', [

]) ?>

<div class="simulators-create-smarty col-sm-12 col-md-11">

    <?php $form = ActiveForm::begin([
        'id' => 'smartyForm',
        'fieldConfig' => [
            'options' => ['class' => 'col-md-7']
        ]
    ]); ?>

        <?= $form->field($smarty, 'name') ?>

        <?= $form->field($smarty, 'rows')->widget(\kartik\touchspin\TouchSpin::class,[
            'name' => 't6',
            //'readonly' => true,
            'pluginOptions' => [
                'verticalbuttons' => true,
                'buttonup_txt' => '<i class="fas fa-caret-up"></i>',
                'buttondown_txt' => '<i class="fas fa-caret-down"></i>',
                'initval' => 1,
                'min' => 2,
                'max' => 6,
            ],
            'pluginEvents' => [
                //"touchspin.on.startspin " => "function() { console.log('touchspin.on.startspin' + '->' + $(this).val()); }",
                //"touchspin.on.startupspin" => "function() { console.log('touchspin.on.startupspin'); }",
            ]
        ]) ?>
        <?= $form->field($smarty, 'quantity')->widget(\kartik\touchspin\TouchSpin::class,[
            'name' => 't6',
            //'readonly' => true,
            'pluginOptions' => [
                'verticalbuttons' => true,
                'buttonup_txt' => '<i class="fas fa-plus"></i>',
                'buttondown_txt' => '<i class="fas fa-minus"></i>',
                'initval' => 1,
                'min' => 1,
                'max' => 40,
                //'step' => 1,
                'boostat' => 5,
                'maxboostedstep' => 10,
            ],
            'pluginEvents' => [
                //"touchspin.on.startspin " => "function() { console.log('touchspin.on.startspin'); }",
                //"touchspin.on.startupspin" => "function() { console.log('touchspin.on.startupspin'); }",
            ]
        ]) ?>

        <div class="form-group col-md-12">
            <label class="control-label" for="wrap_examples">Все примеры</label>
            <div id="wrap_examples"></div>
        </div>


        <div class="form-group col-md-7">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'id' => 'smarty_save_button']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- simulators-create-smarty -->
