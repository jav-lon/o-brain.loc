<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Stage */
/* @var $form ActiveForm */
$this->title = 'Создание ступень';
$this->params['breadcrumbs'][] = [
    'label' => 'Создание курс',
    'url' => ['/course/create-course']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> <small>Ступень состоит из уроков</small></h1>
</div>
<div class="course-create-stage col-sm-5">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title')->textInput() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- course-create-stage -->
