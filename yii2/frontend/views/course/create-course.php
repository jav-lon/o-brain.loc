<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Создание курса';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?> <small>Курс состоит из ступеней</small></h1>
</div>

<!-- alert sobshenii -->
<div class="row">
    <div class="col-sm-10">
        <?php if ( Yii::$app->session->hasFlash('stageCreated') ): // esli stupen sozdan?>
            <div class="alert alert-success alert-dismissible col-sm-6" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?= Yii::$app->session->getFlash('stageCreated') ?></strong>
            </div>
        <?php endif; ?>
        <?php if ( Yii::$app->session->hasFlash('lessonCreated') ): // esli urok sozdan?>
            <div class="alert alert-success alert-dismissible col-sm-6" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?= Yii::$app->session->getFlash('lessonCreated') ?></strong>
            </div>
        <?php endif; ?>
        <?php if ( Yii::$app->session->hasFlash('createStage') ): // esli net stupeney?>
            <div class="alert alert-danger alert-dismissible col-sm-6" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?= Yii::$app->session->getFlash('createStage') ?></strong>
            </div>
        <?php endif; ?>
        <?php if ( Yii::$app->session->hasFlash('createdExercise') ): // esli zadanie sozdan?>
            <?php
                $typeMessage = "success";
                if ( substr(Yii::$app->session->getFlash('createdExercise'), 0, 5) == "Error" ) {
                    $typeMessage = "danger";
                }
            ?>
            <div class="alert alert-<?= $typeMessage ?> alert-dismissible col-sm-6" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?= Yii::$app->session->getFlash('createdExercise') ?></strong>
            </div>
        <?php endif; ?>
        <?php if ( Yii::$app->session->hasFlash('createExercise') ): // esli urok ne sozdan?>
            <div class="alert alert-danger alert-dismissible col-sm-6" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?= Yii::$app->session->getFlash('createExercise') ?></strong>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <ul class="list-group">
            <li class="list-group-item"><?= Html::a('Создать ступень', Url::to(['/course/create-stage']))?></li>
            <li class="list-group-item"><?= Html::a('Создать урок', Url::to(['/course/create-lesson']))?></li>
            <li class="list-group-item"><?= Html::a('Создать задания', Url::to(['/course/create-exercise']))?></li>
        </ul>
    </div>
</div>
