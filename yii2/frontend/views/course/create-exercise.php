<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop; // Yii2 — Данные select, которые зависят от другого выбранного значения?
use vova07\imperavi\Widget;
use frontend\assets\CreateExerciseAsset; // est fayl settings.js

/* @var $this yii\web\View */
/* @var $newExercise frontend\models\Exercise */
/* @var $form ActiveForm */
/* @var $stages \frontend\models\Stage */
/* @var $simulators \frontend\models\Simulators */

/* dlya modalniy okno nastroyki trenajorov */
$css =<<<CSS
#settings,#laws {background-color: #fff;box-shadow: 0 0 10px rgba(0, 0, 0, 0.7);padding: 10px 20px;position: fixed;top: 1%;left: 2%;height: 97%;width: 95%; z-index: 10000;}
CSS;

$this->registerCss($css);


CreateExerciseAsset::register($this);

$this->title = 'Создание задание';
$this->params['breadcrumbs'][] = [
    'label' => 'Создание курс',
    'url' => ['/course/create-course']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> </h1>
</div>

<?php if ( Yii::$app->session->hasFlash('createExercise') ): // esli urok ne sozdan?>
    <div class="alert alert-danger alert-dismissible col-sm-6" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><?= Yii::$app->session->getFlash('createExercise') ?></strong>
    </div>
<?php endif; ?>

<div class="course-create-exercise col-sm-12 col-md-7">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($newExercise, 'id_stage')->dropDownList($stages, ['id' => 'stage-id', 'prompt' => 'Выберите...']) ?>
        <?= $form->field($newExercise, 'id_lesson')->widget(DepDrop::class, [
            'options' => ['id'=>'lesson-id'],
            'pluginOptions'=>[
                'depends'=>['stage-id'],
                'placeholder' => 'Выберите...',
                'url' => Url::to(['/course/subcat'])
            ]
        ]); ?>
        <?php $newExercise->work_type = 'classwork'; ?>
        <?= $form->field($newExercise, 'work_type')->radioList(['classwork' => 'Классная работа', 'homework' => 'Домашнее задание']) ?>
        <?= $form->field($newExercise, 'description')->widget(Widget::className(), [
            'settings' => [
                'lang' => 'ru',
                'minHeight' => 50,
                'plugins' => [
                    'fontfamily',
                    'fontcolor',
                    'fullscreen',
                ],
            ],
        ]); ?>
        <?= $form->field($newExercise, 'id_simulators')->dropDownList($simulators, [
            'prompt' => 'Выберите тренажер...',
            'onchange' => '
                             $.post(
                              "'. Url::toRoute('/course/get-settings-list') .'",
                              {id_simulators: $(this).val()},
                              function(data,){
                                //$("select#city").html(data).attr("disabled", false);
                                //console.log(data);
                                $("#id-settings").html(data);
                                $("#simulator_settings").attr("style", "display:block");
                              }
                             )
                         ',
        ]) ?>

        <div class="form-group" id="simulator_settings" style="display: none;">
            <label class="control-label" for="id-settings">Настройки тренажеров</label>
            <div id="id-settings"></div>
        </div>
    
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- course-create-exercise -->
