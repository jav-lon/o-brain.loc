<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 13.08.2018
 * Time: 11:57
 */

/**
 * @var $this \yii\web\View
 * @var $lessons \frontend\models\Lesson
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Уроки';
?>

<?php if ( Yii::$app->session->hasFlash('hasLessons') ): ?>
    <div class="alert alert-info col-md-8 col-md-offset-2 text-center" role="alert"><h2><?= Yii::$app->session->getFlash('hasLessons') ?></h2></div>
<?php else: ?>
    <h2 class="text-center">Уроки</h2>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
            <div class="list-group">
                <?php foreach ( $lessons as $lesson ): ?>
                    <a href="<?= $lesson->id == $courseResult->id_current_lesson ? ( Url::to(['/course/exercise', 'id_lesson' => $lesson->id]) ) : '#' ?>" class="list-group-item <?= $lesson->id == $courseResult->id_current_lesson ? ' active' : ' disabled' ?>">
                        <span class="badge"><?= $lesson->id > $courseResult->id_current_lesson ? 'Недоступен' : ($lesson->id == $courseResult->id_current_lesson ? 'Доступен' : 'Пройден')?></span>
                        <h4 class="list-group-item-heading"><?= Html::encode($lesson->title) ?></h4>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>