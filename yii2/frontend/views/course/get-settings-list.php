<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 10.08.2018
 * Time: 3:40
 */
use frontend\models\Smarty;
//use kartik\select2\Select2;

/**
 * @var $this \yii\web\View
 * @var $id_simulators integer
 *
 */

?>
<?php if ($id_simulators == 9): ?>
    <?php $settings = Smarty::find()->select('name')->indexBy('id')->column();?>
    <?= \yii\helpers\Html::dropDownList('Settings[id_rules]', [], $settings, ['class' => 'form-control', 'prompt' => 'Выберите правила ...']) ?>
    <?/*= Select2::widget([
        'name' => 'smarty-settings',
        'data' => $settings,
        'options' => [
            'placeholder' => 'Выберите правила ...',
            //'onchange' => $selectJS,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]);*/?>
<?php else: ?>
    <div id="settings_built-in"><?= $this->renderAjax('settings', ['id_job' => $id_simulators]) ?></div>
    <div id="laws" style="display:none;"><?= $this->renderAjax('rules') ?></div>
<?php endif; ?>
