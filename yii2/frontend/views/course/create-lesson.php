<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model frontend\models\Lesson */
/* @var $form ActiveForm */
/* @var $stages \frontend\models\Stage */

$this->title = 'Создание урок';
$this->params['breadcrumbs'][] = [
    'label' => 'Создание курс',
    'url' => ['/course/create-course']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> </h1>
</div>

<div class="course-create-lesson col-sm-12 col-md-7">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id_stage')->dropDownList(
                $stages,
                ['prompt' => "Выберите ступень..."]
        ) ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'plan')->widget(Widget::className(), [
            'settings' => [
                'lang' => 'ru',
                'minHeight' => 200,
                'imageUpload' => Url::to(['/course/image-upload']),
                'imageDelete' => Url::to(['/course/file-delete']),
                'imageManagerJson' => Url::to(['/course/images-get']),
                'plugins' => [
                    'fontfamily',
                    'fontcolor',
                    'fullscreen',
                ],
            ],
            'plugins' => [
                'imagemanager' => 'vova07\imperavi\bundles\ImageManagerAsset',
            ]
        ]); ?>
    
        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- course-create-lesson -->
