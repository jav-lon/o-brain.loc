<div id="loading" style="display:none;"><img src="/images/load.gif" /></div>
<!--<form role="form" id="form_settings">-->
	
	<?php 
	$level1='';$level2='';$level3='';$level4='';
	$rows1='';$rows2='';$rows3='';$rows4='';$rows5='';$rows6='';
	$dl1='';$dl2='';$dl3='';$dl4='';$dl5='';$dl6='';$dl7='';$dl8='';$dl9='';$dl10='';$dl11='';$dl12='';$dl13='';$dl14='';$dl15='';$dl16='';$dl17='';$dl18='';
	$voice1='';$voice2='';$voice3='';
	$oper1='';$oper2='';$oper3='';$oper4='';$oper5='';$oper6='';
	$rules1='';$rules2='';
	$isset_settings = false;
	
	if (isset($_GET['jobSetting'])) {
        if( ($_GET["jobSetting"] == 'MentalMap') || ($_SERVER['REQUEST_URI'] == '/jobs/mentalmap') ) $id_job = 8;
        elseif($_GET["jobSetting"] == 'numbers') $id_job = 1;
        elseif($_GET["jobSetting"] == 'map') $id_job = 2;
        elseif($_GET["jobSetting"] == 'slalom') $id_job = 3;
        elseif($_GET["jobSetting"] == 'multiply') $id_job = 4;
        elseif($_GET["jobSetting"] == 'division') $id_job = 5;
        elseif($_GET["jobSetting"] == 'shulteNumbers') $id_job = 6;
        elseif($_GET["jobSetting"] == 'shulteAbacus') $id_job = 7;
    }
	
	//$isset_settings = SettingsAr::model()->findByAttributes(array('id_user'=>Yii::app()->session['login_user'],'id_job'=>$id_job));
    $isset_settings = false;
	if($isset_settings) {

		$id_levels = substr($isset_settings->level, 0, -1); // удалим последнее ;
		$array_levels = explode(';', $id_levels);
		if(in_array(1, $array_levels)) $level1 = 'checked';
		if(in_array(2, $array_levels)) $level2 = 'checked';
		if(in_array(3, $array_levels)) $level3 = 'checked';
		if(in_array(4, $array_levels)) $level4 = 'checked';
		
		$id_rows = substr($isset_settings->rows, 0, -1); // удалим последнее ;
		$array_rows = explode(';', $id_rows);
		if(in_array(2, $array_rows)) $rows2 = 'checked';
		if(in_array(3, $array_rows)) $rows3 = 'checked';
		if(in_array(4, $array_rows)) $rows4 = 'checked';
		if(in_array(5, $array_rows)) $rows5 = 'checked';
		if(in_array(6, $array_rows)) $rows6 = 'checked';
		
		if($isset_settings->delay == 300) $dl1='checked'; if($isset_settings->delay == 400) $dl2='checked'; if($isset_settings->delay == 500) $dl3='checked'; if($isset_settings->delay == 600) $dl4='checked';
		if($isset_settings->delay == 700) $dl5='checked'; if($isset_settings->delay == 800) $dl6='checked'; if($isset_settings->delay == 900) $dl7='checked'; if($isset_settings->delay == 1000) $dl8='checked';
		if($isset_settings->delay == 1100) $dl9='checked'; if($isset_settings->delay == 1200) $dl0='checked'; if($isset_settings->delay == 1300) $dl11='checked'; if($isset_settings->delay == 1400) $dl12='checked';
		if($isset_settings->delay == 1500) $dl13='checked'; if($isset_settings->delay == 1600) $dl14='checked'; if($isset_settings->delay == 1700) $dl15='checked'; if($isset_settings->delay == 1800) $dl16='checked';
		if($isset_settings->delay == 1900) $dl17='checked'; if($isset_settings->delay == 2000) $dl18='checked';
		
		if($isset_settings->voice == 1) $voice1='checked'; if($isset_settings->voice == 2) $voice2='checked'; if($isset_settings->voice == 3) $voice3='checked';
		
		if($isset_settings->operation == 1) $oper1='checked'; if($isset_settings->operation == 2) $oper2='checked'; if($isset_settings->operation == 3) $oper3='checked';
		if($isset_settings->operation == 4) $oper4='checked'; if($isset_settings->operation == 5) $oper5='checked'; if($isset_settings->operation == 6) $oper6='checked';
		
		if($isset_settings->rules == 1) $rules1='checked'; if($isset_settings->rules == 2) $rules2='checked';
	}
	else {
		$level1='checked'; $rows2='checked'; $dl6='checked'; $voice1='checked'; $oper1='checked'; $rules1='checked';
	}
 ?>
	<!--<h2>Настройки</h2>-->
	<?php if($id_job == 8 || $id_job == 3 || $id_job == 4 || $id_job == 5) { ?>
		<hr><h4>Условия</h4>
		<label class="checkbox-inline"><input type="radio" name="Settings[rules]" value="1" <?=$rules1;?>> Без правил</label>
		<label class="checkbox-inline"><input type="radio" name="Settings[rules]" value="2" <?=$rules2;?>> С правилами</label>
		<button id="btn_rulesOpen" type="button" class="btn btn-link">Выбрать правила</button>
	<?php } ?>

	<?php if($id_job == 8 || $id_job == 3) { ?>
	<hr><h4>Рядность</h4>
		<label class="checkbox-inline"><input type="radio" name="Settings[rows]" value="2" <?=$rows2;?>> двухрядные</label>
		<label class="checkbox-inline"><input type="radio" name="Settings[rows]" value="3" <?=$rows3;?>> трехрядные</label>
		<label class="checkbox-inline"><input type="radio" name="Settings[rows]" value="4" <?=$rows4;?>> четырехрядные</label>
		<label class="checkbox-inline"><input type="radio" name="Settings[rows]" value="5" <?=$rows5;?>> пятирядные</label>
		<?php if($id_job != 3) { ?>
			<label class="checkbox-inline"><input type="radio" name="Settings[rows]" value="6" <?=$rows6;?>> шестирядные</label>
		<?php } ?>
	<?php } ?>

	<?php if($id_job == 1 || $id_job == 8 || $id_job == 2 || $id_job == 3 || $id_job == 4 || $id_job == 5 || $id_job == 6 || $id_job == 7) { ?>
		<hr><h4>Сложность</h4>
		<?php if($id_job != 4 && $id_job != 5) { ?>
			<label class="checkbox-inline"><input type="radio" name="Settings[level]" value="1" <?=$level1;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 1 спица <?php } else { ?> однозначные <?php } ?></label>
		<?php } ?>
		<label class="checkbox-inline"><input type="radio" name="Settings[level]" value="2" <?=$level2;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 2 спицы <?php } else { ?> двухзначные <?php } ?></label>
		<label class="checkbox-inline"><input type="radio" name="Settings[level]" value="3" <?=$level3;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 3 спицы <?php } else { ?> трехзначные <?php } ?></label>
		<?php if($id_job != 3 && $id_job != 4) { ?>
			<label class="checkbox-inline"><input type="radio" name="Settings[level]" value="4" <?=$level4;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 4 спицы <?php } else { ?> четырехзначные <?php } ?></label>
		<?php } ?>
	<?php } ?>

	<?php if($id_job == 8) { ?>
		<hr><h4>Вид заданий</h4>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[operation]" value="1" <?=$oper1;?>> СЛОЖЕНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[operation]" value="2" <?=$oper2;?>> УМНОЖЕНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[operation]" value="3" <?=$oper3;?>> ВЫЧИТАНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[operation]" value="4" <?=$oper4;?>> ДЕЛЕНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[operation]" value="5" <?=$oper5;?>> СЛОЖЕНИЕ И ВЫЧИТАНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[operation]" value="6" <?=$oper6;?>> УМНОЖЕНИЕ И ДЕЛЕНИЕ</label>
	<?php } ?>

	<?php if($id_job == 8 || $id_job == 2) { ?>
		<hr><h4>Задержка</h4>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="300" <?=$dl1;?>> 0,3 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="400" <?=$dl2;?>> 0,4 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="500" <?=$dl3;?>> 0,5 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="600" <?=$dl4;?>> 0,6 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="700" <?=$dl5;?>> 0,7 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="800" <?=$dl6;?>> 0,8 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="900" <?=$dl7;?>> 0,9 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1000" <?=$dl8;?>> 1 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1100" <?=$dl9;?>> 1,1 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1200" <?=$dl10;?>> 1,2 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1300" <?=$dl11;?>> 1,3 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1400" <?=$dl12;?>> 1,4 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1500" <?=$dl13;?>> 1,5 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1600" <?=$dl14;?>> 1,6 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1700" <?=$dl15;?>> 1,7 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1800" <?=$dl16;?>> 1,8 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="1900" <?=$dl17;?>> 1,9 сек.</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="Settings[delay]" value="2000" <?=$dl18;?>> 2 сек.</label>
	<?php } ?>

	<?php if($id_job == 1 || $id_job == 8 || $id_job == 3 || $id_job == 4 || $id_job == 5 || $id_job == 6 || $id_job == 7) { ?>
		<hr><h4>Озвучка</h4>
		<label class="checkbox-inline"><input type="radio" name="Settings[voice]" value="1" <?=$voice1;?>> С голосом</label>
		<label class="checkbox-inline"><input type="radio" name="Settings[voice]" value="2" <?=$voice2;?>> Только голос</label>
		<label class="checkbox-inline"><input type="radio" name="Settings[voice]" value="3" <?=$voice3;?>> Без голоса</label>
	<?php } ?>
	
	<hr><!--
	<p id="btn_settingsReset" class="btn btn-danger">Сбросить настройки</p>
	<button type="button" id="btn_settingsSave" class="btn btn-success">Начать</button>-->
	<!--<p id="btn_settingsClose" class="btn btn-danger">Закрыть</p>-->
	
<!--</form>-->