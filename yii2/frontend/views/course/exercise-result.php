<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 17.08.2018
 * Time: 19:21
 *
 * @var $this \yii\web\View
 * @var $result \frontend\models\ExerciseResult
 * @var $exercise \frontend\models\Exercise
 * @var $lesson_title string
 * @var $isResult boolean
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\rating\StarRating;

$this->title = 'Резултать задание';
?>

<?php if ($isResult): ?>
    <div class="jumbotron">
        <h2><?= $lesson_title . '. ' . $exercise->description ?></h2>
        <p>
        <span><strong>Rating: </strong><?= StarRating::widget([
                'name' => 'rating_35',
                'value' => $result->ratingStar,
                'pluginOptions' => ['displayOnly' => true]
            ]); ?></span>
            <span><strong>Потрачено: </strong><?= Yii::$app->formatter->asDuration($result->time)?></span>
        </p>

        <p><?= Html::a('Задание', Url::previous('exercise'), [
                'class' => 'btn btn-info',
                'role' => "button",
            ])?></p>
    </div>
<?php else: ?>
    <div class="jumbotron">
        <h2><?= $lesson_title . '. ' . $exercise->description ?></h2>

        <p><?= Html::a('Начать', Url::to(['/course/work', 'id_exercise' => $exercise->id]), [
                'class' => 'btn btn-info',
                'role' => "button",
            ])?></p>
    </div>
<?php endif; ?>
