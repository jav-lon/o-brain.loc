<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 15.08.2018
 * Time: 13:29
 *
 * @var $this \yii\web\View
 * @var $classwork \frontend\models\Exercise
 * @var $homework \frontend\models\Exercise
 * @var $courseResult \frontend\models\CourseResult
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Задании';
Url::remember(Url::current(), 'exercise');
?>

<?php if ( Yii::$app->session->hasFlash('hasExercise') ): ?>
    <div class="alert alert-info col-md-8 col-md-offset-2 text-center" role="alert"><h2><?= Yii::$app->session->getFlash('hasExercise') ?></h2></div>
<?php else: ?>
    <h3 class="text-center">Класние задание</h3>

    <!-- Klassnie zadanie -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
            <div class="list-group">
                <?php foreach ( $classwork as $cwork ): ?>
                    <a href="<?= $cwork->id <= $courseResult->id_current_exercise ? ( Url::to(['/course/exercise-result', 'id_exercise' => $cwork->id]) ) : '#' ?>" class="list-group-item <?= $cwork->id == $courseResult->id_current_exercise ? ' active' : ($cwork->id > $courseResult->id_current_exercise ? ' disabled' : '') ?>">
                        <span class="badge"><?= $cwork->id > $courseResult->id_current_exercise ? 'Недоступен' : ($cwork->id == $courseResult->id_current_exercise ? 'Доступен' : 'Пройден')?></span>
                        <h4 class="list-group-item-heading"><?= $cwork->description ?></h4>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <h3 class="text-center">Домашние задание</h3>

    <!-- Domashnie zadanie -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
            <div class="list-group">
                <?php foreach ( $homework as $hwork ): ?>
                    <a href="<?= $hwork->id <= $courseResult->id_current_exercise ? ( Url::to(['/course/exercise-result', 'id_exercise' => $hwork->id]) ) : '#' ?>" class="list-group-item <?= $hwork->id == $courseResult->id_current_exercise ? ' active' : ($hwork->id > $courseResult->id_current_exercise ? ' disabled' : '') ?>">
                        <span class="badge"><?= $hwork->id > $courseResult->id_current_exercise ? 'Недоступен' : ($hwork->id == $courseResult->id_current_exercise ? 'Доступен' : 'Пройден')?></span>
                        <h4 class="list-group-item-heading"><?= $hwork->description ?></h4>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>