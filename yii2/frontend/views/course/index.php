<?php

/**
 * @var $this \yii\web\View
 * @var $session \yii\web\Session
 * @var $stages \frontend\models\Stage
 * @var $courseResult \frontend\models\CourseResult
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Курс';
?>

<?php if ( $session->hasFlash('hasStages') ): ?>
    <div class="alert alert-info col-md-8 col-md-offset-2 text-center" role="alert"><h2><?= $session->getFlash('hasStages') ?></h2></div>
<?php else: ?>
<h2 class="text-center">Курс</h2>

<div class="row">
    <div class="col-sm-12 col-md-8 col-md-offset-2">
        <div class="list-group">
            <?php foreach ( $stages as $stage ): ?>
                <?php
                if ($stage->id == $courseResult->id_current_stage) {
                    echo Html::a(
                        '<span class="badge">' . ( $stage->id > $courseResult->id_current_stage ? 'Недоступен' : ($stage->id == $courseResult->id_current_stage ? 'Доступен' : 'Пройден') ) . '</span>'
                           . '<h4 class="list-group-item-heading">' . ( Html::encode($stage->title) ) . '</h4>',
                        Url::to(['/course/lesson', 'id_stage' => $stage->id]),
                        [
                            'class' => 'list-group-item ' . ( $stage->id == $courseResult->id_current_stage ? 'active' : ' disabled' )
                        ]
                    );
                } else {
                    echo Html::a(
                        '<span class="badge">' . ( $stage->id > $courseResult->id_current_stage ? 'Недоступен' : ($stage->id == $courseResult->id_current_stage ? 'Доступен' : 'Пройден') ) . '</span>'
                        . '<h4 class="list-group-item-heading">' . ( Html::encode($stage->title) ) . '</h4>',
                        '#',
                        [
                            'class' => 'list-group-item ' . ( $stage->id == $courseResult->id_current_stage ? 'active' : ' disabled' ),
                            'onclick' => 'return false;',
                        ]
                    );
                }
                ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif; ?>