<?php
/* @var $this yii\web\View */
/* @var $partners array */

use yii\helpers\Url;

?>

<?php if ( $partners ): ?>

    <div class="col-md-6">
        <div class="list-group">
            <?php
            $i = 1;

            /**
            * @var $partner \frontend\models\User
            */
            foreach ($partners as $partner): ?>
                <a href="<?=Url::to(['/royalty-report/report', 'id_partner' => $partner->id])?>" class="list-group-item">
                    <span class="label label-primary"><?= $i++; ?></span>
                    <?= $partner->fio; ?>
                </a>
            <?php endforeach; ?>
        </div>
    </div>

<?php else: ?>
    <div class="well well-lg">Пусто</div>
<?php endif; ?>
