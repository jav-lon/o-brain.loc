<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 03.02.2019
 * Time: 22:45
 */

/* @var $this \yii\web\View */
/* @var $royalty_reports array */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\User;
use kartik\editable\Editable;

//var_dump($royalty_reports); exit;
$partner = User::findOne($royalty_reports[0]->id_partner);

?>
<div class="page-header">
    <h1>Отчет по роялти <small><?= 'Партнер: ' . ($partner->fio) ?></small></h1>
</div>

<?php
$date_reg_month = ($partner->data_reg != '') ? $partner->data_reg : false;
if ($date_reg_month !== false) {
    $date_reg_month = date('m.Y', $date_reg_month);
}

?>

<div class="row">
    <div class="col-md-12"><a href="<?=Url::to(['/royalty-report'])?>" class="btn btn-primary">Вернуться</a></div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>График оплаты</th>
                    <th>Фактическая дата оплаты</th>
                    <th>Фактическая сумма оплаты</th>
                </tr>
                </thead>
                <?php /* @var $royalty_report \frontend\models\RoyaltyReport */?>
                <?php foreach ($royalty_reports as $royalty_report): ?>
                    <?php
                    $payment_schedule_month = date('m.Y', $royalty_report->payment_schedule);

                    $payment_schedule_start_month = ($payment_schedule_month == $date_reg_month) ? true : false;
                    ?>
                    <tr>
                        <td class="<?=$payment_schedule_start_month?>">
                            <strong><?= date('d.m.Y', $royalty_report->payment_schedule) ?></strong>
                            <?php if ($payment_schedule_start_month): ?>
                            <img src="/images/today_calendar.png" alt="image" title="отметка, с какой даты начинается роялти">
                            <?php endif; ?>
                        </td>
                        <td><?= Editable::widget([
                                'options' => ['id' => 'editable_first_container_'.$royalty_report->id, 'placeholder' => 'день/месяц/год'],
                                'model' => $royalty_report,
                                'attribute' => 'actual_payment_date',
                                'size' => 'sm',
                                'format' => 'button',
                                'afterInput' => function ($form, $widget) {
                                    echo Html::hiddenInput('royalty_report_id', $widget->model->id) .
                                         Html::hiddenInput('royalty_report_editable_attribute', 'actual_payment_date');
                                },
                            ]) ?></td>
                        <td><?= Editable::widget([
                                'options' => ['id' => 'editable_second_container_'.$royalty_report->id],
                                'model' => $royalty_report,
                                'attribute' => 'actual_payment_amount',
                                'displayValue' => $royalty_report->is_debt ? 'Долг' : $royalty_report->actual_payment_amount,
                                'size' => 'sm',
                                'format' => 'button',
                                'afterInput' => function ($form, $widget) {
                                    echo $form->field($widget->model, 'is_debt')->checkbox();
                                    echo Html::hiddenInput('royalty_report_id', $widget->model->id) .
                                         Html::hiddenInput('royalty_report_editable_attribute', 'actual_payment_amount');
                                },
                                'submitButton' => ['icon' => '<i class="glyphicon glyphicon-ok"></i>', 'class' => 'btn btn-success btn-sm'],
                            ]) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>


