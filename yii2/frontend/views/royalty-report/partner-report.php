<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 06.02.2019
 * Time: 21:56
 */

/* @var $this \yii\web\View */
/* @var $royalty_reports array */
/* @var $user \frontend\models\User */

?>

<div class="page-header">
    <h1>Отчет по роялти</h1>
</div>

<?php if ( $royalty_reports ): ?>
    <div class="well well-sm">
        <p><strong>Размер роялти:</strong> <?=$user->royalty?></p>
        <p><strong>Комментарии к роялти:</strong> <?=$user->comment_royalty?></p>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>График оплаты</th>
                <th>Фактическая дата оплаты</th>
                <th>Фактическая сумма оплаты</th>
                <th>Статус</th>
            </tr>
            </thead>
            <?php /* @var $royalty_report \frontend\models\RoyaltyReport */?>
            <?php foreach ($royalty_reports as $royalty_report): ?>
                <?php
                $black_tr = '';
                $status_icon = '';
                if ($royalty_report->is_debt != '1' && strlen($royalty_report->actual_payment_date) > 1 && strlen($royalty_report->actual_payment_amount) > 1) {
                    $black_tr = 'silver';
                    $status_icon = '<span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span>';
                }
                if ($royalty_report->is_debt == '1') {
                    $status_icon = '<span class="glyphicon glyphicon-question-sign text-danger" aria-hidden="true"></span>';
                }
                ?>
                <tr style="background-color:<?=$black_tr?>">
                    <td><strong><?= date('d.m.Y', $royalty_report->payment_schedule) ?></strong></td>
                    <td><?= $royalty_report->actual_payment_date ?></td>
                    <td><?= $royalty_report->is_debt ? '<span style="color:red"><strong>Долг</strong></span>' : $royalty_report->actual_payment_amount ?></td>
                    <td class="text-center" style="background-color: white"><?=$status_icon?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
<?php else: ?>
    <div class="well">Пусто</div>
<?php endif; ?>
