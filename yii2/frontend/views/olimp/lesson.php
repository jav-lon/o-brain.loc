<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 27.08.2018
 * Time: 0:51
 *
 * @var $this \yii\web\View
 * @var $lessons \frontend\models\OlimpLesson
 * @var $lessonStatus \frontend\models\OlimpLessonStatus array
 */

use yii\helpers\Html;
use yii\helpers\Url;

Url::remember(Url::current(), 'lesson');

$this->title = 'Уроки';
$this->params['breadcrumbs'][] = [
    'label' => 'Курс',
    'url' => ['/olimp']
];$this->params['breadcrumbs'][] = [
    'label' => 'Ступень',
    'url' => Url::previous('stage')
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<?php if ( $lessons ): ?>
    <div class="row">
        <?php foreach ($lessons as $lesson): ?>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><strong><?= $lesson->lesson_number ?> - урок</strong></span>
                        <span class="badge pull-right"><?= ($lessonStatus[$lesson->id]['is_public'] == 0 ? 'Недоступен' : ( ( $lessonStatus[$lesson->id]['is_public'] == 1 && $lessonStatus[$lesson->id]['is_end'] == 0 )? 'Доступен' : 'Пройден') ) ?></span>
                    </div>
                    <div class="panel-body">
                        <?= Html::encode($lesson->name) ?>
                    </div>
                    <div class="panel-footer">
                        <?php \yii\bootstrap\Modal::begin([
                            'header' => 'План',
                            'toggleButton' => $lessonStatus[$lesson->id]['is_public'] == 0 ? false : [
                                'label' => 'План урока',
                                'class' => 'btn btn-default',
                            ],
                            'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
                        ])?>
                        <?= \yii\helpers\HtmlPurifier::process($lesson->plan) ?>
                        <?php \yii\bootstrap\Modal::end(); ?>

                        <?= Html::a(
                            'Пройти', Url::to(['/olimp/exercise',
                            'id_olimp' => $lesson->id_olimp, 'id_stage' => $lesson->id_stage, 'id_lesson' => $lesson->id]),
                            [
                                'class' => 'btn btn-primary ' . ($lessonStatus[$lesson->id]['is_public'] == 0 ? ' disabled' : ''),
                                'onclick' => $lessonStatus[$lesson->id]['is_public'] == 0 ? 'return false;' : '',
                            ]
                        ) ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
