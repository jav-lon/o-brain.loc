<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 22.08.2018
 * Time: 12:06
 *
 * @var $this \yii\web\View
 * @var $result \frontend\models\OlimpExerciseResult
 */

use kartik\rating\StarRating;
use yii\helpers\Url;

$this->title = 'Результат задания';
$this->params['breadcrumbs'][] = [
    'label' => 'Курс',
    'url' => ['/olimp']
];
$this->params['breadcrumbs'][] = [
    'label' => 'Ступень',
    'url' => Url::previous('stage')
];
$this->params['breadcrumbs'][] = [
    'label' => 'Урок',
    'url' => Url::previous('lesson')
];
$this->params['breadcrumbs'][] = [
    'label' => 'Задание',
    'url' => Url::previous('exercise')
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="center-block">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Результаты</h3>
                </div>
                <div class="panel-body text-center">
                    <!--<span class="label label-default">Rating: </span>-->
                    <?= StarRating::widget([
                        'name' => 'rating_result',
                        'value' => $result->ratingStar,
                        'pluginOptions' => ['displayOnly' => true]
                    ]); ?>
                    <div><strong>Ошибки: </strong><span class="text-danger"><?= $result->errors?></span></div>
                    <div><strong>Потрачено: </strong><?= $result->id_simulators == 9 ? Yii::$app->formatter->asDuration($result->time/1000) : Yii::$app->formatter->asDuration($result->time)?></div>
                </div>
                <div class="panel-footer">
                    <nav aria-label="...">
                        <ul class="pager">
                            <li><a href="<?= Url::previous('exercise') ?>">Задание</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
