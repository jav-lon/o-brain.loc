<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

$this->title = 'Создание курса';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Alert::widget() ?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<div class="row">
    <div class="col-sm-4">
        <ul class="list-group">
            <li class="list-group-item"><?= Html::a('Создать курс', Url::to(['/olimp/create-new-olimp']))?></li>
            <li class="list-group-item"><?= Html::a('Управление курса', Url::to(['/crud/olimp']))?></li>
        </ul>
    </div>
</div>
