<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 27.08.2018
 * Time: 6:03
 *
 * @var $this \yii\web\View
 * @var $exercises \frontend\models\OlimpExercise
 * @var $exerciseStatus \frontend\models\OlimpExerciseStatus
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;

Url::remember(Url::current(), 'exercise');

$this->title = 'Задание';
$this->params['breadcrumbs'][] = [
    'label' => 'Курс',
    'url' => ['/olimp']
];
$this->params['breadcrumbs'][] = [
    'label' => 'Ступень',
    'url' => Url::previous('stage')
];
$this->params['breadcrumbs'][] = [
    'label' => 'Урок',
    'url' => Url::previous('lesson')
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<?php if ( $exercises ): ?>
    <div class="row">
        <?php foreach ($exercises as $exercise): ?>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><strong><?= $exercise->exercise_number ?> - задание</strong></span>
                        <span class="label label-info"><?= $exercise->work_type == 'classwork' ? 'Классная работа' : 'Домашняя работа'?></span>
                        <span class="badge pull-right"><?= ($exerciseStatus[$exercise->id]['is_public'] == 0 ? 'Недоступен' : ( ( $exerciseStatus[$exercise->id]['is_public'] == 1 && $exerciseStatus[$exercise->id]['is_end'] == 0 )? 'Доступен' : 'Пройден') ) ?></span>
                    </div>
                    <div class="panel-body">
                        <?=  HtmlPurifier::process($exercise->description) ?>
                    </div>
                    <div class="panel-footer">
                        <?= Html::a(
                            ($exerciseStatus[$exercise->id]['is_end'] == 1 ? 'Посмотреть результаты' : 'Пройти'),
                            (
                                $exerciseStatus[$exercise->id]['is_end'] == 1 ?
                                Url::to(['/olimp/result', 'id_exercise' => $exercise->id]) :
                                Url::to(['/simulator/work', 'id_exercise' => $exercise->id, 't' => 'olimp'])
                            ),
                            [
                                'class' => 'btn btn-primary center-block ' . ($exerciseStatus[$exercise->id]['is_public'] == 0 ? ' disabled' : ''),
                                'onclick' => $exerciseStatus[$exercise->id]['is_public'] == 0 ? 'return false;' : '',
                            ]

                        ) ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
