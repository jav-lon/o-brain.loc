<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 24.08.2018
 * Time: 11:08
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop; // Yii2 — Данные select, которые зависят от другого выбранного значения?
use vova07\imperavi\Widget;
use frontend\assets\CreateExerciseAsset; // est fayl settings.js
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $olimpExercise frontend\models\Exercise */
/* @var $form ActiveForm */
/* @var $stages \frontend\models\Stage */
/* @var $simulators \frontend\models\Simulators */
/* @var $id_olimp integer */

/* dlya modalniy okno nastroyki trenajorov */
$css =<<<CSS
#settings,#laws {background-color: #fff;box-shadow: 0 0 10px rgba(0, 0, 0, 0.7);padding: 10px 20px;position: fixed;top: 1%;left: 2%;height: 97%;width: 95%; z-index: 10000;}
CSS;

$this->registerCss($css);


CreateExerciseAsset::register($this);

$this->title = 'Создание задание';
$this->params['breadcrumbs'][] = [
    'label' => 'Все курсы',
    'url' => ['/crud/olimp']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> </h1>
</div>

<div class="olimp-create-exercise col-sm-12 col-md-7">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($olimpExercise, 'id_stage')->widget(Select2::class, [
        'data' => $stages,
        'options' => [
            'id' => 'stage-id',
            //'placeholder' => 'Выберите...',
            'prompt' => 'Выберите...',
            //'onchange' => $selectJS,
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
        'addon' => [
            'append' => [
                'content' => Html::a('Создать', ['/olimp/create-stage', 'id' => $id_olimp],[
                    'class' => 'btn btn-info',
                    'title' => 'Добавить новый ступен',
                    'data-toggle' => 'tooltip'
                ]),
                'asButton' => true
            ]
        ]
    ])?>
    <?= $form->field($olimpExercise, 'id_lesson')->widget(DepDrop::class, [
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options' => [
            'pluginOptions'=>[
                'allowClear' => true,
            ],
            'addon' => [
                'append' => [
                    'content' => Html::a('Создать', ['/olimp/create-lesson', 'id' => $id_olimp],[
                        'class' => 'btn btn-info',
                        'title' => 'Создать новый урок',
                        'data-toggle' => 'tooltip'
                    ]),
                    'asButton' => true
                ]
            ]
        ],
        'options' => ['id'=>'lesson-id'],
        'pluginOptions'=>[
            'tags' => true,
            'depends'=>['stage-id'],
            'placeholder' => 'Выберите...',
            'url' => Url::to(['/olimp/subcat'])
        ],
    ]); ?>
    <?php $olimpExercise->work_type = 'classwork'; ?>
    <?= $form->field($olimpExercise, 'work_type')->radioList(['classwork' => 'Классная работа', 'homework' => 'Домашнее задание']) ?>
    <?= $form->field($olimpExercise, 'description')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 50,
            'plugins' => [
                'fontfamily',
                'fontcolor',
                'fullscreen',
            ],
        ],
    ]); ?>
    <?= $form->field($olimpExercise, 'id_simulators')->dropDownList($simulators, [
        'prompt' => 'Выберите тренажер...',
        'onchange' => '
                             $.post(
                              "'. Url::toRoute('/course/get-settings-list') .'",
                              {id_simulators: $(this).val()},
                              function(data,){
                                //$("select#city").html(data).attr("disabled", false);
                                //console.log(data);
                                $("#id-settings").html(data);
                                $("#simulator_settings").attr("style", "display:block");
                              }
                             )
                         ',
    ]) ?>

    <div class="form-group" id="simulator_settings" style="display: none;">
        <label class="control-label" for="id-settings">Настройки тренажеров</label>
        <div id="id-settings"></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- olimp-create-exercise -->


