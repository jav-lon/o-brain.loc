<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $createNewOlimpForm frontend\models\CreateNewOlimpForm */
/* @var $form ActiveForm */

$this->title = 'Новый курс';
$this->params['breadcrumbs'][] = [
    'label' => 'Все курсы',
    'url' => ['/crud/olimp']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="olimp-create-new-olimp col-sm-5">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($createNewOlimpForm, 'name') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- olimp-create-new-olimp -->
