<?php
/* @var $this yii\web\View */
/* @var $olimps \frontend\models\Olimp */

use yii\helpers\Html;
use yii\helpers\Url;

Url::remember(Url::current(), 'olimp');

$this->title = 'Курсы';

$this->params['breadcrumbs'][] = 'Курс';
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php if ( $olimps ): ?>
    <div class="row">
        <?php foreach ($olimps as $olimp): ?>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Html::encode($olimp->name) ?></h3>
                    </div>

                    <table class="table table-bordered">
                        <tr><td>Создатель:</td><td><?= \frontend\models\User::find()->select('fio')->where(['id' => $olimp->id_user])->scalar() ?></td></tr>
                        <tr><td>Состояние:</td><td><?= $olimp->is_end == 0 ? '<div class="label label-success">Активен</div>' : '<div class="label label-danger">Завершен</div>' ?></td></tr>
                    </table>
                    <div class="panel-footer">
                        <nav aria-label="...">
                            <ul class="pager">
                                <!--<li><a href="#">Previous</a></li>-->
                                <li><a href="<?= Url::to(['/olimp/stage', 'id_olimp' => $olimp->id]) ?>" >Начать <span aria-hidden="true">&rarr;</span></a></li>
                            </ul>
                        </nav></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
