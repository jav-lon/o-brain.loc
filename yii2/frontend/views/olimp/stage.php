<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 26.08.2018
 * Time: 16:37
 *
 * @var $this \yii\web\View
 * @var $stages \frontend\models\OlimpStage
 * @var $stageStatus \frontend\models\OlimpStageStatus
 */

use yii\helpers\Html;
use yii\helpers\Url;

Url::remember(Url::current(), 'stage');

$this->title = 'Ступени';

$this->params['breadcrumbs'][] = [
    'label' => 'Курс',
    'url' => ['/olimp']
];
$this->params['breadcrumbs'][] = 'Ступень';
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<?php if ( $stages ): ?>
    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
            <div class="list-group">
                <?php foreach ( $stages as $stage ): ?>
                    <?= Html::a(
                                '<span class="badge">' . ($stageStatus[$stage->id]['is_public'] == 0 ? 'Недоступен' : ( ( $stageStatus[$stage->id]['is_public'] == 1 && $stageStatus[$stage->id]['is_end'] == 0 )? 'Доступен' : 'Пройден') ) . '</span>'
                                    .'<h4 class="list-group-item-heading">' . $stage->stage_number . '-ступень. <strong>' . Html::encode($stage->name) . '</strong></h4>'
                                    .'<!--<p class="list-group-item-text">...</p>-->',
                        ($stageStatus[$stage->id]['is_public'] == 1 ? Url::to(['/olimp/lesson', 'id_olimp' => $stage->id_olimp, 'id_stage' => $stage->id]) : '#'),
                        [
                            'class' => 'list-group-item ' . ($stageStatus[$stage->id]['is_public'] == 0 ? ' disabled' : ''),
                            'onclick' => ($stageStatus[$stage->id]['is_public'] == 0 ? 'return false;' : '')
                        ]
                    );?>
                <?php endforeach; ?>
            </div>
            <div class="list-group">
                <?php /*foreach ( $stages as $stage ): */?><!--
                    <?php
/*                    if ($stage->id == $courseResult->id_current_stage) {
                        echo Html::a(
                            '<span class="badge">' . ( $stage->id > $courseResult->id_current_stage ? 'Недоступен' : ($stage->id == $courseResult->id_current_stage ? 'Доступен' : 'Пройден') ) . '</span>'
                            . '<h4 class="list-group-item-heading">' . ( Html::encode($stage->name) ) . '</h4>',
                            Url::to(['/olimp/lesson', 'id_olimp' => $stages->id_olimp, 'id_stage' => $stage->id]),
                            [
                                'class' => 'list-group-item ' . ( $stage->id == $courseResult->id_current_stage ? 'active' : ' disabled' )
                            ]
                        );
                    } else {
                        echo Html::a(
                            '<span class="badge">' . ( $stage->id > $courseResult->id_current_stage ? 'Недоступен' : ($stage->id == $courseResult->id_current_stage ? 'Доступен' : 'Пройден') ) . '</span>'
                            . '<h4 class="list-group-item-heading">' . ( Html::encode($stage->title) ) . '</h4>',
                            '#',
                            [
                                'class' => 'list-group-item ' . ( $stage->id == $courseResult->id_current_stage ? 'active' : ' disabled' ),
                                'onclick' => 'return false;',
                            ]
                        );
                    }
                    */?>
                --><?php /*endforeach; */?>
            </div>
        </div>
    </div>
<?php endif; ?>

