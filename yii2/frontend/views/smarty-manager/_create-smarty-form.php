<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 01.09.2018
 * Time: 19:02
 */

/* @var $this yii\web\View */
/* @var $smarty frontend\models\CreateSmartyForm */
/* @var $form \yii\widgets\ActiveForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="smarty-create">
    <?php $form = ActiveForm::begin([
        'id' => 'smartyForm',
        'fieldConfig' => [
            'options' => ['class' => 'col-md-7']
        ]
    ]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($smarty, 'name') ?>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6 form-group">
            <?= $form->field($smarty, 'rows')->widget(\kartik\touchspin\TouchSpin::class,[
                'name' => 't6',
                //'readonly' => true,
                'pluginOptions' => [
                    'verticalbuttons' => true,
                    'buttonup_txt' => '<i class="fas fa-caret-up"></i>',
                    'buttondown_txt' => '<i class="fas fa-caret-down"></i>',
                    'initval' => 1,
                    'min' => 2,
                    'max' => 20,
                ],
                'pluginEvents' => [
                    //"touchspin.on.startspin " => "function() { console.log('touchspin.on.startspin' + '->' + $(this).val()); }",
                    //"touchspin.on.startupspin" => "function() { console.log('touchspin.on.startupspin'); }",
                ]
            ]) ?>
        </div>
        <div class="form-group col-sm-5">
            <?= $form->field($smarty, 'quantity')->widget(\kartik\touchspin\TouchSpin::class,[
                'name' => 't6',
                //'readonly' => true,
                'pluginOptions' => [
                    'verticalbuttons' => true,
                    'buttonup_txt' => '<i class="fas fa-plus"></i>',
                    'buttondown_txt' => '<i class="fas fa-minus"></i>',
                    'initval' => 1,
                    'min' => 1,
                    'max' => 80,
                    //'step' => 1,
                    'boostat' => 5,
                    'maxboostedstep' => 10,
                ],
                'pluginEvents' => [
                    //"touchspin.on.startspin " => "function() { console.log('touchspin.on.startspin'); }",
                    //"touchspin.on.startupspin" => "function() { console.log('touchspin.on.startupspin'); }",
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            <label class="control-label" for="wrap_examples">Все примеры</label>
            <div id="wrap_examples"></div>
        </div>
    </div>


    <div class="form-group col-md-7">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'id' => 'smarty_save_button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
