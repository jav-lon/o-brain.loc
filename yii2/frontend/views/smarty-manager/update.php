<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $smarty frontend\models\CreateSmartyForm */

$cssInputExamples = <<<CSS
.ex_value, .ex_answer{
    padding-left: 3px;
    padding-right: 3px;
}
CSS;

\rmrevin\yii\fontawesome\AssetBundle::register($this);
$this->registerCss($cssInputExamples);
$this->registerJsFile('/js/smartyUpdateSettings.js', ['depends' => [\yii\web\JqueryAsset::class, \kartik\touchspin\TouchSpinAsset::class]]);


$this->title = "Изменить Smarty: {$smarty->name}";
$this->params['breadcrumbs'][] = ['label' => 'Все Smarty', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $smarty->name, 'url' => ['view', 'id' => $smarty->_smarty->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>

<div id="smarty_rows" data-val="<?=$smarty->rows?>"></div>
<div id="smarty_quantity" data-val="<?=$smarty->quantity?>"></div>
<div id="smarty_examples" data-val='<?=json_encode($smarty->examples)?>'></div>

<div class="smarty-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_create-smarty-form', [
        'smarty' => $smarty,
    ]) ?>

</div>
