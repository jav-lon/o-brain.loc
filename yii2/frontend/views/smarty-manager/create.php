<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $smarty frontend\models\CreateSmartyForm */

$cssInputExamples = <<<CSS
.ex_value, .ex_answer{
    padding-left: 3px;
    padding-right: 3px;
}
CSS;

\rmrevin\yii\fontawesome\AssetBundle::register($this);
$this->registerCss($cssInputExamples);
$this->registerJsFile('/js/smartySettings.js', ['depends' => [\yii\web\JqueryAsset::class, \kartik\touchspin\TouchSpinAsset::class]]);

$this->title = 'Создать "Smarty"';
$this->params['breadcrumbs'][] = ['label' => 'Все Smarty', 'url' => ['/smarty-manager']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> </h1>
</div>

<div class="simulators-create-smarty col-sm-12 col-md-11">
    <div id="alert-block">
        <div class="alert alert-danger" role="alert" style="display: none; position: fixed; top: 50px; right:20px; z-index: 1000"></div>
    </div>
    <?= $this->render('_create-smarty-form', [
        'smarty' => $smarty,
    ]) ?>
</div>
