<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\smartymanager\MentalmapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все Smarty';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smarty-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать Smarty', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Создатель',
                'value' => function($data){
                    return $data->user->fio;
                }
            ],
            'name',
            'rows',
            'quantity',
            //'type',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {print}',
                'buttons' => [
                    'print' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-print" aria-hidden="true"></span>', ['/simulator/exercise-print', 'id_smarty' => $model->id], [/*'class' => 'btn btn-warning btn-xs',*/ 'data-pjax' => '0', 'title' => "Печать"]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
