<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Smarty */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все Smarty', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smarty-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'rows',
            'quantity',
            ['attribute' => 'type', 'label' => 'Тип'],
            [
                'label' => 'Все примеры',
                'format' => 'html',
                'value' => function($model, $widget){
                    $smarty_examples = $model->examples;
                    $html = '';
                    $html .= Html::a('<span class="glyphicon glyphicon-print" aria-hidden="true"></span> Печать', ['/simulator/exercise-print', 'id_smarty' => $model->id], ['class' => 'btn btn-default btn-info','data-pjax' => '0', 'title' => "Печать"]);
                    $html .= '<div class="table-responsive">';
                    $html .= '<table class="table table-bordered table-striped">';
                    $html .= '<thead>';
                    $html .= '<tr>';
                    for ($i = 1; $i <= $model->rows; $i++) {
                        $html .= '<th>';
                        $html .= $i . '-число';
                        $html .= '</th>';
                    }
                    $html .= '<th>';
                    $html .= 'Ответ';
                    $html .= '</th>';
                    $html .= '</tr>';
                    $html .= '</thead>';
                    $html .= '<tbody>';
                    foreach ($smarty_examples as $smarty_example) {
                        $html .= '<tr>';
                        $html .= '<td>';
                        $html .= implode('</td><td>', explode('|', $smarty_example->example));
                        $html .= '</td>';
                        $html .= '<td class="info">';
                        $html .= $smarty_example->answer;
                        $html .= '</td>';
                        $html .= '</tr>';
                    }
                    $html .= '</tbody>';
                    $html .= '</table>';
                    $html .= '</div>';

                    return $html;
                }
            ]
        ],
    ]) ?>

</div>
