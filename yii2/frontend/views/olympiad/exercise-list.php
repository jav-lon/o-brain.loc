<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 12.09.2018
 * Time: 18:47
 *
 * @var $this \yii\web\View
 * @var $olympiad \frontend\models\olympiad\Olympiad
 */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

Url::remember(Url::current(), 'remember-olympiad');

$this->title = 'Задание';
$this->params['breadcrumbs'][] = ['label' => 'Олимпиады', 'url' => 'list'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row"><!-- Opisanie Olimpiadi -->
    <div class="col-md-6 col-md-offset-3">
        <div class="center-block">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?=$olympiad->name?></h3>
                </div>
                <div class="panel-body">
                    <h4>Описание</h4>
                    <?=HtmlPurifier::process($olympiad->description)?>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr><td>Создатель: </td><td><?=$olympiad->author->fio?></td></tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="<?=Url::to(['/result/olympiad-high-score-table', 'id_olympiad' => $olympiad->id])?>" class="btn btn-default"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> Результаты</a>
                </div>
            </div>
        </div>
    </div>
</div><!-- Opisanie Olimpiadi -->


<div class="page-header">
    <h2>Задание</h2>
</div>

<div class="row"><!-- Zadanie -->
    <?php foreach ($olympiad->exercises as $exercise): ?>
        <div class="col-md-4">
            <div class="panel panel-default shadow3">
                <div class="panel-heading">
                    <?php
                    $exerciseResult = $exercise->getResult(Yii::$app->session->get('login_user'))->one();
                    Yii::debug($exerciseResult, 'ex-res');
                    ?>
                    <strong>Задание: </strong><?= $exercise->smarty->name ?>
                    <?php if($exerciseResult): ?>
                        <span class="badge pull-right">Пройден</span>
                    <?php else: ?>
                        <span class="badge pull-right">Доступен</span>
                    <?php endif; ?>
                </div>
                <div class="panel-body">
                    <?php if (!$exerciseResult): ?>
                        <?= Html::a('Пройти', Url::to(['/simulator/smarty', 'id_smarty' => $exercise->id_smarty, 't' => 'olympiad', 'id_exercise' => $exercise->id]), [
                            'class' => 'btn btn-primary center-block'
                        ]) ?>
                    <?php else: ?>
                        <?= Html::a('Посмотреть результаты', Url::to(['/olympiad/exercise-result', 'id_olympiad_result' => $exerciseResult->id]), [
                            'class' => 'btn btn-warning center-block'
                        ])?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div><!-- Zadanie -->


<?php

$cssShadowBox = <<<CSS

.shadow3 {
box-shadow: 0 1px 5px rgba(0, 0, 0, 0.15);
}


CSS;

$this->registerCss($cssShadowBox);
