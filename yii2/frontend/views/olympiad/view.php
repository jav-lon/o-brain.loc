<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\olympiad\Olympiad */
/* @var $olympiadExercises object */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все олимпиады', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olympiad-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'author',
            ['label' => 'Создатель', 'value' => $model->author->fio],
            'name',
            'description:html',
//            'date:date',
            ['attribute' => 'date', 'format' => ['date', 'php: d-M Y H:i:s (МСК)']],
            ['attribute' => 'created_at', 'format' => ['date', 'php: d-M Y H:i:s (МСК)']],
//            'is_public',
            ['attribute' => 'is_public', 'label' => 'Является общедоступной','value' => function($model) {return $model->is_public == 1 ? 'Да' : 'Нет';}],
//            'created_at:date',
            //'public_users_ids',
            ['attribute' => 'public_users_ids', 'label' => 'Cписок участников', 'format' => 'html', 'value' => function($model,$widget) {
                $users = $model->users;
                $html = '<ul class="list-group">';
                foreach($users as $user) {
                    $html .= '<li class="list-group-item">';
                    $html .= '<span class="badge">';
                    $html .= $user->type;
                    $html .= '</span>';
                    $html .= $user->fio;
                    $html .= '</li>';
                }
                $html .= '</ul>';

                return $html;
            }]
        ],
    ]) ?>

    <h3 class="page-header">
        Cписок задания
    </h3>
    <?php if ($olympiadExercises): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>#</th>
                            <th>Название Smarty</th>
                            <th>Кол-во примеров</th>
                        </tr>
                        <?php
                        $count_exercise = 1;
                        ?>
                        <?php foreach($olympiadExercises as $exercise): ?>
                            <tr>
                                <td><?=$count_exercise++?></td>
                                <td><?=$exercise->smarty->name?></td>
                                <td><?=$exercise->smarty->quantity?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    <?php else: ?>
        <well>
            <strong>Пусто.</strong>
        </well>
    <?php endif; ?>

</div>
