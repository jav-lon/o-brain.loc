<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\olympiad\Olympiad */
/* @var $form yii\widgets\ActiveForm */
/* @var $students_IDs array */
/* @var $selected_IDs array */

?>

<div class="olympiad-form col-md-7">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal']
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 50,
            'plugins' => [
                'fontfamily',
                'fontcolor',
                'fullscreen',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'tmp_date')->widget(DatePicker::className(), [
        'name' => 'anniversary',
        'readonly' => true,
        //'value' => date('d-M-y', strtotime('+2 days')),
        'options' => ['placeholder' => 'Выберите ...'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
		    'todayHighlight' => true,
            'startDate' => 'd',
        ]
    ]) ?>

    <?php $model->isNewRecord?>

    <?= $form->field($model, 'public_users_ids')->widget(Select2::class,[
        'name' => 'kv-state-240',
        'data' => $students_IDs,
        'theme' => Select2::THEME_KRAJEE,
        'size' => Select2::MEDIUM,
        'options' => [
            'placeholder' => 'Выберите ...',
            'multiple' => true,
            'options' => $model->isNewRecord ? null : $selected_IDs,

        ],
        'pluginOptions' => [
            //'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$cssImperaviToolbar = <<<CSS
.redactor-toolbar {
z-index: 9;
}
CSS;

$this->registerCss($cssImperaviToolbar);

