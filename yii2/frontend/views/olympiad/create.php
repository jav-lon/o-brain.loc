<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\olympiad\Olympiad */
/* @var $students_IDs array */

$this->title = 'Создать олимпиаду';
$this->params['breadcrumbs'][] = ['label' => 'Все олимпиады', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olympiad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'students_IDs' => $students_IDs,
    ]) ?>

</div>
