<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\olympiad\Olympiad */
/* @var $students_IDs array*/
/* @var $selected_IDs array*/

$this->title = 'Обновить олимпиаду: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все олимпиады', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="olympiad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'students_IDs' => $students_IDs,
        'selected_IDs' => $selected_IDs,
    ]) ?>

</div>
