<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\olympiad\OlympiadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все олимпиады';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olympiad-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать олимпиаду', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'id_user',
            [
                'attribute' => 'id_user',
                'label' => 'Создатель',
                'value' => function($model) {
                    return \frontend\models\User::find()->select(['fio'])->where(['id' => $model->id_user])->scalar();
                }
            ],
            'name',
            //'description:html',
            ['attribute' => 'date', 'format' => ['date', 'php: d-M Y H:i:s (МСК)']],
            ['attribute' => 'created_at', 'format' => ['date', 'php: d-M Y H:i:s (МСК)']],
            //'is_public',
            //'public_users_ids',
            //'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {add_exercise} {publish} {hide}',
                'buttons' => [
                    'publish' => function($url, $model, $key){
                        return Html::a('Опубликовать', \yii\helpers\Url::to(['/olympiad/publish-olympiad', 'id_olympiad' => $model->id]), ['class' => 'btn btn-success btn-xs']);
                    },
                    'hide' => function($url, $model, $key){
                        return Html::a('Скрыть', \yii\helpers\Url::to(['/olympiad/hide-olympiad', 'id_olympiad' => $model->id]), ['class' => 'btn btn-warning btn-xs']);
                    },
                    'add_exercise' => function($url, $model, $key) {
                        return Html::a('Добавить задание', \yii\helpers\Url::to(['/smarty-manager/create', 'type' => 'olympiad', 'id' => $model->id]), ['class' => 'btn btn-primary btn-xs']);
                    }
                ],
                'visibleButtons' => [
                    'publish' => function ($model, $key, $index){
                        return $model->is_public === 0;
                    },
                    'hide' => function ($model, $key, $index){
                        return $model->is_public === 1;
                    },
                    'add_exercise' => function ($model, $key, $index){
                        return $model->is_public === 0;
                    },
                ]
            ],
        ],
    ]); ?>
</div>
