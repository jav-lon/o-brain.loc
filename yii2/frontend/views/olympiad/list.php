<?php
/* @var $this yii\web\View */
/* @var $olympiads \frontend\models\Olimp */
/* @var $user \frontend\models\User */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Олимпиады';

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php if ( $olympiads && $user->type == 'student'): ?>
    <div class="row">
        <?php foreach ($olympiads as $olympiad): ?>
            <?php if (in_array($user->id, $olympiad->usersID)): ?>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= Html::encode($olympiad->name) ?></h3>
                        </div>

                        <table class="table table-bordered">
                            <tr><td>Создатель:</td><td><?= $olympiad->author->fio ?></td></tr>
                            <tr><td>Дата проведения:</td><td><?= Yii::$app->formatter->asDate($olympiad->date) ?></td></tr>
                        </table>
                        <div class="panel-footer">
                            <nav aria-label="...">
                                <?php if($olympiad->date <= time()): ?>
                                    <ul class="pager">
                                        <li><a href="<?= Url::to(['/olympiad/exercise-list', 'id_olympiad' => $olympiad->id]) ?>" >Начать <span aria-hidden="true">&rarr;</span></a></li>
                                    </ul>
                                <?php else: ?>
                                    <p class="text-center text-primary"> <strong>Осталось:</strong></p>
                                    <p class="text-success text-center"><strong><?=Yii::$app->formatter->asDuration($olympiad->date - time())?></strong></p>
                                <?php endif; ?>
                            </nav>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php else: ?>
        <?php if($user->type == 'student'): ?>
            <div class="well">
                <p class="text-info">
                    В данный момент актуальных олимпиад нет.
                </p>
            </div>
        <?php else: ?>
            <div class="well">
                <p class="text-danger">
                    Вы не можете пройти олимпиаду!
                </p>
            </div>
        <?php endif; ?>
<?php endif; ?>
