<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 13.09.2018
 * Time: 18:38
 *
 * @var $this \yii\web\View
 * @var $olympiadResult \frontend\models\olympiad\OlympiadResult
 *
 */

use kartik\rating\StarRating;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Результат';
$this->params['breadcrumbs'][] = ['label' => 'Все  олимпиады', 'url' => '/olympiad/list'];
$this->params['breadcrumbs'][] = ['label' => 'Задание', 'url' => Url::previous('remember-olympiad')];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-header">
    <h2><?=Html::encode($this->title)?></h2>
</div>

<?php if ($olympiadResult): ?>
    <div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="center-block">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Результат</h3>
                </div>
                <div class="panel-body text-center">
                    <!--<span class="label label-default">Rating: </span>-->
                    <?= StarRating::widget([
                        'name' => 'rating_result',
                        'value' => $olympiadResult->ratingStar,
                        'pluginOptions' => ['displayOnly' => true]
                    ]); ?>
                    <div><strong>Правильно:</strong><span class="text-success"> <?= $olympiadResult->count_true_answers?></span> </div>
                    <div><strong>Ошибки: </strong><span class="text-danger"><?= $olympiadResult->count_errors?></span></div>
                    <div><strong>Потрачено: </strong><?= Yii::$app->formatter->asDuration($olympiadResult->time/1000)?></div>
                </div>
                <div class="panel-footer">
                    <nav aria-label="...">
                        <ul class="pager">
                            <li><a href="<?= Url::previous('remember-olympiad') ?>">Все задания</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>