<?php
/* @var $this \yii\web\View */

?>

<span class="center-block" style="width: 350px">
    <div class="panel panel-default">
        <div class="panel-body text-center">
            <h3>Благодарим Вас за заказ!</h3>
            <a href="/shop" class="btn btn-sm btn-success">Вернутся</a>
        </div>
    </div>
</span>
