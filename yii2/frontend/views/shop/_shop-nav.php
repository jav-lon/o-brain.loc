<?php
/* @var $this yii\web\View */

use yii\bootstrap\Nav;

$user = \frontend\models\User::findOne(Yii::$app->session->get('login_user'));

$items = [
    [
        'label' => 'Создать заказ',
        'url' => ['/shop'],
        'linkOptions' => [],
        'active' => Yii::$app->controller->id === 'shop',
    ],
    [
        'label' => 'История заказов',
        'url' => ['/order/order/index'],
        'active' => Yii::$app->controller->module->id === 'order',
    ],
];

if ($user->type === 'admin') {
    $items[] = [
            'label' => 'Создать магазин',
            'url' => ['/product'],
            'active' => Yii::$app->controller->id === 'product',
    ];
}

?>
<?=  Nav::widget([
    'items' => $items,
    'options' => ['class' =>'nav-tabs'], // set this to nav-tab to get tab-styled navigation
]); ?>
