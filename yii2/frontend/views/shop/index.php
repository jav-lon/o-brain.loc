<?php
/* @var $this yii\web\View */
/* @var $products array */

use yii\bootstrap\Nav;

?>


<?= $this->render('_shop-nav'); //menyu shop?>

    <br>

    <!--  korzina  -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?=\dvizh\cart\widgets\ElementsList::widget([
                    'type' => \dvizh\cart\widgets\ElementsList::TYPE_FULL,
                    'showTruncate' => true,
                    'showOffer' => true,
                    'showTotal' => true,
                    'offerUrl' => '/shop/offer'
                ])?>
            </div>
        </div>
    </div>

    <h4>Товары</h4>

    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>...</th>
                    </tr>
                </thead>
                <tbody>
                    <?php /* @var $product \frontend\models\Product */ ?>
                    <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?= $product->name ?></td>
                        <td><?= $product->price ?> <span><?= $product->currency ?></span></td>
                        <td>
                            <?= \dvizh\cart\widgets\BuyButton::widget([
                                'model' => $product,
                                'text' => 'Добавить в корзину',
                                'cssClass' => 'btn btn-sm btn-success'
                            ])?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
