<?php
/* @var $this \yii\web\View */

use yii\helpers\Html;
use dvizh\order\widgets\OrderForm;
use dvizh\cart\widgets\CartInformer;

$this->title = 'ОФОРМЛЕНИЕ ЗАКАЗА';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['/shop']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-header">
    <h3><strong><?=Html::encode($this->title)?></strong></h3>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Состав заказа</h4>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <th>Товар</th>
                        <th>Сумма</th>
                        <th>Количество</th>
                        <th>Общая сумма</th>
                    </thead>
                    <tbody>
                    <?php foreach (Yii::$app->cart->elements as $element): ?>
                        <tr>
                            <td><?= $element->getName() ?></td>
                            <td><?= $element->price ?></td>
                            <td><?= $element->count ?></td>
                            <td><?= $element->price * $element->count ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <span class=""><strong>ИТОГО:</strong> <?= Yii::$app->cart->cost ?></span>
            </div>
        </div>
    </div>
</div>

<?=OrderForm::widget([
    'elements' => Yii::$app->cart->elements,
]);?>
