<?php
/* @var $this yii\web\View */
/* @var $courses \frontend\models\Olimp */
/* @var $type string */
/* @var $olympiads array | object*/
/* @var $user \frontend\models\User*/

use yii\helpers\Html;
use yii\helpers\Url;

if ($type == 'course') {
    $this->title = 'Все курсы';
} elseif ($type == 'olympiad') {
    $this->title = 'Результат олимпиады';
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>

<?php if($type == 'course' && $courses !== null): ?>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="center-block">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <?php foreach ($courses as $course): ?>
                            <tr><td><h3><strong><?= $course->name ?></strong></h3></td><td><a href="<?= Url::to(['/result/list-course-members', 'id_olimp' => $course->id]) ?>" class="btn btn-default btn-lg pull-right">Просмотреть результаты</a></td></tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php elseif($type == 'olympiad' && $olympiads !== null): ?>
    <div class="row">
        <div class="col-md-8 col-md-offset2">
            <div class="center-block">

                <?php if ($olympiads): ?>
                    <div class="list-group"><!--List Group-->
                        <?php
                        /**
                         * @var $olympiad \frontend\models\olympiad\Olympiad
                         */

                        foreach ($olympiads as $olympiad): ?>
                            <?php Yii::debug($olympiad->usersID, 'usersID'); ?>
                            <?php Yii::debug($user->id, 'i'); ?>
                            <?php Yii::debug($type, 'type'); ?>
                            <?php Yii::debug(in_array($user->id, $olympiad->usersID), 'inarray'); ?>
                            <?php if (in_array($user->id, $olympiad->usersID) && $user->type == 'student'): ?>
                                <a href="<?=Url::to(['/result/olympiad-high-score-table', 'id_olympiad' => $olympiad->id])?>" class="list-group-item">
                                    <!--<span class="badge">14</span>-->
                                    <h4 class="list-group-item-heading"><strong><?=$olympiad->name?></strong></h4>
                                    <p class="list-group-item-text">
                                    <div><span class="text-primary"><strong>Создатель:</strong></span> <?=$olympiad->author->fio?></div>
                                    <div><span class="text-primary"><strong>Дата проведения:</strong></span><?=Yii::$app->formatter->asDate($olympiad->date)?></div>
                                    </p>
                                </a>
                            <?php elseif($type != 'student'): ?>
                                <a href="<?=Url::to(['/result/olympiad-high-score-table', 'id_olympiad' => $olympiad->id])?>" class="list-group-item">
                                    <!--<span class="badge">14</span>-->
                                    <h4 class="list-group-item-heading"><strong><?=$olympiad->name?></strong></h4>
                                    <p class="list-group-item-text">
                                    <div><span class="text-primary"><strong>Создатель:</strong></span> <?=$olympiad->author->fio?></div>
                                    <div><span class="text-primary"><strong>Дата проведения:</strong></span><?=Yii::$app->formatter->asDate($olympiad->date)?></div>
                                    </p>
                                </a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div><!--List Group-->
                <?php else: ?>
                    <?php if($user->type == 'student'): ?>
                        <div class="well">
                            <p class="text-info">
                                В данный момент актуальных результатов нет.
                            </p>
                        </div>
                    <?php else: ?>
                        <div class="well">
                            <p class="text-danger">
                                Вы не можете пройти олимпиаду!
                            </p>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
<?php else: ?>

<?php endif; ?>