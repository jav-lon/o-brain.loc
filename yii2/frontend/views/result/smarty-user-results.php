<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 04.09.2018
 * Time: 16:43
 *
 * @var $this \yii\web\View
 * @var $smarty_results \frontend\models\User
 * @var $id int
 */

use yii\helpers\Html;

$hasResult = false;

$this->title = 'Результаты Smarty';

$this->params['breadcrumbs'][] = ['label' => 'Выполненные упражнения', 'url' => '/lk/student'];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<nav aria-label="...">
    <ul class="pager">
        <li class="previous"><a href="/lk/student"><span aria-hidden="true">&larr;</span> Выполненные упражнения</a></li>
<!--        <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>-->
    </ul>
</nav>
<?php if ($smarty_results): ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php $count_panels = 1; ?>
        <?php foreach ($smarty_results as $smarty_result): ?>
            <?php if($smarty_result['smartyResults'] && $smarty_result['id'] == $id && $hasResult = true): // izmenit etu stroku?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading_<?=$count_panels?>">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$count_panels?>" aria-expanded="true" aria-controls="collapse_<?=$count_panels?>">
                                <?=$smarty_result['fio']?> <span class="badge"><?=$smarty_result['type']?></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_<?=$count_panels?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_<?=$count_panels++?>">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th>Дата</th>
                                        <th>Потрачено <small>(секунд)</small></th>
                                        <th>Правильно</th>
                                        <th>Ошибки</th>
                                        <th>Действие</th>
                                    </tr>
                                    <tbody>
                                    <?php foreach ($smarty_result['smartyResults'] as $smartyResult): ?>
                                        <tr>
                                            <td><?=Yii::$app->formatter->asDate($smartyResult['created_at'], 'php: d-m-y')?></td>
                                            <td><p class="text-right"><em><?=$smartyResult['time']/1000?></em></p></td>
                                            <td><p class="text-center text-primary"><strong><?=$smartyResult['true_answers']?></strong></p></td>
                                            <td><p class="text-center text-danger"><strong><?=$smartyResult['count_errors']?></strong></p></td>
                                            <td>
                                                <button type="button" id="moreInfoButton" data-loading-text="Ждите..." class="btn btn-primary btn-xs moreInfoButton" autocomplete="off" data-id-smarty-result="<?=$smartyResult['id']?>">
                                                    Подробнее
                                                </button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <div class="well well-lg">
        Нет данных
    </div>
<?php endif; ?>

<?php if (!$hasResult): ?>
    <div class="well well-lg">
        Нет данных
    </div>
<?php endif; ?>

<div id="moreInfoModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Подробнее</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Выйти</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
/*===dlya knopki podrobnee===*/
$moreInfo = <<<JS
$('.moreInfoButton').on('click', function () {
    var btnLoading = $(this).button('loading');
    
    // business logic...
    var idSmartyResults = $(this).attr('data-id-smarty-result');
    $.ajax({
        url: '/result/get-more-info-smarty/',
        method: "POST",
        data: {'id_smarty_results': idSmartyResults},
        success: function(data) {
            console.log(data);
            var moreInfoModal = $('#moreInfoModal');
            var bodyModal = $('#moreInfoModal .modal-body');
            
            var moreInfoHtml = '';
            moreInfoHtml += '<p><span><strong>Создатель:</strong> '+ (data.author == 'deleted' ? 'Задание удалено' : data.author) +'</span></p>'
            moreInfoHtml += '<p><span><strong>Название задание:</strong> '+ (data.name == 'deleted' ? 'Задание удалено' : data.name) +'</span></p>'
            if (data.qa != false) {
                moreInfoHtml += '<div class="table-responsive">';
                moreInfoHtml += '<table class="table table-bordered">';
                moreInfoHtml += '<tr><th>№</th><th>Вопрос</th><th>Ответ пользователя</th>';
                for ( var i = 0; i < data.qa[0].length; i++ ) {
                    var alertMsg = '';
                    if (data.qa[0][i] == data.qa[1][i]) alertMsg = 'success';
                    else alertMsg = 'danger';
                    moreInfoHtml += '<tr class="'+ alertMsg +'">';
                    moreInfoHtml += '<td>';
                    moreInfoHtml += i + 1;
                    moreInfoHtml += '</td>';
                    moreInfoHtml += '<td>';
                    moreInfoHtml += data.qa[0][i];
                    moreInfoHtml += '</td>';
                    moreInfoHtml += '<td>';
                    moreInfoHtml += data.qa[1][i];
                    moreInfoHtml += '</td>';
                    moreInfoHtml += '</tr>';
                }
                moreInfoHtml += '</table>';
                moreInfoHtml += '</div>';                
            } else {
                moreInfoHtml += '<div class="alert alert-danger" role="alert"><strong>Нет данных</strong></div>'
            }
            bodyModal.html(moreInfoHtml);
            moreInfoModal.modal('show');
            btnLoading.button('reset')

        },
        error: function(data) {
            alert('Error');
            btnLoading.button('reset')

        }
    });        
  })
JS;

$this->registerJs($moreInfo);
?>
