<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 14.09.2018
 * Time: 10:50
 *
 */
/* @var $this \yii\web\View */
/* @var $olympiad array | object */
/* @var $olympiad_high_score_table object */

use yii\helpers\Html;

$this->title = 'Результаты';
?>
<div class="page-header">
    <h2><?=Html::encode($this->title)?></h2>
</div>

<div class="row">
    <div class="col-sm-4 col-xs-12"><!--left panel-->
        <div class="panel panel-default">
            <div class="panel-heading">О олимпиаде</div>
            <!--<div class="panel-body">
                Panel content
            </div>-->
            <!--Table-->
            <?php if($olympiad): ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>Название:</td>
                            <td><?=$olympiad->name?></td>
                        </tr>
                        <tr>
                            <td>Создатель:</td>
                            <td><?=$olympiad->author->fio?></td>
                        </tr>
                        <tr>
                            <td>Дата создания:</td>
                            <td><?=Yii::$app->formatter->asDate($olympiad->created_at)?></td>
                        </tr>
                        <tr>
                            <td>Дата проведения:</td>
                            <td><?=Yii::$app->formatter->asDate($olympiad->date)?></td>
                        </tr>
                        <tr>
                            <td>Кол-во участников:</td>
                            <td><?=$olympiad->countMembers?></td>
                        </tr>
                    </table>
                </div>
            <?php else: ?>
                <div class="well">
                    <p class="text-danger">
                        Нет данных!
                    </p>
                </div>
            <?php endif; ?>
        </div>
    </div><!--left panel-->
    <div class="col-sm-8 col-xs-12"><!--right panel-->
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Общий рейтинг</strong></div>

            <!-- Table -->
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Место</th>
                        <th>ФИО</th>
                        <th>Правильные ответы</th>
                        <th>Время (секунд)</th>
                    </tr>
                    <?php if ($olympiad_high_score_table): ?>
                        <?php
                        $count_member = 1;
                        ?>
                        <?php foreach ($olympiad_high_score_table as $member): ?>
                            <tr>
                                <td class="place <?=$count_member < 4 ? 'place-'.$count_member : ''?>"><?= $count_member >= 4 ? $count_member : '' ?></td>
                                <td class="username"><?=$member['student']['fio']?></td>
                                <td class="all-answer"><?=$member['all_true_answers']?></td>
                                <td class="all-time"><?=$member['all_time']/1000?></td>
                            </tr>
                        <?php
                        $count_member++;
                        ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="4">
                                <div class="well">
                                    <p class="text-primary">
                                        Нет данных!
                                    </p>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
        </div><!--right panel-->
    </div>
</div>

<?php
$cssTable = <<<CSS
.gold {
background-color: #ffd700;
}
.silver {
background-color: #b5b5bd
}
.bronze {
background-color: #8c8752;
}

td.place {
text-align: center;
font-size: 16px;
font-weight: bold;
}
td .badge {
font-size: 16px !important;
font-weight: bold;
}

.place-1 {
background-image: url(/images/olympiad/gold-medal.svg);
background-position: center;
background-repeat: no-repeat;
background-size: contain;
}
.place-2 {
background-image: url(/images/olympiad/silver-medal.svg);
background-position: center;
background-repeat: no-repeat;
background-size: contain;
}
.place-3 {
background-image: url(/images/olympiad/bronze-medal.svg);
background-position: center;
background-repeat: no-repeat;
background-size: contain;
}

table td {
height: 60px;
}
td.username {
font-size: 17px;
font-weight: bold;
}
td.all-answer, td.all-time {
font-size: 18px;
}
CSS;

$this->registerCss($cssTable);