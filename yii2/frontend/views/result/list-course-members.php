<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 07.09.2018
 * Time: 16:29
 */

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \frontend\models\User */
/* @var $students \frontend\models\User */
/* @var $teachers \frontend\models\User */
/* @var $partners \frontend\models\User */
/* @var $id_olimp int */

$this->title = 'Участники курса';

$this->params['breadcrumbs'][] = ['label' => 'Все курсы', 'url' => Url::to(['/result', 'type' => 'course'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h2><?=Html::encode($this->title)?></h2>
</div>

<div class="row">
    <div class="col-md-12" style="margin-bottom: 10px">
        <a href="<?=Url::to(['/result/course-result', 'id_olimp' => $id_olimp, 'id_user' => $user->id])?>" class="btn btn-info btn-lg">Мои результаты</a>
    </div>
</div>

<?php if($user->type == 'admin'): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion_admin_teacher" role="tablist" aria-multiselectable="true">
                <?php
                $count_teacher = 1;
                ?>
                <?php foreach ($teachers as $teacher): ?>
                    <div class="panel panel-info">
                        <div class="panel-heading" role="tab" id="heading_teacher_<?=$count_teacher?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_admin_teacher" href="#collapse_teacher_<?=$count_teacher?>" aria-expanded="false" aria-controls="collapseOne">
                                    <?=$teacher->fio?>
                                    <?php $students = $teacher->getStudents($teacher->id); ?>
                                    <span class="label label-primary">Ученики: <?=count($students)?></span>
                                    <span class="badge pull-right"><?=$teacher->type?></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_teacher_<?=$count_teacher?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_teacher_<?=$count_teacher++?>">
                            <!--<div class="panel-body">
                            </div>-->
                            <!-- List group -->
                            <div class="list-group">

                                <?php foreach ($students as $student): ?>
                                    <a href="<?=Url::to(['/result/course-result', 'id_olimp' => $id_olimp, 'id_user' => $student->id])?>" class="list-group-item">
                                        <?= $student->fio ?>
                                        <span class="badge pull-right"><?=$student->type?></span>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion_admin_partner" role="tablist" aria-multiselectable="true">
                <?php
                $count_partner = 1;
                ?>
                <?php foreach ($partners as $partner): ?>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="heading_teacher_<?=$count_partner?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_admin_partner" href="#collapse_partner_<?=$count_partner?>" aria-expanded="false" aria-controls="collapse_teacher_<?=$count_partner?>">
                                    <?=$partner->fio?>
                                    <span class="badge pull-right"><?=$partner->type?></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_partner_<?=$count_partner?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_teacher_<?=$count_partner++?>">
                            <div class="panel-body">
                                <?php $count_partner_teacher = 1 ?>
                                <?php foreach ($partner->getTeachers($partner->id) as $teacher): ?>
                                    <div class="panel-group" id="accordion_admin_partner_teacher" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-info">
                                            <div class="panel-heading" role="tab" id="heading_admin_partner_teacher_<?=$count_partner_teacher?>">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_admin_partner_teacher" href="#collapse_admin_partner_teacher_<?=$count_partner_teacher?>" aria-expanded="false" aria-controls="collapse_admin_partner_teacher_<?=$count_partner_teacher?>">
                                                        <?= $teacher->fio ?>
                                                        <span class="badge pull-right"><?=$teacher->type?></span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_admin_partner_teacher_<?=$count_partner_teacher?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_admin_partner_teacher_<?=$count_partner_teacher++?>">
                                                <!--<div class="panel-body">
                                                </div>-->
                                                <!-- List group -->
                                                <div class="list-group">
                                                    <?php foreach ($teacher->getStudents($teacher->id) as $student): ?>
                                                        <a href="<?=Url::to(['/result/course-result', 'id_olimp' => $id_olimp, 'id_user' => $student->id])?>" class="list-group-item">
                                                            <?= $student->fio ?>
                                                            <span class="badge pull-right"><?=$student->type?></span>
                                                        </a>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php elseif($user->type == 'partner'): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion_partner_teacher" role="tablist" aria-multiselectable="true">
                <?php
                $count_teacher = 1;
                ?>
                <?php foreach ($teachers as $teacher): ?>
                    <div class="panel panel-info">
                        <div class="panel-heading" role="tab" id="heading_teacher_<?=$count_teacher?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_partner_teacher" href="#collapse_teacher_<?=$count_teacher?>" aria-expanded="false" aria-controls="collapseOne">
                                    <?=$teacher->fio?>
                                    <?php $students = $teacher->getStudents($teacher->id); ?>
                                    <span class="label label-primary">Ученики: <?=count($students)?></span>
                                    <span class="badge pull-right"><?=$teacher->type?></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_teacher_<?=$count_teacher?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_teacher_<?=$count_teacher++?>">
                            <!--<div class="panel-body">
                            </div>-->
                            <!-- List group -->
                            <div class="list-group">

                                <?php foreach ($students as $student): ?>
                                    <a href="<?=Url::to(['/result/course-result', 'id_olimp' => $id_olimp, 'id_user' => $student->id])?>" class="list-group-item">
                                        <?= $student->fio ?>
                                        <span class="badge pull-right"><?=$student->type?></span>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php elseif($user->type == 'teacher'): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                <?php foreach ($students as $student): ?>
                    <a href="<?=Url::to(['/result/course-result', 'id_olimp' => $id_olimp, 'id_user' => $student->id])?>" class="list-group-item">
                        <?= $student->fio ?>
                        <span class="badge pull-right"><?=$student->type?></span>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php elseif($user->type == 'student'): ?>

<?php endif; ?>
