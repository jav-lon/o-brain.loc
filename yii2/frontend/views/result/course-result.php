<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 07.09.2018
 * Time: 16:12
 */

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $course \frontend\models\Olimp */
/* @var $id_user int */

$this->title = 'Результаты курса';

$this->params['breadcrumbs'][] = ['label' => 'Все курсы', 'url' => Url::to(['/result', 'type' => 'course'])];
$this->params['breadcrumbs'][] = ['label' => 'Просмотреть результаты', 'url' => Url::to(['/result/list-course-members', 'id_olimp' => $id_olimp])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h2><?=Html::encode($this->title)?></h2>
</div>

<?php
$count_stage = $count_lesson = $count_exercise = 1;
?>

<?php if($course !== null): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion_stage" role="tablist" aria-multiselectable="true">
                <?php foreach($course->stages as $stage): ?>
                    <!--Stupeni-->
                    <div class="panel panel-info">
                        <div class="panel-heading" role="tab" id="heading_stage_<?=$count_stage?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_stage" href="#collapse_stage_<?=$count_stage?>" aria-expanded="false" aria-controls="collapse_stage_<?=$count_stage?>">
                                    <?= $stage->name ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_stage_<?=$count_stage?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_stage_<?=$count_stage++?>">
                            <div class="panel-body">
                            </div>
                            <?php foreach($stage->lessons as $lesson): ?>
                                <!--Lesson Collapse-->
                                <div class="row">
                                    <div class="col-md-11 col-md-offset-1">
                                        <div class="panel-group" id="accordion_lesson" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-success">
                                                <div class="panel-heading" role="tab" id="heading_lesson_<?=$count_lesson?>">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion_lesson" href="#collapse_lesson_<?=$count_lesson?>" aria-expanded="false" aria-controls="collapse_lesson_<?=$count_lesson?>">
                                                            <?=$lesson->name?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_lesson_<?=$count_lesson?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_lesson_<?=$count_lesson++?>">
                                                    <!--<div class="panel-body">

                                                    </div>-->
                                                    <?php if ($lesson->exercises): //esli est zadanie?>
                                                        <!--Exercise Table-->
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Тип задание</th>
                                                                    <th>Действие</th>
                                                                </tr>
                                                                <?php foreach ($lesson->exercises as $exercise): ?>
                                                                <tr>
                                                                    <td><?=$exercise->exercise_number?></td>
                                                                    <td><?=$exercise->work_type == 'classwork' ? 'Классная работа' : 'Домашняя работа'?></td>
                                                                    <td>
                                                                        <?php
                                                                        $user_exercise_status = $exercise->getStatus()->where(['id_user' => $id_user])->one();
                                                                        ?>
                                                                        <?php if($user_exercise_status !== null && $user_exercise_status->is_end == 1): ?>
                                                                        <?=Html::button('Результат', [
                                                                            'id' => "resultButton",
                                                                            'data-loading-text' => "Ждите...",
                                                                            'autocomplete' => "off",
                                                                            'data-id-exercise' => $exercise->id,
                                                                            'data-id-user' => $id_user,
                                                                            'class' => 'btn btn-primary btn-xs resultButton'
                                                                        ])?>
                                                                        <?php else: ?>
                                                                        <span class="badge">Не пройден</span>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach; ?>
                                                            </table>
                                                        </div><!--Exercise Table-->
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div><!--Lesson Collapse-->

                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div><!--Stupeni-->
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div id="resultExerciseModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Подробнее</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Выйти</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
/*===dlya knopki podrobnee===*/
$moreInfo = <<<JS
$('.resultButton').on('click', function () {
    var btnLoading = $(this).button('loading');
    
    // business logic...
    var idExercise = $(this).attr('data-id-exercise');
    var idUser = $(this).attr('data-id-user');
    $.ajax({
        url: '/result/get-exercise-result/',
        method: "POST",
        data: {'id_exercise': idExercise, 'id_user': idUser},
        success: function(data) {
            console.log(data);
            var resultExerciseModal = $('#resultExerciseModal');
            var bodyModal = $('#resultExerciseModal .modal-body');
            
            var resultExerciseHtml = '';
            resultExerciseHtml += '<p><span><strong>Название тренажер:</strong> '+ data.simulator_name +'</span></p>';
            resultExerciseHtml += '<p><span><strong>Дата:</strong> '+ data.created_at +'</span></p>';
            resultExerciseHtml += '<p><span><strong>Потрачено:</strong> '+ data.time +'</span></p>';
            resultExerciseHtml += '<p><span><strong>Ошибки:</strong> '+ data.errors +'</span></p>';
            if (data.qa != false) {
                resultExerciseHtml += '<div class="table-responsive">';
                resultExerciseHtml += '<table class="table table-bordered">';
                resultExerciseHtml += '<tr><th>№</th><th>Вопрос</th><th>Ответ пользователя</th>';
                if (data.id_simulator == 9) {                    
                    var alertMsg = '';
                    for ( var i = 0; i < data.qa[0].length; i++ ) {
                        if (data.qa[0][i] == data.qa[1][i]) alertMsg = 'success';
                        else alertMsg = 'danger';
                        resultExerciseHtml += '<tr class="'+ alertMsg +'">';
                        resultExerciseHtml += '<td>';
                        resultExerciseHtml += i + 1;
                        resultExerciseHtml += '</td>';
                        resultExerciseHtml += '<td>';
                        resultExerciseHtml += data.qa[0][i];
                        resultExerciseHtml += '</td>';
                        resultExerciseHtml += '<td>';
                        resultExerciseHtml += data.qa[1][i];
                        resultExerciseHtml += '</td>';
                        resultExerciseHtml += '</tr>';
                    }
                } else {
                    var qa = data.qa;
                    for(var prop in qa) {
                        if (qa[prop].answer == qa[prop].question) alertMsg = 'success';
                        else alertMsg = 'danger';
                        resultExerciseHtml += '<tr class="'+ alertMsg +'">';
                        resultExerciseHtml += '<td>';
                        resultExerciseHtml += qa[prop].nomer_job;
                        resultExerciseHtml += '</td>';
                        resultExerciseHtml += '<td>';
                        resultExerciseHtml += qa[prop].question;
                        resultExerciseHtml += '</td>';
                        resultExerciseHtml += '<td>';
                        resultExerciseHtml += qa[prop].answer;
                        resultExerciseHtml += '</td>';
                        resultExerciseHtml += '</tr>';
                    }
                    console.log(data.qa);
                }
                resultExerciseHtml += '</table>';
                resultExerciseHtml += '</div>';                
            } else {
                resultExerciseHtml += '<div class="alert alert-danger" role="alert"><strong>Нет данных</strong></div>'
            }
            bodyModal.html(resultExerciseHtml);
            resultExerciseModal.modal('show');
            btnLoading.button('reset')

        },
        error: function(data) {
            alert('Error');
            btnLoading.button('reset')

        }
    });        
  })
JS;

$this->registerJs($moreInfo);
?>
