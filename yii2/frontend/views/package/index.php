<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\package\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все пакеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать пакет', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Выбрать пользователя', ['select-user'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'url:url',
            //'type',
            [
                'attribute' => 'type',
                'value' => 'package',
                'filter' => [
                    'partner' => 'Партнерский пакет',
                    'franchise' => 'Франчайзинговый пакет'
                ],
                'filterInputOptions' => [
                    'prompt' => 'Все пакеты',
                    'class' => 'form-control',
                ]
            ],
            [
                'attribute' => 'theme',
                'value' => function($model) {
                    return ($value = $model->getThemeName()) === false ? '<span class="text-danger">Не задано</span>' : $value;
                },
                'format' => 'raw',
                'filter' => \frontend\models\package\PackageTheme::getThemes(),
                'filterInputOptions' => [
                    'prompt' => 'Все тематика',
                    'class' => 'form-control',
                ]
            ],
            //'public_users',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
