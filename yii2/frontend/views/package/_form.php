<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\package\Package */
/* @var $userIDs array */
/* @var $form yii\widgets\ActiveForm */
/* @var $selected_IDs array */

$selected_IDs = isset($selected_IDs) ? $selected_IDs : [];
?>

<div class="package-form col-md-7">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->hint('http:// или https://')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'franchise' => 'Франчайзинговый пакет', 'partner' => 'Партнерский пакет', ], ['prompt' => 'Выберите...', 'id' => 'package_parent']) ?>

    <?php
    // Additional input fields passed as params to the child dropdown's pluginOptions
    if (!$model->isNewRecord) echo Html::hiddenInput('model-theme-id', $model->theme, ['id'=>'model-theme-id']);
    ?>

    <?= $form->field($model, 'theme')->hint('Чтобы создать новый тематика: Напишите <strong>название</strong> и нажмите <kbd>Enter</kbd> или <kbd>Пробел</kbd>')->widget(DepDrop::class, [
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options' => [
            'pluginOptions'=>[
                'allowClear' => true,
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 255,
            ],
        ],
        'options' => ['id'=>'theme-id'],
        'pluginOptions'=>[
            'tags' => true,
            'depends'=>['package_parent'],
            'placeholder' => 'Выберите...',
            'url' => Url::to(['/package/get-themes']),
            'params' => ['model-theme-id'],
            'initialize' => ($model->isNewRecord ? false : true),
            'initDepends'=>['package_parent'],
        ],
    ]); ?>

    <?= $form->field($model, 'public_users')->widget(Select2::class,[
        'name' => 'kv-state-240',
        'data' => $userIDs,
        'theme' => Select2::THEME_KRAJEE,
        'size' => Select2::MEDIUM,
        'options' => [
            'placeholder' => 'Выберите ...',
            'multiple' => true,
            'options' => $selected_IDs
        ],
        'pluginOptions' => [
            //'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
