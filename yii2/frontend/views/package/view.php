<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\package\Package */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'url:url',
            //'type',
            [
                'attribute' => 'type',
                'value' => $model->getPackage(),
            ],
            [
                'attribute' => 'theme',
                'value' => ($value = $model->getThemeName()) === false ? '<span class="text-danger">Не задано</span>' : $value,
                'format' => 'raw',
            ],
            //'public_users',
            ['attribute' => 'public_users', 'format' => 'html', 'value' => function($model,$widget) {
                $users = $model->users;
                $html = '<ul class="list-group">';
                foreach($users as $user) {
                    $html .= '<li class="list-group-item">';
                    $html .= '<span class="badge">';
                    $html .= $user->type;
                    $html .= '</span>';
                    $html .= $user->fio;
                    $html .= '</li>';
                }
                $html .= '</ul>';

                return $html;
            }]
        ],
    ]) ?>

</div>
