<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\package\Package */
/* @var $userIDs array */

$this->title = 'Создать пакет';
$this->params['breadcrumbs'][] = ['label' => 'Все пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'userIDs' => $userIDs
    ]) ?>

</div>
