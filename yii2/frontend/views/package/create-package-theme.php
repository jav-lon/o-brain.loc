<?php
/**
 * @var $this \yii\web\View
 * @var $newTheme \frontend\models\package\PackageTheme
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Тематика';
$this->params['breadcrumbs'][] = ['label' => 'Все пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="create-package-theme">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($newTheme, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>