<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\package\Package */
/* @var $userIDs array */
/* @var $selected_IDs array */

$this->title = "Обновить пакет: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Все пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="package-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'userIDs' => $userIDs,
        'selected_IDs' => $selected_IDs,
    ]) ?>

</div>
