<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 29.08.2018
 * Time: 0:15
 *
 * @var $this \yii\web\View
 * @var $buttons array
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Партнерский пакет';
?>
<div class="page-header">
    <h2>Партнерский пакет</h2>
</div>

<?php if($buttons): ?>
    <div class="col-md-5">
        <div class="list-group">
            <?php foreach ($buttons as $button): ?>
                <a href="<?=Url::to($button->url)?>" target="_blank" class="list-group-item"><strong><?=Html::encode($button->name)?></strong></a>
            <?php endforeach; ?>
        </div>
    </div>
<?php else: ?>
    <div class="alert alert-danger" role="alert"><h3><strong>У вас недостаточно прав для доступа!</strong></h3></div>
<?php endif; ?>
