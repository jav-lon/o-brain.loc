<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model frontend\models\package\Package */
/* @var $userIDs array */

$this->title = 'Выбрать пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Все пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="select-user-form col-md-7">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user')->widget(Select2::class,[
            'name' => 'select-user',
            'data' => $userIDs,
            'theme' => Select2::THEME_KRAJEE,
            'size' => Select2::MEDIUM,
            'options' => [
                'placeholder' => 'Выберите ...',
            ],
            'pluginOptions' => [
                //'allowClear' => true
            ],
        ]) ?>

        <?= $form->field($model, 'franchisePackage')->checkbox() ?>
        <?= $form->field($model, 'partnerPackage')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton('Дать доступ', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
