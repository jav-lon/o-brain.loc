<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 19.02.2019
 * Time: 0:27
 */
/* @var $this \yii\web\View */
/* @var $ppa array */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget

?>
<div class="page-header">
    <h3>Соглашение о политике конфидециальности</h3>
</div>

<div class="col-sm-12 col-md-9">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($ppa, 'title_ppa')->textInput()?>
    <?= $form->field($ppa, 'text_ppa')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 100,
            'plugins' => [
                'fontfamily',
                'fontcolor',
                'fullscreen',
            ],
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>


