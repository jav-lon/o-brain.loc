<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 19.02.2019
 * Time: 0:20
 */

/* @var $this \yii\web\View */
use yii\helpers\Url;

?>
<div class="page-header">
    <h2>Настройки сайта</h2>
</div>

<div class="col-md-6">
    <div class="list-group">
        <a href="<?=Url::to(['/site-settings/privacy-policy-agreement'])?>" class="list-group-item">Соглашение о политике конфидециальности</a>
    </div>
</div>
