<?php
/* @var $this yii\web\View */
/* @var $privacy_policy array */

use yii\helpers\Html;

$this->title = $privacy_policy[0]['title_ppa'];
?>

<div class="page-header">
    <h3><?= Html::encode($this->title) ?></h3>
</div>

<p>
    <?= $privacy_policy[0]['text_ppa'] ?>
</p>
