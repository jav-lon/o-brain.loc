<?php

namespace frontend\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "olimp_lesson_status".
 *
 * @property int $id
 * @property int $id_olimp
 * @property int $id_user
 * @property int $id_lesson
 * @property int $is_end
 * @property int $is_public
 * @property int $id_stage
 */
class OlimpLessonStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_lesson_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_olimp', 'id_user', 'id_lesson', 'id_stage'], 'required'],
            [['id_olimp', 'id_user', 'id_lesson', 'id_stage'], 'integer'],
            [['is_end', 'is_public'], 'in', 'range' => [0, 1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_olimp' => 'Id Olimp',
            'id_user' => 'Id User',
            'id_lesson' => 'Id Lesson',
            'is_end' => 'Is End',
            'is_public' => 'Is Public',
        ];
    }

    public static function initUserLessonStatus($id_olimp, $id_stage) {
        $olimpLessonIds = OlimpLesson::getOlimpLesson($id_olimp, $id_stage)->select(['id'])->column();
        $allLessonStatusIds = self::getUserLessonStatus($id_olimp, $id_stage)->select(['id_lesson'])->column();

        if ( $allLessonStatusIds == null ) {
            foreach ( $olimpLessonIds as $olimpLessonId ) {
                $newLessonStatus = new self();
                $newLessonStatus->id_olimp = $id_olimp;
                $newLessonStatus->id_user = Yii::$app->session->get('login_user');
                $newLessonStatus->id_stage = $id_stage;
                $newLessonStatus->id_lesson = $olimpLessonId;
                if (!$newLessonStatus->save()) {
                    throw new Exception('Ошибка в сохранени OlimpLessonStatus', $newLessonStatus->errors);
                }
            }
        } else {
            foreach ( $olimpLessonIds as $olimpLessonId) {
                if (  !in_array($olimpLessonId, $allLessonStatusIds) ) {
                    $newLessonStatus = new self();
                    $newLessonStatus->id_olimp = $id_olimp;
                    $newLessonStatus->id_user = Yii::$app->session->get('login_user');
                    $newLessonStatus->id_stage = $id_stage;
                    $newLessonStatus->id_lesson = $olimpLessonId;
                    if (!$newLessonStatus->save()) {
                        throw new Exception('Ошибка в сохранени OlimpLessonStatus', $newLessonStatus->errors);
                    }
                }
            }
        }
    }

    public static function getUserLessonStatus($id_olimp, $id_stage) {

        return self::find()->where(['id_olimp' => $id_olimp, 'id_stage' => $id_stage, 'id_user' => Yii::$app->session->get('login_user')]);
    }

    public static function setCurrentLessonStatusAllow($id_olimp, $id_stage) {
        $currentLessonNumber = OlimpUserProperties::getCurrentLessonNumber($id_olimp);
        $currentLessonId = OlimpLesson::getCurrentLessonId($id_olimp, $id_stage, $currentLessonNumber);
        $currentLesson = self::getUserLessonStatus($id_olimp, $id_stage)->andWhere(['id_lesson' => $currentLessonId])->one();
        if ($currentLesson) {
            $currentLesson->is_public = '1';
            if ( !$currentLesson->save() ) {
                throw new Exception('Ошибка в сохранени OlimpLessonStatus', $currentLesson->errors);
            }
        }
    }
}
