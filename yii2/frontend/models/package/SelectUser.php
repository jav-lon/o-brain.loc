<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 23.10.2018
 * Time: 21:06
 */

namespace frontend\models\package;


use yii\base\Model;
use \Yii;

class SelectUser extends Model
{
    public $user;
    public $franchisePackage;
    public $partnerPackage;
    public $tmpNotice;

    private $_public_users;

    public function rules()
    {
        return [
            [['user'], 'integer'],
            [['user'], 'required'],
            [['franchisePackage', 'partnerPackage'], 'boolean'],
            [['franchisePackage', 'partnerPackage'], 'validateSelectSomeOne', 'skipOnEmpty' => false]
        ];
    }

    public function validateSelectSomeOne($attribute, $params) {
        if (!$this->partnerPackage && !$this->franchisePackage ) {
            $this->addError($this->tmpNotice, 'Выберите хотяби один пакет.');
            Yii::$app->session->setFlash('error', '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span> Выберите хотяби один пакет.');
        }
    }

    public function addUserInPackage() {
        if ($this->partnerPackage || $this->franchisePackage) {
            $query = Package::find();

            if ($this->partnerPackage) {
                $query->orWhere(['type' => 'partner']);
            }

            if ($this->franchisePackage) {
                $query->orWhere(['type' => 'franchise']);
            }

            $packages = $query->all();

            //viborka paketov
            foreach ($packages as $package) {
                if (!$this->hasUser($this->user, $package->public_users)) {

                    $package->public_users = $this->addUserArray($this->user);
                    $package->save(false);
                }
            }
        }
    }

    private function hasUser($user, $public_users) {
        $this->_public_users = unserialize($public_users);

        return in_array($user, $this->_public_users);
    }

    private function addUserArray($user) {
        $this->_public_users[] = $user;

        return $this->_public_users;
    }

    public function attributeLabels()
    {
        return [
            'user' => 'Пользователь',
            'franchisePackage' => 'Франчайзинговый пакет',
            'partnerPackage' => 'Партнерский пакет',
        ];
    }
}