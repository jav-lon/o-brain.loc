<?php

namespace frontend\models\package;

use Yii;

/**
 * This is the model class for table "package_theme".
 *
 * @property int $id
 * @property string $name
 * @property string $package_name
 */
class PackageTheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_theme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            ['package_name', 'required'],
            [['package_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getThemes($package_name = null) {
        return self::find()->select(['name'])
            ->andFilterWhere(['package_name' => $package_name])
            ->indexBy('id')
            ->column();
    }
}
