<?php

namespace frontend\models\package;

use frontend\models\User;
use Yii;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $type
 * @property resource $public_users
 * @property int theme
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'type', 'public_users'], 'required'],
            [['type'], 'string'],
            [['name', 'url'], 'string', 'max' => 255],
            [['url'], 'url'],
            ['theme', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'Ссылка',
            'type' => 'Тип пакета',
            'public_users' => 'Публичный пользователи',
            'theme' => 'Тематика'
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->public_users = serialize($this->public_users);

        return true;
    }

    public function getPackage(){
        return $this->type == 'franchise' ? 'Франчайзинговый пакет' : 'Партнерский пакет';
    }

    public function getUsers(){
        $userIDs = unserialize($this->public_users);
        $usernames = User::findAll($userIDs);

        return $usernames;
    }

    public function getThemeName() {
        return $this->hasOne(PackageTheme::class, ['id' => 'theme'])->select('name')->scalar();
    }

    public function saveAll() {
        $newTheme = null;
        if (!is_numeric($this->theme)) {
            $newTheme = new PackageTheme();
            $newTheme->name = $this->theme;
            $newTheme->package_name = $this->type;
            if(!$newTheme->save()) {
                $this->addError('theme', $newTheme->getFirstErrors());

                return false;
            }

            Yii::$app->session->addFlash('success', '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Добавлено новая тематика: <strong>' . $newTheme->name . '</strong>');
        }

        $this->theme = is_numeric($this->theme) ? $this->theme : $newTheme->getPrimaryKey();
        if (!$this->save(false)) {
            return false;
        }

        return true;
    }
}
