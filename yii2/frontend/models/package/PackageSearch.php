<?php

namespace frontend\models\package;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\package\Package;

/**
 * PackageSearch represents the model behind the search form of `frontend\models\package\Package`.
 */
class PackageSearch extends Package
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'url', 'type', 'theme'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Package::find();

        //dobavim sortirovka
        $query->orderBy([
            'type' => SORT_ASC,
            'theme' => SORT_ASC,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'type', $this->type]);
            //->andFilterWhere(['like', 'public_users', $this->public_users]);

        $query->andFilterWhere([
            'theme' => $this->theme
        ]);

        return $dataProvider;
    }
}
