<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property string $currency
 */
class Product extends \yii\db\ActiveRecord implements \dvizh\cart\interfaces\CartElement
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'currency'], 'required'],
            [['price'], 'integer'],
            [['name', 'currency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'price' => 'Цена',
            'currency' => 'Currency',
        ];
    }

    public function getCartId() {
        return $this->id;
    }

    public function getCartName() {
        return $this->name;
    }

    public function getCartPrice() {
        return $this->price;
    }

    public function getCartOptions()
    {
        return [];
    }
}
