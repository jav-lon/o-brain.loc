<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "stage".
 *
 * @property int $id
 * @property int $id_user kto sozdal stupen
 * @property string $title
 */
class Stage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stage';
    }

    public function getLessons() {

        return $this->hasMany(Lesson::class, ['id_stage' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'title'], 'required'],
            [['id_user'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
        ];
    }

    /**
     * Vse dostupnie stupeni
     *
     * @return Stage
     */
    public static function getPublicStages() {
        $stage_ids = self::find()->select(['id'])->asArray()->all();
        $public_ids = null;
        foreach ($stage_ids as $stage_id) {
            $is_public = Exercise::find()->where(['id_stage' => $stage_id, 'is_public' => '1'])->count();
            if ( $is_public ) {
                $public_ids[] = $stage_id;
            }
        }

        if ( $public_ids ) {
            $stages = self::find()->where(['id' => $public_ids]);

            return $stages;
        }

        return null;
    }
}
