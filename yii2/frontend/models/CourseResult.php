<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_results".
 *
 * @property int $id
 * @property int $id_user tolko odin polzovatel, odin zapis
 * @property int $id_current_stage
 * @property int $id_current_lesson
 * @property int $id_current_exercise
 */
class CourseResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'id_current_stage', 'id_current_lesson', 'id_current_exercise'], 'integer'],
            [['id_user'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_current_stage' => 'Id Current Stage',
            'id_current_lesson' => 'Id Current Lesson',
            'id_current_exercise' => 'Id Current Exercise',
        ];
    }
}
