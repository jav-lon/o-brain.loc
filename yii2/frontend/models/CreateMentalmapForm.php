<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 19.08.2018
 * Time: 16:36
 */

namespace frontend\models;

use frontend\models\olympiad\OlympiadExercise;
use Yii;
use yii\base\Model;
use yii\db\Exception;

class CreateMentalmapForm extends Model {
    const SCENARIO_UPDATE = 'update';

    /* Mentalmap table */
    public $name;
    public $rows;
    public $quantity;
    public $public_users;
    public $id_examples;

    /* Smarty_examples table */
    /*public $example;
    public $answer;*/
    public $examples;

    /**
     * @var Smarty
     */
    public $_mentalmap;

    /**
     * @var MentalmapExample
     */
    public $_mentalmap_examples;

    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario[self::SCENARIO_UPDATE] = ['name', 'quantity', 'rows', 'public_users', 'examples'];

        return $scenario;
    }

    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['name'], 'string'],
            [['name'], 'required'],
            [['quantity'], 'integer', 'min' => 1, 'max' => 40],
            [['quantity'], 'required'],
            [['rows'], 'integer', 'min' => 2, 'max' => 20],
            [['rows'], 'required'],

            [['public_users'], 'required'],

            [['examples'], 'required'],
            [['examples'], 'validatorExamples', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @param string $attribute атрибут проверяемый в настоящее время
     * @param array $params дополнительные пары имя-значение, заданное в правиле
     */
    public function validatorExamples($attribute, $params) {
        /*if (!is_array($attribute)) {
            $this->addError($attribute, "Поля неправильно заполнен!");
        }*/
    }

    public function findMentalmap($id) {
        $this->_mentalmap = Mentalmap::findOne($id);
        $this->name = $this->_mentalmap->name;
        $this->rows = $this->_mentalmap->rows;
        $this->quantity = $this->_mentalmap->quantity;
        $this->public_users = $this->_mentalmap->public_users;

        //primeri
        $this->_mentalmap_examples = MentalmapExample::find()->select(['example', 'answer'])->where(['id_mentalmap' => $id])->asArray()->all();
        foreach ($this->_mentalmap_examples as $id => $mentalmap_example) {
            $this->id_examples[] = $id;
            $this->_mentalmap_examples[$id]['example'] = explode('|', $mentalmap_example['example']);
        }
        $this->examples = $this->_mentalmap_examples;
    }

    public function updateMentalmap() {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->_mentalmap->name = $this->name;
            $this->_mentalmap->rows = $this->rows;
            $this->_mentalmap->quantity = $this->quantity;
            $this->_mentalmap->public_users = $this->public_users;
            if (!$this->_mentalmap->save()) {
                throw new Exception('Error! Not save db', $this->_mentalmap->errors);
            }

            //prejde chem udalim vse primeri
            if (!MentalmapExample::deleteAll(['id_mentalmap' => $this->_mentalmap->id])) {
                throw new Exception('Error! Not deleted db');
            }

            foreach ($this->examples as $mentalmap_example) {
                $newMentalmapExample = new MentalmapExample();
                $newMentalmapExample->id_mentalmap = $this->_mentalmap->id;
                $newMentalmapExample->example = implode('|', (array)$mentalmap_example['example']);
                $newMentalmapExample->answer = $mentalmap_example['answer'];
                if (!$newMentalmapExample->save()) {
                    throw new Exception('Error! Not saved MentalmapExample', $newMentalmapExample->errors);
                }
            }

            $transaction->commit();
            Yii::$app->session->setFlash('success', 'Mentalmap обновлен!');
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Error! Примеры Mentalmap не сохранен!');
            return false;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Error! Примеры Mentalmap не сохранен!');
            return false;
        }
    }

    public function saveMentalmap($type, $id) {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $mentalmap = new Mentalmap();
            if ($type == 'olympiad') {
                $mentalmap->type = $type;
            }
            $mentalmap->id_user = Yii::$app->session->get('login_user');
            $mentalmap->name = $this->name;
            $mentalmap->rows = $this->rows;
            $mentalmap->quantity = $this->quantity;
            $mentalmap->public_users = $this->public_users;
            if (!$mentalmap->save()) {

                Yii::$app->session->setFlash('error', 'Error! Примеры Mentalmap не сохранен!');
                throw new Exception('Mentalmap examples ne soxraneno!', $mentalmap->errors);
            }

            /*==sozdanie zadanie dlya olimpiadu==*/
            if ($type == 'olympiad') {
                $newOlympiadExercise = new OlympiadExercise();
                $newOlympiadExercise->id_smarty = $mentalmap->id;
                $newOlympiadExercise->id_olympiad = $id;
                if (!$newOlympiadExercise->save()) {
                    Yii::$app->session->setFlash('error', 'Error! Zadanie olimpiadi не сохранен!');
                    throw new Exception('Zadanie olimpiadi не сохранен!', $newOlympiadExercise->errors);
                }
            }

            //Yii::debug($smarty->getPrimaryKey());
            foreach ($this->examples as $ex) {
                $example = implode('|', $ex['example']);
                $answer = $ex['answer'];
                $mentalmap_examples = new MentalmapExample();
                $mentalmap_examples->id_mentalmap = $mentalmap->id;
                $mentalmap_examples->example = $example;
                $mentalmap_examples->answer = $answer;
                if (!$mentalmap_examples->save()) {
                    Yii::$app->session->setFlash('error', 'Error! Примеры Mentalmap не сохранен!');
                    throw new Exception('Mentalmap examples ne soxraneno!', $mentalmap_examples->errors);
                }
            }

            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
            return false;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return false;
        }

        return true;

    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'quantity' => 'Число примеров',
            'rows' => 'Количество чисел в примере',
            'public_users' => 'Публичный пользователи',
            'examples' => 'Все примеры',
        ];
    }


}