<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "olimp_exercise".
 *
 * @property int $id
 * @property int $id_olimp
 * @property int $id_user Kto sozdal
 * @property int $id_stage
 * @property int $id_lesson svyazano s id_stage
 * @property string $description opisanie zadanie
 * @property string $work_type
 * @property int $id_simulators
 * @property int $id_settings svyazano s id_simulators
 * @property int $exercise_number
 */
class OlimpExercise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_exercise';
    }

    /*svyaz*/
    public function getStatus() {
        return $this->hasOne(OlimpExerciseStatus::className(), ['id_exercise' => 'id']);
    }

    public function getSimulatorName(){
        return $this->hasOne(Simulators::className(), ['id' => 'id_simulators'])->select('name')->scalar();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_olimp', 'id_user', 'id_stage', 'id_lesson', 'id_simulators', 'id_settings', 'exercise_number'], 'required'],
            [['id_olimp', 'id_user', 'id_stage', 'id_lesson', 'id_simulators', 'id_settings', 'exercise_number'], 'integer'],
            [['description', 'work_type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_olimp' => 'Id Olimp',
            'id_user' => 'Id User',
            'id_stage' => 'Ступень',
            'id_lesson' => 'Урок',
            'description' => 'Описание',
            'work_type' => 'Тип работы',
            'id_simulators' => 'Тренажер',
            'id_settings' => 'Настройки тренажеров',
            'exercise_number' => 'Exercise Number',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $maxExerciseNumber = self::find()->select('exercise_number')->where(['id_olimp' => $this->id_olimp, 'id_lesson' => $this->id_lesson])->max('exercise_number');
        if ($maxExerciseNumber != null) {
            $this->exercise_number = ++$maxExerciseNumber;
        }
        return true;
    }

    public static function getOlimpExercise ( $id_olimp, $id_stage, $id_lesson ) {

        return self::find()->where(['id_olimp' => $id_olimp, 'id_stage' => $id_stage, 'id_lesson' => $id_lesson]);
    }

    public static function getCurrentExerciseId($id_olimp, $id_stage, $id_lesson, $current_exercise_number) {

        return self::getOlimpExercise($id_olimp, $id_stage, $id_lesson)->andWhere(['exercise_number' => $current_exercise_number])->select(['id'])->scalar();
    }
}
