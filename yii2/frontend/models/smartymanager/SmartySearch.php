<?php

namespace frontend\models\smartymanager;

use frontend\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Smarty;

/**
 * SmartySearch represents the model behind the search form of `frontend\models\Smarty`.
 */
class SmartySearch extends Smarty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rows', 'quantity'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Smarty::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $user_type = User::find()->select('type')->where(['id' => Yii::$app->session->get('login_user')])->scalar();
        $query->andWhere(['type' => 'simulator']); //tolko trenajornie Smarty
        if ( $user_type == 'teacher' ) {
            // teacher smarty
            $query->andWhere([
                'id_user' => Yii::$app->session->get('login_user')
            ]);
        }

        // user jadvali bilan birga
        $query->with('user');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rows' => $this->rows,
            'quantity' => $this->quantity,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
