<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "simulators_results".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_simulator
 * @property int $time milliseconds
 * @property int $count_errors
 * @property int $true_answers
 * @property double $rating
 * @property int $created_at
 * @property int $ratingStar
 * @property int $id_smarty
 * @property int $qa
 */
class SimulatorsResults extends ActiveRecord
{
    public $_ratingStar;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'simulators_results';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getSimulator(){
        return $this->hasOne(Simulators::class, ['id' => 'id_simulator']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

    public function getSmarty() {
        return $this->hasOne(Smarty::class, ['id' => 'id_smarty']);
    }

    public function getRatingStar() {
        switch ( $this->count_errors ) {
            case 0: $this->_ratingStar = 5; break;
            case 1: $this->_ratingStar = 4; break;
            case 2: $this->_ratingStar = 3; break;
            case 3: $this->_ratingStar = 2; break;
            default: $this->_ratingStar = 1;
        }

        return $this->_ratingStar;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_simulator', 'time', 'count_errors', 'true_answers', 'id_smarty'], 'required'],
            [['id_user', 'id_simulator', 'time', 'count_errors', 'true_answers', 'id_smarty'], 'integer'],
            [['created_at', 'qa'], 'safe'],
            [['rating'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_simulator' => 'Id Simulator',
            'time' => 'Time',
            'count_errors' => 'Errors',
            'true_answers' => 'True Answers',
            'rating' => 'Rating',
            'created_at' => 'Created At',
        ];
    }
}
