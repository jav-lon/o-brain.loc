<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 24.08.2018
 * Time: 11:21
 */

namespace frontend\models;

use Yii;
use yii\base\Model;

class CreateNewOlimpForm extends Model {
    public $name;

    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['name'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
        ];
    }

    public function saveOlimp() {
        $olimp = new Olimp();
        $olimp->name = $this->name;
        $olimp->id_user = Yii::$app->session->get('login_user');
        if (!$olimp->validate()) {
            $this->addError('name', $olimp->getErrors('name'));
            return false;
        }
        $olimp->save(false);
        return true;
    }
}