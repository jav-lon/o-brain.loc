<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "smarty".
 *
 * @property int $id
 * @property string $name
 * @property int $rows
 * @property int $quantity
 * @property array $examples
 * @property int $id_user
 * @property string $type
 * @property string $public_users
 */
class Mentalmap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mentalmap';
    }

    public function getExamples() {
        return $this->hasMany(MentalmapExample::class, ['id_mentalmap' => 'id']);
    }

    public function getUser() {
        return $this->hasOne(User::class, ['id' => 'id_user'])
            ->select(['fio', 'id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'rows', 'quantity', 'id_user'], 'required'],
            [['rows', 'quantity', 'id_user'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['type', 'string'],
        ];
    }

    public function getUsers(){
        Yii::debug('public_users', $this->public_users);
        $userIDs = unserialize($this->public_users);
        $usernames = User::findAll($userIDs);

        return $usernames;
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->public_users = serialize($this->public_users);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'rows' => 'Количество чисел в примере',
            'quantity' => 'Число примеров',
            'public_users' => 'Публичный пользователи',
        ];
    }
}
