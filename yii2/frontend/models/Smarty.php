<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "smarty".
 *
 * @property int $id
 * @property string $name
 * @property int $rows
 * @property int $quantity
 * @property array $examples
 * @property int $id_user
 * @property string $type
 */
class Smarty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'smarty';
    }

    public function getExamples() {
        return $this->hasMany(SmartyExample::class, ['id_smarty' => 'id']);
    }

    public function getUser() {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'rows', 'quantity', 'id_user'], 'required'],
            [['rows', 'quantity', 'id_user'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['type', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'rows' => 'Количество чисел в примере',
            'quantity' => 'Число примеров',
        ];
    }
}
