<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_job
 * @property string $level
 * @property string $rows
 * @property int $operation
 * @property int $voice
 * @property int $rules
 * @property int $delay
 * @property string $id_rules
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_job'], 'required'],
            [['id_user', 'id_job', 'operation', 'voice', 'rules', 'delay'], 'integer'],
            [['level', 'rows', 'id_rules'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_job' => 'Id Job',
            'level' => 'Level',
            'rows' => 'Rows',
            'operation' => 'Operation',
            'voice' => 'Voice',
            'rules' => 'Rules',
            'delay' => 'Delay',
            'id_rules' => 'Id Rules',
        ];
    }

    public function initAttributes()
    {
        $str_rules = ''; $str_levels = ''; $str_rows = '';
        $str_rules = implode(';', (array)$this->id_rules); $str_rules .= $str_rules != '' ? ';' : '';
        $str_levels = implode(';', (array)$this->level); $str_levels .= $str_levels != '' ? ';' : '';
        $str_rows = implode(';', (array)$this->rows); $str_rows .= $str_rows != '' ? ';' : '';
        if ( $this->id_job == 1 || $this->id_job == 6 || $this->id_job == 7 ) {
            $this->level = $str_levels;
            $this->rows = '0';
            $this->operation = 0;
            $this->rules = 0;
            $this->delay = 0;
            $this->id_rules = 'no_used';
        } elseif ( $this->id_job == 2) {
            $this->level = $str_levels;
            $this->rows = '0';
            $this->operation = 0;
            $this->rules = 0;
            $this->voice = 0;
            $this->id_rules = 'no_used';
        } elseif ( $this->id_job == 3) {
            $this->level = $str_levels;
            $this->rows = $str_rows;
            $this->operation = 0;
            $this->delay = 0;
            $this->id_rules = $str_rules;
        } elseif ( $this->id_job == 4 || $this->id_job == 5 ) {
            $this->level = $str_levels;
            $this->rows = '2;';
            $this->operation = 0;
            $this->delay = 0;
            $this->id_rules = $str_rules;
        }  elseif ( $this->id_job == 8 ) {
            $this->level = $str_levels;
            $this->rows = $str_rows;
            $this->id_rules = $str_rules;
        } elseif ( $this->id_job == 9) {
            $this->id_rules = $str_rules;
        }
    }
}
