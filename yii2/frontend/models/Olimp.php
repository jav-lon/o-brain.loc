<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "olimp".
 *
 * @property int $id
 * @property string $name
 * @property int $is_public
 * @property int $is_end
 * @property int $id_user
 */
class Olimp extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['is_public', 'is_end'], 'string', 'max' => 1],
            [['is_public', 'is_end'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'is_public' => 'Публичный',
            'is_end' => 'Состояние',
            'created_at' => 'Дата создание',
        ];
    }

    //vse stupeni, sortirovan uvilecheniem
    public function getStages(){
        return $this->hasMany(OlimpStage::class, ['id_olimp' => 'id'])->orderBy('stage_number ASC');
    }

    public static function hasOlimp($id) {
        if ( self::findOne(['id' => $id, 'id_user' => Yii::$app->session->get('login_user')]) !== null ) {
            return true;
        }

        return false;
    }
}
