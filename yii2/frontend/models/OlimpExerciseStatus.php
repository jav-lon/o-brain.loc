<?php

namespace frontend\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "olimp_exercise_status".
 *
 * @property int $id
 * @property int $id_olimp
 * @property int $id_user
 * @property int $id_exercise
 * @property int $is_end
 * @property int $is_public
 * @property int $start_count
 * @property int $id_stage
 * @property int $id_lesson
 */
class OlimpExerciseStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_exercise_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_olimp', 'id_user', 'id_exercise', 'start_count', 'id_stage', 'id_lesson'], 'required'],
            [['id_olimp', 'id_user', 'id_exercise', 'start_count', 'id_stage', 'id_lesson'], 'integer'],
            [['is_end', 'is_public'], 'in', 'range' => [0,1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_olimp' => 'Id Olimp',
            'id_user' => 'Id User',
            'id_exercise' => 'Id Exercise',
            'is_end' => 'Is End',
            'is_public' => 'Is Public',
            'start_count' => 'Start Count',
            'id_stage' => 'Id Stage',
            'id_lesson' => 'Id Lesson',
        ];
    }

    public static function getUserExerciseStatus($id_olimp, $id_stage, $id_lesson) {

        return self::find()->where(['id_olimp' => $id_olimp, 'id_stage' => $id_stage, 'id_lesson' => $id_lesson, 'id_user' => Yii::$app->session->get('login_user')]);
    }

    public static function initUserExerciseStatus ( $id_olimp, $id_stage, $id_lesson ){
        $olimpExerciseIds = OlimpExercise::getOlimpExercise($id_olimp, $id_stage, $id_lesson)->select(['id'])->column();
        $allExerciseStatusIds = self::getUserExerciseStatus($id_olimp, $id_stage, $id_lesson)->select(['id_exercise'])->column();

        if ( $allExerciseStatusIds == null ) {
            foreach ( $olimpExerciseIds as $olimpExerciseId ) {
                $newExerciseStatus = new self();
                $newExerciseStatus->id_olimp = $id_olimp;
                $newExerciseStatus->id_user = Yii::$app->session->get('login_user');
                $newExerciseStatus->id_stage = $id_stage;
                $newExerciseStatus->id_lesson = $id_lesson;
                $newExerciseStatus->id_exercise = $olimpExerciseId;
                $newExerciseStatus->start_count = 0;
                if (!$newExerciseStatus->save()) {
                    throw new Exception('Ошибка в сохранени OlimpExerciseStatus', $newExerciseStatus->errors);
                }
            }
        } else {
            foreach ( $olimpExerciseIds as $olimpExerciseId) {
                if (  !in_array($olimpExerciseId, $allExerciseStatusIds) ) {
                    $newExerciseStatus = new self();
                    $newExerciseStatus->id_olimp = $id_olimp;
                    $newExerciseStatus->id_user = Yii::$app->session->get('login_user');
                    $newExerciseStatus->id_stage = $id_stage;
                    $newExerciseStatus->id_lesson = $id_lesson;
                    $newExerciseStatus->id_exercise = $olimpExerciseId;
                    $newExerciseStatus->start_count = 0;
                    if (!$newExerciseStatus->save()) {
                        throw new Exception('Ошибка в сохранени OlimpExerciseStatus', $newExerciseStatus->errors);
                    }
                }
            }
        }
    }

    public static function setCurrentExerciseStatusAllow ( $id_olimp, $id_stage, $id_lesson ) {
        $currentExerciseNumber = OlimpUserProperties::getCurrentExerciseNumber($id_olimp);
        $currentExerciseId = OlimpExercise::getCurrentExerciseId($id_olimp, $id_stage, $id_lesson, $currentExerciseNumber);
        $currentExercise = self::getUserExerciseStatus($id_olimp, $id_stage, $id_lesson)->andWhere(['id_exercise' => $currentExerciseId])->one();
        if ($currentExercise) {
            $currentExercise->is_public = '1';
            if ( !$currentExercise->save() ) {
                throw new Exception('Ошибка в сохранени OlimpExerciseStatus', $currentExercise->errors);
            }
        }
    }
}
