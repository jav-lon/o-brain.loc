<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 25.08.2018
 * Time: 1:08
 */

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * Class OlimpCreateExerciseForm
 * @package frontend\models
 *
 * @property string $stage_name
 * @property string $lesson_name
 * @property string $lesson_plan
 * @property string $exercise_name
 */

class OlimpCreateExerciseForm extends Model {
    public $id_olimp;
    public $id_stage;
    public $id_lesson;
    public $description;
    public $work_type;
    public $id_simulators;
    public $settingsValues;

    public function rules()
    {
        return [
            [['description'], 'trim' ],
            [['id_olimp', 'id_stage', 'id_lesson', 'work_type', 'id_simulators', 'settingsValues'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_stage' => 'Ступень',
            'id_lesson' => 'Урок',
            'plan' => 'План',
            'description' => 'Описание урока',
            'work_type' => 'Тип работы',
            'id_simulators' => 'Тренажер',
            'settingsValues' => 'Настройки тренажеров',
        ];
    }

    public function saveExercise() {
        $newExercise = new OlimpExercise();
        $newSettings = new Setting();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            Yii::debug($newExercise->id_simulators, 'id_job');
            $newSettings->attributes = $this->settingsValues;
            $newSettings->id_job = $this->id_simulators;
            $newSettings->id_user = Yii::$app->session->get('login_user');
            $newSettings->initAttributes();

            $isValid = $newSettings->save();

            $newExercise->id_olimp = $this->id_olimp;
            $newExercise->id_user = Yii::$app->session->get('login_user');
            $newExercise->id_stage = $this->id_stage;
            $newExercise->id_lesson = $this->id_lesson;
            $newExercise->exercise_number = 1;
            $newExercise->description = $this->description;
            $newExercise->work_type = $this->work_type;
            $newExercise->id_simulators = $this->id_simulators;
            $newExercise->id_settings = $newSettings->id;

            $isValid = $isValid && $newExercise->save();

            if ( !$isValid ) {
                throw new Exception("tranzaksii ne srabotal");
            }

            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->addErrors($newExercise->errors);
            Yii::debug($newSettings->errors, 'newSettings_errors');
            return false;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->addErrors($newExercise->errors);
            Yii::debug($newSettings->errors, 'newSettings_errors');
            return false;
        }
    }
}