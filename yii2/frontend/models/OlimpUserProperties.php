<?php

namespace frontend\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "olimp_user_properties".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_olimp
 * @property int $id_current_stage
 * @property int $id_current_lesson
 * @property int $id_current_exercise
 * @property int $number_current_stage
 * @property int $number_current_lesson
 * @property int $number_current_exercise
 * @property string $type_current_exercise
 * @property int $is_end
 */
class OlimpUserProperties extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_user_properties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_olimp'], 'required'],
            [['id_user', 'id_olimp', 'id_current_stage', 'id_current_lesson', 'id_current_exercise'], 'integer'],
            [['type_current_exercise'], 'string', 'max' => 20],
            [['is_end'], 'in', 'range' => [0, 1]],
            [['id_current_stage', 'id_current_lesson', 'id_current_exercise'], 'default', 'value' => 0],
            [['number_current_stage', 'number_current_lesson', 'number_current_exercise'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_olimp' => 'Id Olimp',
            'number_current_stage' => 'Number Current Stage',
            'number_current_lesson' => 'Number Current Lesson',
            'number_current_exercise' => 'Number Current Exercise',
            'type_current_exercise' => 'Type Current Exercise',
            'is_end' => 'Is End',
        ];
    }

    public static function initUserProperties($id_olimp, $id_stage = null, $id_lesson = null) {

        $userProperties = self::getUserProperties($id_olimp)->one();
        $idCurrentStage = OlimpStage::getCurrentStageId($id_olimp, 1);
        $idCurrentLesson = OlimpLesson::getCurrentLessonId($id_olimp, $id_stage, 1);
        $idCurrentExercise = OlimpExercise::getCurrentExerciseId($id_olimp, $id_stage, $id_lesson, 1);
        if ( $userProperties == null ) {

            $newUserProperties = new self();
            $newUserProperties->id_user = Yii::$app->session->get('login_user');
            $newUserProperties->id_olimp = $id_olimp;
            $newUserProperties->id_current_stage = (int)$idCurrentStage;

            if (!$newUserProperties->save()) {
                throw new Exception('Ошибка в сохранении', $newUserProperties->errors);
            }
            return;
        }
        if ($userProperties->id_current_stage == 0) {
            $userProperties->id_current_stage = (int)$idCurrentStage;
        }
        if ($userProperties->id_current_lesson == 0) {
            $userProperties->id_current_lesson = (int)$idCurrentLesson;
        }
        if ($userProperties->id_current_exercise == 0) {
            $userProperties->id_current_exercise = (int)$idCurrentExercise;
        }
        if (!$userProperties->save()) {
            throw new Exception('Ошибка в валидации', $userProperties->errors);
        }
    }

    public static function getUserProperties($id_olimp) {

        return self::find()->where(['id_user' => Yii::$app->session->get('login_user'), 'id_olimp' => $id_olimp ]);
    }

    public static function getCurrentStageNumber($id_olimp) {

        return self::getUserProperties($id_olimp)->select(['number_current_stage'])->scalar();
    }

    public static function getCurrentLessonNumber($id_olimp) {

        return self::getUserProperties($id_olimp)->select(['number_current_lesson'])->scalar();
    }

    public static function getCurrentExerciseNumber($id_olimp) {

        return self::getUserProperties($id_olimp)->select(['number_current_exercise'])->scalar();
    }

    public static function initNextExercise($id_olimp, $id_exercise){
        $userProperties = self::getUserProperties($id_olimp)->one();

        /* tekushiy zadaniy sdelayem zavershenniy i ustanovim schotchik */
        $currentExerciseStatus = OlimpExerciseStatus::getUserExerciseStatus($id_olimp, $userProperties->id_current_stage, $userProperties->id_current_lesson)->andWhere(['id_exercise' => $id_exercise])->one();
        $currentExerciseStatus->is_end = 1;
        $currentExerciseStatus->start_count = 1;
        if ( !$currentExerciseStatus->save() ) {
            throw new Exception('Ошибка в сохранени', $currentExerciseStatus->errors);
        }

        $exerciseNumbers = OlimpExercise::getOlimpExercise($id_olimp, $userProperties->id_current_stage, $userProperties->id_current_lesson)->select('exercise_number')->asArray()->column();
        if (count($exerciseNumbers) > $userProperties->number_current_exercise) {
            $userProperties->number_current_exercise += 1;

            $nextExerciseId = OlimpExercise::getOlimpExercise($id_olimp, $userProperties->id_current_stage, $userProperties->id_current_lesson)->select('id')->andWhere(['exercise_number' => $userProperties->number_current_exercise])->scalar();
            $nextOlimpExerciseStatus = OlimpExerciseStatus::getUserExerciseStatus($id_olimp, $userProperties->id_current_stage, $userProperties->id_current_lesson)->andWhere(['id_exercise' => $nextExerciseId])->one();
            if ( $nextOlimpExerciseStatus ) { // esli est zadani
                $nextOlimpExerciseStatus->is_public = 1;
                if ( !$nextOlimpExerciseStatus->save() ) {
                    throw new Exception('Ошибка в сохранени', $nextOlimpExerciseStatus->errors);
                }
            }

            $userProperties->id_current_exercise = (int)$nextExerciseId;
        } else {
            $userProperties->number_current_exercise  = 1;

            $currentLessonStatus = OlimpLessonStatus::getUserLessonStatus($id_olimp, $userProperties->id_current_stage)->andWhere(['id_lesson' => $userProperties->id_current_lesson])->one();
            $currentLessonStatus->is_end = 1;
            if ( !$currentLessonStatus->save() ) {
                throw new Exception('Oshibka v soxraneni', $currentLessonStatus->errors);
            }

            $lessonNumbers = OlimpLesson::getOlimpLesson($id_olimp, $userProperties->id_current_stage)->select('lesson_number')->asArray()->column();
            if ( count($lessonNumbers) > $userProperties->number_current_lesson ) {
                $userProperties->number_current_lesson += 1;

                $nextLessonId = OlimpLesson::getOlimpLesson($id_olimp, $userProperties->id_current_stage)->select('id')->andWhere(['lesson_number' => $userProperties->number_current_lesson])->scalar();
                $nextOlimpLessonStatus = OlimpLessonStatus::getUserLessonStatus($id_olimp, $userProperties->id_current_stage)->andWhere(['id_lesson' => $nextLessonId])->one();
                if ( $nextOlimpLessonStatus ) { // esli est uroki v stupeni
                    $nextOlimpLessonStatus->is_public = 1;
                    if ( !$nextOlimpLessonStatus->save() ) {
                        throw new Exception('Ошибка в сохранени', $nextOlimpLessonStatus->errors);
                    }
                }

                $userProperties->id_current_lesson = (int)$nextLessonId;

                $nextExerciseId = OlimpExercise::getOlimpExercise($id_olimp, $userProperties->id_current_stage, $nextLessonId)->select('id')->andWhere(['exercise_number' => $userProperties->number_current_exercise])->scalar();
                $nextOlimpExerciseStatus = OlimpExerciseStatus::getUserExerciseStatus($id_olimp, $userProperties->id_current_stage, $userProperties->id_current_lesson)->andWhere(['id_exercise' => $nextExerciseId])->one();
                if ( $nextOlimpExerciseStatus ) { // esli est v uroke zadani
                    $nextOlimpExerciseStatus->is_public = 1;
                    if ( !$nextOlimpExerciseStatus->save() ) {
                        throw new Exception('Ошибка в сохранени', $nextOlimpExerciseStatus->errors);
                    }
                }

                $userProperties->id_current_exercise = (int) $nextExerciseId;
            } else {
                $userProperties->number_current_lesson = 1;

                $currentStageStatus = OlimpStageStatus::getUserStageStatus($id_olimp)->andWhere(['id_stage' => $userProperties->id_current_stage])->one();
                $currentStageStatus->is_end = 1;
                if ( !$currentStageStatus->save() ) {
                    throw new Exception('oshika va soxraneni', $currentStageStatus->errors);
                }

                $stageNumbers = OlimpStage::getOlimpStage($id_olimp)->select('stage_number')->asArray()->column();
                if ( count($stageNumbers) > $userProperties->number_current_exercise ) {
                    $userProperties->number_current_stage += 1;

                    $nextStageId = OlimpStage::getOlimpStage($id_olimp)->select('id')->andWhere(['stage_number' => $userProperties->number_current_stage])->scalar();
                    $nextOlimpStageStatus = OlimpStageStatus::getUserStageStatus($id_olimp)->andWhere(['id_stage' => $nextStageId])->one();
                    if ( $nextOlimpStageStatus ) { // esli est stupeni
                        $nextOlimpStageStatus->is_public = 1;
                        if ( !$nextOlimpStageStatus->save() ) {
                            throw new Exception('Ошибка в сохранени', $nextOlimpStageStatus->errors);
                        }
                    }

                    $userProperties->id_current_stage = (int) $nextStageId;

                    $nextLessonId = OlimpLesson::getOlimpLesson($id_olimp, $userProperties->id_current_stage)->select('id')->andWhere(['lesson_number' => $userProperties->number_current_lesson])->scalar();
                    $nextOlimpLessonStatus = OlimpLessonStatus::getUserLessonStatus($id_olimp, $userProperties->id_current_stage)->andWhere(['id_lesson' => $nextLessonId])->one();
                    if ( $nextOlimpLessonStatus ) { //esli est uroki
                        $nextOlimpLessonStatus->is_public = 1;
                        if ( !$nextOlimpLessonStatus->save() ) {
                            throw new Exception('Ошибка в сохранени', $nextOlimpLessonStatus->errors);
                        }
                    }

                    $userProperties->id_current_lesson = (int) $nextLessonId;

                    $nextExerciseId = OlimpExercise::getOlimpExercise($id_olimp, $userProperties->id_current_stage, $nextLessonId)->select('id')->andWhere(['exercise_number' => $userProperties->number_current_exercise])->scalar();
                    $nextOlimpExerciseStatus = OlimpExerciseStatus::getUserExerciseStatus($id_olimp, $userProperties->id_current_stage, $nextLessonId)->andWhere(['id_exercise' => $nextExerciseId])->one();
                    if ( $nextOlimpExerciseStatus ) {
                        $nextOlimpExerciseStatus->is_public = 1;
                        if ( !$nextOlimpExerciseStatus->save() ) {
                            throw new Exception('Ошибка в сохранени', $nextOlimpExerciseStatus->errors);
                        }
                    }

                    $userProperties->id_current_exercise = (int) $nextExerciseId;

                } else {
                    $userProperties->number_current_stage = 1;
                    $userProperties->is_end = 1;
                }
            }
        }

        if (!$userProperties->save()) {
            throw new Exception('Ошибка в сохранени', $userProperties->errors);
        }

    }

}
