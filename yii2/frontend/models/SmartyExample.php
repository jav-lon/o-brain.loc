<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "smarty_examples".
 *
 * @property int $id
 * @property int $id_smarty
 * @property string $example
 * @property int $answer
 */
class SmartyExample extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'smarty_examples';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_smarty', 'example', 'answer'], 'required'],
            [['id_smarty', 'answer'], 'integer'],
            [['example'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_smarty' => 'Id Smarty',
            'example' => 'Example',
            'answer' => 'Answer',
        ];
    }
}
