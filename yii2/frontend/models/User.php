<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $id_teacher
 * @property string $id_who_learn кто обучает, кто партнер
 * @property string $type
 * @property string $fio
 * @property string $date_rojd партнер
 * @property int $age
 * @property string $city
 * @property string $addr партнер
 * @property string $addr_reserv партнер
 * @property string $email
 * @property string $email_reserv партнер
 * @property string $phone
 * @property string $phone_reserv партнер
 * @property string $link
 * @property string $password
 * @property string $data_reg
 * @property string $locked
 * @property string $num_dogovor партнер
 * @property string $date_dogovor партнер
 * @property string $royalty партнер
 * @property string $date_royalty партнер
 * @property string $comment_royalty партнер
 * @property string $fio_tmp
 * @property string $email_tmp
 * @property string $phone_tmp
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_who_learn', 'type', 'fio', 'age', 'city', 'email', 'phone', 'password', 'data_reg'], 'required'],
            [['age'], 'integer'],
            [['id_teacher', 'id_who_learn'], 'string', 'max' => 10],
            [['type'], 'string', 'max' => 15],
            [['fio', 'date_rojd', 'city', 'addr', 'addr_reserv', 'email', 'email_reserv', 'link', 'password', 'data_reg', 'num_dogovor', 'date_dogovor', 'royalty', 'date_royalty', 'fio_tmp', 'email_tmp'], 'string', 'max' => 100],
            [['phone', 'phone_reserv', 'phone_tmp'], 'string', 'max' => 25],
            [['locked'], 'string', 'max' => 3],
            [['comment_royalty'], 'string', 'max' => 150],
        ];
    }

    public function getStudents($teacher_id) {
        return self::find()->where(['id_who_learn' => $teacher_id])->all();
    }

    public function getTeachers($partner_id) {
        return self::find()->where(['id_who_learn' => $partner_id])->all();
    }

    public function getSmartyResults() {
        return $this->hasMany(SimulatorsResults::class, ['id_user' => 'id'])
            ->andWhere(['id_simulator' => 9]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_teacher' => 'Id Teacher',
            'id_who_learn' => 'Id Who Learn',
            'type' => 'Type',
            'fio' => 'Fio',
            'date_rojd' => 'Date Rojd',
            'age' => 'Age',
            'city' => 'City',
            'addr' => 'Addr',
            'addr_reserv' => 'Addr Reserv',
            'email' => 'Email',
            'email_reserv' => 'Email Reserv',
            'phone' => 'Phone',
            'phone_reserv' => 'Phone Reserv',
            'link' => 'Link',
            'password' => 'Password',
            'data_reg' => 'Data Reg',
            'locked' => 'Locked',
            'num_dogovor' => 'Num Dogovor',
            'date_dogovor' => 'Date Dogovor',
            'royalty' => 'Royalty',
            'date_royalty' => 'Date Royalty',
            'comment_royalty' => 'Comment Royalty',
            'fio_tmp' => 'Fio Tmp',
            'email_tmp' => 'Email Tmp',
            'phone_tmp' => 'Phone Tmp',
        ];
    }
}
