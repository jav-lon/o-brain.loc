<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "royalty_report".
 *
 * @property int $id
 * @property int $id_partner
 * @property int $payment_schedule
 * @property string $actual_payment_date
 * @property string $actual_payment_amount
 * @property int $is_debt
 */
class RoyaltyReport extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'royalty_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_partner', 'payment_schedule'], 'required'],
            [['id_partner', 'payment_schedule'], 'integer'],
            [['actual_payment_date', 'actual_payment_amount'], 'string', 'max' => 255],
            [['is_debt'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_partner' => 'Id Partner',
            'payment_schedule' => 'График оплаты',
            'actual_payment_date' => 'Фактическая дата оплаты',
            'actual_payment_amount' => 'Фактическая   оплаты',
            'is_debt' => 'Долг',
        ];
    }
}
