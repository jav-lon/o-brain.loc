<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "exercises".
 *
 * @property int $id
 * @property int $id_user Kto sozdal
 * @property int $id_stage
 * @property int $id_lesson svyazano s id_stage
 * @property string $description opisanie zadanie
 * @property string $work_type
 * @property int $id_simulators
 * @property int $id_settings svyazano s id_simulators, nujno utochnit
 * @property int $is_draft chernovik
 * @property int $is_public svyazano s is_draft
 */
class Exercise extends \yii\db\ActiveRecord
{
    const SCENARIO_ADMIN = 'admin';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercises';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADMIN] = ['id_user', 'id_stage', 'id_lesson', 'id_simulators', 'id_settings', 'is_draft', 'is_public', 'description', 'work_type'];

        return parent::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_stage', 'id_lesson', 'id_simulators', 'id_settings', 'is_draft', 'is_public'], 'required'],
            [['id_user', 'id_stage', 'id_lesson', 'id_simulators', 'id_settings'], 'integer'],
            [['description', 'work_type'], 'string'],
            [['is_draft', 'is_public'], 'string', 'max' => 1],

            ['description', 'default', 'on' => self::SCENARIO_ADMIN],
            ['description', 'required', 'on' => self::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_stage' => 'Ступень',
            'id_lesson' => 'Урок',
            'description' => 'Описание задании',
            'work_type' => 'Тип работы',
            'id_simulators' => 'Тренажер',
            'simulators_settings' => 'Настройки тренажеров',
            'is_public' => 'Сделат общедоступной',
        ];
    }
}
