<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "exercise_result".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_exercise
 * @property int $id_simulators
 * @property double $time
 * @property int $errors
 * @property double $rating
 * @property string $created_at
 * @property string $updated_at
 * @property int $ratingStar
 */
class ExerciseResult extends \yii\db\ActiveRecord
{
    public $_ratingStar;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise_result';
    }

    public function getRatingStar() {
        switch ( $this->errors ) {
            case 0: $this->_ratingStar = 5; break;
            case 1: $this->_ratingStar = 4; break;
            case 2: $this->_ratingStar = 3; break;
            case 3: $this->_ratingStar = 2; break;
            default: $this->_ratingStar = 1;
        }

        return $this->_ratingStar;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    // если вместо метки времени UNIX используется datetime:
                    // 'value' => new Expression('NOW()'),
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_exercise', 'id_simulators', 'time', 'errors', 'rating'], 'required'],
            [['id_user', 'id_exercise', 'id_simulators', 'errors'], 'integer'],
            [['time', 'rating'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_exercise' => 'Id Exercise',
            'id_simulators' => 'Id Simulators',
            'time' => 'Time',
            'errors' => 'Errors',
            'rating' => 'Rating',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
