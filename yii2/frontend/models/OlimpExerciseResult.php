<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "olimp_exercise_result".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_exercise
 * @property int $id_simulators
 * @property int $time
 * @property int $errors
 * @property double $rating
 * @property int $created_at
 * @property int $qa
 */
class OlimpExerciseResult extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_exercise_result';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']
                    // если вместо метки времени UNIX используется datetime:
                    // 'value' => new Expression('NOW()'),
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_exercise', 'id_simulators', 'time', 'errors', 'rating', 'qa'], 'required'],
            [['id_user', 'id_exercise', 'id_simulators', 'time', 'errors' ], 'integer'],
            [['rating'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_exercise' => 'Id Exercise',
            'id_simulators' => 'Id Simulators',
            'time' => 'Time',
            'errors' => 'Errors',
            'rating' => 'Rating',
            'created_at' => 'Created At',
        ];
    }

    public function getSimulator() {
        return $this->hasOne(Simulators::class, ['id' => 'id_simulators']);
    }

    public function getRatingStar() {
        $star = 1;

        switch ($this->errors) {
            case 0: $star = 5; break;
            case 1: $star = 4; break;
            case 2: $star = 3; break;
            case 3: $star = 2; break;
            default: $star = 1;
        }

        return $star;
    }
}
