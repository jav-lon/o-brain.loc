<?php

namespace frontend\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "olimp_stage_status".
 *
 * @property int $id
 * @property int $id_olimp
 * @property int $id_user
 * @property int $id_stage
 * @property int $is_end
 * @property int $is_public
 */
class OlimpStageStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_stage_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_olimp', 'id_user', 'id_stage'], 'required'],
            [['id_olimp', 'id_user', 'id_stage'], 'integer'],
            [['is_end', 'is_public'], 'in', 'range' => [0, 1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_olimp' => 'Id Olimp',
            'id_user' => 'Id User',
            'id_stage' => 'Id Stage',
            'is_end' => 'Is End',
            'is_public' => 'Is Public',
        ];
    }

    public static function initUserStageStatus($id_olimp) {
        $olimpStageIds = OlimpStage::getOlimpStage($id_olimp)->select(['id'])->column();
        $allStageStatusIds = self::getUserStageStatus($id_olimp)->select(['id_stage'])->column();

        if ($allStageStatusIds == null) {
            foreach ( $olimpStageIds as $olimpStageId ) {
                $newStageStatus = new self();
                $newStageStatus->id_olimp = $id_olimp;
                $newStageStatus->id_user = Yii::$app->session->get('login_user');
                $newStageStatus->id_stage = $olimpStageId;
                if (!$newStageStatus->save()) {
                    throw new Exception('Ошибка в сохранени OlimpStageStatus', $newStageStatus->errors);
                }
            }
        } else {
            foreach ( $olimpStageIds as $olimpStageId) {
                if (  !in_array($olimpStageId, $allStageStatusIds) ) {
                    $newStageStatus = new self();
                    $newStageStatus->id_olimp = $id_olimp;
                    $newStageStatus->id_user = Yii::$app->session->get('login_user');
                    $newStageStatus->id_stage = $olimpStageId;
                    if (!$newStageStatus->save()) {
                        throw new Exception('Ошибка в сохранени OlimpStageStatus', $newStageStatus->errors);
                    }
                }
            }
        }
    }

    public static function getUserStageStatus($id_olimp){

        return self::find()->where(['id_olimp' => $id_olimp, 'id_user' => Yii::$app->session->get('login_user')]);
    }

    public static function setCurrentStageStatusAllow($id_olimp) {
        $currentStageNumber = OlimpUserProperties::getCurrentStageNumber($id_olimp);
        $currentStageId = OlimpStage::getCurrentStageId($id_olimp, $currentStageNumber);
        $currentStage = self::getUserStageStatus($id_olimp)->andWhere(['id_stage' => $currentStageId])->one();
        if ($currentStage) {
            $currentStage->is_public = '1';
            if ( !$currentStage->save() ) {
                throw new Exception('Ошибка в сохранени OlimpStageStatus', $currentStage->errors);
            }
        }
    }
}
