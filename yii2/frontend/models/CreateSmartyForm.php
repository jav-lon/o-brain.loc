<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 19.08.2018
 * Time: 16:36
 */

namespace frontend\models;

use frontend\models\olympiad\OlympiadExercise;
use Yii;
use yii\base\Model;
use yii\db\Exception;

class CreateSmartyForm extends Model {
    /* Smarty table */
    public $name;
    public $rows;
    public $quantity;
    public $id_examples;

    /* Smarty_examples table */
    /*public $example;
    public $answer;*/
    public $examples;

    /**
     * @var Smarty
     */
    public $_smarty;

    /**
     * @var SmartyExample
     */
    public $_smarty_examples;

    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['name'], 'string'],
            [['name'], 'required'],
            [['quantity'], 'integer', 'min' => 1, 'max' => 80],
            [['quantity'], 'required'],
            [['rows'], 'integer', 'min' => 2, 'max' => 20],
            [['rows'], 'required'],

            [['examples'], 'required'],
            [['examples'], 'validatorExamples', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @param string $attribute атрибут проверяемый в настоящее время
     * @param array $params дополнительные пары имя-значение, заданное в правиле
     */
    public function validatorExamples($attribute, $params) {
        /*if (!is_array($attribute)) {
            $this->addError($attribute, "Поля неправильно заполнен!");
        }*/
    }

    public function findSmarty($id) {
        $this->_smarty = Smarty::findOne($id);
        $this->name = $this->_smarty->name;
        $this->rows = $this->_smarty->rows;
        $this->quantity = $this->_smarty->quantity;

        //primeri
        $this->_smarty_examples = SmartyExample::find()->select(['example', 'answer'])->where(['id_smarty' => $id])->asArray()->all();
        foreach ($this->_smarty_examples as $id => $smarty_example) {
            $this->id_examples[] = $id;
            $this->_smarty_examples[$id]['example'] = explode('|', $smarty_example['example']);
        }
        $this->examples = $this->_smarty_examples;
    }

    public function updateSmarty() {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->_smarty->name = $this->name;
            $this->_smarty->rows = $this->rows;
            $this->_smarty->quantity = $this->quantity;
            if (!$this->_smarty->save()) {
                throw new Exception('Error! Not save db', $this->_smarty->errors);
            }

            //prejde chem udalim vse primeri
            if (!SmartyExample::deleteAll(['id_smarty' => $this->_smarty->id])) {
                throw new Exception('Error! Not deleted db');
            }

            foreach ($this->examples as $smarty_example) {
                $newSmartyExample = new SmartyExample();
                $newSmartyExample->id_smarty = $this->_smarty->id;
                $newSmartyExample->example = implode('|', (array)$smarty_example['example']);
                $newSmartyExample->answer = $smarty_example['answer'];
                if (!$newSmartyExample->save()) {
                    throw new Exception('Error! Not saved SmartyExample', $newSmartyExample->errors);
                }
            }

            $transaction->commit();
            Yii::$app->session->setFlash('success', 'Smarty обновлен!');
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Error! Примеры Smarty не сохранен!');
            return false;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Error! Примеры Smarty не сохранен!');
            return false;
        }
    }

    public function saveSmarty($type, $id) {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $smarty = new Smarty();
            if ($type == 'olympiad') {
                $smarty->type = $type;
            }
            $smarty->id_user = Yii::$app->session->get('login_user');
            $smarty->name = $this->name;
            $smarty->rows = $this->rows;
            $smarty->quantity = $this->quantity;
            if (!$smarty->save()) {

                Yii::$app->session->setFlash('error', 'Error! Примеры Smarty не сохранен!');
                throw new Exception('smarty examples ne soxraneno!', $smarty->errors);
            }

            /*==sozdanie zadanie dlya olimpiadu==*/
            if ($type == 'olympiad') {
                $newOlympiadExercise = new OlympiadExercise();
                $newOlympiadExercise->id_smarty = $smarty->id;
                $newOlympiadExercise->id_olympiad = $id;
                if (!$newOlympiadExercise->save()) {
                    Yii::$app->session->setFlash('error', 'Error! Zadanie olimpiadi не сохранен!');
                    throw new Exception('Zadanie olimpiadi не сохранен!', $newOlympiadExercise->errors);
                }
            }

            //Yii::debug($smarty->getPrimaryKey());
            foreach ($this->examples as $ex) {
                $example = implode('|', $ex['example']);
                $answer = $ex['answer'];
                $smarty_examples = new SmartyExample();
                $smarty_examples->id_smarty = $smarty->id;
                $smarty_examples->example = $example;
                $smarty_examples->answer = $answer;
                if (!$smarty_examples->save()) {
                    Yii::$app->session->setFlash('error', 'Error! Примеры Smarty не сохранен!');
                    throw new Exception('smarty examples ne soxraneno!', $smarty_examples->errors);
                }
            }

            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
            return false;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return false;
        }

        return true;

    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'quantity' => 'Число примеров',
            'rows' => 'Количество чисел в примере',

            'examples' => 'Все примеры',
        ];
    }


}