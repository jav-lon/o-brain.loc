<?php

namespace frontend\models\olympiad;

use frontend\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\olympiad\Olympiad;

/**
 * OlympiadSearch represents the model behind the search form of `frontend\models\olympiad\Olympiad`.
 */
class OlympiadSearch extends Olympiad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'date', 'created_at'], 'integer'],
            [['name', 'description', 'is_public', 'public_users_ids'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = User::findOne(Yii::$app->session->get('login_user'));

        $query = Olympiad::find();

        if ($user->type == 'teacher') {
            $query->where(['id_user' => $user->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date' => $this->date,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'is_public', $this->is_public])
            ->andFilterWhere(['like', 'public_users_ids', $this->public_users_ids]);

        return $dataProvider;
    }
}
