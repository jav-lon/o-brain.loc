<?php

namespace frontend\models\olympiad;

use frontend\models\Smarty;
use Yii;

/**
 * This is the model class for table "olympiad_exercise".
 *
 * @property int $id
 * @property int $id_olympiad
 * @property int $id_smarty
 */
class OlympiadExercise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olympiad_exercise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_olympiad', 'id_smarty'], 'required'],
            [['id_olympiad', 'id_smarty'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_olympiad' => 'Id Olympiad',
            'id_smarty' => 'Id Smarty',
        ];
    }

    public function getSmarty() {
        return $this->hasOne(Smarty::className(), ['id' => 'id_smarty']);
    }

    public function getResult($id_user) {
        return $this->hasOne(OlympiadResult::className(), ['id_exercise' => 'id'])->where(['id_user' => $id_user]);
    }
}
