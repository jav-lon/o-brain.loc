<?php

namespace frontend\models\olympiad;

use frontend\models\User;
use Yii;

/**
 * This is the model class for table "olympiad_result".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_olympiad
 * @property int $id_exercise
 * @property int $time
 * @property resource $qa
 * @property int $count_true_answers
 * @property int $count_errors
 */
class OlympiadResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olympiad_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_olympiad', 'id_exercise', 'time', 'qa', 'count_true_answers', 'count_errors'], 'required'],
            [['id_user', 'id_olympiad', 'id_exercise', 'time', 'count_true_answers', 'count_errors'], 'integer'],
            [['qa'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_olympiad' => 'Id Olympiad',
            'id_exercise' => 'Id Exercise',
            'time' => 'Time',
            'qa' => 'Qa',
            'count_true_answers' => 'Count True Answers',
            'count_errors' => 'Count Errors',
        ];
    }

    public function getStudent() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getRatingStar() {
        $star = 1;
        switch ($this->count_errors) {
            case 0: $star = 5; break;
            case 1: $star = 4; break;
            case 2: $star = 3; break;
            case 3: $star = 2; break;
            case 4: $star = 1; break;
        }

        return $star;
    }
}
