<?php

namespace frontend\models\olympiad;

use frontend\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "olympiad".
 *
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property string $description
 * @property int $date
 * @property int $is_public
 * @property resource $public_users_ids
 * @property int $created_at
 * @property int $datetime
 */
class Olympiad extends \yii\db\ActiveRecord
{
    const SCENARIO_PUBLISH = 'publish';
    public $tmp_date;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olympiad';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PUBLISH] = ['is_public'];
        return $scenarios;
    }

    public function getExercises(){
        return $this->hasMany(OlympiadExercise::class, ['id_olympiad' => 'id']);
    }

    public function getAuthor(){
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

    public function getResults(){
        return $this->hasMany(OlympiadResult::class, ['id_olympiad' => 'id']);
    }

    public function getUsers(){
        $userIDs = unserialize($this->public_users_ids);
        $usernames = User::findAll($userIDs);

        return $usernames;
    }

    public function getCountMembers() {
        $userIDs = unserialize($this->public_users_ids);
        return User::find()->where(['id' => $userIDs])->count();
    }

    /**
     * @return array
     */
    public function getUsersID() {
        return unserialize($this->public_users_ids);
    }

    public static function isActive($id_olympiad) {
        $date = self::find()->where(['id' => $id_olympiad])->select('date')->scalar();

        if ($date == mktime(0,0,0)) {
            return true;
        }

        return false;
    }

    public static function isPublic($id_olympiad, $id_user) {
        $olympiad = self::findOne($id_olympiad);

        if ($olympiad !== null && in_array($id_user, unserialize($olympiad->public_users_ids))) {
            return true;
        }

        return false;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['tmp_date'], 'required'],
            [['tmp_date'], 'date', 'format' => 'php: d.m.Y'],
            [['id_user', 'name', 'description', 'is_public', 'public_users_ids'], 'required'],
            [['id_user', 'created_at'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['is_public'], 'default', 'value' => 0],
            [['is_public'], 'in', 'range' => [0,1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'name' => 'Название',
            'description' => 'Описание (правила)',
            'datetime' => 'Дата проведения',
            'date' => 'Дата проведения',
            'tmp_date' => 'Дата проведения',
            'is_public' => 'Is Public',
            'public_users_ids' => 'Выбрать пользователей',
            'created_at' => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        /*==string date to timestamp==*/
        if ($this->isAttributeActive('tmp_date')) {
            $this->date = strtotime($this->tmp_date);
        }

        if ($this->isNewRecord || !$this->isAttributeChanged('is_public')) {
            $this->public_users_ids = serialize($this->public_users_ids);
        }

        return true;
    }


}
