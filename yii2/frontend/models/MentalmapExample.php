<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "smarty_examples".
 *
 * @property int $id
 * @property int $id_mentalmap
 * @property string $example
 * @property int $answer
 */
class MentalmapExample extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mentalmap_examples';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mentalmap', 'example', 'answer'], 'required'],
            [['id_mentalmap', 'answer'], 'integer'],
            [['example'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_smarty' => 'Id Smarty',
            'example' => 'Example',
            'answer' => 'Answer',
        ];
    }
}
