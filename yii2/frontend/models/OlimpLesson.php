<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "olimp_lesson".
 *
 * @property int $id
 * @property int $id_olimp
 * @property int $id_user kto sozdal urok
 * @property int $id_stage
 * @property string $name nazvanie uroka
 * @property string $plan
 * @property int $lesson_number
 */
class OlimpLesson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_lesson';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_olimp', 'id_user', 'id_stage', 'name', 'plan', 'lesson_number'], 'required'],
            [['id_olimp', 'id_user', 'id_stage', 'lesson_number'], 'integer'],
            [['plan'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_olimp' => 'Id Olimp',
            'id_user' => 'Id User',
            'id_stage' => 'Ступен',
            'name' => 'Название',
            'plan' => 'План',
            'lesson_number' => 'Lesson Number',
        ];
    }

    public function getExercises(){
        return $this->hasMany(OlimpExercise::class, ['id_lesson' => 'id'])->orderBy('exercise_number ASC');
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $maxLessonNumber = self::find()->select('lesson_number')->where(['id_olimp' => $this->id_olimp, 'id_stage' => $this->id_stage])->max('lesson_number');
        if ($maxLessonNumber != null) {
            $this->lesson_number = ++$maxLessonNumber;
        }
        return true;
    }

    public static function getOlimpLesson($id_olimp, $id_stage) {

        return self::find()->where(['id_olimp' => $id_olimp, 'id_stage' => $id_stage]);
    }

    public static function getCurrentLessonId($id_olimp, $id_stage, $current_lesson_number) {

        return self::getOlimpLesson($id_olimp, $id_stage)->andWhere(['lesson_number' => $current_lesson_number])->select(['id'])->scalar();
    }
}
