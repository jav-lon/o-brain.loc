<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "olimp_stage".
 *
 * @property int $id
 * @property int $id_olimp
 * @property int $id_user kto sozdal stupen
 * @property string $name
 * @property int $stage_number
 */
class OlimpStage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'olimp_stage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_olimp', 'id_user', 'name', 'stage_number'], 'required'],
            [['id_olimp', 'id_user', 'stage_number'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_olimp' => 'Id Olimp',
            'id_user' => 'Id User',
            'name' => 'Название',
            'stage_number' => 'Stage Number',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $maxStageNumber = self::find()->select('stage_number')->where(['id_olimp' => $this->id_olimp])->max('stage_number');
        Yii::debug($maxStageNumber, 'maxStageNumber');
        if ($maxStageNumber != null) {
            $this->stage_number = ++$maxStageNumber;
        }
        return true;
    }

    public function getLessons() {
        return $this->hasMany(OlimpLesson::class, ['id_stage' => 'id'])->orderBy('lesson_number ASC');
    }

    /**
     * @param $id_olimp
     * @return ActiveQuery
     */
    public static function getOlimpStage($id_olimp) {
        return self::find()->where(['id_olimp' => $id_olimp]);
    }

    public static function getCurrentStageId($id_olimp, $current_stage_number) {
        return self::getOlimpStage($id_olimp)->andWhere(['stage_number' => $current_stage_number])->select(['id'])->scalar();
    }
}
