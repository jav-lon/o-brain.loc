<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "privacy_policy_agreement".
 *
 * @property int $id
 * @property string $title_ppa
 * @property string $text_ppa
 */
class PrivacyPolicyAgreement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'privacy_policy_agreement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ppa', 'text_ppa'], 'required'],
            [['text_ppa'], 'string'],
            [['title_ppa'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ppa' => 'Тема',
            'text_ppa' => 'Текст',
        ];
    }
}
