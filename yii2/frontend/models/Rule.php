<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rules".
 *
 * @property int $id
 * @property string $name
 * @property string $descr
 * @property string $link_video
 */
class Rule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'descr', 'link_video'], 'required'],
            [['name', 'descr'], 'string', 'max' => 50],
            [['link_video'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'descr' => 'Descr',
            'link_video' => 'Link Video',
        ];
    }
}
