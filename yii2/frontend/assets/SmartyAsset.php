<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * dlya trenajora Smarty.
 */
class SmartyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/simulator/smarty.css',
    ];
    public $js = [
        'js/lib/howler.min.js',
        'js/jtlib.js',
        'js/smarty.js',
    ];
    public $depends = [
        JqueryAsset::class,
    ];
}
