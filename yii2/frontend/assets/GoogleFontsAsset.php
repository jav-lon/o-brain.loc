<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class GoogleFontsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'https://fonts.googleapis.com/css?family=Neucha&amp;subset=cyrillic',
        //'https://fonts.googleapis.com/css?family=Pangolin&amp;subset=cyrillic', // norm
        //'https://fonts.googleapis.com/css?family=Amatic+SC&amp;subset=cyrillic', // boladi
        //'https://fonts.googleapis.com/css?family=Bad+Script&amp;subset=cyrillic',
        //'https://fonts.googleapis.com/css?family=Underdog&amp;subset=cyrillic',
        //'https://fonts.googleapis.com/css?family=Comfortaa:400,700&amp;subset=cyrillic',
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=cyrillic', //cool
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
