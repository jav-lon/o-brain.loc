<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 02.11.2018
 * Time: 15:25
 */

namespace frontend\assets;


use yii\jui\JuiAsset;
use yii\web\AssetBundle;

class MentalMapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/abacus-open/mentalmap.css',
    ];
    public $js = [
        'js/lib/howler.min.js',
        'js/abacus-open/mentalmap.js',
        'js/jtlib.js',
        'css/my_js.js'
    ];
    public $depends = [
        MaterializeCssAsset::class,
        JuiAsset::class,
    ];
}