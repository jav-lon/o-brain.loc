<?php

namespace frontend\assets;

use yii\bootstrap\BootstrapPluginAsset;
use yii\jui\JuiAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Main frontend application asset bundle.
 */
class CreateExerciseAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        '/js/settings.js',
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapPluginAsset::class,
        JuiAsset::class,
    ];
}
