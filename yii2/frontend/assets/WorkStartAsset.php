<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 15.08.2018
 * Time: 16:31
 */

namespace frontend\assets;


use yii\bootstrap\BootstrapPluginAsset;
use yii\jui\JuiAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class WorkStartAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/ie.css',
        'css/my_css.css',
    ];
    public $js = [
        'js/work.js',
        'css/howler/howler.js',
    ];
    public $depends = [
        YiiAsset::class,
        JuiAsset::class,
        BootstrapPluginAsset::class,
    ];
}