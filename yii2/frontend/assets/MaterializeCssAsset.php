<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MaterializeCssAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "https://fonts.googleapis.com/icon?family=Material+Icons",
        'https://fonts.googleapis.com/css?family=Neucha&amp;subset=cyrillic',
        'css/materialize/sass/materialize.css',
    ];
    public $js = [
        'js/materialize/js/bin/materialize.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
