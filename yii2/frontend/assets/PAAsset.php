<?php
/**
 * Created by PhpStorm.
 * User: Javlonbek
 * Date: 28.10.2018
 * Time: 0:22
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class PAAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/pa/dashboard.css',
        'css/pa/sidebar.css',
    ];
    public $js = [
        'js/pa/sidebar.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}