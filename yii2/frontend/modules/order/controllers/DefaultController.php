<?php
namespace dvizh\order\controllers;

use frontend\components\filters\AccessAdminPartnerFilter;
use yii;
use yii\web\Controller;
use yii\filters\AccessControl;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
//                        'roles' => $this->module->adminRoles,
                    ]
                ]
            ],*/
            'access-admin-partner' => [
                'class' => AccessAdminPartnerFilter::class,
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
