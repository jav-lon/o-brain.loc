<?php

use yii\db\Migration;

/**
 * Handles the creation of table `smarty_examples`.
 */
class m180819_111314_create_smarty_examples_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('smarty_examples', [
            'id' => $this->primaryKey(),
            'id_smarty' => $this->integer()->notNull(),
            'example' => $this->string()->notNull(),
            'answer' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('smarty_examples');
    }
}
