<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_results`.
 */
class m180811_152657_create_course_results_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('course_results', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->unique()->notNull()->comment('tolko odin polzovatel, odin zapis'),
            'id_current_stage' => $this->integer()->null(),
            'id_current_lesson' => $this->integer()->null(),
            'id_current_exercise' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('course_results');
    }
}
