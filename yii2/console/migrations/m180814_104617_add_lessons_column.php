<?php

use yii\db\Migration;

/**
 * Class m180814_104617_add_lessons_column
 */
class m180814_104617_add_lessons_column extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('lessons', 'is_public', $this->boolean()->defaultValue('0'));
    }

    public function down()
    {
        $this->dropColumn('lessons', 'is_public');
    }

}
