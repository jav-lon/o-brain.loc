<?php

use yii\db\Migration;

/**
 * Handles the creation of table `privacy_policy_agreement`.
 */
class m190218_191010_create_privacy_policy_agreement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('privacy_policy_agreement', [
            'id' => $this->primaryKey(),
            'title_ppa' => $this->string()->notNull(),
            'text_ppa' => $this->text()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('privacy_policy_agreement');
    }
}
