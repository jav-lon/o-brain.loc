<?php

use yii\db\Migration;

/**
 * Class m180813_102712_add_lessons_column
 */
class m180813_102712_add_lessons_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('lessons', 'is_draft', $this->boolean()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('lessons', 'is_draft');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180813_102712_add_lessons_column cannot be reverted.\n";

        return false;
    }
    */
}
