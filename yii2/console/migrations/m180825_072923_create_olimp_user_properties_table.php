<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_user_properties`.
 */
class m180825_072923_create_olimp_user_properties_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_user_properties', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_olimp' => $this->integer()->notNull(),
            'number_current_stage' => $this->integer()->defaultValue(1)->notNull(),
            'number_current_lesson' => $this->integer()->defaultValue(1)->notNull(),
            'number_current_exercise' => $this->integer()->defaultValue(1)->notNull(),
            'type_current_exercise' => $this->string(20)->defaultValue('classwork')->notNull(),
            'is_end' => $this->boolean()->defaultValue(0)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_user_properties');
    }
}
