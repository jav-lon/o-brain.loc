<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_exercise_status`.
 */
class m180825_065921_create_olimp_exercise_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_exercise_status', [
            'id' => $this->primaryKey(),
            'id_olimp' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'id_exercise' => $this->integer()->notNull(),
            'is_end' => $this->boolean()->defaultValue(0)->notNull(),
            'is_public' => $this->boolean()->defaultValue(0)->notNull(),
            'start_count' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_exercise_status');
    }
}
