<?php

use yii\db\Migration;

/**
 * Class m190217_094150_add_count_mentally_column
 */
class m190217_094150_add_count_mentally_column extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('settings', 'count_mentally', $this->boolean()->null());
    }

    public function down()
    {
        $this->dropColumn('settings', 'count_mentally');
    }

}
