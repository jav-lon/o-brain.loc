<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_lesson_status`.
 */
class m180825_065727_create_olimp_lesson_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_lesson_status', [
            'id' => $this->primaryKey(),
            'id_olimp' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'id_lesson' => $this->integer()->notNull(),
            'is_end' => $this->boolean()->defaultValue(0)->notNull(),
            'is_public' => $this->boolean()->defaultValue(0)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_lesson_status');
    }
}
