<?php

use yii\db\Migration;

/**
 * Handles the creation of table `exercise_result`.
 */
class m180817_094817_create_exercise_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('exercise_result', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_exercise' => $this->integer()->notNull(),
            'id_simulators' => $this->integer()->notNull(),
            'time' => $this->integer()->notNull(),
            'errors' => $this->integer()->notNull(),
            'rating' => $this->float()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('exercise_result');
    }
}
