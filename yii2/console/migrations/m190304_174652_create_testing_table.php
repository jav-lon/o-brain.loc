<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%testing}}`.
 */
class m190304_174652_create_testing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%testing}}', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%testing}}');
    }
}
