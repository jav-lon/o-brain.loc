<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olympiad_exercise`.
 */
class m180910_110956_create_olympiad_exercise_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olympiad_exercise', [
            'id' => $this->primaryKey(),
            'id_olympiad' => $this->integer()->notNull(),
            'id_smarty' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olympiad_exercise');
    }
}
