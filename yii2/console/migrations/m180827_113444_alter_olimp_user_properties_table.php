<?php

use yii\db\Migration;

/**
 * Class m180827_113444_alter_olimp_user_properties_table
 */
class m180827_113444_alter_olimp_user_properties_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('olimp_user_properties', 'number_current_stage');
        $this->dropColumn('olimp_user_properties', 'number_current_lesson');
        $this->dropColumn('olimp_user_properties', 'number_current_exercise');

        $this->addColumn('olimp_user_properties', 'id_current_stage', $this->integer()->notNull());
        $this->addColumn('olimp_user_properties', 'id_current_lesson', $this->integer()->notNull());
        $this->addColumn('olimp_user_properties', 'id_current_exercise', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180827_113444_alter_olimp_user_properties_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180827_113444_alter_olimp_user_properties_table cannot be reverted.\n";

        return false;
    }
    */
}
