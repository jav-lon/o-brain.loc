<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mentalmap`.
 */
class m181104_100337_create_mentalmap_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('mentalmap', [
            'id' => $this->primaryKey(),
            'id_user'=> $this->integer()->notNull(),
            'type' => $this->string()->defaultValue('simulator')->notNull(),
            'name' => $this->string()->notNull(),
            'rows' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'public_users' => $this->binary(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('mentalmap');
    }
}
