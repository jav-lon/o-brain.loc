<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olympiad`.
 */
class m180910_110529_create_olympiad_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olympiad', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'date' => $this->integer()->notNull(),
            'is_public' => $this->boolean()->notNull(),
            'public_users_ids' => $this->binary()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olympiad');
    }
}
