<?php

use yii\db\Migration;

/**
 * Class m180825_053259_add_olimp_column
 */
class m180825_053259_add_olimp_column extends Migration
{

    /*public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m180825_053259_add_olimp_column cannot be reverted.\n";

        return false;
    }*/


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('olimp', 'id_user', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('olimp', 'id_user');
    }

}
