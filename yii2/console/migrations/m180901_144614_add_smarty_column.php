<?php

use yii\db\Migration;

/**
 * Class m180901_144614_add_smarty_column
 */
class m180901_144614_add_smarty_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('smarty', 'id_user', $this->integer()->defaultValue(15)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('smarty', 'id_user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180901_144614_add_smarty_column cannot be reverted.\n";

        return false;
    }
    */
}
