<?php

use yii\db\Migration;

/**
 * Class m190301_142653_add_order_column
 */
class m190301_142653_add_order_column extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('order', 'country', $this->string()->notNull());
        $this->addColumn('order', 'city', $this->string()->notNull());
        $this->addColumn('order', 'street', $this->string()->notNull());
        $this->addColumn('order', 'house', $this->string()->null()->comment('дом/строение'));
        $this->addColumn('order', 'office', $this->string()->null()->comment('квартира/офис'));
    }

    public function down()
    {
        $this->dropColumn('order', 'country');
        $this->dropColumn('order', 'city');
        $this->dropColumn('order', 'street');
        $this->dropColumn('order', 'house');
        $this->dropColumn('order', 'office');
    }

}
