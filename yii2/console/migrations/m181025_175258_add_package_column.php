<?php

use yii\db\Migration;

/**
 * Class m181025_175258_add_package_column
 */
class m181025_175258_add_package_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('package', 'theme', $this->integer());
        $this->addColumn('package', 'position', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('package', 'theme');
        $this->dropColumn('package', 'position');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181025_175258_add_package_column cannot be reverted.\n";

        return false;
    }
    */
}
