<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_lesson`.
 */
class m180825_063251_create_olimp_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_lesson', [
            'id' => $this->primaryKey(),
            'id_olimp' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull()->comment('kto sozdal urok'),
            'id_stage' => $this->integer()->notNull(),
            'name' => $this->string()->notNull()->comment('nazvanie uroka'),
            'plan' => $this->text()->notNull(),
            'lesson_number' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_lesson');
    }
}
