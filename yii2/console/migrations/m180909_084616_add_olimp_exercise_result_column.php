<?php

use yii\db\Migration;

/**
 * Class m180909_084616_add_olimp_exercise_result_column
 */
class m180909_084616_add_olimp_exercise_result_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('olimp_exercise_result', 'qa', $this->binary()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('olimp_exercise_result', 'qa');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180909_084616_add_olimp_exercise_result_column cannot be reverted.\n";

        return false;
    }
    */
}
