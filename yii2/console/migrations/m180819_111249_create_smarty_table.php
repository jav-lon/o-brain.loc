<?php

use yii\db\Migration;

/**
 * Handles the creation of table `smarty`.
 */
class m180819_111249_create_smarty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('smarty', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'rows' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('smarty');
    }
}
