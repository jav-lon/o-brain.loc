<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_exercise`.
 */
class m180825_063541_create_olimp_exercise_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_exercise', [
            'id' => $this->primaryKey(),
            'id_olimp' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull()->comment('Kto sozdal'),
            'id_stage' => $this->integer()->notNull(),
            'id_lesson' => $this->integer()->notNull()->comment('svyazano s id_stage'),
            'description' => $this->text()->comment('opisanie zadanie'),
            'work_type' => "ENUM('classwork', 'homework')",
            'id_simulators' => $this->integer()->notNull(),
            'id_settings' => $this->integer()->notNull()->comment('svyazano s id_simulators'),
            'exercise_number' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_exercise');
    }
}
