<?php

use yii\db\Migration;

/**
 * Handles the creation of table `royalty_report`.
 */
class m190203_161028_create_royalty_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('royalty_report', [
            'id' => $this->primaryKey(),
            'id_partner' => $this->integer()->notNull(),
            'payment_schedule' => $this->integer()->notNull(),
            'actual_payment_date' => $this->string(),
            'actual_payment_amount' => $this->string(),
            'is_debt' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('royalty_report');
    }
}
