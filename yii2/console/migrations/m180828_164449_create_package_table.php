<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package`.
 */
class m180828_164449_create_package_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('package', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'type' => "ENUM('franchise', 'partner') NOT NULL",
            'public_users' => $this->binary(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('package');
    }
}
