<?php

use yii\db\Migration;

/**
 * Class m180904_200806_add_simulators_results_column
 */
class m180904_200806_add_simulators_results_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('simulators_results', 'id_smarty', $this->integer()->defaultValue(0)->notNull());
        $this->addColumn('simulators_results', 'qa', $this->binary()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('simulators_results', 'id_smarty');
        $this->dropColumn('simulators_results', 'qa');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180904_200806_add_simulators_results_column cannot be reverted.\n";

        return false;
    }
    */
}
