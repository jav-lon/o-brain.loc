<?php

use yii\db\Migration;

/**
 * Handles adding twinkle to table `{{%settings}}`.
 */
class m190315_193703_add_twinkle_column_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('settings', 'twinkle', $this->integer()->null()->comment('for map jobs'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('settings', 'twinkle');
    }
}
