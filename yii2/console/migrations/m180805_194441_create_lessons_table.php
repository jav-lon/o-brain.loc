<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lessons`.
 */
class m180805_194441_create_lessons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('lessons', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull()->comment('kto sozdal urok'),
            'id_stage' => $this->integer()->notNull(),
            'title' => $this->string()->notNull()->comment('nazvanie uroka'),
            'plan' => $this->text()->comment('plan uroka, adminu mojno propustit'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('lessons');
    }
}
