<?php

use yii\db\Migration;

/**
 * Handles the creation of table `simulators`.
 */
class m180806_103809_create_simulators_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('simulators', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
        ]);

        $this->batchInsert('simulators', ['name'], [
            ['Набор цифр на Абакусе'],
            ['Определение числа с карты Абакус'],
            ['Примеры на сложение / вычитание'],
            ['Примеры на умножение'],
            ['Примеры на деление'],
            ['Bemoreclever'],
            ['Bottomofabac'],
            ['Mentalmap'],
            //['Smarty'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('simulators');
    }
}
