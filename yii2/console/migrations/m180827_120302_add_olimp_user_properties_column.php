<?php

use yii\db\Migration;

/**
 * Class m180827_120302_add_olimp_user_properties_column
 */
class m180827_120302_add_olimp_user_properties_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('olimp_user_properties', 'number_current_stage', $this->integer()->defaultValue(1)->notNull());
        $this->addColumn('olimp_user_properties', 'number_current_lesson', $this->integer()->defaultValue(1)->notNull());
        $this->addColumn('olimp_user_properties', 'number_current_exercise', $this->integer()->defaultValue(1)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('olimp_user_properties', 'number_current_stage');
        $this->dropColumn('olimp_user_properties', 'number_current_lesson');
        $this->dropColumn('olimp_user_properties', 'number_current_exercise');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180827_120302_add_olimp_user_properties_column cannot be reverted.\n";

        return false;
    }
    */
}
