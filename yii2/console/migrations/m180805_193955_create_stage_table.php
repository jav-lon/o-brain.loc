<?php

use yii\db\Migration;

/**
 * Handles the creation of table `stage`.
 */
class m180805_193955_create_stage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('stage', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull()->comment('kto sozdal stupen'),
            'title' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('stage');
    }
}
