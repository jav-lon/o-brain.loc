<?php

use yii\db\Migration;

/**
 * Class m190926_212142_add_column_examples_up_to_x_to_settings_table
 */
class m190926_212142_add_column_examples_up_to_x_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("settings", "examples_up_to_x", $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("settings", "examples_up_to_x");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190926_212142_add_column_examples_up_to_x_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
