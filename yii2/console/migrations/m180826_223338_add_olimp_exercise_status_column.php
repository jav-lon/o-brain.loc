<?php

use yii\db\Migration;

/**
 * Class m180826_223338_add_olimp_exercise_status_column
 */
class m180826_223338_add_olimp_exercise_status_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('olimp_exercise_status', 'id_stage', $this->integer()->notNull());
        $this->addColumn('olimp_exercise_status', 'id_lesson', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('olimp_exercise_status', 'id_stage');
        $this->dropColumn('olimp_exercise_status', 'id_lesson');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180826_223338_add_olimp_exercise_status_column cannot be reverted.\n";

        return false;
    }
    */
}
