<?php

use yii\db\Migration;

/**
 * Class m180911_170208_add_smarty_column
 */
class m180911_170208_add_smarty_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('smarty', 'type', $this->string()->defaultValue('simulator')->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('smarty', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180911_170208_add_smarty_column cannot be reverted.\n";

        return false;
    }
    */
}
