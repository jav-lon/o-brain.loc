<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp`.
 */
class m180824_061154_create_olimp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'is_public' => $this->boolean()->defaultValue(0),
            'is_end' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp');
    }
}
