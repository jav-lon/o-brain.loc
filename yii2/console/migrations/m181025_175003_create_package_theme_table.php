<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package_theme`.
 */
class m181025_175003_create_package_theme_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('package_theme', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'package_name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('package_theme');
    }
}
