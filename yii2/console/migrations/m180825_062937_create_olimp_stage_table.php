<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_stage`.
 */
class m180825_062937_create_olimp_stage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_stage', [
            'id' => $this->primaryKey(),
            'id_olimp'     => $this->integer()->notNull(),
            'id_user'      => $this->integer()->notNull()->comment('kto sozdal stupen'),
            'name'         => $this->string()->notNull(),
            'stage_number' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_stage');
    }
}
