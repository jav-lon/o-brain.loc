<?php

use yii\db\Migration;

/**
 * Class m190310_210817_alter_jobs_table
 */
class m190310_210817_alter_jobs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('jobs', 'rating', $this->float()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190310_210817_alter_jobs_table cannot be reverted.\n";

        return false;
    }
    */
}
