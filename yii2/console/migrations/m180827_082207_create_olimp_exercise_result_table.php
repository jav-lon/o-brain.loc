<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_exercise_result`.
 */
class m180827_082207_create_olimp_exercise_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_exercise_result', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_exercise' => $this->integer()->notNull(),
            'id_simulators' => $this->integer()->notNull(),
            'time' => $this->integer()->notNull(),
            'errors' => $this->integer()->notNull(),
            'rating' => $this->float()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_exercise_result');
    }
}
