<?php

use yii\db\Migration;

/**
 * Class m180822_030930_add_simulators_column
 */
class m180822_030930_add_simulators_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('simulators', ['name' => 'Smarty']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->delete('simulators', ['id' => 9]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180822_030930_add_simulators_column cannot be reverted.\n";

        return false;
    }
    */
}
