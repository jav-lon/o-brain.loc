<?php

use yii\db\Migration;

/**
 * Class m181030_072914_add_settings_columnn
 */
class m181030_072914_add_settings_columnn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('settings', 'voice_lang', $this->string()->defaultValue('ru-RU')->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('settings', 'voice_lang');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181030_072914_add_settings_columnn cannot be reverted.\n";

        return false;
    }
    */
}
