<?php

use yii\db\Migration;

/**
 * Class m180826_200216_add_olimp_lesson_status_column
 */
class m180826_200216_add_olimp_lesson_status_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('olimp_lesson_status', 'id_stage', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('olimp_lesson_status', 'id_stage');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180826_200216_add_olimp_lesson_status_column cannot be reverted.\n";

        return false;
    }
    */
}
