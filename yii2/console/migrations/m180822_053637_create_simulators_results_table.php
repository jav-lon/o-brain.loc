<?php

use yii\db\Migration;

/**
 * Handles the creation of table `simulators_results`.
 */
class m180822_053637_create_simulators_results_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('simulators_results', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_simulator' => $this->integer()->notNull(),
            'time' => $this->integer()->notNull()->comment("milliseconds"),
            'count_errors' => $this->integer()->notNull(),
            'true_answers' => $this->integer()->notNull(),
            'rating' => $this->float()->defaultValue(0),
            'created_at' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('simulators_results');
    }
}
