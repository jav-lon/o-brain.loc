<?php

use yii\db\Migration;

/**
 * Handles the creation of table `exercises`.
 */
class m180808_091907_create_exercises_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('exercises', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull()->comment('Kto sozdal'),
            'id_stage' => $this->integer()->notNull(),
            'id_lesson' => $this->integer()->notNull()->comment('svyazano s id_stage'),
            'description' => $this->text()->comment('opisanie zadanie'),
            'work_type' => "ENUM('classwork', 'homework')",
            'id_simulators' => $this->integer()->notNull(),
            'id_settings' => $this->integer()->notNull()->comment('svyazano s id_simulators'),
            'is_draft' => $this->boolean()->notNull()->comment('chernovik'),
            'is_public' => $this->boolean()->notNull()->comment('svyazano s is_draft'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('exercises');
    }
}
