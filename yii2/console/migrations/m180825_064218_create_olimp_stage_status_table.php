<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olimp_stage_status`.
 */
class m180825_064218_create_olimp_stage_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olimp_stage_status', [
            'id' => $this->primaryKey(),
            'id_olimp' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'id_stage' => $this->integer()->notNull(),
            'is_end' => $this->boolean()->defaultValue(0)->notNull(),
            'is_public' => $this->boolean()->defaultValue(0)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olimp_stage_status');
    }
}
