<?php

use yii\db\Migration;

/**
 * Handles the creation of table `olympiad_result`.
 */
class m180910_111457_create_olympiad_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('olympiad_result', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_olympiad' => $this->integer()->notNull(),
            'id_exercise' => $this->integer()->notNull(),
            'time' => $this->integer()->notNull(),
            'qa' => $this->binary()->notNull(),
            'count_true_answers' => $this->integer()->notNull(),
            'count_errors' => $this->integer()->notNull(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('olympiad_result');
    }
}
