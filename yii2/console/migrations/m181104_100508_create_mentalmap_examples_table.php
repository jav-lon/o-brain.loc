<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mentalmap_examples`.
 */
class m181104_100508_create_mentalmap_examples_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('mentalmap_examples', [
            'id' => $this->primaryKey(),
            'id_mentalmap' => $this->integer()->notNull(),
            'example' => $this->string()->notNull(),
            'answer' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('mentalmap_examples');
    }
}
