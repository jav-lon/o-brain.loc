<h1>Module Generator</h1>

<p>This generator helps you to generate the skeleton code needed by a Yii module.</p>

<?php $form=$this->beginWidget('CCodeForm', array('model'=>$createNewOlimpForm)); ?>

	<div class="row">
		<?php echo $form->labelEx($createNewOlimpForm,'moduleID'); ?>
		<?php echo $form->textField($createNewOlimpForm,'moduleID',array('size'=>65)); ?>
		<div class="tooltip">
			Module ID is case-sensitive. It should only contain word characters.
			The generated module class will be named after the module ID.
			For example, a module ID <code>forum</code> will generate the module class
			<code>ForumModule</code>.
		</div>
		<?php echo $form->error($createNewOlimpForm,'moduleID'); ?>
	</div>

<?php $this->endWidget(); ?>
