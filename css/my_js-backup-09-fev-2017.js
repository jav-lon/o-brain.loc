$(document).ready(function() {

	/* кнапка сброса Абакуса */
	$('#reset_btn').click(function(){
		resetAbacus(0);
	});
	/* сброс Абакуса */
	function resetAbacus(del=600) {
		setTimeout(function () {
			$('.row_up').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top - 45}; });
			$('.row1').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			$('.row2').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			$('.row3').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			$('.row4').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			itogo = 0; $("#itogo").html(itogo);
		}, del); // время в мс
	}
	
	function generatePrimer() {
		rows = parseInt($("#rows").attr('data-val')); 
		primer_summ = 0;  primer_text = '';
		while (rows) { // при i, равном 0, значение в скобках будет false и цикл остановится
			
			part = getRandomInRange(min_in, max_in);
			if( (Math.random() * (2 - 0) + 2) == 1 ) added = true; else added = false;   // operator = Math.random() * (2 - 0) + 2;
			if(added) {
				primer_summ = primer_summ + part;
				primer_text = primer_text + ' + ' + String(part);
			} else {
				if( (primer_summ - part) < 0 ) {
					primer_summ = primer_summ + part; primer_text = primer_text + ' + ' + String(part);
				} else {
					primer_summ = primer_summ - part; primer_text = primer_text + ' - ' + String(part);
				}
			}
			rows--;
		}
		//alert( primer_text.substr(3) + '; ' + primer_summ );
		source_number = primer_summ;
		$("#source_number").html(primer_text.substr(3));
	}
	
	/* начальные установки */
	var errors = 0;
	var count_job = 0;
	var number_job = parseInt($("#number_job").attr('data-val'));
	var time_begin = parseInt(new Date().getTime()/1000);
	var source_number = 0;
	var itogo = 0;   $("#itogo").html(itogo);
	
	/* определим уровень сложности и генерацию числа */
	var level = parseInt($("#level").attr('data-val'));
	var min_in = 0; var max_in = 0;
	if( parseInt($("#level").attr('data-val')) == 1 ) { min_in = 1; max_in = 9; }
	if( parseInt($("#level").attr('data-val')) == 2 ) { min_in = 10; max_in = 99; }
	if( parseInt($("#level").attr('data-val')) == 3 ) { min_in = 100; max_in = 999; }
	if( parseInt($("#level").attr('data-val')) == 4 ) { min_in = 1000; max_in = 9999; }
	function getRandomInRange(min, max) {
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	/* === инициализация для 1 задания === */
	if( number_job == 1 ) {
		/* сгенерим число первоначально */
		source_number = getRandomInRange(min_in, max_in);
		$("#source_number").html(source_number);
		/* кнопка Следующая */
		$('#next_number').click(function(){
			if( parseInt($("#source_number").text()) != parseInt($("#itogo").text()) ) {
				errors = errors + 1; $("#errors").html(errors);
			}
			count_job = count_job + 1;
			if( count_job == 5 ) { 
				var time_end = parseInt(new Date().getTime()/1000); 
				var time_job = time_end - time_begin;
				alert('Задание Закончено! Ошибок: ' + errors + '. Время выполнения: ' + time_job + ' сек.');
				// отправка результата
				id_user = $("#id_user").attr('data-val');
				//$.post("/css/completedJob.php", {id_user: id_user, number_job: number_job, time_job: time_job, errors: errors});
				$("#source_number").html('');
				$("#next_number").attr("style", "display:none");
				$("#repeat_job").attr("style", "display:block");
			} else {
				resetAbacus();
				/* сгенерим следующее число */
				setTimeout(function () {
					source_number = getRandomInRange(min_in, max_in);
					$("#source_number").html(source_number);	//if( count_job == 4 ) $("#next_number").html('Закончить!');
				}, 600);
			}
		});
	}
	
	/* === инициализация для 3 задания === */
	if( number_job == 3 ) {
		/* сгенерим пример первоначально */
		generatePrimer();
		
		/* кнопка Следующая */
		$('#next_number').click(function(){
			if( source_number != itogo ) {
				errors = errors + 1; $("#errors").html(errors);
				$("#source_number").attr("style", "color:#FF6666");
			}
			count_job = count_job + 1;
			if( count_job == 5 ) { 
				var time_end = parseInt(new Date().getTime()/1000); 
				var time_job = time_end - time_begin;
				alert('Задание Закончено! Ошибок: ' + errors + '. Время выполнения: ' + time_job + ' сек.');
				// отправка результата
				id_user = $("#id_user").attr('data-val');
				//$.post("/css/completedJob.php", {id_user: id_user, number_job: number_job, time_job: time_job, errors: errors});
				$("#source_number").html('');
				$("#next_number").attr("style", "display:none");
				$("#repeat_job").attr("style", "display:block");
			} else {
				resetAbacus();
				/* сгенерим следующий пример */
				setTimeout(function () {
					generatePrimer();
					$("#source_number").attr("style", "color:#000000");
				}, 1600);
			}
		});
	}

	$('.bead').click(function(){
		
		var moving = true; // изначально разрешено всем двигаться
		// для нижних четырех строк проверим, есть ли им куда двигаться (вверх \ вниз)
		if ( $(this).hasClass("row1") ) {  
			numCol = $(this).attr('class').substr( $(this).attr('class').indexOf('col'), 4);    //alert (res.toSource());
			if ( $(".row2").filter('.'+numCol).filter('.on').length ) { moving = false; }
		}
		if ( $(this).hasClass("row2") ) {  
			numCol = $(this).attr('class').substr( $(this).attr('class').indexOf('col'), 4);
			if ( !$(".row1").filter('.'+numCol).filter('.on').length || $(".row3").filter('.'+numCol).filter('.on').length ) { moving = false; }
		}
		if ( $(this).hasClass("row3") ) {  
			numCol = $(this).attr('class').substr( $(this).attr('class').indexOf('col'), 4);
			if ( !$(".row2").filter('.'+numCol).filter('.on').length || $(".row4").filter('.'+numCol).filter('.on').length ) { moving = false; }
		}
		if ( $(this).hasClass("row4") ) {  
			numCol = $(this).attr('class').substr( $(this).attr('class').indexOf('col'), 4);
			if ( !$(".row3").filter('.'+numCol).filter('.on').length ) { moving = false; }
		}
		
		if (moving) {
			//var itogo = parseInt($("#itogo").text());
			if ( $(this).hasClass("on") ) { // если косточка включена (в верхней позиции)
				if ( $(this).hasClass("row_up") ) {
					$(this).animate({ top: '-=45', }, 300, function() { /*Закончили анимацию.*/ });
				} else {
					$(this).animate({ top: '+=45', }, 300, function() { /*Закончили анимацию.*/ });
				}
				// вычитаем значение
				itogo = itogo - parseInt($(this).attr('data-val'));
			} else {
				if ( $(this).hasClass("row_up") ) {
					$(this).animate({ top: '+=45', }, 300, function() { /*Закончили анимацию.*/ });
				} else {
					$(this).animate({ top: '-=45', }, 300, function() { /*Закончили анимацию.*/ });
				}
				// прибавляем значение
				itogo = itogo + parseInt($(this).attr('data-val'));
			}
			$(this).toggleClass( "on" );
			$("#itogo").html(itogo);		//console.log(itogo);
			
			/* ================ 1 ЗАДАНИЕ ================= */
			if( number_job == 1 ) {
				/* если при очередном передвижении кости угадали число */
				if( source_number == itogo ) {
					
					count_job = count_job + 1;  $("#count_job").html(count_job);
					if( count_job == 5 ) {
						$("#source_number").attr("style", "color:#8AE595");
						var time_end = parseInt(new Date().getTime()/1000); 
						var time_job = time_end - time_begin;
						alert('Задание Закончено! Ошибок: ' + errors + '. Время выполнения: ' + time_job + ' сек.');
						// отправка результата
						id_user = $("#id_user").attr('data-val');
						//$.post("/css/completedJob.php", {id_user: id_user, number_job: number_job, time_job: time_job, errors: errors});
						$("#source_number").html('');
						$("#next_number").attr("style", "display:none");
						$("#repeat_job").attr("style", "display:block");
					} else {
						resetAbacus();
						setTimeout(function () { $("#source_number").attr("style", "color:#8AE595"); }, 600);
						setTimeout(function () { $("#source_number").attr("style", "color:#000000"); }, 1600);
						/* сгенерим следующее число */
						setTimeout(function () {
							source_number = getRandomInRange(min_in, max_in);
							$("#source_number").html(source_number);	//if( count_job == 4 ) $("#next_number").html('Закончить!');
						}, 1600);
					}
				}
			} /* END 1 задание */
			
			/* ================ 3 ЗАДАНИЕ ================= */
			if( number_job == 3 ) {
				/* если при очередном передвижении кости угадали число */
				if( source_number == itogo ) {
					
					count_job = count_job + 1;  $("#count_job").html(count_job);
					if( count_job == 5 ) { 
						$("#source_number").attr("style", "color:#8AE595");
						var time_end = parseInt(new Date().getTime()/1000); 
						var time_job = time_end - time_begin;
						alert('Задание Закончено! Ошибок: ' + errors + '. Время выполнения: ' + time_job + ' сек.');
						// отправка результата
						id_user = $("#id_user").attr('data-val');
						//$.post("/css/completedJob.php", {id_user: id_user, number_job: number_job, time_job: time_job, errors: errors});
						$("#source_number").html('');
						$("#next_number").attr("style", "display:none");
						$("#repeat_job").attr("style", "display:block");
					} else {
						resetAbacus();
						setTimeout(function () { $("#source_number").attr("style", "color:#8AE595"); }, 600);
						setTimeout(function () { $("#source_number").attr("style", "color:#000000"); }, 1600);
						/* сгенерим следующий пример */
						setTimeout(function () {
							generatePrimer();
						}, 1600);
					}
				}
			} /* END 3 задание */
		}
	});
	
	
	/* ================ 2 ЗАДАНИЕ ================= */
	if( number_job == 2 ) {
	
		/* для создания карты ( 2 задание ) */
		function createMap(source_number, col) {
			if(source_number == 1) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
			if(source_number == 2) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
			if(source_number == 3) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
			if(source_number == 4) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row4').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
			if(source_number == 5) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; });  }
			if(source_number == 6) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
			if(source_number == 7) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
			if(source_number == 8) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
			if(source_number == 9) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row4').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
		}
		function generateMap(source_number, level) {
			if(level == 1) { createMap(source_number, 4); }
			if(level == 2) { source_number = String(source_number); createMap( source_number[1], 4); createMap( source_number[0], 3); }
			if(level == 3) { source_number = String(source_number);	createMap( source_number[2], 4); createMap( source_number[1], 3); createMap( source_number[0], 2); }
			if(level == 4) { source_number = String(source_number);	createMap( source_number[3], 4); createMap( source_number[2], 3); createMap( source_number[1], 2); createMap( source_number[0], 1);	}
		}
		/* покажем и спрячем карту */
		setTimeout(function () {
			$("#abacus").attr("style", "visibility:hidden");
		}, 4400);
		
		/* сгенерим число первоначально */
		var source_number = getRandomInRange(min_in, max_in);
		$("#j2_generated_number").html(source_number); // for test
		/* и первоначально сгенерируем map abacus */
		generateMap(source_number, level);
		
		$('#next_number').click(function(){
		
			// проверка числа из Инпута
			if( source_number != parseInt($("#j2_input_number").val()) ) {
				$("#j2_input_number").attr("style", "color:#FF6666");
				errors = errors + 1;
				$("#errors").html(errors);
			} else $("#j2_input_number").attr("style", "color:#8AE595");
			
			count_job = count_job + 1;  $("#count_job").html(count_job);
			if( count_job == 5 ) {
				var time_end = parseInt(new Date().getTime()/1000); 
				var time_job = time_end - time_begin;
				alert('Задание Закончено! Ошибок: ' + errors + '. Время выполнения: ' + time_job + ' сек.');
				// отправка результата
				id_user = $("#id_user").attr('data-val');
				//$.post("/css/completedJob.php", {id_user: id_user, number_job: number_job, time_job: time_job, errors: errors});
				$("#j2_input_number").attr("style", "display:none");
				$("#next_number").attr("style", "display:none");
				$("#repeat_job").attr("style", "display:block");
			} else {
				setTimeout(function () { 
					$("#j2_input_number").attr("style", "color:#000000"); 
					$("#j2_input_number").val(''); // очистим инпут
				}, 600);
				/* сгенерим следующее число */
				source_number = getRandomInRange(min_in, max_in);
				/* и сгенерируем map abacus */
				resetAbacus();
				setTimeout(function () {
					generateMap(source_number, level);
				}, 620); // время в мс
				$("#abacus").attr("style", "visibility:none");
				setTimeout(function () {
					$("#abacus").attr("style", "visibility:hidden");
				}, 4400);
				$("#j2_generated_number").html(source_number); // for test
			}
			
		});
	}
	
});