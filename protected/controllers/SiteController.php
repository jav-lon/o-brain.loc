<?php

class SiteController extends Controller
{
	public $layout = '//layouts/main';
	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		/* АВТОРИЗАЦИЯ */
		/*$modellogin=new LoginForm; $login_notice = '';
		if(isset($_POST['LoginForm'])) {
				$modellogin->attributes=$_POST['LoginForm'];
				if( ($modellogin->nameadmin != '') && ($modellogin->passadmin != '') ) {
					if( $login_user = UsersAr::model()->findByAttributes(array('email'=>$modellogin->nameadmin,'password'=>$modellogin->passadmin)) ) {
						if($login_user->locked == 'yes') {
							$login_notice = 'Ваш вход в Личный кабинет заблокирован, обратитесь к своему преподавателю';
						} else {
							Yii::app()->session['login_user'] = $login_user->id;
							// log
							$log = new LogAr;
							$log->id_user = $login_user->id; $log->data_auth_in = time(); $log->save();		//$log->time_session = time();
						}
					}
					else $login_notice = 'Неверные логин / пароль';
				} else {
					$login_notice = 'Неверные логин / пароль';
					Yii::app()->session['login_user'] = ''; 
				}
		}
		$this->render('index',array('modellogin'=>$modellogin,'login_notice'=>$login_notice));
		*/
		//$this->render('index');
        $this->redirect('lk/index');
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionLogout()
	{
		Yii::app()->session['login_user'] = '';
		//$this->redirect('index');
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionTest()
	{
		$this->render('test');
	}
}