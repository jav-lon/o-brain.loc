<?php
set_time_limit(350);

class LkController extends Controller
{
	public $layout = '//layouts/lk/main';
	public function actions() {
		return array( 'captcha'=>array('class'=>'NumberCCaptchaAction','backColor'=>0xFFFFFF, 'minLength' => 4, 'maxLength' => 4),'page'=>array('class'=>'CViewAction'));
	}
	public static function goToOwnPage() {
		$login_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if($login_user->type == 'admin' || $login_user->type == 'partner') LkController::redirect('/lk/teachers');
		if($login_user->type == 'teacher') LkController::redirect('/lk/teacher');
		if($login_user->type == 'student') LkController::redirect('/lk/student');
	}
		
	public function actionIndex()
	{
		// если авторизован, отправим на свою страницу
		if ( Yii::app()->session['login_user'] != '' ) {
			LkController::goToOwnPage();
		} else {
			/* АВТОРИЗАЦИЯ */
			$modellogin=new LoginForm; $login_notice = '';
			if(isset($_POST['LoginForm'])) {
				$modellogin->attributes=$_POST['LoginForm'];
				if( $modellogin->validate() ) {
					//if( ($login_user = UsersAr::model()->findByAttributes(array('email'=>$modellogin->nameadmin))) && ($modellogin->passadmin == '000000') ) {
					if( $login_user = UsersAr::model()->findByAttributes(array('email'=>$modellogin->nameadmin,'password'=>$modellogin->passadmin)) ) {
						if($login_user->locked == 'yes') {
							$login_notice = 'Ваш вход в Личный кабинет заблокирован, обратитесь к своему преподавателю';
						} else {
							Yii::app()->session['login_user'] = $login_user->id;
							// log
							$log = new LogAr;
							$log->id_user = $login_user->id; $log->data_auth_in = time(); $log->save();		//$log->time_session = time();
							// сразу редирект на свою страницу
							if($login_user->type == 'admin' || $login_user->type == 'partner') $this->redirect('teachers');
							if($login_user->type == 'teacher') $this->redirect('teacher');
							if($login_user->type == 'student') $this->redirect('student');
						}
					}
					else $login_notice = 'Неверные логин / пароль';
				} else {
					$login_notice = 'Неверные логин / пароль';
                    if ($modellogin->hasErrors('verifyCode')) {
                        $login_notice = $modellogin->getError('verifyCode');
                    }
					Yii::app()->session['login_user'] = '';
				}
			}
			$this->render('index',array('modellogin'=>$modellogin,'login_notice'=>$login_notice));
		}
	}
	
	/* -- ====================== ADMIN - СТРАНИЦА ПАРТНЕРЫ ============== -- */
	public function actionPartners()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type != 'admin' ) $this->redirect('index');

		/* ОПЕРАЦИИ С партнером */
		if(isset($_POST["lock_partner"])) {
			$lockuser = UsersAr::model()->findByPk($_POST["id_partner"]);
			if($lockuser->locked == 'no') $lockuser->locked = 'yes'; else $lockuser->locked = 'no';
			$lockuser->save();
		}
		if(isset($_POST["delete_partner"])) {
			$user = UsersAr::model()->findByPk($_POST["id_partner"]);
			// сначала освободим текущих преподов
			if( $current_teachers = UsersAr::model()->findAllByAttributes(array('id_who_learn'=>$user->id)) ) {
				foreach($current_teachers as $current_teacher) {
					$current_teacher->id_who_learn = '-';
					$current_teacher->save();
				}
			}
			$user->delete();
		}
		if(isset($_POST["send_email_to_partner"])) {
			$user = UsersAr::model()->findByPk($_POST["id_partner"]);
			require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
			require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
			$mail->Subject = "Письмо от администратора OBRAIN_works.com";
			$mail->AddAddress($user->email, 'Портал OBRAIN_works.com');		//$mail->AddAddress('grigoryvolchok@gmail.com', 'Портал OBRAIN_works.com');
			$mail->Body = $_POST['text_email'];
			$mail->send();
			//$notice = '<p style="color:green;">Письма успешно разосланы.</p>';
		}
		
		$this->render('partners'/*,array('modeloperuser'=>$modeloperuser,'notice'=>$notice)*/);
	}
	
	/* -- ====================== ADMIN/PARTNER - СТРАНИЦА ПРЕПОДАВАТЕЛИ ============== -- */
	public function actionTeachers()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'teacher' || $type_user->type == 'student' ) $this->redirect('index');

		/* ОПЕРАЦИИ С ПОЛЬЗОВАТЕЛЕМ */
		$modeloperuser = New OperuserForm; $notice = '';
		if(isset($_POST['OperuserForm'])) {
			$modeloperuser->attributes = $_POST['OperuserForm'];
			// блокировка пользователя
			if($modeloperuser->operation == 'lock') {
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
				$lockuser = UsersAr::model()->findByPk($modeloperuser->id);
				if($lockuser->locked == 'no') {
					$lockuser->locked = 'yes';
					$mail->Body = 'Ваш вход в OBRAIN_works.com заблокирован, просьба обратиться к администратору';
				} else {
					$lockuser->locked = 'no';
					$mail->Body = 'Вам предоставлен доступ в <a target="_blanc" href="http://'.Yii::app()->request->baseUrl.'/lk">Личный кабинет</a> на портале OBRAIN_works.com<br><br>';
					$mail->Body .= 'Ф.И.О.: ' . $lockuser->fio . '<br>';
					$mail->Body .= 'ID: ' . $lockuser->id_teacher . '<br>';
					$mail->Body .= 'Город: ' . $lockuser->city . '<br>';
					$mail->Body .= 'Телефон: ' . $lockuser->phone . '<br>';
					$mail->Body .= 'Email: ' . $lockuser->email . '<br>';
					$mail->Body .= 'Пароль: ' . $lockuser->password;
				}
				$lockuser->save();
				$mail->Subject = "Доступ в личный кабинет OBRAIN_works.com";
				$mail->AddAddress($lockuser->email, 'Портал OBRAIN_works.com');
				$mail->send();
				//mail($lockuser->email, 'Доступ в личный кабинет OBRAIN_works.com', $message, $headers);
			}
			// удаление препода
			if($modeloperuser->operation == 'delete_user') {
				// во вьюхе проверяется, если у препода есть ученики, то кнопку Удалить НЕ показываем
				$user = UsersAr::model()->findByPk($modeloperuser->id);
				$user->delete();
			}
			// смена ИД преподу
			if($modeloperuser->operation == 'edit_id_user') {
				$user = UsersAr::model()->findByPk($modeloperuser->id);
				$user->id_teacher = $modeloperuser->value;
				$user->save();
			}
			// удаление истории авторизации пользователя
			if($modeloperuser->operation == 'clear_history_auth') {
				$user = UsersAr::model()->findByPk($modeloperuser->id);
				LogAr::model()->deleteAll("id_user = {$user->id}");
			}
			$this->refresh();
		}
		$this->render('teachers',array('modeloperuser'=>$modeloperuser,'notice'=>$notice));
	}
	
	/* -- ====================== ADMIN/PARTNER - СТРАНИЦА УЧЕНИКИ ================= -- */
	public function actionStudents()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'teacher' || $type_user->type == 'student' ) $this->redirect('index');

		/* ОПЕРАЦИИ С ПОЛЬЗОВАТЕЛЕМ */
		$modeloperuser = New OperuserForm; $notice = '';
		if(isset($_POST['OperuserForm'])) {
			$modeloperuser->attributes = $_POST['OperuserForm'];
			// блокировка пользователя
			if($modeloperuser->operation == 'lock') {
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
				$lockuser = UsersAr::model()->findByPk($modeloperuser->id);
				if($lockuser->locked == 'no') {
					$lockuser->locked = 'yes';
					$mail->Body = 'Ваш вход в OBRAIN_works.com заблокирован, просьба обратиться к своему преподавателю';
				} else {
					$lockuser->locked = 'no';
					$mail->Body = 'Вам предоставлен доступ в <a target="_blanc" href="http://'.Yii::app()->request->baseUrl.'/lk">Личный кабинет</a> на портале OBRAIN_works.com';
				}
				$lockuser->save();
				$mail->Subject = "Доступ в личный кабинет OBRAIN_works.com";
				$mail->AddAddress($lockuser->email, 'Портал OBRAIN_works.com');
				$mail->send();
				//mail($lockuser->email, 'Доступ в личный кабинет OBRAIN_works.com', $message, $headers);
			}
			// удаление пользователя
			if($modeloperuser->operation == 'delete_user') {
				$user = UsersAr::model()->findByPk($modeloperuser->id);
				$user->delete();
			}
			// смена ИД пользователю
			if($modeloperuser->operation == 'edit_id_user') {
				$user = UsersAr::model()->findByPk($modeloperuser->id);
				$user->id_teacher = $modeloperuser->value;
				$user->save();
			}
			// удаление истории авторизации пользователя
			if($modeloperuser->operation == 'clear_history_auth') {
				$user = UsersAr::model()->findByPk($modeloperuser->id);
				LogAr::model()->deleteAll("id_user = {$user->id}");
			}
			$this->refresh();
		}
		/* ПОДТВЕРЖДЕНИЕ ИЛИ ОТКЛОНЕНИЕ ПРАВКИ ПРОФИЛЯ СТУДЕНТОМ */
		if(isset($_GET["confirmEditProfile"])) {
			$user = UsersAr::model()->findByPk($_GET["id_user"]);
			if($_GET["confirmEditProfile"] == 'OK') {
				$user->fio = $user->fio_tmp; $user->fio_tmp = null;
				$user->email = $user->email_tmp; $user->email_tmp = null;
				$user->phone = $user->phone_tmp; $user->phone_tmp = null;
			} else {
				$user->fio_tmp = null;
				$user->email_tmp = null;
				$user->phone_tmp = null;
			}
			$user->save();
		}
		
		$this->render('students',array('modeloperuser'=>$modeloperuser,'notice'=>$notice));
	}
	
	/* -- ====================== СТРАНИЦА ПРЕПОДАВАТЕЛЯ =========================== -- */
	public function actionTeacher()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type != 'teacher' ) $this->redirect('index');
	
		/* ОПЕРАЦИИ С ПОЛЬЗОВАТЕЛЕМ */
		$modeloperuser = New OperuserForm;
		if(isset($_POST['OperuserForm'])) {
			$modeloperuser->attributes = $_POST['OperuserForm'];
			// блокировка пользователя
			if($modeloperuser->operation == 'lock') {
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
				$lockuser = UsersAr::model()->findByPk($modeloperuser->id);
				if($lockuser->locked == 'no') {
					$lockuser->locked = 'yes';
					$mail->Body = 'Ваш вход в OBRAIN_works.com заблокирован, просьба обратиться к своему преподавателю';
				} else {
					$lockuser->locked = 'no';
					$mail->Body = 'Вам предоставлен доступ в <a target="_blanc" href="http://'.Yii::app()->request->baseUrl.'/lk">Личный кабинет</a> на портале OBRAIN_works.com';
				}
				$lockuser->save();
				$mail->Subject = "Доступ в личный кабинет OBRAIN_works.com";
				$mail->AddAddress($lockuser->email, 'Портал OBRAIN_works.com');
				$mail->send();
				//mail($lockuser->email, 'Доступ в личный кабинет OBRAIN_works.com', $message, $headers);
			}
			// просмотр информации по ученику
			if($modeloperuser->operation == 'view_student') {
				Yii::app()->session['id_view_user'] = $modeloperuser->id; 
				$this->redirect('student');
			}
			$this->refresh();
		}
		$this->render('teacher',array('modeloperuser'=>$modeloperuser));
	}
	
	/* -- ====================== СТРАНИЦА УЧЕНИКА =========================== -- */
	public function actionStudent()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		
		// определим доступ к странице
		//$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		//if( $type_user->type == 'admin' ) $this->redirect('index'); // можно только преподавателю и ученику

		$this->render('student'/*,array('modeloperuser'=>$modeloperuser)*/);
	}

	/* -- ====================== РЕГИСТРАЦИЯ ===================== -- */
	public function actionRegistration()
	{
		// если авторизован, отправим на свою страницу
		if ( Yii::app()->session['login_user'] != '' ) {
			LkController::goToOwnPage();
		} else {
			/* регистрация */
			$modelregistration=new RegistrationForm; $notice = ''; $notice_OK = '';
			if(isset($_POST['RegistrationForm'])) {
				$modelregistration->attributes=$_POST['RegistrationForm'];
				
				/* валидация данных */
				if( $modelregistration->type == 'student' ) {
					$id_teacher = '-';
					if( $found_who_learn = UsersAr::model()->findByAttributes(array('id_teacher'=>$modelregistration->id_teacher)) ) $id_who_learn = $found_who_learn->id;
					else $notice = 'Указан неверный ID преподавателя';
					$locked = 'no';
				} else {
					$create_id_teacher = UsersAr::model()->findBySql("SELECT `id` FROM `users` ORDER BY `id` DESC");
					$id_teacher = (int)$create_id_teacher->id + 1;
					$id_teacher = str_pad($id_teacher, 5, "0", STR_PAD_LEFT); // дополним слева нулями до длины 5
					$id_who_learn = '-';
					$locked = 'yes';
				}
				if( $modelregistration->fio == '' ) $notice .= '<br>Укажите Ф.И.О.';
				if( $isset_fio = UsersAr::model()->findByAttributes(array('fio'=>$modelregistration->fio)) ) $notice .= '<br>Пользователь с указанными Ф.И.О. уже зарегистрирован';
				if( $modelregistration->age == '' ) $notice .= '<br>Укажите возраст';
				if( $modelregistration->city == '' ) $notice .= '<br>Укажите город проживания';
				//$phone = substr($modelregistration->phone, 2);//if ( is_numeric($phone) && (substr($phone, 0, 1) == '9') && (strlen($phone) == 10) ) { } else $notice .= '<br>Неверный формат телефона';
				$phone = substr($modelregistration->phone, 1); if(((int)$phone) > 0) {} else $notice .= '<br>Неверный формат телефона';
				if( $isset_phone = UsersAr::model()->findByAttributes(array('phone'=>$modelregistration->phone)) ) $notice .= '<br>Пользователь с указанным телефоном уже зарегистрирован';
				if(((int)$modelregistration->age) > 0) {} else $notice .= '<br>Некорректное значение возраста';
				//if(eregi("[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,3}", $modelregistration->email)) {} else $notice .= '<br>Некорректное значение email';
				if( $isset_email = UsersAr::model()->findByAttributes(array('email'=>$modelregistration->email)) ) $notice .= '<br>Пользователь с указанным email уже зарегистрирован';
				if( $modelregistration->allow_personal_date != 1 ) $notice .= '<br>Подтвердите согласие на обработку и хранения предоставленных вами данных';
				if( $modelregistration->captcha != Yii::app()->session['captcha'] ) $notice .= '<br>Неверное проверочное число';

				if ($notice == '') {
					$add_user = New UsersAr;
					$add_user->id_teacher = $id_teacher;
					$add_user->id_who_learn = $id_who_learn;
					$add_user->type = $modelregistration->type;
					$add_user->fio = $modelregistration->fio;
					$add_user->age = $modelregistration->age;
					$add_user->city = $modelregistration->city;
					$add_user->email = $modelregistration->email;
					$add_user->phone = $modelregistration->phone;
						$chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP"; $max=10; $size=StrLen($chars)-1; $password=null; while($max--) $password.=$chars[rand(0,$size)];
					$add_user->password = $password;
					$add_user->data_reg = time();
					$add_user->locked = $locked;
					$add_user->save();
					
					/* отправка почты */
					require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
					require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
					
					if( $modelregistration->type == 'teacher' ) {
						//$headers = "Content-Type: text/html; charset=UTF-8\r\n From:info@OBRAIN.ORG\r\n"; mail($modelregistration->email, 'Регистрация на портале OBRAIN_works.com', $message, $headers);
						// отправка преподавателю
						$mail->Subject = "Регистрация на портале OBRAIN_works.com";
						$mail->Body = "Благодарим за регистрацию, вам будет предоставлен доступ после подтверждения администратором.";
						$mail->AddAddress($modelregistration->email, 'Портал OBRAIN_works.com');
						$mail->send();
						$notice_OK = 'Благодарим за регистрацию, вам будет предоставлен доступ после подтверждения администратором.';
						
						// отправка админу, чтоб подтвердил регистрацию
						$mail->Subject = "Подтвердите регистрацию нового преподавателя на OBRAIN_works.com";
						$mail->Body = 'Подтвердите регистрацию (разблокируйте) <a target="_blanc" href="'.$this->createAbsoluteUrl('/').'/lk">нового преподавателя</a><br><br>';
						$mail->Body .= 'Ф.И.О.: ' . $modelregistration->fio . '<br>';
						$mail->Body .= 'ID: ' . $id_teacher . '<br>';
						$mail->Body .= 'Город: ' . $modelregistration->city . '<br>';
						$mail->Body .= 'Телефон: ' . $modelregistration->phone . '<br>';
						$mail->Body .= 'Email: ' . $modelregistration->email;
						$mail->ClearAddresses();
						$mail->AddAddress('obrain_kz@mail.ru', 'Портал OBRAIN_works.com');
						$mail->send();
						//mail('obrain_kz@mail.ru', 'Подтвердите регистрацию нового преподавателя на OBRAIN_works.com', $message, $headers);
					
					} else { // регится ученик
						$notice_OK = 'Благодарим вас, вы успешно зарегистрированы.<br>На ваш почтовый ящик отправлен пароль для входа в Личный кабинет';
						$mail->Subject = "Пароль для входа в Личный кабинет на OBRAIN_works.com";
						$mail->Body = 'Вы успешно зарегистрированы на OBRAIN_works.<br>Пароль для входа в Личный кабинет: ' . $password;
						$mail->AddAddress($modelregistration->email, 'Портал OBRAIN_works.com');
						$mail->send();
						//mail($modelregistration->email, 'Пароль для входа в Личный кабинет на OBRAIN_works.com', $message, $headers);
					}
				}
			}
			$modelregistration->type = 'student'; // poumolchaniyu
			$this->render('registration',array('modelregistration'=>$modelregistration,'notice'=>$notice,'notice_OK'=>$notice_OK));
		}
	}
	
	/* -- ====================== РЕГИСТРАЦИЯ ПАРТНЕРА ===================== -- */
	public function actionRegPartner()
	{
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type != 'admin' ) $this->redirect('index');
		
		/* регистрация */
		$notice = '';
		if(isset($_POST["save_regPartner"])) {

			if($_POST["id_partner"] == 'new') $partner = New UsersAr; 
				else $partner = UsersAr::model()->findByPk($_POST["id_partner"]);
			$partner->type = 'partner';
			$partner->id_teacher = $_POST["id_teacher"];
			$partner->id_who_learn = '-';
			$partner->fio = $_POST["fio"];
			$partner->date_rojd = $_POST["date_rojd"];
			$partner->city = $_POST["city"];
			$partner->addr = $_POST["addr"];
			$partner->addr_reserv = $_POST["addr_reserv"];
			$partner->email = $_POST["email"];
			$partner->email_reserv = $_POST["email_reserv"];
			$partner->password = $_POST["password"];
			$partner->phone = $_POST["phone"];
			$partner->phone_reserv = $_POST["phone_reserv"];
			$partner->num_dogovor = $_POST["num_dogovor"];
			$partner->date_dogovor = $_POST["date_dogovor"];
			$partner->royalty = $_POST["royalty"];
			$partner->date_royalty = $_POST["date_royalty"];
			$partner->comment_royalty = $_POST["comment_royalty"];

            // add date reg (03.02.2019)
            $partner->data_reg = time();

			$partner->save();
			$notice = 'Партнер успешно сохранен';
			// закрепим этого партнера за преподавателями
			$found_who_learn = UsersAr::model()->findByAttributes(array('id_teacher'=>$_POST["id_teacher"]));
			// сначала освободим текущих преподов
			if( $current_teachers = UsersAr::model()->findAllByAttributes(array('id_who_learn'=>$found_who_learn->id)) ) {
				foreach($current_teachers as $current_teacher) {
					$current_teacher->id_who_learn = '-';
					$current_teacher->save();
				}
			}
			// затем назначим их поновой
			// nemnogo ispravil...
            if ( isset($_POST['teachers']) ) {
                foreach ($_POST['teachers'] as $value) {
                    $teacher = UsersAr::model()->findByPk($value);
                    $teacher->id_who_learn = $found_who_learn->id;
                    $teacher->save();
                }
            }
		}
		$this->render('regPartner',array('notice'=>$notice));
	}
	
	/* -- ====================== PARTNER - СДАЧА ОТЧЕТА ПАРТНЕРОМ ===================== -- */
	public function actionReports()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'teacher' || $type_user->type == 'student' ) $this->redirect('index'); // только админу и партнерам
		
		$notice = '';
		if(isset($_POST["save_report"])) {
			if($_POST["id_report"] == 'new') $report = New ReportsAr;
			else $report = ReportsAr::model()->findByPk($_POST["id_report"]);

			$report->id_partner = Yii::app()->session['login_user'];
			$report->date_report = time();
			$report->locked = 'yes';
			$report->price_one = $_POST["price_one"];
			$report->price_month = $_POST["price_month"];
			$report->count_students = $_POST["count_students"];
			$report->count_teachers = $_POST["count_teachers"];
			$report->count_partners = isset($_POST["count_partners"]) ? $_POST["count_partners"] : '';
			$report->count_teachers_partners = isset($_POST["count_teachers_partners"]) ? $_POST["count_teachers_partners"] : '';
			$report->profit_from_partners = $_POST["profit_from_partners"];
			$report->profit_themselve = $_POST["profit_themselve"];
			$report->summ_zp_teachers = $_POST["summ_zp_teachers"];
			$report->rent_areas = $_POST["rent_areas"];
			$report->summ_others = $_POST["summ_others"];
			$report->profit_before_nalog = $_POST["profit_before_nalog"];
			$report->events = $_POST["events"];
			$report->summ_royalty = $_POST["summ_royalty"];
			if ($_FILES['uploadfile']['error'] === UPLOAD_ERR_OK) {
			    $randomString = substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,10))), 1, 5);
			    $fileName = date('d-m-Y') . '_' . $randomString . '_'.$_FILES['uploadfile']["name"];
			    $path = '/uploads/reports/';
                if ( move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$path.$fileName) ) {
                    $report->uploadfile = $path.$fileName;
                }
			}
			$report->save();
			$notice = 'Отчет успешно сохранен';
		}
		// прием админом отчета
		if(isset($_POST["allow_report"])) {
			$report = ReportsAr::model()->findByPk($_POST["id_report"]);
			$report->allowed_report = 'yes';
			$report->locked = 'yes';
			$report->comment_deny_report = $_POST["comment_deny_report"];
			$report->save();
			$notice = 'Отчет успешно принят!';
		}
		// отклонение админом отчета
		if(isset($_POST["deny_report"])) {
			$report = ReportsAr::model()->findByPk($_POST["id_report"]);
			$report->allowed_report = 'no';
			$report->locked = 'no';
			$report->comment_deny_report = $_POST["comment_deny_report"];
            $report->save();
            $notice = 'Отчет отклонен!';
        }

		// udalenie otcheta (tolko admin mojet udalit)
        if (isset($_POST["delete_report"]) && $type_user->type == 'admin') {
            $report = ReportsAr::model()->findByPk($_POST["id_report"]);
            //esli ne nayden report
            if ($report === null) {
                $notice = 'Отчет не найден.';
            } else {
                $report->delete();
                $notice = 'Отчет удален!';
            }
        }

		$this->render('reports',array('notice'=>$notice));
	}
	
	/* -- ====================== ADMIN - НОВОСТИ ===================== -- */
	public function actionNews()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		//$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		//if( $type_user->type != 'admin' ) $this->redirect('index'); // только admin
		
		$notice = '';
		if(isset($_POST["save_news"])) {
			//if($_POST["id_report"] == 'new') $report = New ReportsAr; else $report = ReportsAr::model()->findByPk($_POST["id_report"]);
			$news = New NewsAr;
			$news->date_create = time();
			//$news->id_who_show = $_POST["price_one"];
			$news->date_from = strtotime($_POST["date_from"].' 00:01');
			$news->date_to = strtotime($_POST["date_to"].' 23:59');
			$news->text = $_POST["text"];
			foreach ($_POST['users'] as $value) {
				$news->id_who_show .= ';'.$value;
			}
			$news->id_who_show .= ';';
			if ($_FILES['uploadfile']['error'] === UPLOAD_ERR_OK) {
				$news->image = $this->createAbsoluteUrl('/').'/assets/files/news/'.$_FILES['uploadfile']["name"];
				move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].'/assets/files/news/'.$_FILES["uploadfile"]["name"]);
			}
			$news->save();
			$notice = 'Новость успешно добавлена';
		}
		if(isset($_GET["id_news_delete"])) {
			if($news = NewsAr::model()->findByPk($_GET["id_news_delete"])) $news->delete();
		}
		
		$this->render('news',array('notice'=>$notice));
	}
	
	/* -- ====================== ОПЛАТА ================== -- */
	public function actionPayment()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'partner' ) $this->redirect('index');
		
		$notice = '';
		if(isset($_POST["add_pay"])) {
		
			$report = New PaymentAr;
			$report->id_user = $_POST["id_user"];
			$report->data_pay = $_POST["data_pay"];
			$report->summ = $_POST["summ"];
			$report->pay_do = $_POST["pay_do"];
			$report->count_payed_class = $_POST["count_payed_class"];
			$report->count_worked_class = $_POST["count_worked_class"];
			$report->comment = $_POST["comment"];
			if ($_FILES['uploadfile']['error'] === UPLOAD_ERR_OK) {
				$report->uploadfile = $this->createAbsoluteUrl('/').'/assets/files/payments/'.$_FILES['uploadfile']["name"];
				move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].'/assets/files/payments/'.$_FILES["uploadfile"]["name"]);
			}
			$report->save();
			$notice = 'Оплата успешно добавлена';
		}
		$this->render('payment');
	}
	
	/* -- ====================== ЖУРНАЛ ================== -- */
	public function actionJournal()
	{
		// если не авторизован, отправим на авторизацию
		if ( Yii::app()->session['login_user'] == '' ) {
			LkController::redirect('index');
		} else {
			if(isset($_POST['add_user_to_journal'])) {
				$journal = New JournalAr;
				$journal->id_user = $_POST['id_user'];
				$journal->data = $_POST['data'];
				$journal->attended = $_POST['attended'];
				$journal->save();
			}
			if(isset($_GET['id_user'])) {
				$journals = JournalAr::model()->findAllByAttributes(array('id_user'=>$_GET['id_user']));
				foreach($journals as $journal) {
					$journal->delete();
				}
				$this->redirect('journal');
			}
			$this->render('journal');
		}
	}
	
	/* -- ====================== ВОССТАНОВЛЕНИЕ ПАРОЛЯ ================== -- */
	public function actionRemind()
	{
		// если авторизован, отправим на свою страницу
		if ( Yii::app()->session['login_user'] != '' ) {
			LkController::goToOwnPage();
		} else {
			/* восстановление пароля */
			$modelremind=new RemindForm; $notice = ''; $notice_OK = '';
			if(isset($_POST['RemindForm'])) {
				$modelremind->attributes=$_POST['RemindForm'];
				
				/* валидация данных */
				if( $modelremind->email == '' ) $notice = 'Укажите ваш email';
				
				if ($notice == '') {
					if( $user = UsersAr::model()->findByAttributes(array('email'=>$modelremind->email)) ) {
						if( ($user->type == 'teacher') && ($user->locked == 'yes') ) $notice = "Ваш доступ пока не одобрен администратором";
						else {
							require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
							require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
							$mail->Subject = "Пароль к личному кабинету OBRAIN_works.com";
							$mail->Body = 'Пароль для входа в личный кабинет OBRAIN_works.com - ' . $user->password;
							$mail->AddAddress($modelremind->email, 'Портал OBRAIN_works.com');
							$mail->send();
							//$notice_OK = 'Пароль для входа в личный кабинет отправлен на эл. почту <b>'.$modelremind->email.'</b>';
							$this->redirect('index');
						}
					} else {
						$notice = "Пользователь с эл. почтой <b>{$modelremind->email}</b> не зарегистрирован";
					}
				}
			}
			$this->render('remind',array('modelremind'=>$modelremind,'notice'=>$notice,'notice_OK'=>$notice_OK));
		}
	}
	
	/* -- ====================== ПРИМЕТЫ MENTAL MAP ================== -- */
	public function actionRules()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type != 'admin' ) $this->redirect('index');
		
		// добавление примера
		$add_ok = '';
		if(isset($_POST["ex_add"])) {
			for($i = 1; $i <= 50; $i++) {
				$ex_example = ''; $count_row = 0;
				for($j = 1; $j <= 6; $j++) {
					if($_POST["{$i}ex_value_{$j}"] != '') {
					
						if(strpos($_POST["{$i}ex_value_{$j}"], "-") !== false) $ex_example .= '|'.'-'.'|'.substr($_POST["{$i}ex_value_{$j}"], 1);
						elseif(strpos($_POST["{$i}ex_value_{$j}"], "*") !== false) $ex_example .= '|'.'*'.'|'.substr($_POST["{$i}ex_value_{$j}"], 1);
						elseif(strpos($_POST["{$i}ex_value_{$j}"], "/") !== false) $ex_example .= '|'.'/'.'|'.substr($_POST["{$i}ex_value_{$j}"], 1);
						else $ex_example .= '|'.'+'.'|'.$_POST["{$i}ex_value_{$j}"];
						$count_row = $count_row + 1;
					}
				}
				if($ex_example != '') {
					$ex_example = substr($ex_example, 3);
					$add_ex = New ExamplesAr;
					$add_ex->id_rules = $_POST["ex_id_rules"];
					$add_ex->rows = $count_row; //$_POST["ex_rows"];
					$add_ex->example = $ex_example;
					$add_ex->answer = $_POST["{$i}ex_answer"];
					$add_ex->save();
					if($add_ok != 'пример успешно добавлен') $add_ok = 'пример успешно добавлен';
				}
			}
		}
		// удаление примера
		if(isset($_GET["del"])) {
			$ex_example = ExamplesAr::model()->findByPk($_GET["del"]);
			$ex_example->delete();
			$this->redirect('rules');
		}
		
		$this->render('rules',array('add_ok'=>$add_ok));
	}
	
	/* -- ====================== СМЕНА ПАРОЛЯ ================== -- */
	public function actionChangePassword()
	{
		// если НЕ авторизован, отправим на стартовую в ЛК
		if ( Yii::app()->session['login_user'] == '' ) {
			LkController::redirect('index');
		} else {
			/* смена пароля */
			$modelchangepassword=new ChangePasswordForm; $notice = ''; $notice_OK = '';
			if(isset($_POST['ChangePasswordForm'])) {
				$modelchangepassword->attributes=$_POST['ChangePasswordForm'];
				
				// валидация данных
				if( $modelchangepassword->password == '' ) $notice = 'Введите новый пароль<br>';
				if( $modelchangepassword->confirmpassword == '' ) $notice .= 'Введите подтверждение пароля';
				if ($notice == '') if( $modelchangepassword->password != $modelchangepassword->confirmpassword ) $notice = 'Введенный пароль и его подтверждение не совпадают';
				
				if ($notice == '') {
					$login_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
					$login_user->password = $modelchangepassword->password;
					$login_user->save();
					$notice_OK = 'Ваш пароль успешно изменен.';
					Yii::app()->session['status'] = 'Ваш пароль успешно изменен.';
					LkController::redirect('index');
				}
			}
			$this->render('changePassword',array('modelchangepassword'=>$modelchangepassword,'notice'=>$notice,'notice_OK'=>$notice_OK));
		}
	}
	
	/* -- ====================== ПРОФИЛЬ УЧЕНИКА ================== -- */
	public function actionProfile()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'admin' || $type_user->type == 'partner' ) $this->redirect('index'); // можно только ученику (препод тоже может зайти)
		
		$modelprofile = new ProfileForm; $notice = ''; $notice_OK = '';
		// передадим данные форме
		$modelprofile->fio = $type_user->fio;
		$modelprofile->email = $type_user->email;
		$modelprofile->phone = $type_user->phone;
		$modelprofile->id = $type_user->id;
		
		if(isset($_POST['ProfileForm'])) {
			$modelprofile->attributes=$_POST['ProfileForm'];
				
			// валидация данных
			if( $modelprofile->fio == '' ) $notice = 'Введите ФИО<br>';
			if( $modelprofile->email == '' ) $notice = 'Введите email<br>';
			if( $modelprofile->phone == '' ) $notice .= 'Введите телефон';
			
			if ($notice == '') {
				$user = UsersAr::model()->findByPk($modelprofile->id);
				$user->fio_tmp = $modelprofile->fio;
				$user->email_tmp = $modelprofile->email;
				$user->phone_tmp = $modelprofile->phone;
				$user->save();
				$notice_OK = 'Изменения сохранятся через 24 часа или после одобрения администратором.';
				// отправка админу, чтоб подтвердил редактирование профиля
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
				require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
				$mail->Subject = "Подтвердение изменений профиля пользователя на OBRAIN_works.com";
				$mail->Body = '<a target="_blanc" href="'.$this->createAbsoluteUrl('/').'/lk/">Авторизуйтесь</a> как Администратор и подтвердите <a target="_blanc" href="'.$this->createAbsoluteUrl('/').'/lk/students/'.$modelprofile->id.'">изменения профиля пользователя</a><br><br>';
				$mail->Body .= 'Новые Ф.И.О.: ' . $modelprofile->fio . '<br>';
				$mail->Body .= 'Новый телефон: ' . $modelprofile->phone . '<br>';
				$mail->Body .= 'Новый еmail: ' . $modelprofile->email;
				$mail->AddAddress('obrain_kz@mail.ru', 'Портал OBRAIN_works.com');
				//$mail->AddAddress('v_grigory@mail.ru', 'Портал OBRAIN_works.com');
				$mail->send();
				//$this->refresh();
			}
		}
		$this->render('profile',array('modelprofile'=>$modelprofile,'notice'=>$notice,'notice_OK'=>$notice_OK));
	}
	
	/* -- ====================== РЕКОРДЫ УЧЕНИКА ================== -- */
	public function actionRecords()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		//$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		//if( $type_user->type == 'admin' ) $this->redirect('index'); // можно только ученику (препод тоже может зайти)
		
		
		$this->render('records'/*,array('modelprofile'=>$modelprofile,'notice'=>$notice,'notice_OK'=>$notice_OK)*/);
	}
	
	/* -- ====================== ФИЛЬТР ================== -- */
	public function actionFilter()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'student' ) $this->redirect('index'); // только админу, партнеру и преподу
		
		$this->render('filter');
	}
	
	/* -- ====================== РАССЫЛКА ПИСЕМ ================== -- */
	public function actionSendEmail()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'student' ) $this->redirect('index'); // только админу, партнеру и преподу
		
		$notice = '';
		if(isset($_POST["btn_send_email_to_users"])) {
			require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/class.phpmailer.php');
			require_once($_SERVER['DOCUMENT_ROOT'].'/sendEmail/init_sendMail.php');
			$mail->Subject = "Рассылка писем с OBRAIN_works.com";
			foreach ($_POST['users'] as $value) {
				$email_user = UsersAr::model()->findByPk($value);
				$mail->AddAddress($email_user->email, 'Портал OBRAIN_works.com');
			}
			//$mail->AddAddress('v_grigory@mail.ru', 'Портал OBRAIN_works.com');
			//$mail->AddAddress('grigoryvolchok@gmail.com', 'Портал OBRAIN_works.com');
			$mail->Body = $_POST['text_email'];
			if ($_FILES['uploadfile']['error'] === UPLOAD_ERR_OK) {
				$mail->Body .= '<br><br>Файл: <a href="'.$this->createAbsoluteUrl('/').'/assets/files/'.$_FILES['uploadfile']["name"].'">'.$_FILES['uploadfile']["name"].'</a>';
				move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].'/assets/files/'.$_FILES["uploadfile"]["name"]);
			}
			$mail->send();
			$notice = '<p style="color:green;">Письма успешно разосланы.</p>';
		}
		
		$this->render('sendEmail',array('notice'=>$notice));
	}
	
	/* -- ====================== ВЫХОД =========================== -- */
	public function actionLogout()
	{
		if( $log = LogAr::model()->findByAttributes(array('id_user'=>Yii::app()->session['login_user'])) ) {
			$log->data_auth_out = time(); $log->save();
		}
		Yii::app()->session['login_user'] = '';
		//Yii::app()->session['id_view_student_for_teacher'] = '';
		Yii::app()->session['id_view_user'] = '';
		//$this->redirect('index');
		$this->redirect('/' . Yii::app()->defaultController);
	}
	public function actionTest()
	{
		//$headers = "Content-Type: text/html; charset=UTF-8\r\n";
		//$message = 'Подтвердите регистрацию (разблокируйте) <a target="_blanc" href="'.$this->createAbsoluteUrl('/').'/lk">нового преподавателя</a><br><br>';
		//mail('v_grigory@mail.ru', 'Подтвердите регистрацию нового преподавателя на OBRAIN_works.com', $message, $headers);
		$this->render('test');
	}

	/* -- ====================== НАСТРОЙКИ САЙТА ================== -- */
	public function actionSite()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('index');
		// определим доступ к странице
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'teacher' || $type_user->type == 'student' ) $this->redirect('index');
		$notice = '';

		if(isset($_POST["edit_main_image"])) {
		
			if ($_FILES['uploadfile']['error'] === UPLOAD_ERR_OK) {
				move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].'/assets/files/site/'.Yii::app()->session['login_user'].'/mainImg.png'); //.$_FILES["uploadfile"][mainImg]);
			}
			
			$notice = 'Оплата успешно добавлена';
		}
		
		
		$this->render('site',array('notice'=>$notice));
	}
}