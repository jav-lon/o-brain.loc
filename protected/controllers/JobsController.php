<?php
class JobsController extends Controller
{
	
	public $layout = '//layouts/jobs/main';
	public function actions()
	{
		return array(
				'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
                    'class'=>'NumberCCaptchaAction',
                    'backColor'=>0xFFFFFF,
                    'minLength' => 4,
                    'maxLength' => 4
			),
				'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionTraining()
	{
		/* АВТОРИЗАЦИЯ */
		$modellogin=new LoginForm; $login_notice = '';
		if(isset($_POST['LoginForm'])) {
				$modellogin->attributes=$_POST['LoginForm'];
				if( $modellogin->validate()/*($modellogin->nameadmin != '') && ($modellogin->passadmin != '')*/ ) {
					if( $login_user = UsersAr::model()->findByAttributes(array('email'=>$modellogin->nameadmin,'password'=>$modellogin->passadmin)) ) {
						if($login_user->locked == 'yes') {
							$login_notice = 'Ваш вход в Личный кабинет заблокирован, обратитесь к своему преподавателю';
						} else {
							Yii::app()->session['login_user'] = $login_user->id;
							// log
							$log = new LogAr;
							$log->id_user = $login_user->id; $log->data_auth_in = time(); $log->save();		//$log->time_session = time();
						}
					}
					else $login_notice = 'Неверные логин / пароль';
				} else {
					$login_notice = 'Неверные логин / пароль';
                    if ($modellogin->hasErrors('verifyCode')) {
                        $login_notice = $modellogin->getError('verifyCode');
                    }
					Yii::app()->session['login_user'] = ''; 
				}
		}

		$this->render('training',array('modellogin'=>$modellogin,'login_notice'=>$login_notice));
	}


	public function actionNumbers()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
	
		/*$level = '';
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) ) {
				$level = $_GET["lev"];
				$voice = 1;
			}
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);*/
	
		$this->render('numbers'/*,array('level'=>$level,'voice'=>$voice)*/);
	}
	
	public function actionMap()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
	
		/*$level = '';
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) )
				$level = $_GET["lev"];
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);*/
	
		$this->render('map'/*,array('level'=>$level,'model'=>$model)*/);
	}
	
	public function actionSlalom()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
	
		/*$level = ''; $rows = '';
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) )
				$level = $_GET["lev"];
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);
		
		if( $level != '' ) {
			if( isset($_GET["rows"]) ) {
				$arr_rows = array('1','2','3','4','5');
				if( in_array($_GET["rows"], $arr_rows) )
					$rows = $_GET["rows"];
				else $this->redirect(Yii::app()->homeUrl);
				
			} else $this->redirect(Yii::app()->homeUrl);
		}*/
	
		$this->render('slalom'/*,array('level'=>$level,'rows'=>$rows)*/);
	}
	
	public function actionMultiply()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
	
		/*$level = ''; $rows = '';
		
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) )
				$level = $_GET["lev"];
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);
		
		if( $level != '' ) {
			if( isset($_GET["rows"]) ) {
				$arr_rows = array('1','2','3','4','5');
				if( in_array($_GET["rows"], $arr_rows) )
					$rows = $_GET["rows"];
				else $this->redirect(Yii::app()->homeUrl);
				
			} else $this->redirect(Yii::app()->homeUrl);
		}*/
	
		$this->render('multiply');
	}
	
	public function actionDivision()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
	
		/*$level = ''; $rows = '';
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) )
				$level = $_GET["lev"];
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);
		if( $level != '' ) {
			if( isset($_GET["rows"]) ) {
				$arr_rows = array('1','2','3','4','5');
				if( in_array($_GET["rows"], $arr_rows) )
					$rows = $_GET["rows"];
				else $this->redirect(Yii::app()->homeUrl);
				
			} else $this->redirect(Yii::app()->homeUrl);
		}*/
	
		$this->render('division');
	}
	
	public function actionShulteNumbers()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
		/*$level = '';
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) )
				$level = $_GET["lev"];
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);*/
	
		$this->render('shulteNumbers');
	}
	
	public function actionShulteAbacus()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
		/*$level = '';
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) )
				$level = $_GET["lev"];
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);*/
		
		$this->render('shulteAbacus');
	}
	
	public function actionMentalmap()
	{
		if ( Yii::app()->session['login_user'] == '' ) $this->redirect('/site/index');
		/*
		$level = '';
		if( isset($_GET["lev"]) ) {
			$arr_lev = array('1','2','3','4');
			if( in_array($_GET["lev"], $arr_lev) )
				$level = $_GET["lev"];
			else $this->redirect(Yii::app()->homeUrl);
			
		} else $this->redirect(Yii::app()->homeUrl);
		*/
		$this->render('mentalmap'/*,array('level'=>$level/*'model'=>$model)*/);
	}
	
	
}