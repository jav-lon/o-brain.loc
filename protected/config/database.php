<?php
$commonLocalConfig = require __DIR__ . '/../../yii2/common/config/main-local.php';
$db = $commonLocalConfig['components']['db'];

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
    'connectionString' => $db['dsn'],
    'emulatePrepare' => true,
    'username' => $db['username'],
    'password' => $db['password'],
    'charset' => 'utf8',
    'enableProfiling'=>true, // nujen dlya debug panel

	/*'connectionString' => 'mysql:host=localhost;dbname=p-101_obrain',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'data2222',
	'charset' => 'utf8',
    'enableProfiling'=>true, // nujen dlya debug panel*/
    'enableParamLogging'=>true, // nujen dlya debug panel

    // noviy xosting
    /*'connectionString' => 'mysql:host=localhost;dbname=p-15213_obrain',
    'emulatePrepare' => true,
    'username' => 'p-152_obrain',
    'password' => 'hlGn13!0G5bKP5',
    'charset' => 'utf8',*/

    // isxodniy nastroyki

    /*'connectionString' => 'mysql:host=localhost;dbname=p-101_obrain',
    'emulatePrepare' => true,
    'username' => 'p-101_obrain',
    'password' => 'hlGn13!0G5bKP5',
    'charset' => 'utf8',*/


);