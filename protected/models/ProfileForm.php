<?php

class ProfileForm extends CFormModel
{
	public $id;
	public $fio;
	public $email;
	public $phone;

    public function rules()
	{
        return array(
			array('id,fio,email,phone', 'required')
		);
	}

}
