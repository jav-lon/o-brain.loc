<?php

class RegistrationForm extends CFormModel
{
	public $id_teacher;
	public $type;
	public $fio;
	public $age;
	public $city;
	public $email;
	public $phone;
	public $allow_personal_date;
	public $captcha;

    public function rules()
	{
        return array(
			array('id_teacher,type,fio,age,city,email,phone,allow_personal_date,captcha', 'required')
		);
	}

}
