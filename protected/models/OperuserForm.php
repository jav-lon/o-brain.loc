<?php

class OperuserForm extends CFormModel
{
	public $id;
	public $operation;
	public $value;

	public function rules()
	{
		return array(
			array('id,operation,value', 'required'),
		);
	}
}