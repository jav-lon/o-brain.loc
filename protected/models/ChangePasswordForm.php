<?php

class ChangePasswordForm extends CFormModel
{
	public $password;
	public $confirmpassword;

    public function rules()
	{
        return array(array('password,confirmpassword', 'required'));
	}

}
