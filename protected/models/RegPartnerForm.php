<?php

class RegPartnerForm extends CFormModel
{
	public $id_teacher; // Регистрационный номер
	//public $type; --> 'partner'
	public $fio;
	public $date_rojd;
	public $city;
	public $email;
	public $email_reserv;
	public $phone;
	public $phone_reserv;
	public $addr;
	public $addr_reserv;
	public $num_dogovor;
	public $date_dogovor;
	public $royalty;
	public $date_royalty;
	public $comment_royalty;

    public function rules()
	{
        return array(
			array('id_teacher,fio,date_rojd,city,email,email_reserv,phone,phone_reserv,addr,addr_reserv,num_dogovor,date_dogovor,royalty,date_royalty,comment_royalty', 'required')
		);
	}

}
