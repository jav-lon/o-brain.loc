<?php

class LoginForm extends CFormModel
{
	public $nameadmin;
	public $passadmin;
	public $verifyCode;

    public function rules()
	{
        return array(
			array('nameadmin,passadmin', 'required'),
            array('verifyCode', 'captcha', 'allowEmpty' => false, 'message' => 'Код проверки неверно')
		);
	}

}
