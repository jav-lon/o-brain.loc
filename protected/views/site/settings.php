<div id="loading" style="display:none;"><img src="/images/load.gif" /></div>
<?php if ( $_SERVER['REQUEST_URI'] == '/jobs/mentalmap' || isset($_GET["jobSetting"]) ): ?>
<form role="form" id="form_settings">

	<?php
	$level1='';$level2='';$level3='';$level4='';
	$rows1='';$rows2='';$rows3='';$rows4='';$rows5='';$rows6='';
	$dl01 = $dl02 = '';
	$dl1='';$dl2='';$dl3='';$dl4='';$dl5='';$dl6='';$dl7='';$dl8='';$dl9='';$dl10='';$dl11='';$dl12='';$dl13='';$dl14='';$dl15='';$dl16='';$dl17='';$dl18='';
	$voice1='';$voice2='';$voice3='';
	$oper1='';$oper2='';$oper3='';$oper4='';$oper5='';$oper6='';
	$rules1='';$rules2='';
	$lang_ru = ''; $lang_en = '';
	$count_mentally = '';
	$map_twinkle = '';
	$examples_up_to_x = '';
	$isset_settings = false;

	if(  ($_SERVER['REQUEST_URI'] == '/jobs/mentalmap') || ($_GET["jobSetting"] == 'MentalMap') ) $id_job = 8;
	elseif($_GET["jobSetting"] == 'numbers') $id_job = 1;
	elseif($_GET["jobSetting"] == 'map') $id_job = 2;
	elseif($_GET["jobSetting"] == 'slalom') $id_job = 3;
	elseif($_GET["jobSetting"] == 'multiply') $id_job = 4;
	elseif($_GET["jobSetting"] == 'division') $id_job = 5;
	elseif($_GET["jobSetting"] == 'shulteNumbers') $id_job = 6;
	elseif($_GET["jobSetting"] == 'shulteAbacus') $id_job = 7;

	$isset_settings = SettingsAr::model()->findByAttributes(array('id_user'=>Yii::app()->session['login_user'],'id_job'=>$id_job));
	if($isset_settings) {

		$id_levels = substr($isset_settings->level, 0, -1); // удалим последнее ;
		$array_levels = explode(';', $id_levels);
		if(in_array(1, $array_levels)) $level1 = 'checked';
		if(in_array(2, $array_levels)) $level2 = 'checked';
		if(in_array(3, $array_levels)) $level3 = 'checked';
		if(in_array(4, $array_levels)) $level4 = 'checked';

		$id_rows = substr($isset_settings->rows, 0, -1); // удалим последнее ;
		$array_rows = explode(';', $id_rows);
		if(in_array(2, $array_rows)) $rows2 = 'checked';
		if(in_array(3, $array_rows)) $rows3 = 'checked';
		if(in_array(4, $array_rows)) $rows4 = 'checked';
		if(in_array(5, $array_rows)) $rows5 = 'checked';
		if(in_array(6, $array_rows)) $rows6 = 'checked';

		// tolko dlya map (2)
		if($isset_settings->delay == 100) $dl01='checked';
		if($isset_settings->delay == 200) $dl02='checked';

		if($isset_settings->delay == 300) $dl1='checked'; if($isset_settings->delay == 400) $dl2='checked'; if($isset_settings->delay == 500) $dl3='checked'; if($isset_settings->delay == 600) $dl4='checked';
		if($isset_settings->delay == 700) $dl5='checked'; if($isset_settings->delay == 800) $dl6='checked'; if($isset_settings->delay == 900) $dl7='checked'; if($isset_settings->delay == 1000) $dl8='checked';
		if($isset_settings->delay == 1100) $dl9='checked'; if($isset_settings->delay == 1200) $dl10='checked'; if($isset_settings->delay == 1300) $dl11='checked'; if($isset_settings->delay == 1400) $dl12='checked';
		if($isset_settings->delay == 1500) $dl13='checked'; if($isset_settings->delay == 1600) $dl14='checked'; if($isset_settings->delay == 1700) $dl15='checked'; if($isset_settings->delay == 1800) $dl16='checked';
		if($isset_settings->delay == 1900) $dl17='checked'; if($isset_settings->delay == 2000) $dl18='checked';

		if($isset_settings->voice == 1) /*$voice1='checked';*/$voice2='checked'; if($isset_settings->voice == 2) $voice2='checked'; if($isset_settings->voice == 3) $voice3='checked';

		if($isset_settings->operation == 1) $oper1='checked'; if($isset_settings->operation == 2) $oper2='checked'; if($isset_settings->operation == 3) $oper3='checked';
		if($isset_settings->operation == 4) $oper4='checked'; if($isset_settings->operation == 5) $oper5='checked'; if($isset_settings->operation == 6) $oper6='checked';

		if($isset_settings->rules == 1) $rules1='checked'; if($isset_settings->rules == 2) $rules2='checked';

		//yazik
        if($isset_settings->voice_lang == 'ru-RU') $lang_ru = 'checked';
        if($isset_settings->voice_lang == 'en-US') $lang_en = 'checked';

        //Count mentally
        if ($isset_settings->count_mentally === null || $isset_settings->count_mentally == 0) $count_mentally = '';
        else $count_mentally = 'checked';

        //Count mentally
        if ($isset_settings->twinkle == 0) $map_twinkle = '';
        else $map_twinkle = 'checked';

        //examples_up_to_x
        if ($isset_settings->examples_up_to_x !== null) {
            $examples_up_to_x = $isset_settings->examples_up_to_x;
        }
	}
	else {
		if ($id_job == 3 || $id_job == 4 || $id_job == 5) $level2 = 'checked';
		else $level1='checked';
		$rows2='checked';
		$dl14='checked';
		/*$voice1='checked';*/$voice2='checked';
		$oper1='checked'; $rules1='checked';
		$lang_ru = 'checked';
	}
 ?>
	<!--<h2>Настройки</h2>-->
	<?php if($id_job == 8 || $id_job == 3 /*|| $id_job == 4 || $id_job == 5*/) { ?>
		<hr><h3>Условия</h3>
		<label class="checkbox-inline"><input type="radio" name="rules" value="1" <?=$rules1;?>> Примеры с разными правилами</label>
		<label class="checkbox-inline"><input type="radio" name="rules" value="2" <?=$rules2;?>> Выбрать правила для примеров</label>
		<button id="btn_rulesOpen" type="button" class="btn btn-link">Выбрать правила</button>

        <?php
        $user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
        ?>

        <?php if ( $id_job == 8 ): ?>

            <a href="/simulator/mental-map" class="btn btn-primary">Задания</a>

            <?php if ( $user->type == 'admin' || $user->type == 'teacher' ): ?>
                <a href="/mentalmap-manager" class="btn btn-success">Создать примеры</a>
            <?php endif; ?>

        <?php endif; ?>
	<?php } ?>

	<?php if($id_job == 8 || $id_job == 3) { ?>
            <hr><h3>Рядность</h3>
	    <?php if ($id_job == 3): ?>
            <?php for($i = 2; $i <= 20; $i++): ?>
                <label class="radio-inline"><input type="radio" name="rows" value="<?=$i?>" <?= $array_rows[0] == $i ? 'checked': '' ?>> <?=$i?> рядные</label>
            <?php endfor; ?>
        <?php endif; ?>
        <?php if ($id_job == 8): ?>
            <?php for($i = 2; $i <= 20; $i++): ?>
                <label class="radio-inline"><input type="radio" name="rows" value="<?=$i?>" <?= $array_rows[0] == $i ? 'checked': '' ?>> <?=$i?> рядные</label>
            <?php endfor; ?>
        <?php endif; ?>
	<?php } ?>

	<?php if($id_job == 1 || $id_job == 8 || $id_job == 2 || $id_job == 3 || $id_job == 4 || $id_job == 5 || $id_job == 6 || $id_job == 7) { ?>
		<hr><h3>Сложность</h3>
		<?php if($id_job != 4 && $id_job != 5) { ?>
			<label class="checkbox-inline"><input type="radio" name="level" value="1" <?=$level1;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 1 спица <?php } else { ?> однозначные <?php } ?></label>
		<?php } ?>
		<label class="checkbox-inline"><input type="radio" name="level" value="2" <?=$level2;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 2 спицы <?php } else { ?> двухзначные <?php } ?></label>
		<label class="checkbox-inline"><input type="radio" name="level" value="3" <?=$level3;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 3 спицы <?php } else { ?> трехзначные <?php } ?></label>
		<?php if($id_job != 3 && $id_job != 4) { ?>
			<label class="checkbox-inline"><input type="radio" name="level" value="4" <?=$level4;?>> <?php if($id_job == 2 || $id_job == 7) { ?> 4 спицы <?php } else { ?> четырехзначные <?php } ?></label>
		<?php } ?>
	<?php } ?>

    <?php if ( $id_job == 3 ): ?>
        <hr><h3>Примеры до</h3>
        <?php for ( $i = 2; $i <= 9; $i++ ): ?>
            <label class="radio-inline"><input type="radio" name="examples_up_to_x" value="<?=$i;?>" <?=$examples_up_to_x == $i ? 'checked' : '';?>> Примеры до <?=$i;?>-х</label>
        <?php endfor; ?>
    <?php endif; ?>

	<?php if($id_job == 8) { ?>
		<hr><h3>Вид заданий</h3>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="operation" value="1" <?=$oper1;?>> СЛОЖЕНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="operation" value="2" <?=$oper2;?>> УМНОЖЕНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="operation" value="3" <?=$oper3;?>> ВЫЧИТАНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="operation" value="4" <?=$oper4;?>> ДЕЛЕНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="operation" value="5" <?=$oper5;?>> СЛОЖЕНИЕ И ВЫЧИТАНИЕ</label>
		<label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="operation" value="6" <?=$oper6;?>> УМНОЖЕНИЕ И ДЕЛЕНИЕ</label>
	<?php } ?>

	<?php if($id_job == 8 || $id_job == 2) { ?>
		<span id="delay_dom_elem">
            <hr><h3>Задержка</h3>
            <?php if ($id_job == 2): ?>
                <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="100" <?=$dl01;?>> 0,1 сек.</label>
                <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="200" <?=$dl02;?>> 0,2 сек.</label>
            <?php endif; ?>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="300" <?=$dl1;?>> 0,3 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="400" <?=$dl2;?>> 0,4 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="500" <?=$dl3;?>> 0,5 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="600" <?=$dl4;?>> 0,6 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="700" <?=$dl5;?>> 0,7 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="800" <?=$dl6;?>> 0,8 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="900" <?=$dl7;?>> 0,9 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1000" <?=$dl8;?>> 1 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1100" <?=$dl9;?>> 1,1 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1200" <?=$dl10;?>> 1,2 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1300" <?=$dl11;?>> 1,3 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1400" <?=$dl12;?>> 1,4 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline" data-toggle="tooltip" data-placement="top" title="Tooltip on top"><input type="radio" name="delay" value="1500" <?=$dl13;?>> 1,5 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1600" <?=$dl14;?>> 1,6 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1700" <?=$dl15;?>> 1,7 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1800" <?=$dl16;?>> 1,8 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="1900" <?=$dl17;?>> 1,9 сек.</label>
            <label style="margin:0;padding-left:10px;" class="checkbox-inline"><input type="radio" name="delay" value="2000" <?=$dl18;?>> 2 сек.</label>
        </span>
	<?php } ?>

    <!-- В тренажер «определить цифру» добавить функцию «Мелькание» и рядом стоит галочка «Двойное» -->
    <?php if ( $id_job  == 2 ): ?>
        <hr><h3>Считать ментально</h3>
        <label class="checkbox-inline"><input type="checkbox" name="map_twinkle" <?=$map_twinkle?>> Двойное</label>
    <?php endif; ?>

    <!-- На тренажере «Решение примеров» добавить функцию «Считать ментально» -->
    <?php if ( $id_job == 3 || $id_job == 4 || $id_job == 5 ): ?>
        <hr><h3>Мелькание</h3>
        <label class="checkbox-inline"><input type="checkbox" name="count_mentally" <?=$count_mentally?>> Считать ментально</label>
    <?php endif; ?>

	<?php if($id_job == 1 || $id_job == 8 || $id_job == 3 || $id_job == 4 || $id_job == 5 || $id_job == 6 || $id_job == 7) { ?>
		<hr><h3>Озвучка</h3>
		<!--<label class="checkbox-inline"><input type="radio" name="voice" value="1" <?/*=$voice1;*/?>> С голосом</label>-->
		<label class="checkbox-inline"><input type="radio" name="voice" value="2" <?=$voice2;?>> Произношение цифр</label>
		<label class="checkbox-inline"><input type="radio" name="voice" value="3" <?=$voice3;?>> <?= in_array($id_job, [3, 4, 5]) ? "Показ цифр" : "Мелькание цифр"?></label>
	<?php } ?>

    <?php if( $id_job == 1 || $id_job == 6 || $id_job == 7 || $id_job == 8 || $id_job == 3 || $id_job == 4 || $id_job == 5  ): ?>
        <span id="voice_lang_dom_elem">
        <hr>
        <h3>Язык</h3>
            <label class="radio-inline"><input type="radio" name="voice_lang" value="ru-RU" <?=$lang_ru;?>> Русский</label>
            <label class="radio-inline"><input type="radio" name="voice_lang" value="en-US" <?=$lang_en;?>> Английский</label>
        </span>
    <?php endif; ?>

	<hr>
	<p id="btn_settingsReset" class="btn btn-danger btn-lg">Сбросить настройки</p>
	<button type="button" id="btn_settingsSave" class="btn btn-success btn-lg">Начать</button>
	<!--<p id="btn_settingsClose" class="btn btn-danger">Закрыть</p>-->

</form>
<?php endif; ?>