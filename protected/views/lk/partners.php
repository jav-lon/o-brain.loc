<div class="row">
    <div class="col-lg-12">
	
		<h3 class="page-header">Партнеры</h3>
		
		<?php if( $partners = UsersAr::model()->findAll("type='partner'") ) { 
			foreach($partners as $partner) { ?>
				<p>Город: <b><?=$partner->city;?></b></p>
				<p>ФИО: <b><?=$partner->fio;?></b></p>
				<!--
				<h4><?=$partner->fio;?></h4>
				<p>Регистрационный номер: <b><?=$partner->id_teacher;?></b></p>
				<p>Дата рождения: <b><?=$partner->date_rojd;?></b></p>
				<p>Город: <b><?=$partner->city;?></b></p>
				<p>Эл. почта: <b><?=$partner->email;?></b></p>
				<p>Запасная эл. почта: <b><?=$partner->email_reserv;?></b></p>
				<p>Телефон: <b><?=$partner->phone;?></b></p>
				<p>Запасной телефон: <b><?=$partner->phone_reserv;?></b></p>
				<p>Адрес: <b><?=$partner->addr;?></b></p>
				<p>Запасной адрес: <b><?=$partner->addr_reserv;?></b></p>
				<p>Номер договора: <b><?=$partner->num_dogovor;?></b></p>
				<p>Дата договора: <b><?=$partner->date_dogovor;?></b></p>
				<p>Размер роялти: <b><?=$partner->royalty;?></b></p>
				<p>Дата оплаты роялти: <b><?=$partner->date_royalty;?></b></p>
				<p>Комментарий к оплате роялти: <b><?=$partner->comment_royalty;?></b></p>
				<p>Преподаватели партнера: <?php 
				if( $teachers = UsersAr::model()->findAll("id_who_learn='".$partner->id."'") ) { 
					foreach($teachers as $teacher) {
						echo '<b>'.$teacher->fio.'</b>. ';
					}
				} ?></p>
				-->
				<form style="display:inline-block;margin-bottom:5px;" action="/lk/regPartner" method="post">
					<input type="hidden" name="id_partner" value="<?=$partner->id;?>">
					<button name="edit_partner" type="submit" class="btn btn-success btn-sm">Просмотр / Редактировать</button>
				</form>
				<form style="display:inline-block;margin-bottom:5px;" action="/lk/partners" method="post">
					<input type="hidden" name="id_partner" value="<?=$partner->id;?>">
					<button name="lock_partner" type="submit" class="btn btn-danger btn-sm"><?php if($partner->locked == 'no') echo 'Заблокировать'; else echo 'Разблокировать'; ?></button>
				</form>
				<form style="display:inline-block;margin-bottom:5px;" action="/lk/partners" method="post">
					<input type="hidden" name="id_partner" value="<?=$partner->id;?>">
					<button name="delete_partner" type="submit" class="btn btn-danger btn-sm">Удалить</button>
				</form>
				<a class="btn btn-success btn-sm" title="Открыть отчеты" href="<?=$this->createAbsoluteUrl('/').'/lk/reports?id_partner='.$partner->id;?>">Отчеты</a>
				<form action="/lk/partners" method="post">
					<input type="hidden" name="id_partner" value="<?=$partner->id;?>">
					<textarea name="text_email" class="form-control" rows="2"></textarea>
					<button style="margin-top:5px;" name="send_email_to_partner" type="submit" class="btn btn-info btn-sm">Отправить письмо</button>
				</form>
				<hr>
			<?php } ?>
		<?php } ?>
		
		<a style="margin-bottom:20px;" href="<?=$this->createAbsoluteUrl('/').'/lk/regPartner';?>" type="button" class="btn btn-primary">Создать партнера</a>
    </div>
</div>