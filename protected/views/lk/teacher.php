<div class="row">
    <div class="col-lg-12">
		<?php 		
			$user = UsersAr::model()->findByPk(Yii::app()->session['login_user']); ?>
			<!--заголовок-->
			<h3 class="page-header"><?=$user->fio;?></h3>
			<?php if(Yii::app()->session['status'] != '') { echo '<p class="status">'.Yii::app()->session['status'].'</p>'; Yii::app()->session['status'] = ''; } ?>
			<p>Статус: <b style="color:#666666;">преподаватель</b></p>
			<p>ID: <b style="color:#666666;"><?=$user->id_teacher;?></b></p>
			<p>Персональная ссылка: <b style="color:#666666;"><?=$this->createAbsoluteUrl('/') . '/lk/registration/' . $user->id_teacher;?></b></p>
			<hr>
			<h3>Мои ученики</h3>
			<?php if( $students = UsersAr::model()->findAll("id_who_learn=".$user->id) ) { ?>
				<table class="table table-bordered table-striped table-responsive">
					<thead><tr>
							<th>ФИО ученика</th><th>Дата регистрации</th><th>Город</th><th>Последнее задание</th><th>Пароль</th><th>Действия</th>
					</tr></thead>
					<tbody> <?php 
						foreach($students as $student) { ?>
							<tr>
								<td><?php
									$form=$this->beginWidget('CActiveForm', array(
									'id'=>'operuser-form',
									'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
									echo $form->hiddenField($modeloperuser,'id',array('value'=>$student->id));
									echo $form->hiddenField($modeloperuser,'operation',array('value'=>'view_student'));
									$fio = $student->fio;
									echo CHtml::submitButton($fio,array('class'=>'btn btn-link my_btn-link','style'=>'padding:0;'));
									$this->endWidget(); ?>
								</td>
								<td>
									<?php echo date("d-m-Y H:i:s", $student->data_reg); ?>
								</td>
								<td>
									<?php echo $student->city; ?>
								</td>
								<td>
									<?php if( $job = JobsAr::model()->findByAttributes(array('id_user'=>$student->id)) ) echo $job->name; ?>
								</td>
								<td>
									<?php echo $student->password; ?>
								</td>
								<td><?php 
									$form=$this->beginWidget('CActiveForm', array(
									'id'=>'operuser-form',
									'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
									echo $form->hiddenField($modeloperuser,'id',array('value'=>$student->id));
									echo $form->hiddenField($modeloperuser,'operation',array('value'=>'lock'));
									if($student->locked == 'no') {$btn_lock_text = 'Заблокировать'; $style_btn="btn-success";} else {$btn_lock_text = 'Разблокировать';$style_btn="btn-danger";}
									echo CHtml::submitButton($btn_lock_text,array('class'=>"btn {$style_btn}",'style'=>'padding:0 7px;'));
									$this->endWidget(); ?>
									<a class="btn btn-info btn-xs" style="margin-top:3px;" href="<?php echo $this->createAbsoluteUrl('/').'/lk/payment?user='.$student->id; ?>">Оплата</a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			<?php } ?>
    </div>
</div>