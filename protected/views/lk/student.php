<div class="row">
    <div class="col-lg-12">
		<?php 
			if( Yii::app()->session['id_view_user'] != '' ) $id_user = Yii::app()->session['id_view_user']; // если смотрит студента его учитель
			else $id_user = Yii::app()->session['login_user']; // иначе студент сам на своей странице
			
			$user = UsersAr::model()->findByPk($id_user); ?>
			<h3 class="page-header"><?=$user->fio;?></h3>
			<?php if(Yii::app()->session['status'] != '') { echo '<p class="status">'.Yii::app()->session['status'].'</p>'; Yii::app()->session['status'] = ''; } ?>

			<h3>Кол-во выполненных упражнений</h3>

            <?php
            // podklyucheni db s pomoshyu DAO
            $connection = Yii::app()->db; //
            $command = $connection->createCommand("SELECT COUNT(*) FROM simulators_results WHERE `id_simulator`=9 AND `id_user`= :id_user;");
            $command->bindParam(':id_user', $id_user, PDO::PARAM_STR);
            $result = $command->queryAll();

            $name_job_9_rating = $result[0]['COUNT(*)'];
            //var_dump($result, $name_job_9_rating); exit;
            ?>

			<?php $name_job_1 = [];$name_job_2 = [];$name_job_3 = [];$name_job_4 = [];$name_job_5 = [];$name_job_6 = [];$name_job_7 = [];$name_job_8 = [];
			$name_job_1_rating=0;$name_job_2_rating=0;$name_job_3_rating=0;$name_job_4_rating=0;$name_job_5_rating=0;$name_job_6_rating=0;$name_job_7_rating=0;$name_job_8_rating=0;
			if( $jobs = JobsAr::model()->findAll("id_user={$id_user}") ) {
				foreach($jobs as $job) {
//					if($job->name == 'Набор цифр на Абакусе' && $job->rating != NULL) $name_job_1[] = $job->rating;
//					if($job->name == 'Определение числа с карты Абакус' && $job->rating != NULL) $name_job_2[] = $job->rating;
//					if($job->name == 'Примеры на сложение / вычитание' && $job->rating != NULL) $name_job_3[] = $job->rating;
//					if($job->name == 'Примеры на умножение' && $job->rating != NULL) $name_job_4[] = $job->rating;
//					if($job->name == 'Примеры на деление' && $job->rating != NULL) $name_job_5[] = $job->rating;
//					if($job->name == 'Bemoreclever' && $job->rating != NULL) $name_job_6[] = $job->rating;
//					if($job->name == 'Bottomofabac' && $job->rating != NULL) $name_job_7[] = $job->rating;
//					if($job->name == 'Mentalmap' && $job->rating != NULL) $name_job_8[] = $job->rating;
                    if($job->name == 'Набор цифр на Абакусе') $name_job_1_rating++;
					if($job->name == 'Определение числа с карты Абакус') $name_job_2_rating++;
					if($job->name == 'Примеры на сложение / вычитание') $name_job_3_rating++;
					if($job->name == 'Примеры на умножение') $name_job_4_rating++;
					if($job->name == 'Примеры на деление') $name_job_5_rating++;
					if($job->name == 'Bemoreclever') $name_job_6_rating++;
					if($job->name == 'Bottomofabac') $name_job_7_rating++;
					if($job->name == 'Mentalmap') $name_job_8_rating++;
				}
			}
//			foreach($name_job_1 as $job) { $name_job_1_rating += $job; }
//			foreach($name_job_2 as $job) { $name_job_2_rating += $job; }
//			foreach($name_job_3 as $job) { $name_job_3_rating += $job; }
//			foreach($name_job_4 as $job) { $name_job_4_rating += $job; }
//			foreach($name_job_5 as $job) { $name_job_5_rating += $job; }
//			foreach($name_job_6 as $job) { $name_job_6_rating += $job; }
//			foreach($name_job_7 as $job) { $name_job_7_rating += $job; }
//			foreach($name_job_8 as $job) { $name_job_8_rating += $job; }
			?>
			<table class="table table-bordered table-striped table-responsive">
				<thead><tr><th>Упражнение</th><th>Кол-во</th></tr></thead>
				<tbody>
<!--					<tr><td>Набор цифр на Абакусе</td><td>--><?php //if(count($name_job_1)>0) $name_job_1_rating = round($name_job_1_rating / count($name_job_1)); echo $name_job_1_rating; ?><!--</td></tr>-->
<!--					<tr><td>Определение числа с карты Абакус</td><td>--><?php //if(count($name_job_2)>0) $name_job_2_rating = round($name_job_2_rating / count($name_job_2)); echo $name_job_2_rating; ?><!--</td></tr>-->
<!--					<tr><td>Примеры на сложение / вычитание</td><td>--><?php //if(count($name_job_3)>0) $name_job_3_rating = round($name_job_3_rating / count($name_job_3)); echo $name_job_3_rating; ?><!--</td></tr>-->
<!--					<tr><td>Примеры на умножение</td><td>--><?php //if(count($name_job_4)>0) $name_job_4_rating = round($name_job_4_rating / count($name_job_4)); echo $name_job_4_rating; ?><!--</td></tr>-->
<!--					<tr><td>Примеры на деление</td><td>--><?php //if(count($name_job_5)>0) $name_job_5_rating = round($name_job_5_rating / count($name_job_5)); echo $name_job_5_rating; ?><!--</td></tr>-->
<!--					<tr><td>Bemoreclever</td><td>--><?php //if(count($name_job_6)>0) $name_job_6_rating = round($name_job_6_rating / count($name_job_6)); echo $name_job_6_rating; ?><!--</td></tr>-->
<!--					<tr><td>Bottomofabac</td><td>--><?php //if(count($name_job_7)>0) $name_job_7_rating = round($name_job_7_rating / count($name_job_7)); echo $name_job_7_rating; ?><!--</td></tr>-->
<!--					<tr><td>Mentalmap</td><td>--><?php //if(count($name_job_8)>0) $name_job_8_rating = round($name_job_8_rating / count($name_job_8)); echo $name_job_8_rating; ?><!--</td></tr>-->
<!--					<tr><td><b>ИТОГО:</b></td><td>--><?php //echo $name_job_1_rating+$name_job_2_rating+$name_job_3_rating+$name_job_4_rating+$name_job_5_rating+$name_job_6_rating+$name_job_7_rating+$name_job_8_rating; ?><!--</td></tr>-->

                    <tr><td>Набор цифр на Абакусе</td><td><?=$name_job_1_rating;?></td></tr>
                    <tr><td>Определение числа с карты Абакус</td><td><?=$name_job_2_rating;?></td></tr>
                    <tr><td>Примеры на сложение / вычитание</td><td><?=$name_job_3_rating;?></td></tr>
                    <tr><td>Примеры на умножение</td><td><?=$name_job_4_rating;?></td></tr>
                    <tr><td>Примеры на деление</td><td><?=$name_job_5_rating;?></td></tr>
                    <tr><td>Bemoreclever</td><td><?=$name_job_6_rating;?></td></tr>
                    <tr><td>Bottomofabac</td><td><?=$name_job_7_rating;?></td></tr>
                    <tr><td>Mentalmap</td><td><?=$name_job_8_rating;?></td></tr>
                    <tr><td>Smarty</td><td><?=$name_job_9_rating;?></td></tr>
                    <tr><td><b>ИТОГО:</b></td><td><?php echo $name_job_1_rating+$name_job_2_rating+$name_job_3_rating+$name_job_4_rating+$name_job_5_rating+$name_job_6_rating+$name_job_7_rating+$name_job_8_rating+$name_job_9_rating; ?></td></tr>
				</tbody>
			</table>
			<h3>Упражнения</h3>

			<!-- изначально, группировка по датам -->
			<?php
            $jobs = JobsAr::model()->findAllBySql("SELECT *,FROM_UNIXTIME(date, '%Y-%m-%d') as f_date FROM `jobs` where id_user=".$user->id." group by f_date order by f_date DESC");

            $sql_smarty = "SELECT *,FROM_UNIXTIME(created_at, '%Y-%m-%d') as f_date FROM `simulators_results` where id_user=".$user->id." and id_simulator=9 group by f_date order by f_date DESC";
            $simulators_results = SimulatorsResultsAr::model()->findAllBySql($sql_smarty);

            $jobs_array = [];
            $simulators_results_array = [];

            foreach ($jobs as $job) {
                $jobs_array[] = $job->date;
            }
            foreach ($simulators_results as $simulators_result) {
                $simulators_results_array[] = $simulators_result->created_at;
            }
            for($i = 0; $i < count($simulators_results_array); $i++) {
                if (!in_array($simulators_results_array[$i], $jobs_array)) $jobs_array[] = $simulators_results_array[$i];
            }

            rsort($jobs_array);

            if( $jobs_array  ) { ?>
					<?php foreach($jobs_array as $item) {
						echo '<a class="a_dateJob" href="/lk/student/?dateJob='.date("Y-m-d", $item).'">'.date("d-m-Y", $item).'</a>';
					} ?>
			<?php } else { ?>

            <?php } ?>
            <br><br>
           <!-- <h3>Упражнения Smarty</h3>
            <a class="btn btn-default" href="/result/smarty-user-results?id=<?/*=$id_user*/?>" role="button">Просмотреть (Smarty)</a> <br> <br>-->

			<!-- вывод списка заданий по конкретной дате -->
			<?php if(isset($_GET["dateJob"])) { ?>

                <?php
                $jobs = JobsAr::model()->findAllBySql("SELECT *,FROM_UNIXTIME(date, '%Y-%m-%d') as f_date FROM `jobs` where id_user=".$user->id." AND FROM_UNIXTIME(date, '%Y-%m-%d') = '".$_GET["dateJob"]."' order by f_date DESC");

                $sql_smarty = "SELECT *,FROM_UNIXTIME(created_at, '%Y-%m-%d') as f_date FROM `simulators_results` where id_user=".$user->id." and FROM_UNIXTIME(created_at, '%Y-%m-%d')='".$_GET["dateJob"]."' and id_simulator=9 order by f_date DESC";
                $simulators_results = SimulatorsResultsAr::model()->findAllBySql($sql_smarty);

                $jobs_array = [];
                $simulators_results_array = [];

                $i = 0;
                foreach ($jobs as $job) {
                    $jobs_array[$i]['id'] = $job->id;
                    $jobs_array[$i]['name'] = $job->name;
                    $jobs_array[$i]['time'] = $job->time;
                    $jobs_array[$i]['date'] = $job->date;
                    $jobs_array[$i]['errors'] = $job->errors;
                    $jobs_array[$i++]['rating'] = $job->rating;
                }

                $i = 0;
                foreach ($simulators_results as $simulators_result) {
                    $simulators_results_array[$i]['id'] = $simulators_result->id;
                    $simulators_results_array[$i]['name'] = 'Smarty';
                    $simulators_results_array[$i]['time'] = $simulators_result->time / 1000;
                    $simulators_results_array[$i]['date'] = $simulators_result->created_at;
                    $simulators_results_array[$i]['errors'] = $simulators_result->count_errors;
                    $simulators_results_array[$i++]['rating'] = $simulators_result->rating;
                }
                for($i = 0; $i < count($simulators_results_array); $i++) {
                    $jobs_array[] = $simulators_results_array[$i];
                }
                /*var_dump($jobs_array);
                exit;*/

                usort($jobs_array, function ($a,$b) {
                    if ($a['date'] == $b['date']) {
                        return 0;
                    }
                    return ($a['date'] < $b['date']) ? -1 : 1;
                });
                /*var_dump($jobs_array);
                exit;*/
                ?>

				<?php if( $jobs_array ) { ?>
					<table class="table table-bordered table-striped table-responsive">
						<thead>
                            <tr>
                                <th>Упражнение</th><th>Затраченное время</th><th>Дата выполнения</th><th>Кол-во ошибок</th><th>Рейтинг</th>
                            </tr>
                        </thead>
						<tbody> <?php 
							foreach($jobs_array as $job) { ?>
								<tr>
									<td>
										<?php echo '<a href="/lk/student/?jobDetailed='.$job['id'].'&smarty='.($job["name"] == 'Smarty' ? 'yes' : 'no').'">'.$job["name"].'</a>'; ?>
									</td>
									<td>
										<?php echo $job['time']; ?>
									</td>
									<td>
										<?php echo date("d-m-Y H:i:s", $job['date']); ?>
									</td>
									<td>
										<?php echo $job['errors']; ?>
									</td>
									<td>
										<?php echo $job['rating']; ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
			<?php } ?>

			<!-- вывод конкретного детализованного задания -->
			<?php if(isset($_GET["jobDetailed"]) && isset($_GET['smarty'])) { ?>
				<?php if($_GET['smarty'] == 'no'): ?>
                    <?php if( $jobs = JobDetailedAr::model()->findAllBySql("SELECT * FROM `job_detailed` where id_job=".$_GET["jobDetailed"]." order by num ASC") ) { ?>
                        <table class="table table-bordered table-striped table-responsive">
                            <thead><tr>
                                <th>№ примера</th><th>Вопрос</th><th>Ответ</th>
                            </tr></thead>
                            <tbody> <?php
                            foreach($jobs as $job) { ?>
                                <tr>
                                    <td>
                                        <?php echo $job->num; ?>
                                    </td>
                                    <td>
                                        <?php echo $job->question; ?>
                                    </td>
                                    <td>
                                        <?php echo $job->answer; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <h4>ИТОГО:</h4>
                    <?php $job = JobsAr::model()->findByPk($_GET["jobDetailed"]); ?>
                    <p>Упражнение: <?=$job->name;?></p>
                    <p>Дата выполнения: <?=date("d-m-Y H:i:s", $job->date);?></p>
                    <p>Затраченное время: <?=$job->time;?></p>
                    <p>Кол-во ошибок: <?=$job->errors;?></p>
                    <p>Рейтинг: <?=$job->rating;?></p>
                <?php endif; ?>

                <?php if ($_GET['smarty'] == 'yes'): ?>
                    <?php if( $jobs = SimulatorsResultsAr::model()->findByPk($_GET["jobDetailed"]) ) { ?>
                        <?php
                        $examplesResults = unserialize($jobs->qa);
                        if ($examplesResults != false) {
                        ?>
                            <table class="table table-bordered table-striped table-responsive">
                                <thead><tr>
                                    <th>№ примера</th><th>Вопрос</th><th>Ответ</th>
                                </tr></thead>
                                <tbody> <?php

                                for($i = 0; $i < count($examplesResults[0]); $i++) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $i+1; ?>
                                        </td>
                                        <td>
                                            <?php echo $examplesResults[0][$i]; ?>
                                        </td>
                                        <td>
                                            <?php echo $examplesResults[1][$i]; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        <?php } else {?>
                                <!--<div class="well">
                                    Нет данных
                                </div>-->
                            <? } ?>
                    <h4>ИТОГО:</h4>
                    <p>Упражнение: <?='Smarty'?></p>
                    <p>Дата выполнения: <?=date("d-m-Y H:i:s", $jobs->created_at);?></p>
                    <p>Затраченное время: <?=$jobs->time/1000;?></p>
                    <p>Кол-во ошибок: <?=$jobs->count_errors;?></p>
<!--                    <p>Рейтинг: --><?//=$job->rating;?><!--</p>-->
                    <?php } ?>
                <?php endif; ?>
				
			<?php } ?>

            <!-- История посещений личного кабинета -->
            <hr>
            <h3>История посещений личного кабинета</h3>
            <?php if( $logs = LogAr::model()->findAll("id_user=".$id_user) ) { ?>
                <table class="table table-bordered table-striped table-condensed table-responsive">
                    <thead><tr>
                        <th>Дата посещения</th><th>Длительность сессии</th>
                    </tr></thead>
                    <tbody> <?php
                    foreach($logs as $log) { ?>
                        <tr>
                            <td>
                                <?php echo date("d-m-Y H:i:s", $log->data_auth_in); ?>
                            </td>
                            <td>
                                <?php echo date("H:i:s", (int)$log->data_auth_out - (int)$log->data_auth_in);
                                //echo $log->time_session; ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } ?>

    </div>
</div>