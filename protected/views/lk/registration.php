<?php

$connection = Yii::app()->db;

$command = $connection->createCommand('SELECT * FROM `privacy_policy_agreement`');

$row_ppa = $command->queryRow();

//var_dump($row_ppa); exit;

?>

<div class="row">
    <div class="col-lg-4">
	
			<!-- ====== регистрация ====== -->
			<h2 class="page-header">Регистрация</h2>
			<?php if($notice_OK == ''): ?>
				<p style="color:red;"><?=$notice;?></p>
				<div class="form" style="width:270px;">
					<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'registration-form',
							'clientOptions'=>array('validateOnSubmit'=>true),
							'htmlOptions'=>array('class'=>'form-horizontal', 'data-input-type' => 'true')
					)); ?>
						<div id="dop_block_login">
							
							<div class="div_inp_login">
								<p style="margin:13px 0 5px;">Вы являетесь:</p>
								<?php echo $form->radioButtonList($modelregistration,'type',array(
                                    'student'=>'Учеником','teacher'=>'Преподавателем')/*$type_array*/
                                ); ?>
							</div>

							<div class="div_inp_login" id="id_prepod">
								<p style="margin:13px 0 5px;">ID преподавателя</p>
								<?php $get_id_teacher_teacher = explode ('/', $_SERVER['REQUEST_URI']); 
								if(isset($get_id_teacher_teacher[3]) && $get_id_teacher_teacher[3] != '') {
                                    $text_for_id_teacher = $get_id_teacher_teacher[3];
                                    $modelregistration->id_teacher = $text_for_id_teacher;
                                }
								else $text_for_id_teacher = 'Введите ID вашего преподавателя';	?>
								<?php echo $form->textField($modelregistration,'id_teacher',array('class'=>'input_f_logon form-control',/*'value'=>'',*/'placeholder'=>$text_for_id_teacher)); ?>
							</div>
							
							<div class="div_inp_login">
								<p style="margin:13px 0 5px;">Ваши Ф.И.О.</p>
								<?php echo $form->textField($modelregistration,'fio',array('class'=>'input_f_logon form-control','value'=>'')); ?>
							</div>
							
							<div class="div_inp_login">
								<p style="margin:13px 0 5px;">Ваш возраст</p>
								<?php echo $form->textField($modelregistration,'age',array('class'=>'input_f_logon form-control','value'=>'')); ?>
							</div>
							
							<div class="div_inp_login">
								<p style="margin:13px 0 5px;">Город проживания</p>
								<?php echo $form->textField($modelregistration,'city',array('class'=>'input_f_logon form-control','value'=>'')); ?>
							</div>
							
							<div class="div_inp_login">
								<p style="margin:13px 0 5px;">Ваша эл. почта</p>
								<?php echo $form->textField($modelregistration,'email',array('class'=>'input_f_logon form-control','value'=>'')); ?>
							</div>
							
							<div class="div_inp_login">
								<p style="margin:13px 0 5px;">Ваш телефон</p>
								<?php echo $form->textField($modelregistration,'phone',array('class'=>'input_f_logon form-control','value'=>'+7')); ?>
							</div>
							
							<div class="div_inp_login">
								<p style="margin:13px 0 0 0;">Я согласен на обработку и хранения предоставленных мною данных</p>
								<?php echo $form->checkbox($modelregistration,'allow_personal_date',array('style'=>'width:auto;','class'=>'input_f_logon form-control','value'=>'1')); ?>
							</div>

                            <div class="div_inp_login">
                                <div class="reg_captcha">
                                    <?php Yii::app()->session['captcha'] = mt_rand(10000, 99999); echo Yii::app()->session['captcha']; ?>
                                </div>
                                <p style="margin:13px 0 0 0;">Введите указанные символы</p>
                                <?php echo $form->textField($modelregistration,'captcha',array('class'=>'input_f_logon form-control','value'=>'', 'autocomplete' => 'off')); ?>
                            </div>
						</div>
                        <br>
						<?php echo CHtml::submitButton('ЗАРЕГИСТРИРОВАТЬСЯ',array('class'=>'btn btn-primary btn-confirmContact','name'=>'btnLoginAdmin')); ?>
						<?php $this->endWidget(); ?>
				</div>
				<br>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/remind">Восстановить забытый пароль</a>
				
			<?php else: ?>
				<p style="color:green;"><?=$notice_OK;?></p>
				<a href="<?= Yii::app()->request->baseUrl; ?>/lk">Авторизация</a>
			<?php endif; ?>
			
    </div>
    <div class="col-lg-8">
        <div id="privacy_policy_text" class="panel panel-default" style="margin-left:40px; display: none; position:fixed; top:100px; right:50px; height:500px; width:inherit; max-width: 600px">
            <div class="panel-heading" style="position:relative;">
                <button id="close_button" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3><strong><?= $row_ppa['title_ppa'] ?></strong></h3>
            </div>
            <div class="panel-body" style="position: relative; overflow-y: auto; width: auto; height: 370px; max-width: 600px">
                <?= $row_ppa['text_ppa']; ?>
            </div>
        </div>
    </div>
</div>

<script>
    var privacy_policy_agreement_checkbox_elem = $('#RegistrationForm_allow_personal_date'); // checkbox
    var close_button = $('#close_button');

    privacy_policy_agreement_checkbox_elem.change(function () {
       if($(this).prop('checked')) {//esli polzovatel stavil galochku otkroetsya vsplivayushiy okno
           // window.open('/privacy-policy', '_blank');

           $('#privacy_policy_text').show();
       }
    });

    close_button.click(function () {
        $('#privacy_policy_text').hide();
    });
</script>