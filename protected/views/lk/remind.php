<div class="row">
    <div class="col-lg-12">
	
			<!-- ====== восстановление пароля ====== -->
			<h2 class="page-header">Восстановление пароля</h2>
			<p style="color:red;"><?=$notice;?></p>
			<p style="color:green;"><?=$notice_OK;?></p>
			<div class="form" style="width:250px;">
				<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'remind-form',
						'clientOptions'=>array('validateOnSubmit'=>true,),
						'htmlOptions'=>array('class'=>'form-horizontal',),
				)); ?>
					<div id="dop_block_login">
						<div class="div_inp_login">
							<p style="margin:13px 0 5px;">Ваш email</p>
							<?php echo $form->textField($modelremind,'email',array('class'=>'input_f_logon form-control','value'=>'')); ?>
						</div>
					</div><br>
					<?php echo CHtml::submitButton('ВОССТАНОВИТЬ',array('class'=>'btn btn-primary btn-confirmContact','name'=>'btnLoginAdmin')); ?>
					<?php $this->endWidget(); ?>
			</div>
			<br>
			<p>Пароль для входа в личный кабинет будет отправлен на вашу эл. почту</p>
    </div>
</div>