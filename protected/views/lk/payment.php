<div class="row">
    <div class="col-lg-12">
	
		<h2 class="page-header">Оплата</h2>
		<p style="color:green;"><?=$notice;?></p>
		
		<?php 
		if( isset($_GET["user"]) && $pays = PaymentAr::model()->findAll("id_user=".$_GET["user"]) ) {
			$itog_summ = 0; $itog_payed_class = 0; $itog_worked_class = 0; $itog_passed_class = 0;
			foreach($pays as $pay) {
				$itog_summ = $itog_summ + $pay->summ;
				$itog_payed_class = $itog_payed_class + $pay->count_payed_class;
				$itog_worked_class = $itog_worked_class + $pay->count_worked_class;
				$itog_passed_class = $itog_passed_class + $pay->count_passed_class;
			}
		} ?>
		
		<?php if( isset($_GET["user"]) && $user = UsersAr::model()->findByPk($_GET["user"]) ) { // если вобще есть такой юзер ?>
			<table class="table table-bordered table-striped table-responsive">
				<thead>
					<tr>
						<th>- Дата начала занятий<br>- Общая сумма<br>-<br>- Кол-во оплаченных занятий<br>- Кол-во проведенных занятий<br>- Кол-во пропущенных занятий</th>
						<?php if( isset($_GET["user"]) && $pays = PaymentAr::model()->findAll("id_user=".$_GET["user"]) ) {
						foreach($pays as $pay) { ?>
							<th>- Дата оплаты<br>- Сумма<br>- Оплаченный период до...<br>- Кол-во оплаченных занятий<br>- Кол-во проведенных занятий<br>- Кол-во пропущенных занятий</th>
						<?php } } ?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>- <?=date("d.m.Y", $user->data_reg);?><br>- <?=$itog_summ;?><br>-<br>- <?=$itog_payed_class;?><br>- <?=$itog_worked_class;?><br>- <?=$itog_passed_class;?></td>
						<?php if( isset($_GET["user"]) && $pays = PaymentAr::model()->findAll("id_user=".$_GET["user"]) ) {
						foreach($pays as $pay) { ?>
							<td>- <?=$pay->data_pay;?><br>- <?=$pay->summ;?><br>- <?=$pay->pay_do;?><br>- <?=$pay->count_payed_class;?><br>- <?=$pay->count_worked_class;?><br>- <?=$pay->count_passed_class;?><?php if($pay->uploadfile != NULL) { ?><br><a href="<?=$pay->uploadfile;?>" target="_blanc">Посмотреть платежный документ</a><?php } ?></td>
						<?php } } ?>
					</tr>
				</tbody>
			</table>
		<?php } ?>
		
		<?php $type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type != 'student') { ?>
			<!-- === добавить оплату === -->
			<hr><h3>Добавить оплату</h3>
			<form class="form-inline" action="/lk/payment?user=<?=$_GET["user"];?>" method="post" enctype="multipart/form-data">
			
				<p style="margin:13px 0 5px;"><b>Дата оплаты</b></p>
				<div class="form-group">
					<input type="text" class="form-control" name="data_pay" value="<?=date("d.m.Y", time());?>">
				</div><br>
				
				<p style="margin:13px 0 5px;"><b>Сумма</b></p>
				<div class="form-group">
					<input type="text" class="form-control" name="summ" value="">
				</div><br>
				
				<p style="margin:13px 0 5px;"><b>До какого числа</b></p>
				<div class="form-group">
					<input type="text" class="form-control" name="pay_do" value="">
				</div><br>
				
				<p style="margin:13px 0 5px;"><b>За какое кол-во занятий</b></p>
				<div class="form-group">
					<input type="text" class="form-control" name="count_payed_class" value="">
				</div><br>
				
				<p style="margin:13px 0 5px;"><b>Кол-во проведенных занятий в прошлый период</b></p>
				<div class="form-group">
					<input type="text" class="form-control" name="count_worked_class" value="">
				</div><br>
				
				<p style="margin:13px 0 5px;"><b>Комментарий</b></p>
				<div class="form-group">
					<input type="text" class="form-control" name="comment" value="">
				</div><br>
				
				<p style="margin:13px 0 5px;"><b>Прикрепить платежный документ</b></p>
				<div class="form-group">
					<input type="file" name="uploadfile">
				</div><br>
				
				<br>
				<input type="hidden" name="id_user" value="<?=$_GET["user"];?>">
				<button name="add_pay" type="submit" class="btn btn-success">Добавить</button>
				<br><br>
			</form>
		<?php } ?>
		
    </div>
</div>