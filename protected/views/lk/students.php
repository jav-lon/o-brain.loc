<div class="row">
    <div class="col-lg-12">
	
		<?php // смотрим список учеников, либо конкретного ученика
		$id_user = explode ( '/', $_SERVER['REQUEST_URI']);
		if( isset($id_user[3]) && $id_user[3] != '' ) {
			if( $user = UsersAr::model()->findByPk($id_user[3]) ) { ?>
			<!-- КОНКРЕТНЫЙ УЧЕНИК -->
			
				<?php Yii::app()->session['id_view_user'] = $id_user[3]; ?>
				<h3 class="page-header">Ученик <?=$user->fio;?></h3>
				<p style="color:red;"><?=$notice;?></p>
				<table class="table table-bordered table-striped table-responsive">
						<thead><tr>
								<!--<th>ID ученика</th>--><th>Город</th><th>Возраст</th><th>Телефон</th><th>Эл. почта</th><th>Дата регистрации</th><th>Преподаватель</th><th>Действия</th>
						</tr></thead>
						<tbody>
								<tr>
									<!--<td> <?php
										/*$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'edit_id_user'));
										echo $form->textField($modeloperuser,'value',array('class'=>'form-control','value'=>$user->id_teacher,'style'=>'float:left;height:24px;margin-right:6px;padding:2px 12px;width:70px;'));
										echo CHtml::submitButton('Сохранить',array('class'=>'btn btn-link my_btn-link','style'=>'padding:0;'));
										$this->endWidget();*/ ?>
									</td>-->
									<td>
										<?php echo $user->city; ?>
									</td>
									<td>
										<?php echo $user->age; ?>
									</td>
									<td>
										<?php echo $user->phone; ?>
									</td>
									<td>
										<?php echo $user->email; ?>
									</td>
									<td>
										<?php echo date("d-m-Y H:i:s", $user->data_reg); ?>
									</td>
									<td>
										<?php $teacher = UsersAr::model()->findByPk($user->id_who_learn);
										echo "<a title='На страницу преподавателя' href='".$this->createAbsoluteUrl('/') . '/lk/teachers/' . $teacher->id."'>".$teacher->fio."</a>"; ?>
									</td>
									<td><?php 
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'lock'));
										if($user->locked == 'no') {$btn_lock_text = 'Заблокировать'; $style_btn="btn-success";} else {$btn_lock_text = 'Разблокировать';$style_btn="btn-danger";}
										echo CHtml::submitButton($btn_lock_text,array('class'=>"btn btn-xs {$style_btn}",'style'=>'padding:0 7px;float:left;margin:0 5px 2px 0;'));
										$this->endWidget(); 
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'delete_user'));
										echo CHtml::submitButton('Удалить',array('class'=>'btn btn-danger btn-xs','style'=>'padding:0 7px;'));
										$this->endWidget(); ?>
									</td>
								</tr>
						</tbody>
				</table>
				
				<!-- == Изменения в профиле == -->
				<?php if($user->fio_tmp != NULL || $user->email_tmp != NULL || $user->phone_tmp != NULL) { ?>
					<hr>
					<h3>Изменения профиля, ожидающие подтверждения администратором</h3>
					ФИО: <b><?=$user->fio_tmp;?></b> <br>
					Телефон: <b><?=$user->phone_tmp;?></b> <br>
					Email: <b><?=$user->email_tmp;?></b> <br><br>
					<a class="btn btn-success" href="<?php echo $this->createAbsoluteUrl('/').$_SERVER['REQUEST_URI'].'?confirmEditProfile=OK&id_user='.$user->id; ?>">Подтвердить</a>
					<a class="btn btn-danger" href="<?php echo $this->createAbsoluteUrl('/').$_SERVER['REQUEST_URI'].'?confirmEditProfile=NO&id_user='.$user->id; ?>">Отклонить</a>
				<?php } ?>
				
				<hr>
				<a href="/lk/student/"><h3>Выполненные упражнения</h3></a>
				
				<hr>
				<h3>История посещений личного кабинета</h3>
				<?php if( $logs = LogAr::model()->findAll("id_user=".$user->id) ) { ?>
					<table class="table table-bordered table-striped table-condensed table-responsive">
						<thead><tr>
								<th>Дата посещения</th><th>Длительность сессии</th>
						</tr></thead>
						<tbody> <?php 
							foreach($logs as $log) { ?>
								<tr>
									<td>
										<?php echo date("d-m-Y H:i:s", $log->data_auth_in); ?>
									</td>
									<td>
										<?php echo date("H:i:s", (int)$log->data_auth_out - (int)$log->data_auth_in);
										//echo $log->time_session; ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
				<?php 
				$form=$this->beginWidget('CActiveForm', array(
				'id'=>'operuser-form',
				'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
				echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
				echo $form->hiddenField($modeloperuser,'operation',array('value'=>'clear_history_auth'));
				echo CHtml::submitButton('Очистить историю',array('class'=>'btn btn-link','style'=>'padding:0;'));
				$this->endWidget(); ?>
		
			
			<?php }
			} else { ?>
				
				<!-- ================== ВСЕ УЧЕНИКИ ===================== -->
				<h3 class="page-header">Ученики</h3>
				<?php 
				$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
				if( $type_user->type == 'partner') 
					$condition_sql = "SELECT * FROM `users` where type='student' AND id_who_learn IN (SELECT id FROM `users` where id_who_learn IN (SELECT id FROM `users` where id=".$type_user->id."))"; 
				else $condition_sql = "SELECT * FROM `users` where type='student'";
				if( $students = UsersAr::model()->findAllBySql($condition_sql) ) { ?>
					<table class="table table-bordered table-striped table-responsive">
						<thead><tr>
								<th>№ п/п</th><!--<th>ID преподавателя</th>--><th>ФИО</th><th>Город</th><th>Возраст</th><th>Телефон</th><th>Эл. почта</th><th>Пароль</th><th>Дата регистрации</th><th>Преподаватель</th><th>Дейстия</th>
						</tr></thead>
						<tbody> <?php  
							$count = 0;
							foreach($students as $student) { ?>
								<tr>
									<td>
										<?php echo ++$count; ?>
									</td>
									<!--<td> <?php
										/*$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$teacher->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'edit_id_user'));
										echo $form->textField($modeloperuser,'value',array('class'=>'form-control','value'=>$teacher->id_teacher,'style'=>'float:left;height:24px;margin-right:6px;padding:2px 12px;width:70px;'));
										echo CHtml::submitButton('Сохранить',array('class'=>'btn btn-link my_btn-link','style'=>'padding:0;'));
										$this->endWidget();*/ ?>
									</td>-->
									<td>
										<?php echo "<a title='На страницу ученика' href='".$this->createAbsoluteUrl('/') . '/lk/students/' . $student->id."'>".$student->fio."</a>"; ?>
									</td>
									<td>
										<?php echo $student->city; ?>
									</td>
									<td>
										<?php echo $student->age; ?>
									</td>
									<td>
										<?php echo $student->phone; ?>
									</td>
									<td>
										<?php echo $student->email; ?>
									</td>
									<td>
										<?php echo $student->password; ?>
									</td>
									<td>
										<?php echo date("d-m-Y H:i:s", $student->data_reg); ?>
									</td>
									<td>
										<?php $teacher = UsersAr::model()->findByPk($student->id_who_learn);
										echo "<a title='На страницу преподавателя' href='".$this->createAbsoluteUrl('/') . '/lk/teachers/' . $teacher->id."'>".$teacher->fio."</a>"; ?>
									</td>
									<td><?php 
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$student->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'lock'));
										if($student->locked == 'no') {$btn_lock_text = 'Заблокировать'; $style_btn="btn-success";} else {$btn_lock_text = 'Разблокировать';$style_btn="btn-danger";}
										echo CHtml::submitButton($btn_lock_text,array('class'=>"btn {$style_btn}",'style'=>'padding:0 7px;float:left;margin:0 5px 2px 0;'));
										$this->endWidget(); 
										
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$student->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'delete_user'));
										echo CHtml::submitButton('Удалить',array('class'=>'btn btn-danger','style'=>'padding:0 7px;'));
										$this->endWidget(); ?>
										<a class="btn btn-info btn-xs" style="margin-top:3px;" href="<?php echo $this->createAbsoluteUrl('/').'/lk/payment?user='.$student->id; ?>">Оплата</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
	<?php }	?>
    </div>
</div>