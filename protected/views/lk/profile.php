<div class="row">
    <div class="col-lg-12">
	
			<!-- ====== профиль ученика ====== -->
			<h2 class="page-header">Профиль</h2>
			<p style="color:red;"><?=$notice;?></p>
			<div class="form" style="width:250px;">
				<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'profile-form',
						'clientOptions'=>array('validateOnSubmit'=>true,),
						'htmlOptions'=>array('class'=>'form-horizontal',),
				)); ?>
				
				<div id="dop_block_login">
					<div class="div_inp_login">
						<p style="margin:13px 0 5px;">ФИО</p>
						<?php echo $form->textField($modelprofile,'fio',array('class'=>'input_f_logon form-control')); ?>
					</div>
				</div>
				
				<div id="dop_block_login">
					<div class="div_inp_login">
						<p style="margin:13px 0 5px;">Email</p>
						<?php echo $form->textField($modelprofile,'email',array('class'=>'input_f_logon form-control')); ?>
					</div>
				</div>
				
				<div id="dop_block_login">
					<div class="div_inp_login">
						<p style="margin:13px 0 5px;">Номер телефона</p>
						<?php echo $form->textField($modelprofile,'phone',array('class'=>'input_f_logon form-control')); ?>
					</div>
				</div><br>
				
				<?php 
				echo $form->hiddenField($modelprofile,'id',array('value'=>$modelprofile->id));  // !!!! id текущего пользователя
				echo CHtml::submitButton('Сохранить',array('class'=>'btn btn-primary')); ?>
				<?php $this->endWidget(); ?>
			</div>
			<br>
			<p style="color:green;"><?=$notice_OK;?></p>
    </div>
</div>