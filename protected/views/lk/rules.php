<div class="row">
    <div class="col-lg-12">
	
			<h2 class="page-header">Примеры Mental Map</h2>
		
			<?php 
			$rules = RulesAr::model()->findAll();
			foreach($rules as $rule) {
					if( $ex = ExamplesAr::model()->findAll("id_rules=".$rule->id) ) {
						echo "<p class='btn btn-link title_example'>{$rule->name}: {$rule->descr}</p>";
						echo '<div id="wrap_rules" style="display:none;">';
							for($i = 1; $i <= 6; $i++) {
								$examples = ExamplesAr::model()->findAll("id_rules=".$rule->id." and `rows`=".$i);
								echo '<div class="col-lg-2">';
									echo "<p class='title_rows'>{$i} - рядные</p>";
									foreach($examples as $example) {
										echo '<div class="one_ex">';
											echo '<span style="float:left;">'.str_replace('|', '', $example->example).' = '.$example->answer.'</span>';
											echo '<a style="float:right;" href="/lk/rules?del='.$example->id.'" type="button" class="btn btn-danger btn-xs">Удалить</a>';
											echo '<div style="clear:both;"></div>';	
										echo '</div>';
									}
								echo '</div>';
							}
							echo '<div class="clearfix"> </div>';
						echo '</div>';
						/*echo '<table class="table table-bordered table-striped table-responsive table-condensed">';
						foreach($examples as $example) {
							echo '<tr>';
								echo '<td>
									<span style="float:left;">'.str_replace('|', '', $example->example).' = '.$example->answer.'</span>
									<a style="float:right;" href="/lk/rules?del='.$example->id.'" type="button" class="btn btn-danger btn-xs">Удалить</a>
								</td>';
								//echo '<td><a href="/lk/rules?del={$example->id}" type="button" class="btn btn-danger">Удалить</a></td>';
							echo '</tr>';
						}
						echo '</table>';*/
					}
			}
			?>
			<br>
			<hr>
			<h3>Добавить пример</h3>
			<p style="background-color:#eee;font-weight:bold;padding:4px;">Допустимые знаки: " - ", " * ", " / "</p>
			<p style="color:green;"><?=$add_ok;?></p>
			<form action="/lk/rules" method="post">
				<p style="margin:13px 0 5px;">Выберите закон</p>
				<select name="ex_id_rules" class="form-control">
					<?php $rules = RulesAr::model()->findAll();
					foreach($rules as $rule) { ?>
						<option value="<?=$rule->id;?>"><?=$rule->name;?>: <?=$rule->descr;?></option>
					<?php } ?>
				</select>
				
				<!--<p style="margin:13px 0 5px;">Выберите рядность</p>
				<select name="ex_rows" class="form-control">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>-->
				
				<div id="ex_wrap_primers" style="display:block;">
					<div id="1">
						<div class="col-lg-1">
							<p style="margin:13px 0 5px;">1 число</p>
							<input class="form-control ex_value" value="" name="1ex_value_1" type="text">
						</div>
						<div class="col-lg-1">
							<p style="margin:13px 0 5px;">2 число</p>
							<input class="form-control ex_value" value="" name="1ex_value_2" type="text">
						</div>
						<div class="col-lg-1">
							<p style="margin:13px 0 5px;">3 число</p>
							<input class="form-control ex_value" value="" name="1ex_value_3" type="text">
						</div>
						<div class="col-lg-1">
							<p style="margin:13px 0 5px;">4 число</p>
							<input class="form-control ex_value" value="" name="1ex_value_4" type="text">
						</div>
						<div class="col-lg-1">
							<p style="margin:13px 0 5px;">5 число</p>
							<input class="form-control ex_value" value="" name="1ex_value_5" type="text">
						</div>
						<div class="col-lg-1">
							<p style="margin:13px 0 5px;">6 число</p>
							<input class="form-control ex_value" value="" name="1ex_value_6" type="text">
						</div>
						<div class="col-lg-1">
							<p style="margin:13px 0 5px;">Ответ</p>
							<input class="form-control ex_answer" value="" name="1ex_answer" type="text">
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				
				<br>
				<div style="display:block;">
					<div class="col-lg-1">
						<button type="button" id="add_ex_primer" class="btn btn-info">Добавить пример</button>
					</div>
					<div class="col-lg-11"></div>
					<div class="clearfix"> </div>
				</div>
				<br>
				<div style="display:block;">
					<div class="col-lg-2">
						<input style="width:100%;" type="submit" name="ex_add" class="btn btn-success" value="Сохранить" >
					</div>
					<div class="col-lg-10"></div>
					<div class="clearfix"> </div>
				</div>
				<!--<div class="col-lg-1">
						<p style="margin:13px 0 5px;"><b>Знак</b></p>
						<input class="form-control" value="" name="ex_znak_1" type="text">
					</div>-->
				
			</form>
    </div>
</div>
<br><br>