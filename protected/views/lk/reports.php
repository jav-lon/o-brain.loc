<?php
Yii::app()->clientScript->registerScriptFile("/js/pa/reports.js");
?>

<div class="row">
    <div class="col-lg-12">

        <?php $type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']); ?>
	
		<h2 class="page-header">Отчеты</h2>
        <?php if ($type_user->type == 'admin'): ?>
            <p>
                <a href="/royalty-report" class="btn btn-info">Отчет по роялти</a>
            </p>
        <?php else: ?>
            <p>
                <a href="/royalty-report/partner-report" class="btn btn-info">Отчет по роялти</a>
            </p>
        <?php endif; ?>
		<p style="color:green;"><?=$notice;?></p>
		
		<?php if(!isset($_POST["edit_report"])) { ?>
		
			<?php if( $type_user->type == 'partner') { ?>
				<form class="form-inline" action="/lk/reports" method="post">
					<input type="hidden" name="id_report" value="">
					<button name="edit_report" type="submit" class="btn btn-primary">Новый отчет</button>
				</form>
			<?php } ?>
				
			<!-- сданные отчеты -->
			<!-- если админ, сначала список партнеров -->
			<?php if( $type_user->type == 'admin' && !isset($_GET["id_partner"])) {
					if( $partners = UsersAr::model()->findAll("type='partner'") ) {
						foreach($partners as $partner) {
							echo '<p><a title="Открыть отчеты" href="'.$this->createAbsoluteUrl('/').'/lk/reports?id_partner='.$partner->id.'">'.$partner->fio.'</a></p>';
						}
					}
				}
			?>
			<?php 
			if( $type_user->type == 'partner') {
				echo '<h3 class="page-header">Отчеты</h3>';
				$reports = ReportsAr::model()->findAll("id_partner=".$type_user->id);
			}
			elseif(isset($_GET["id_partner"])) {
				$fio_partner = UsersAr::model()->findByPk($_GET["id_partner"]); echo '<h3 class="page-header">Отчеты партнера: '.$fio_partner->fio.'</h3>';
				$reports = ReportsAr::model()->findAll("id_partner=".$_GET["id_partner"]);
			}
			else $reports = NULL;
			if( $reports ) { ?>
				<table class="table table-bordered table-striped table-responsive">
					<thead><tr>
						<th>Дата сдачи</th>
                        <th>Статус</th>
                        <th>Цена одного занятия</th>
                        <th>Цена месячного абонемента</th>
                        <th>Кол-во учеников на конец месяца</th>
                        <th>Кол-во преподавателей</th>
                        <?php if (false): //deleted by Javlon ?>
                            <th>Кол-во субпартнеров</th>
                            <th>Кол-во преподавателей субпартнеров</th>
                        <?php endif; ?>
                        <th>Сумма дохода от субпартнеров</th>
                        <th>Оборот от своей деятельности</th>
                        <th>Сумма выплаченной ЗП преподавателям</th>
                        <th>Аренда помещения (ий)</th>
                        <th>Сумма иных расходов (расписать)</th>
                        <th>Чистая прибыль до налогообложения</th>
                        <th>Проведенные мероприятия (расписать)</th>
                        <th>Сумма роялти к выплате</th>
                        <th>Дейстия</th>
					</tr></thead>
					<tbody><?php 
						foreach($reports as $report) { ?>
							<tr>
								<td>
									<?php echo date("d-m-Y H:i:s", $report->date_report); ?>
								</td>
								<td>
									<?php if($report->allowed_report == 'yes') echo '<b style="color:green;">Отчет принят</b>'; else echo '<b style="color:red;">Отчет не принят</b>'; ?>
								</td>
								<td>
									<?php echo $report->price_one; ?>
								</td>
								<td>
									<?php echo $report->price_month; ?>
								</td>
								<td>
									<?php echo $report->count_students; ?>
								</td>
								<td>
									<?php echo $report->count_teachers; ?>
								</td>
                                <?php if (false): // deleted by Javlon?>
                                    <td>
                                        <?php echo $report->count_partners; ?>
                                    </td>
                                    <td>
                                        <?php echo $report->count_teachers_partners; ?>
                                    </td>
                                <?php endif; ?>
								<td>
									<?php echo $report->profit_from_partners; ?>
								</td>
								<td>
									<?php echo $report->profit_themselve; ?>
								</td>
								<td>
									<?php echo $report->summ_zp_teachers; ?>
								</td>
								<td>
									<?php echo $report->rent_areas; ?>
								</td>
								<td>
									<?php echo $report->summ_others; ?>
								</td>
								<td>
									<?php echo $report->profit_before_nalog; ?>
								</td>
								<td>
									<?php echo $report->events; ?>
								</td>
								<td>
									<?php echo $report->summ_royalty; ?><br>
									<img style="width:150px;" src="<?=$report->uploadfile;?>">
									<a href="<?=$report->uploadfile;?>" target="_blanc">Открыть</a>
								</td>
								<td>
									<form class="form-inline" action="/lk/reports" method="post">
										<input type="hidden" name="id_report" value="<?=$report->id;?>">
										<button name="edit_report" type="submit" class="btn btn-success btn-sm">Смотреть / редактировать</button>
                                        <?php if ($type_user->type == 'admin'):?>
                                            <button id="delete_report_button" name="delete_report" type="submit" class="btn btn-danger">Удалить отчет</button>
                                        <?php endif; ?>
									</form>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			<?php } ?>
		<?php } ?>
		
		<!-- сдать отчет -->			
		<?php if(isset($_POST["edit_report"])) { ?>
				<?php $report = ReportsAr::model()->findByPk($_POST["id_report"]); ?>
				
				<?php if( $type_user->type == 'admin') {
					$fio_partner = UsersAr::model()->findByPk($report->id_partner);	echo '<h3 class="page-header">Отчеты партнера: '.$fio_partner->fio.'</h3>';
					$disabled = 'disabled'; 
				} else $disabled = ''; ?>
				<form class="form-inline" action="/lk/reports" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<p style="margin:13px 0 5px;">Цена одного занятия</p>
						<input style="width:400px;" type="text" class="form-control" name="price_one" <?=$disabled;?> value="<?=$report === null ? '' : $report->price_one;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Цена месячного абонемента</p>
						<input style="width:400px;" type="text" class="form-control" name="price_month" <?=$disabled;?> value="<?=$report === null ? '' : $report->price_month;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Кол-во учеников на конец месяца</p>
						<input style="width:400px;" type="text" class="form-control" name="count_students" <?=$disabled;?> value="<?=$report === null ? '' : $report->count_students;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Кол-во преподавателей</p>
						<input style="width:400px;" type="text" class="form-control" name="count_teachers" <?=$disabled;?> value="<?=$report === null ? '' : $report->count_teachers;?>">
					</div>
					<br>
                    <?php if (false): //deleted by Javlon   ?>
                        <div class="form-group">
                            <p style="margin:13px 0 5px;">Кол-во субпартнеров</p>
                            <input style="width:400px;" type="text" class="form-control" name="count_partners" <?=$disabled;?> value="<?=$report === null ? '' : $report->count_partners;?>">
                        </div>
                        <br>
                        <div class="form-group">
                            <p style="margin:13px 0 5px;">Кол-во преподавателей субпартнеров</p>
                            <input style="width:400px;" type="text" class="form-control" name="count_teachers_partners" <?=$disabled;?> value="<?=$report === null ? '' : $report->count_teachers_partners;?>">
                        </div>
                        <br>
                    <?php endif; ?>
                    <div class="form-group">
                        <p style="margin:13px 0 5px;">Сумма дохода от субпартнеров</p>
                        <input style="width:400px;" type="text" class="form-control" name="profit_from_partners" <?=$disabled;?> value="<?=$report === null ? '' : $report->profit_from_partners;?>">
                    </div>
                    <br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Оборот от своей деятельности</p>
						<input style="width:400px;" type="text" class="form-control" name="profit_themselve" <?=$disabled;?> value="<?=$report === null ? '' : $report->profit_themselve;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Сумма выплаченной ЗП преподавателям</p>
						<input style="width:400px;" type="text" class="form-control" name="summ_zp_teachers" <?=$disabled;?> value="<?=$report === null ? '' : $report->summ_zp_teachers;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Аренда помещения (ий)</p>
						<input style="width:400px;" type="text" class="form-control" name="rent_areas" <?=$disabled;?> value="<?=$report === null ? '' : $report->rent_areas;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Сумма иных расходов (расписать)</p>
						<textarea style="width:400px;" name="summ_others" class="form-control" rows="3" <?=$disabled;?>><?=$report === null ? '' : $report->summ_others;?></textarea>
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Чистая прибыль до налогообложения</p>
						<input style="width:400px;" type="text" class="form-control" name="profit_before_nalog" <?=$disabled;?> value="<?=$report === null ? '' : $report->profit_before_nalog;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Проведенные мероприятия (расписать)</p>
						<textarea style="width:400px;" name="events" class="form-control" rows="3" <?=$disabled;?>><?=$report === null ? '' : $report->events;?></textarea>
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Сумма роялти к выплате</p>
						<input style="width:400px;" type="text" class="form-control" name="summ_royalty" <?=$disabled;?> value="<?=$report === null ? '' : $report->summ_royalty;?>">
					</div>
					<br>
					<div class="form-group">
						<p style="margin:13px 0 5px;">Прикрепить платежный документ</p>
						<input <?=$disabled;?> type="file" name="uploadfile">
					</div>

					<br><br>
					<?php if($type_user->type == 'partner') {
						if($report) {
							if($report->locked == 'no') $show_btn_save = true; else $show_btn_save = false;
							$id_p = $report->id;
						} else {
							$id_p = 'new';
							$show_btn_save = true;
						} 
						if($show_btn_save) { ?>
							<input type="hidden" name="id_report" value="<?=$id_p;?>">
							<button name="save_report" type="submit" class="btn btn-success">Сохранить</button>
						<?php } ?>	
					<?php } ?>
					<?php if( $type_user->type == 'admin') { ?>
						<hr>
						<div class="form-group">
							<p style="margin:13px 0 5px;">Комментарий</p>
							<textarea style="width:400px;" name="comment_deny_report" class="form-control" rows="3"></textarea>
						</div>
						<br><br>
						<input type="hidden" name="id_report" value="<?=$report->id;?>">
						<button name="allow_report" type="submit" class="btn btn-success">Принять отчет</button>
						<button name="deny_report" type="submit" class="btn btn-danger">Отклонить отчет</button>
						<button name="delete_report" type="submit" class="btn btn-danger">Удалить отчет</button>
					<?php } ?>
					<br><br>
				</form>
		<?php } ?>
    </div>
</div>