<div class="row">
    <div class="col-lg-12">
	
		<?php if(Yii::app()->session['status'] != '') { echo '<br><br><p class="status">'.Yii::app()->session['status'].'</p>'; Yii::app()->session['status'] = ''; } ?>
		<?php // смотрим список преподавателей, либо конкретного препода
		$id_user = explode ('/', $_SERVER['REQUEST_URI']);
		if( isset($id_user[3]) && $id_user[3] != '' ) {
			if( $user = UsersAr::model()->findByPk($id_user[3]) ) { ?>
			<!-- КОНКРЕТНЫЙ ПРЕПОДАВАТЕЛЬ -->
			
				<h3 class="page-header">Преподаватель <?=$user->fio;?></h3>
				<p style="color:red;"><?=$notice;?></p>
				<table class="table table-bordered table-striped table-responsive">
						<thead><tr>
								<th>ID преподавателя</th><th>Город</th><th>Возраст</th><th>Телефон</th><th>Эл. почта</th><th>Дата регистрации</th><th>Действия</th>
						</tr></thead>
						<tbody>
								<tr>
									<td> <?php
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'edit_id_user'));
										echo $form->textField($modeloperuser,'value',array('class'=>'form-control','value'=>$user->id_teacher,'style'=>'float:left;height:24px;margin-right:6px;padding:2px 12px;width:70px;'));
										echo CHtml::submitButton('Сохранить',array('class'=>'btn btn-link my_btn-link','style'=>'padding:0;'));
										$this->endWidget(); ?>
									</td>
									<td>
										<?php echo $user->city; ?>
									</td>
									<td>
										<?php echo $user->age; ?>
									</td>
									<td>
										<?php echo $user->phone; ?>
									</td>
									<td>
										<?php echo $user->email; ?>
									</td>
									<td>
										<?php echo date("d-m-Y H:i:s", $user->data_reg); ?>
									</td>
									<td><?php 
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'lock'));
										if($user->locked == 'no') {$btn_lock_text = 'Заблокировать'; $style_btn="btn-success";} else {$btn_lock_text = 'Разблокировать';$style_btn="btn-danger";}
										echo CHtml::submitButton($btn_lock_text,array('class'=>"btn {$style_btn}",'style'=>'padding:0 7px;float:left;margin:0 5px 2px 0;'));
										$this->endWidget(); 
										// если у этого учителя есть ученики, удалять его не надо
										if( !$check_students = UsersAr::model()->findByAttributes(array('id_who_learn'=>$user->id)) ) {
											$form=$this->beginWidget('CActiveForm', array(
											'id'=>'operuser-form',
											'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
											echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
											echo $form->hiddenField($modeloperuser,'operation',array('value'=>'delete_user'));
											echo CHtml::submitButton('Удалить',array('class'=>'btn btn-danger','style'=>'padding:0 7px;'));
											$this->endWidget(); 
										} ?>
									</td>
								</tr>
						</tbody>
				</table>
				
				<hr>
				<h3>Ученики преподавателя</h3>
				<?php if( $students = UsersAr::model()->findAll("id_who_learn=".$user->id) ) { ?>
					<table class="table table-bordered table-striped table-responsive">
						<thead><tr>
								<th>ФИО ученика</th><th>Дата регистрации</th><th>Город</th><th>Последнее задание</th><th>Действия</th>
						</tr></thead>
						<tbody> <?php 
							foreach($students as $student) { ?>
								<tr>
									<td>
										<?php echo "<a title='На страницу ученика' href='".$this->createAbsoluteUrl('/') . '/lk/students/' . $student->id."'>".$student->fio."</a>"; ?>
									</td>
									<td>
										<?php echo date("d-m-Y H:i:s", $student->data_reg); ?>
									</td>
									<td>
										<?php echo $student->city; ?>
									</td>
									<td>
										<?php if( $job = JobsAr::model()->findByAttributes(array('id_user'=>$student->id)) ) echo $job->name; ?>
									</td>
									<td><?php 
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$student->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'lock'));
										if($student->locked == 'no') {$btn_lock_text = 'Заблокировать'; $style_btn="btn-success";} else {$btn_lock_text = 'Разблокировать';$style_btn="btn-danger";}
										echo CHtml::submitButton($btn_lock_text,array('class'=>"btn {$style_btn}",'style'=>'padding:0 7px;float:left;margin:0 5px 2px 0;'));
										$this->endWidget(); 
										
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$student->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'delete_user'));
										echo CHtml::submitButton('Удалить',array('class'=>'btn btn-danger','style'=>'padding:0 7px;'));
										$this->endWidget(); ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
				
				<hr>
				<h3>История посещений личного кабинета</h3>
				<?php if( $logs = LogAr::model()->findAll("id_user=".$user->id) ) { ?>
					<table class="table table-bordered table-striped table-condensed table-responsive">
						<thead><tr>
								<th>Дата посещения</th><th>Длительность сессии</th>
						</tr></thead>
						<tbody> <?php 
							foreach($logs as $log) { ?>
								<tr>
									<td>
										<?php echo date("d-m-Y H:i:s", $log->data_auth_in); ?>
									</td>
									<td>
										<?php echo date("H:i:s", (int)$log->data_auth_out - (int)$log->data_auth_in);
										//echo $log->time_session; ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
				<?php 
				$form=$this->beginWidget('CActiveForm', array(
				'id'=>'operuser-form',
				'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
				echo $form->hiddenField($modeloperuser,'id',array('value'=>$user->id));
				echo $form->hiddenField($modeloperuser,'operation',array('value'=>'clear_history_auth'));
				echo CHtml::submitButton('Очистить историю',array('class'=>'btn btn-link','style'=>'padding:0;'));
				$this->endWidget(); ?>
				<!--<p style="background-color:#c0c0c0;font-size:12px;display:inline-block;margin:3px;padding:2px 6px;"><b>2017-01-17 22:33:23</b>, продолжительность сессии <b>23 мин.</b></p>
				<p style="background-color:#c0c0c0;font-size:12px;display:inline-block;margin:3px;padding:2px 6px;"><b>2017-01-17 22:33:23</b>, продолжительность сессии <b>23 мин.</b></p>
				<p style="background-color:#c0c0c0;font-size:12px;display:inline-block;margin:3px;padding:2px 6px;"><b>2017-01-17 22:33:23</b>, продолжительность сессии <b>23 мин.</b></p>
				<p style="background-color:#c0c0c0;font-size:12px;display:inline-block;margin:3px;padding:2px 6px;"><b>2017-01-17 22:33:23</b>, продолжительность сессии <b>23 мин.</b></p>
				<p style="background-color:#c0c0c0;font-size:12px;display:inline-block;margin:3px;padding:2px 6px;"><b>2017-01-17 22:33:23</b>, продолжительность сессии <b>23 мин.</b></p>
				<p style="background-color:#c0c0c0;font-size:12px;display:inline-block;margin:3px;padding:2px 6px;"><b>2017-01-17 22:33:23</b>, продолжительность сессии <b>23 мин.</b></p>
				<p style="background-color:#c0c0c0;font-size:12px;display:inline-block;margin:3px;padding:2px 6px;"><b>2017-01-17 22:33:23</b>, продолжительность сессии <b>23 мин.</b></p>
			-->
			
			
			<?php }
			} else { ?>
				
				<!-- ================== ВСЕ ПРЕПОДАВАТЕЛИ ===================== -->
				<h3 class="page-header">Преподаватели</h3>
				<?php 
				$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
				if( $type_user->type == 'partner') $condition_findAll = "type='teacher' AND id_who_learn=".$type_user->id; else $condition_findAll = "type='teacher'";
				if( $teachers = UsersAr::model()->findAll($condition_findAll) ) { ?>
					<table class="table table-bordered table-striped table-responsive">
						<thead><tr>
								<th>№ п/п</th><th>ID преподавателя</th><th>ФИО</th><th>Ученики<br>все/заблокирован</th><th>Город</th><th>Возраст</th><th>Телефон</th><th>Эл. почта</th><th>Пароль</th><th>Дата регистрации</th>
								<?php if( $type_user->type == 'admin') { ?> <th>фИО парнтера</th> <?php } ?><th>Дейстия</th>
						</tr></thead>
						<tbody> <?php  
							$count = 0;
							foreach($teachers as $teacher) { ?>
                                <?php
                                //ucheniki
                                $students_locked_cnt = UsersAr::model()->count("type='student' AND id_who_learn='" . $teacher->id . "' AND locked='yes'");
                                $students_cnt = UsersAr::model()->count("type='student' AND id_who_learn='" . $teacher->id . "'");
                                ?>
								<tr>
									<td>
										<?php echo ++$count; ?>
									</td>
									<td> <?php
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$teacher->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'edit_id_user'));
										echo $form->textField($modeloperuser,'value',array('class'=>'form-control','value'=>$teacher->id_teacher,'style'=>'float:left;height:24px;margin-right:6px;padding:2px 12px;width:70px;'));
										echo CHtml::submitButton('Сохранить',array('class'=>'btn btn-link my_btn-link','style'=>'padding:0;'));
										$this->endWidget(); ?>
									</td>
									<td>
										<?php echo "<a title='На страницу преподавателя' href='".$this->createAbsoluteUrl('/') . '/lk/teachers/' . $teacher->id."'>".$teacher->fio."</a>"; ?>
									</td>
                                    <td <?= $students_cnt == 0 ? 'class="danger"' : ''?>>
                                        <?php echo $students_cnt . ' / ' . '<span class="text-danger">'.$students_locked_cnt.'</span>' ?>
                                    </td>
									<td>
										<?php echo $teacher->city; ?>
									</td>
									<td>
										<?php echo $teacher->age; ?>
									</td>
									<td>
										<?php echo $teacher->phone; ?>
									</td>
									<td>
										<?php echo $teacher->email; ?>
									</td>
									<td>
										<?php echo $teacher->password; ?>
									</td>
									<td>
										<?php echo date("d-m-Y H:i:s", $teacher->data_reg); ?>
									</td>
									<?php if( $type_user->type == 'admin') { ?>
										<td>
											<?php if($name_partner = UsersAr::model()->findByPk($teacher->id_who_learn)) echo $name_partner->fio; ?>
										</td>
									<?php } ?>
									<td><?php 
										$form=$this->beginWidget('CActiveForm', array(
										'id'=>'operuser-form',
										'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
										echo $form->hiddenField($modeloperuser,'id',array('value'=>$teacher->id));
										echo $form->hiddenField($modeloperuser,'operation',array('value'=>'lock'));
										if($teacher->locked == 'no') {$btn_lock_text = 'Заблокировать'; $style_btn="btn-success";} else {$btn_lock_text = 'Разблокировать';$style_btn="btn-danger";}
										echo CHtml::submitButton($btn_lock_text,array('class'=>"btn {$style_btn}",'style'=>'padding:0 7px;float:left;margin:0 5px 2px 0;'));
										$this->endWidget(); 
										// если у этого учителя есть ученики, удалять его не надо
										if( !$check_students = UsersAr::model()->findByAttributes(array('id_who_learn'=>$teacher->id)) ) {
											$form=$this->beginWidget('CActiveForm', array(
											'id'=>'operuser-form',
											'enableClientValidation'=>false,'clientOptions'=>array('validateOnSubmit'=>true,), ));
											echo $form->hiddenField($modeloperuser,'id',array('value'=>$teacher->id));
											echo $form->hiddenField($modeloperuser,'operation',array('value'=>'delete_user'));
											echo CHtml::submitButton('Удалить',array('class'=>'btn btn-danger','style'=>'padding:0 7px;'));
											$this->endWidget(); 
										} ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
	 <?php } ?>
    </div>
</div>