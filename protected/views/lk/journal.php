<div class="row">
    <div class="col-lg-12">
	
		<h2 class="page-header">Журнал</h2>
		<?php 
		$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
		if( $type_user->type == 'admin') {
			$sql = 'SELECT id_user FROM `journal` group by id_user order by id_user';
		} elseif($type_user->type == 'teacher') {
			$sql = 'SELECT id_user FROM `journal` WHERE id_user IN (SELECT id FROM `users` WHERE id_who_learn = '.Yii::app()->session['login_user'].') group by id_user order by id_user';
		} elseif($type_user->type == 'partner') {
			$sql = 'SELECT id_user FROM `journal` WHERE id_user IN (SELECT id FROM `users` WHERE id_who_learn IN (SELECT id FROM `users` WHERE id_who_learn = '.Yii::app()->session['login_user'].')  ) group by id_user order by id_user';
		} else {
			$sql = 'SELECT id_user FROM `journal` WHERE id_user = '.Yii::app()->session['login_user'].' group by id_user order by id_user';
		}
		$journals = JournalAr::model()->findAllbySql($sql);
		foreach($journals as $journal) { ?>
			<div>
				<div class="blockJournal">
					<div class="blockJournalTitle">Дата</div>
					<div class="blockJournalBody"><?php $user = UsersAr::model()->findByPk($journal->id_user); echo str_replace(' ', '<br>', $user->fio); ?></div>
				</div>
				<?php $journals_by_ids = JournalAr::model()->findAll('id_user='.$journal->id_user);
				foreach($journals_by_ids as $journals_by_id) { ?>
					<div class="blockJournal">
						<div class="blockJournalTitle"><?php echo $journals_by_id->data; //date("d.m.Y", $journals_by_id->data);?></div>
						<div class="blockJournalBody"><?php if($journals_by_id->attended == 1) echo '<div class="byl attendAdded"></div>'; else echo '<div class="nebyl attendAdded"></div>'; ?></div>
					</div>
				<?php }	?>
					
				<div class="blockJournal">
					<div class="blockJournalTitle"><input id="data_<?=$user->id;?>" style="width:80px;" name="dataBlockJournal" value="<?php echo date("d.m.Y", time()); ?>"></div>
					<div class="blockJournalBody" id="body_<?=$user->id;?>" style="text-align:center;">
						<div data-val="1" id="byl" class="byl"></div><div data-val="0" id="nebyl" class="nebyl"></div>
						<div class="added" id="<?=$user->id;?>">Добавить</div>
					</div>
				</div>
				
				<a href="/lk/journal?id_user=<?=$user->id;?>" type="button" style="float:left;margin:6px 0 0 10px;" class="btn btn-danger btn-xs">Удалить ученика</a>
			</div>
			<div style="clear:both;"></div><br>
		<?php }	?>
		
		<?php if( $type_user->type != 'student') { ?>
			<hr><h3>Добавить в журнал нового ученика</h3>
			<form class="form-inline" action="/lk/journal" method="post">
				<p style="margin:13px 0 5px;"><b>Ученик</b></p>
				<select class="form-control" name="id_user">
				  <?php 
					if( $type_user->type == 'admin') {
						$sql = 'SELECT * FROM `users` WHERE id NOT IN (SELECT id_user FROM journal)';
					} elseif($type_user->type == 'teacher') {
						$sql = 'SELECT * FROM `users` WHERE id NOT IN (SELECT id_user FROM journal) AND id IN (SELECT id FROM `users` WHERE id_who_learn = '.Yii::app()->session['login_user'].')';
					} elseif($type_user->type == 'partner') {
						$sql = 'SELECT * FROM `users` WHERE id NOT IN (SELECT id_user FROM journal) AND id IN (SELECT id FROM `users` WHERE id_who_learn IN (SELECT id FROM `users` WHERE id_who_learn = '.Yii::app()->session['login_user'].') )';
					}
				  
				  $users = UsersAr::model()->findAllbySql($sql);
				  foreach($users as $user) {
						echo '<option value="'.$user->id.'">'.$user->fio.'</option>';
				  } ?>
				</select>
				<br>
				<p style="margin:13px 0 5px;"><b>Дата оплаты</b></p>
				<div class="form-group">
					<input type="text" class="form-control" name="data" value="<?=date("d.m.Y", time());?>">
				</div><br>
				<p style="margin:13px 0 5px;"><b>Был или нет</b></p>
				<div class="radio">
					<label>
						<input type="radio" name="attended" value="1" checked> Был
					</label>
				</div>
				<br>
				<div class="radio">
					<label>
						<input type="radio" name="attended" value="2"> Не был
					</label>
				</div>
				<br><br>
				<button name="add_user_to_journal" type="submit" class="btn btn-success">Добавить</button>
				<br><br>
			</form>
		<?php } ?>
		
    </div>
</div>