<div class="row">
    <div class="col-lg-12">
	
		<h2 class="page-header">Новости</h2>
		<p style="color:green;"><?=$notice;?></p>
		<?php $type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']); ?>
		
		<?php 
		// конкретная новость
		if(isset($_GET["id_news"])) {
			$news = NewsAr::model()->findByPk($_GET["id_news"]);
			echo '<p><img src="'.$news->image.'"></p>';
			echo '<p>'.$news->text.'</p>';
		}
		// все новости
		elseif( $news = NewsAr::model()->findAll() ) {
			foreach($news as $new) {
				if( $type_user->type == 'admin' ) $delete = ' <a class="btn btn-danger btn-xs" href="'.$this->createAbsoluteUrl('/').'/lk/news?id_news_delete='.$new->id.'">Удалить</a>'; else $delete = '';
				echo '<p>Дата размещения: '.date("d.m.Y", $new->date_create).$delete.'  <a href="'.$this->createAbsoluteUrl('/').'/lk/news?id_news='.$new->id.'">'.$new->text.'</a></p>';
			}
		} ?>
		
		<!-- === добавить новость (админом) === -->
		<?php 
		if( $type_user->type == 'admin' ) { ?>
			<hr><h3>Добавить новость</h3>
			<form class="form-inline" action="/lk/news" method="post" enctype="multipart/form-data">
				<p style="margin:13px 0 5px;"><b>Период показа</b></p>
				<div class="form-group">
					<label for="date_from">с</label>
					<?php if(isset($_POST["date_from"])) $date_from = $_POST["date_from"]; else $date_from = date("d.m.Y", time());	?>
					<input type="text" class="form-control" id="date_from" name="date_from" value="<?=$date_from;?>">
				</div>
				<div class="form-group">
					<label for="date_to">по</label>
					<?php if(isset($_POST["date_to"])) $date_to = $_POST["date_to"]; else $date_to = date("d.m.Y", time());	?>
					<input type="text" class="form-control" id="date_to" name="date_to" value="<?=$date_to;?>">
				</div>
				<br>
				<div class="form-group" style="width:100%;">
					<p style="margin:13px 0 5px;"><b>Текст новости</b></p>
					<textarea style="width:100%;" name="text" class="form-control" rows="3"><?/*=$report->events;*/?></textarea>
				</div>
				<br>
				<div class="form-group">
					<p style="margin:13px 0 5px;"><b>Изображение</b></p>
					<input type="file" name="uploadfile">
				</div>
				<br>
				<div class="form-group">
					<p style="margin:13px 0 5px;"><b>Кому показывать</b></p>
					<p type="button" id="btn_select_all" class="btn btn-info btn-xs">Показывать всем</p>
					<p type="button" id="btn_unselect_all" class="btn btn-warning btn-xs">Снять выделение</p><br><br>
					<?php 
					//$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='teacher' OR type='partner'");
					echo '<h4>Партнеры</h4>';
					$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='partner'");
					if($users) {
						foreach($users as $user) {
							echo '<label><input class="usersForSendEmail" name="users[]" value="'.$user->id.'" type="checkbox"/> '.$user->fio.'</label><br>';
						} 
					}
					echo '<h4>Преподаватели</h4>';
					$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='teacher'");
					if($users) {
						foreach($users as $user) {
							echo '<label><input class="usersForSendEmail" name="users[]" value="'.$user->id.'" type="checkbox"/> '.$user->fio.'</label><br>';
						} 
					}
                    echo '<h4>Ученики</h4>';
                    $users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='student'");
                    if($users) {
                        foreach($users as $user) {
                            echo '<label><input class="usersForSendEmail" name="users[]" value="'.$user->id.'" type="checkbox"/> '.$user->fio.'</label><br>';
                        }
                    } ?>
				</div>
				
				<br><br>
				<?php //if($report) $id_p = $report->id; else $id_p = 'new'; ?>
				<!--<input type="hidden" name="id_report" value="<?/*=$id_p;*/?>">-->
				<button name="save_news" type="submit" class="btn btn-success">Добавить</button>
				<br><br>
			</form>
		<?php } ?>
		
		
    </div>
</div>