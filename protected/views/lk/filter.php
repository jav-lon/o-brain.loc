<div class="row">
    <div class="col-lg-12">
	
			<h2 class="page-header">Фильтр</h2>
			
			<!--<input class="datepicker" data-provide="datepicker">-->
			<form class="form-inline" action="/lk/filter" method="post">
				<h3>Дата</h3>
				<div class="form-group">
					<label for="date_from">с</label>
					<?php if(isset($_POST["date_from"])) $date_from = $_POST["date_from"]; else $date_from = date("d.m.Y", time());	?>
					<input type="text" class="form-control" id="date_from" name="date_from" value="<?=$date_from;?>">
				</div>
				<div class="form-group">
					<label for="date_to">по</label>
					<?php if(isset($_POST["date_to"])) $date_to = $_POST["date_to"]; else $date_to = date("d.m.Y", time());	?>
					<input type="text" class="form-control" id="date_to" name="date_to" value="<?=$date_to;?>">
				</div>
				<!--<button name="find_by_date" type="submit" class="btn btn-success">Найти</button>-->
				<h3>Выполненные упражнения</h3>
				<div class="form-group">
					<?php if(isset($_POST["job"])) {if($_POST["job"] == 'Набор цифр на Абакусе') $chk1 = 'selected';elseif($_POST["job"] == 'Определение числа с карты Абакус') $chk2 = 'selected';elseif($_POST["job"] == 'Примеры на сложение / вычитание') $chk3 = 'selected';elseif($_POST["job"] == 'Примеры на умножение') $chk4 = 'selected';elseif($_POST["job"] == 'Примеры на деление') $chk5 = 'selected';elseif($_POST["job"] == 'Bemoreclever') $chk6 = 'selected';elseif($_POST["job"] == 'Bottomofabac') $chk7 = 'selected';else $chk8 = 'selected';} ?>
					<select name="job" class="form-control">
						<option <?=$chk1;?> value="Набор цифр на Абакусе">Набор цифр на Абакусе</option>
						<option <?=$chk2;?> value="Определение числа с карты Абакус">Определение числа с карты Абакус</option>
						<option <?=$chk3;?> value="Примеры на сложение / вычитание">Примеры на сложение / вычитание</option>
						<option <?=$chk4;?> value="Примеры на умножение">Примеры на умножение</option>
						<option <?=$chk5;?> value="Примеры на деление">Примеры на деление</option>
						<option <?=$chk6;?> value="Bemoreclever">Bemoreclever</option>
						<option <?=$chk7;?> value="Bottomofabac">Bottomofabac</option>
						<option <?=$chk8;?> value="Mentalmap">Mentalmap</option>
					</select>
				</div>
				<!--<button name="find_by_job" type="submit" class="btn btn-success">Найти</button>-->
				<br><br><button name="find_by_filter" type="submit" class="btn btn-success">Лучший результат</button>
			</form>
			
			<hr>
			<!-- === ВЫВОД РЕЗУЛЬТАТОВ ПОИСКА === -->
			<?php
			if(isset($_POST["find_by_filter"])) {
				$date_from = strtotime($_POST["date_from"].' 00:01');
				$date_to = strtotime($_POST["date_to"].' 23:59');
				if($jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where date > '".$date_from."' AND date < '".$date_to."' AND name = '".$_POST["job"]."' AND errors=0 GROUP BY `id_user` ORDER BY `time` ASC")) {
					foreach($jobs as $job) {
						$user = UsersAr::model()->findByPk($job->id_user);
						if($user->type == 'student') echo '<p><a title="Упражнение детально" href="'.$this->createAbsoluteUrl('/').'/lk/filter?job_detail='.$job->id.'">'.$user->fio.'</a></p>';
					}
				}
			}
			if(isset($_GET["job_detail"])) {
				$job = JobsAr::model()->findByPk($_GET["job_detail"]);
				echo '<h4>Результаты выполнения упражнения '.$job->name.'</h4><br>';
				echo '<p>Дата выполнения упражнения: <b>'.date("d-m-Y", $job->date).'</b></p>';
				echo '<p>Время выполнения упражнения: <b>'.$job->time.'</b></p>';
				echo '<p>Кол-во ошибок: <b>'.$job->errors.'</b></p>';
				//echo '<p>Рейтинг: <b>'.$job->rating.'</b></p>';
			}
			/*if(isset($_POST["find_by_date"])) {
				$date_from = strtotime($_POST["date_from"].' 00:01');
				$date_to = strtotime($_POST["date_to"].' 23:59');
				if($jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where date > '".$date_from."' AND date < '".$date_to."' GROUP BY `id_user` ORDER BY `time` ASC")) {
					foreach($jobs as $job) {
						$user = UsersAr::model()->findByPk($job->id_user);
						if($user->type == 'student') echo '<p><a href="'.$this->createAbsoluteUrl('/').'/lk/filter?jobsByUser='.$user->id.'">'.$user->fio.'</a></p>';
					}
				}
			}*/
			/*if(isset($_GET["jobsByUser"])) {
				$user = UsersAr::model()->findByPk($_GET["jobsByUser"]);
				echo '<h4>Упражнения выполненные учеником '.$user->fio.':</h4>';
				$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".$user->id." GROUP BY `name`");
				foreach($jobs as $job) {
					echo '<p><a href="'.$this->createAbsoluteUrl('/').'/lk/filter?job_detail='.$job->id.'">'.$job->name.'</a></p>';
				}
			}*/
			/*if(isset($_POST["find_by_job"])) {
				if($jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where name = '".$_POST["job"]."' group by `id_user`")) {
					foreach($jobs as $job) {
						$user = UsersAr::model()->findByPk($job->id_user);
						if($user->type == 'student') echo '<p><a href="'.$this->createAbsoluteUrl('/').'/lk/filter?jobsByUser='.$user->id.'">'.$user->fio.'</a></p>';
					}
				}
			}
			if(isset($_POST["find_by_bestRezult"])) {
				$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where errors = 0 GROUP BY `id_user` ORDER BY `time` ASC");
				foreach($jobs as $job) {
					$user = UsersAr::model()->findByPk($job->id_user);
					if($user->type == 'student') echo '<p><a href="'.$this->createAbsoluteUrl('/').'/lk/filter?jobsByUser='.$user->id.'">'.$user->fio.'</a></p>';
				}
			}*/
			?>
    </div>
</div>