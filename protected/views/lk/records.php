<div class="row">
    <div class="col-lg-12">
	
			<h2 class="page-header">Рекорды</h2>

			<!-- СПИСОК РЕКОРДСМЕНОВ ПО КОНКРЕТНОМУ УПРАЖНЕНИЮ -->
			<?php 
			if(isset($_GET["recordsForJob"])) {
				$name_job[1] = "Набор цифр на Абакусе";$name_job[2] = "Определение числа с карты Абакус";$name_job[3] = "Примеры на сложение / вычитание";$name_job[4] = "Примеры на умножение";
				$name_job[5] = "Примеры на деление";$name_job[6] = "Bemoreclever";$name_job[7] = "Bottomofabac";$name_job[8] = "Mentalmap";
			
				echo '<h3>Рекордсмены упражнения: '.$name_job[$_GET["recordsForJob"]].'</h3>';
				echo '<table class="table table-bordered table-striped table-responsive">';
				echo '<thead><tr><th>Результат</th><th>ФИО</th><th>Город</th></tr></thead>';
				echo '<tbody>';
				if($jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where errors = 0 AND name = '".$name_job[$_GET["recordsForJob"]]."' ORDER BY `time` ASC")) {
					foreach($jobs as $job) {
						echo '<tr>';
							echo '<td>'.date("d.m.Y", $job->date) . ' time ' . $job->time.'</td>';
							$user = UsersAr::model()->findByPk($job->id_user);
							echo '<td>'.$user->fio.'</td>';
							echo '<td>'.$user->city.'</td>';
						echo '</tr>';
					}
				}
				echo '</tbody>';
				echo '</table>';
			
			} else { ?>
			
			<!-- СПИСОК РЕКОРДОВ ПО ВСЕМ УПРАЖНЕНИЯМ -->
            <a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records'; ?>" style="display:inline-block;margin: 0 10px 15px 0;">Без ошибок</a>
            <a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?oneError'; ?>" style="display:inline-block;margin: 0 0px 15px 0;">С 1-ой ошибкой</a>
            <?php if(isset($_GET["oneError"])) $error = 1; else $error = 0; ?>
			<table class="table table-bordered table-striped table-responsive">
				<thead><tr><th>Упражнение</th><th>Дата / Результат</th><th>ФИО</th><th>Город</th></tr></thead>
				<tbody>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=1'; ?>">Набор цифр на Абакусе</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Набор цифр на Абакусе' ORDER BY `time` ASC LIMIT 1")) {
								echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job->id_user); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=2'; ?>">Определение числа с карты Абакус</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Определение числа с карты Абакус' ORDER BY `time` ASC LIMIT 1")) {
                                echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job->id_user); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=3'; ?>">Примеры на сложение / вычитание</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Примеры на сложение / вычитание' ORDER BY `time` ASC LIMIT 1")) {
                                echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job->id_user); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=4'; ?>">Примеры на умножение</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Примеры на умножение' ORDER BY `time` ASC LIMIT 1")) {
                                echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job->id_user); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=5'; ?>">Примеры на деление</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Примеры на деление' ORDER BY `time` ASC LIMIT 1")) {
                                echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job != null ? $job->id_user : null); echo $user != null ? $user->fio : ''; ?></td>
						<td><?php echo $user != null ? $user->city : ''; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=6'; ?>">Bemoreclever</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Bemoreclever' ORDER BY `time` ASC LIMIT 1")) {
                                echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job->id_user); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=7'; ?>">Bottomofabac</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Bottomofabac' ORDER BY `time` ASC LIMIT 1")) {
                                echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job->id_user); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/lk/records?recordsForJob=8'; ?>">Mentalmap</a></td>
						<td>
							<?php if($job = JobsAr::model()->findBySql("SELECT * FROM `jobs` where errors = {$error} AND name = 'Mentalmap' ORDER BY `time` ASC LIMIT 1")) {
                                echo date("d.m.Y H:i:s", $job->date) . ' / time ' . $job->time;
							} ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($job->id_user); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
					<tr>
						<td><a href="<?php echo $this->createAbsoluteUrl('/').'/simulator/high-score?id_simulator=9&oneError=' . $error; ?>">Smarty</a></td>
						<td>
                            <?php
                            $simulator_result = Yii::app()->db
                                ->createCommand('SELECT * FROM `simulators_results` WHERE count_errors=:error AND id_simulator=9 ORDER BY `time` ASC LIMIT 1')
                                ->bindParam(":error", $error, PDO::PARAM_STR)
                                ->query()->read();
                            ?>
                            <?php if ($simulator_result): ?>
                            <?php echo date("d.m.Y H:i:s", $simulator_result['created_at']) . ' / time ' . ($simulator_result['time'] / 1000) ; ?>
                            <?php endif; ?>
						</td>
						<td><?php $user = UsersAr::model()->findByPk($simulator_result['id_user']); echo $user->fio; ?></td>
						<td><?php echo $user->city; ?></td>
					</tr>
				</tbody>
			</table>
			<?php } ?>
    </div>
</div>