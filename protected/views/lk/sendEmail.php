<div class="row">
    <div class="col-lg-12">
	
			<h2 class="page-header">Рассылка писем</h2>
			<?=$notice;?>
			
			<?php $type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
			
			if( $type_user->type == 'admin' && !isset($_POST["btn_category_user"]) ) { ?>
			
				<h3>Выберите категорию</h3>
				<form class="form-inline" action="/lk/sendEmail" method="post">
					<label><input class="usersForSendEmail" name="partners" value="1" type="checkbox"/> Партнеры</label><br>
					<label><input class="usersForSendEmail" name="teachers" value="1" type="checkbox"/> Преподаватели</label><br>
					<label><input class="usersForSendEmail" name="students" value="1" type="checkbox"/> Ученики</label><br>
					<button name="btn_category_user" type="submit" class="btn btn-success">Далее</button>
				</form>
				
			<?php } else { ?>
			
				<?php if( $type_user->type != 'admin' ) { ?><h3>Выберите учеников</h3> <?php } ?>
				<button type="button" id="btn_select_all" class="btn btn-info btn-xs">Выделить всех</button>
				<button type="button" id="btn_unselect_all" class="btn btn-warning btn-xs">Снять выделение</button><br><br>
				<form class="form-inline" action="/lk/sendEmail" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<?php 
						if( $type_user->type != 'admin' ) {
							if( $type_user->type == 'teacher' )
								// только своим ученикам....
								$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='student' AND id_who_learn='".Yii::app()->session['login_user']."'");
							else // $type_user->type == 'partner' 
								// только ученикам своих преподов ....
								$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` where id_who_learn IN (SELECT id FROM `users` where id_who_learn IN (SELECT id FROM `users` where id=".$type_user->id."))");

							if($users) {
								foreach($users as $user) {
									//if( $type_user->type == 'partner' ) $tp_user = 
									echo '<label><input class="usersForSendEmail" name="users[]" value="'.$user->id.'" type="checkbox"/> '.$user->fio.'</label><br>';
								} 
							}
						} else  { // $type_user->type == 'admin'
							if(isset($_POST["partners"])) {
								echo '<h3>Выберите партнеров</h3>';
								$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='partner'");
								foreach($users as $user) {
									echo '<label><input class="usersForSendEmail" name="users[]" value="'.$user->id.'" type="checkbox"/> '.$user->fio.'</label><br>';
								}
							}
							if(isset($_POST["teachers"])) {
								echo '<h3>Выберите преподавателей</h3>';
								$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='teacher'");
								foreach($users as $user) {
									echo '<label><input class="usersForSendEmail" name="users[]" value="'.$user->id.'" type="checkbox"/> '.$user->fio.'</label><br>';
								}
							}
							if(isset($_POST["students"])) {
								echo '<h3>Выберите учеников</h3>';
								$users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='student'");
								foreach($users as $user) {
									echo '<label><input class="usersForSendEmail" name="users[]" value="'.$user->id.'" type="checkbox"/> '.$user->fio.'</label><br>';
								}
							}
						}
						?>
					</div>
					<h3>Текст письма</h3>
					<div class="form-group">
						<textarea name="text_email" class="form-control" rows="3" cols="85"></textarea>
					</div>
					<h3>Файл</h3>
					<div class="form-group">
						<input type="file" name="uploadfile">
					</div>
					<br><br>
					<button name="btn_send_email_to_users" type="submit" class="btn btn-success">Отправить</button>
					<br><br>
				</form>
			<?php } ?>
    </div>
</div>