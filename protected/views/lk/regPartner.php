<div class="row">
    <div class="col-lg-12">
	
			<h2 class="page-header">Регистрация партнера</h2>
			<p style="color:green;"><?=$notice;?></p>
				
			<?php if(isset($_POST["edit_partner"])) $partner = UsersAr::model()->findByPk($_POST["id_partner"]); ?>
					<form class="form-inline" action="/lk/regPartner" method="post">
							
							<div class="form-group">
								<p style="margin:13px 0 5px;">Регистрационный номер</p>
								<?php if($partner) $id_teacher = $partner->id_teacher;
								else {
									if( $partners = UsersAr::model()->findBySql("SELECT * FROM `users` WHERE type='partner' ORDER BY id DESC") ) {
										$id_teacher = substr($partners->id_teacher, 1); // P000001, возьмем без первого Р
										$id_teacher += 1;
										$id_teacher = str_pad($id_teacher, 6, "0", STR_PAD_LEFT);
										$id_teacher = 'P'.$id_teacher;
									} else {
										$id_teacher = 'P000001'; 
									}
								} ?>
								<input style="width:400px;" type="text" class="form-control" name="id_teacher" readonly='readonly' value="<?=$id_teacher;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Ф.И.О.</p>
								<input style="width:400px;" type="text" class="form-control" name="fio" value="<?=$partner->fio;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Дата рождения</p>
								<input style="width:400px;" type="text" class="form-control" name="date_rojd" value="<?=$partner->date_rojd;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Город</p>
								<input style="width:400px;" type="text" class="form-control" name="city" value="<?=$partner->city;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Домашний адрес</p>
								<input style="width:400px;" type="text" class="form-control" name="addr" value="<?=$partner->addr;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Запасной домашний адрес</p>
								<input style="width:400px;" type="text" class="form-control" name="addr_reserv" value="<?=$partner->addr_reserv;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Эл. почта</p>
								<input style="width:400px;" type="text" class="form-control" name="email" value="<?=$partner->email;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Запасная эл. почта</p>
								<input style="width:400px;" type="text" class="form-control" name="email_reserv" value="<?=$partner->email_reserv;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Пароль</p>
								<input style="width:400px;" type="text" class="form-control" name="password" value="<?=$partner->password;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Телефон</p>
								<input style="width:400px;" type="text" class="form-control" name="phone" value="<?=$partner->phone;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Запасной телефон</p>
								<input style="width:400px;" type="text" class="form-control" name="phone_reserv" value="<?=$partner->phone_reserv;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Номер договора</p>
								<input style="width:400px;" type="text" class="form-control" name="num_dogovor" value="<?=$partner->num_dogovor;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Дата договора</p>
								<input style="width:400px;" type="text" class="form-control" name="date_dogovor" value="<?=$partner->date_dogovor;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Размер роялти</p>
								<input style="width:400px;" type="text" class="form-control" name="royalty" value="<?=$partner->royalty;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Дата оплаты роялти</p>
								<input style="width:400px;" type="text" class="form-control" name="date_royalty" value="<?=$partner->date_royalty;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;">Комментарий к оплате роялти</p>
								<input style="width:400px;" type="text" class="form-control" name="comment_royalty" value="<?=$partner->comment_royalty;?>">
							</div>
							<br>
							<div class="form-group">
								<p style="margin:13px 0 5px;"><b>Преподаватели партнера</b></p>
								<?php $users = UsersAr::model()->findAllBySql("SELECT * FROM `users` WHERE type='teacher' AND (id_who_learn='-' OR id_who_learn='".$partner->id."')");
									foreach($users as $user) {
										if($user->id_who_learn == $partner->id) $checked='checked'; else $checked='';
										echo '<input name="teachers[]" value="'.$user->id.'" type="checkbox" '.$checked.'/> '.$user->fio.'<br>';
									} ?>
							</div>
							<br><br>
							<?php if($partner) $id_p = $partner->id; else $id_p = 'new'; ?>
							<input type="hidden" name="id_partner" value="<?=$id_p;?>">
							<button name="save_regPartner" type="submit" class="btn btn-success">Сохранить</button>
							<br><br>
					</form>
    </div>
</div>