<div class="row">
    <div class="col-lg-12">
	
		<!-- ====== АВТОРИЗАЦИЯ ====== -->
		<h2 class="page-header">Авторизация или <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/registration">Регистрация</a></h2>
		<p style="color:red;"><?=$login_notice;?></p>
		<div class="form" style="width:250px;">
			<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'login-form',
					'clientOptions'=>array('validateOnSubmit'=>true,),
					'htmlOptions'=>array('class'=>'form-horizontal',),
			)); ?>
				<div id="dop_block_login">
					<div class="div_inp_login">
						<p style="margin:13px 0 5px;">Ваш email</p>
						<?php echo $form->textField($modellogin,'nameadmin',array('class'=>'input_f_logon form-control','value'=>'')); ?>
					</div>
					<div class="div_inp_login">
						<p style="margin:13px 0 5px;">Пароль</p>
						<?php echo $form->passwordField($modellogin,'passadmin',array('class'=>'input_f_logon form-control','value'=>'')); ?>
					</div>
					<div class="div_inp_login">
                        <p style="margin:13px 0 5px;">Код проверки</p>
                        <div class="row">
                            <div class="col-xs-5">
                                <?php echo $form->textField($modellogin,'verifyCode',array('class'=>'form-control','value'=>'')); ?>
                            </div>
                            <div class="col-xs-5">
                                <?php $this->widget('CCaptcha', [
                                    'clickableImage' => true,
                                    'showRefreshButton' => false,
                                ])?>
                            </div>
                        </div>
					</div>
				</div><br>
				<?php echo CHtml::submitButton('ВОЙТИ',array('class'=>'btn btn-primary btn-confirmContact','name'=>'btnLoginAdmin')); ?>
				<?php $this->endWidget(); ?>
		</div>
		<br>
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/remind">Восстановить забытый пароль</a>

    </div>
</div>