<a id="a_home" href="/jobs/training"></a>
<div id="settings" style="display:none;"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>

<div> 

	<?php $settings = SettingsAr::model()->findByAttributes(array('id_user'=>Yii::app()->session['login_user'],'id_job'=>2)); ?>
    <?php
//    var_dump($settings); die;
    ?>
	<div id="level" data-val="<?php echo substr($settings->level, 0, -1); ?>"></div>
	<div id="rows" data-val="0"></div> <!-- для JQuery для 3 задания, здесь не используется -->
	<div id="delay" data-val="<?=$settings->delay;?>"></div>
	<div id="map_twinkle" data-val="<?=$settings->twinkle;?>"></div>
	<div id="number_job" data-val="2"></div>

	
	<div style="float:left;margin-left: 20px;">
	
		<div style="display:inline-block;vertical-align: top;">
<!--			<a href="/jobs/training/?jobSetting=map" style="margin:15px 0 20px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
            <a href="/jobs/training/?jobSetting=map" style="margin:15px 0 20px 0; border-radius: 100%; padding: 13px 12px 10px 12px" class="btn btn-danger btn-lg"><span style="font-size: 30px" class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
			<!--<button id="btn_settingsOpen" class="btn btn-danger btn_settingsOpen_from_job" style="margin:15px 0 20px 0;" type="button">SETTINGS</button>-->
			<br>


			<form id="form_next_number" role="form">
				<input id="j2_input_number" type="text" class="form-control input_number" name="input1" autocomplete="off">

                <!--for jobs 2-->
                <?php if ($settings->twinkle == 1): ?>
                    <br>
                    <input id="testtt" type="text" class="form-control input_number" name="input2" autocomplete="off">
                <?php endif; ?>
			</form>
			<br>
			<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
		</div>
		
		<div style="display:inline-block;vertical-align: top;">
			<br><br>
			<div id="info_job" style="margin: 0px 0px 0px 30px;">
				<span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
				<p id="back_time"></p>
			</div>
			<br>
			<button id="next_number" style="margin: 0px 0px 0px 30px;" type="button" class="active-button btn btn-info">NEXT</button>
		</div>
		
		<!--<div style="display:inline-block;vertical-align: top;margin-left:30px;">
			<br><br>
			<button id="next_number" style="" type="button" class="btn btn-info">NEXT</button>
		</div>-->
		
		<!-- for test -->
		<!--<span id="j2_generated_number">0</span> <span>-->
		<br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
	</div>

	<div style="float:right;margin-right: 20px;">
		<?php require_once('abacus_html.php'); ?>
	</div>
		
</div>
