<a id="a_home" href="/jobs/training"></a>

<div> 
	<?php $settings = SettingsAr::model()->findByAttributes(array('id_user'=>Yii::app()->session['login_user'],'id_job'=>3)); ?>
	<div id="level" data-val="<?php echo substr($settings->level, 0, -1); ?>"></div>
	<div id="rows" data-val="<?php echo substr($settings->rows, 0, -1); ?>"></div>
	<div id="rules" data-val="<?=$settings->rules;?>"></div>
	<div id="voice" data-val="<?=$settings->voice;?>"></div>
	<div id="voice_lang" data-val="<?=$settings->voice_lang;?>"></div>
	<div id="count_mentally" data-val="<?=$settings->count_mentally;?>"></div>
	<div id="examples_up_to_x" data-val="<?=$settings->examples_up_to_x;?>"></div>
	<div id="number_job" data-val="3"></div>
	
	<?php
	$arr_examples = '';
	if($settings->id_rules != '' ) {
		$id_rules = substr($settings->id_rules, 0, -1); // удалим последнее ;
		$list_rules = str_replace(';', ',', $id_rules);
		
		if( $examples = ExamplesAr::model()->findAllBySql("SELECT * FROM `examples` WHERE id_rules IN ({$list_rules}) AND `rows` = ".$settings->rows." ORDER BY `id` ASC") ) {
			foreach($examples as $example) {
				$rulesAr = RulesAr::model()->findByPk($example->id_rules);
				$arr_examples = $arr_examples . $example->example . '=' . $example->answer . '#' . '<a href='.$rulesAr->link_video.' target=blanc title=Видео-инструкция>'.$rulesAr->descr.'</a>' . '&';
			}
			$arr_examples = substr($arr_examples, 0, -1); // удалим последнее &
		}
	}
	?>
	<div id="id_rules" data-val="<?=$arr_examples;?>"></div>
	

	<div id="wrap_numbers" style="float:left;margin-left: 20px;">
		
		<div style="display:inline-block;vertical-align: top;">
			<a href="/jobs/training/?jobSetting=slalom" style="margin:15px 0 20px 0; border-radius: 100%; padding: 13px 12px 10px 12px" class="btn btn-danger btn-lg"><span style="font-size: 35px" class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
            <br>
            <?php if ($settings->count_mentally == 1): ?>
                <form role="form" id="form_next_number">
                    <input id="j2_input_number" type="text" class="form-control" autocomplete="off">
                </form>
                <br>
            <?php endif; ?>
			<button id="next_number" style="" type="button" class="active-button btn btn-info">NEXT</button>
			<div id="info_job">
				<span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
				<p id="back_time"></p>
			</div>
			<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
		</div>
		
		<div style="display:inline-block;">
			<p class="numbers_slalom"><span id="source_number"></span></p>
		</div>
		
		<!-- for test -->
		<!--<span id="for_test">0</span> <span>-->
		<br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
	</div>
	
	<?php if ($settings->count_mentally == 0): ?>
        <div style="float:right;margin-right: 20px;">
            <?php require_once('abacus_html.php'); ?>
        </div>
    <?php endif; ?>
		
</div>
