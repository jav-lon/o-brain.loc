<a id="a_home" href="/jobs/training"></a>
<div id="settings" style="display:none;"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
<!--<button id="btn_settingsOpen" class="btn btn-danger btn_settingsOpen_from_job" style="margin:15px 0 0 20px;" type="button">НАСТРОЙКИ</button>-->
<!--<a href="/jobs/training/?jobSetting=MentalMap" style="margin:15px 0 20px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
<a href="/jobs/training/?jobSetting=MentalMap" style="margin:15px 0 20px 15px; border-radius: 100%; padding: 13px 12px 10px 12px" class="btn btn-danger btn-lg"><span style="font-size: 30px" class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>

<div> 

	<?php $settings = SettingsAr::model()->findByAttributes(array('id_user'=>Yii::app()->session['login_user'],'id_job'=>8)); ?>
	<div id="level" data-val="<?php echo substr($settings->level, 0, -1); ?>"></div>
	<div id="rows" data-val="<?php echo substr($settings->rows, 0, -1); ?>"></div>
	<div id="operation" data-val="<?=$settings->operation;?>"></div>
	<div id="voice" data-val="<?=$settings->voice;?>"></div>
	<div id="voice_lang" data-val="<?=$settings->voice_lang;?>"></div>
	<div id="rules" data-val="<?=$settings->rules;?>"></div>
	<div id="delay" data-val="<?=$settings->delay;?>"></div>
	<div id="number_job" data-val="8"></div>
	
	<?php
	$arr_examples = '';
	if($settings->id_rules != '' ) {
		$id_rules = substr($settings->id_rules, 0, -1); // удалим последнее ;
		//$array_rules = explode(';', $id_rules);
		$list_rules = str_replace(';', ',', $id_rules);
		
		if( $examples = ExamplesAr::model()->findAllBySql("SELECT * FROM `examples` WHERE id_rules IN ({$list_rules}) AND `rows` = ".$settings->rows." ORDER BY `id` ASC") ) {
			foreach($examples as $example) {
				$rulesAr = RulesAr::model()->findByPk($example->id_rules);
				$arr_examples = $arr_examples . $example->example . '=' . $example->answer . '#' . '<a href='.$rulesAr->link_video.' target=blanc title=Видео-инструкция>'.$rulesAr->descr.'</a>' . '&';
			}
			$arr_examples = substr($arr_examples, 0, -1); // удалим последнее &
		}
	}
	?>
	<div id="id_rules" data-val="<?=$arr_examples;?>"></div>

	
	<div style="float:left;margin-left: 20px;">
	
		<div style="display:inline-block;vertical-align: top;">
			<br><br>
			<form role="form" id="form_next_number">
				<input id="j2_input_number" type="text" class="form-control" autocomplete="off">
			</form>
			<br>
			<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;margin-bottom:20px;" class="btn btn-info">Try again</a>
		</div>
		
		<div style="display:inline-block;vertical-align: top;">
			<br><br>
			<div id="info_job" style="margin: 0px 0px 0px 30px;">
				<span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
				<p id="back_time"></p>
			</div>
			<br>
			<button id="next_number" style="margin: 0px 0px 0px 30px;" type="button" class="active-button btn btn-info">NEXT</button>
			<br><button id="answer_number" style="margin: 15px 0px 0px 30px;font-size: 30px;" type="button" class="active-button btn btn-info">ANSWER</button>
		</div>
		
		<!--<br><div id="btn_zvuk" class="zvuk_on"></div>-->
	</div>

	<div id="wrap_job8" style="">
		<?php //require_once('abacus_html.php'); ?>
	</div>
		
</div>
