<a id="a_home" href="/jobs/training"></a>

<div> 
	<?php $settings = SettingsAr::model()->findByAttributes(array('id_user'=>Yii::app()->session['login_user'],'id_job'=>6)); ?>
	<div id="level" data-val="<?php echo substr($settings->level, 0, -1); ?>"></div>
	<div id="rows" data-val="0"></div>
	<div id="voice" data-val="<?=$settings->voice;?>"></div>
	<div id="voice_lang" data-val="<?=$settings->voice_lang;?>"></div>
	<div id="number_job" data-val="6"></div>

	<div id="wrap_numbers" style="float:left;margin-left: 20px;">
<!--		<a href="/jobs/training/?jobSetting=shulteNumbers" style="margin:15px 0 0px 0;" class="btn btn-danger">НАСТРОЙКИ</a>-->
        <a href="/jobs/training/?jobSetting=shulteNumbers" style="margin:15px 0 20px 0; border-radius: 100%; padding: 13px 12px 10px 12px" class="btn btn-danger btn-lg"><span style="font-size: 30px" class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
		<p class="numbers"><span id="source_number"></span></p>
		<button id="next_number" style="" type="button" class="active-button btn btn-info">NEXT</button>
		<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" id="repeat_job" style="display:none;" class="btn btn-info">Try again</a>
		<?php //echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
		
		<div id="info_job">
			<span id="count_job">0</span> <span> / </span> <span>20</span> <span> / </span> <span id="errors"></span>
			<p id="back_time"></p>
		</div>
		
		<br><br><br><br><div id="btn_zvuk" class="zvuk_on"></div>
	</div>
	
		
	<div id="shulteNumbers" style="float:right;margin-right: 20px;">
		<?php require_once('shulte_html.php'); ?>
	</div>
		
</div>
