<?php if ( Yii::app()->session['login_user'] == '' ) { ?>
	<div class="container" id="page">
			<div class="row">
				<div class="col-lg-12">
				
					<!-- ====== АВТОРИЗАЦИЯ ====== -->
					<h2 class="page-header">Авторизация или <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/registration">Регистрация</a></h2>
					<p style="color:red;"><?=$login_notice;?></p>
					<div class="form" style="width:250px;">
						<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'login-form',
								'clientOptions'=>array('validateOnSubmit'=>true,),
								'htmlOptions'=>array('class'=>'form-horizontal',),
						)); ?>
							<div id="dop_block_login">
								<div class="div_inp_login">
									<p style="margin:13px 0 5px;">Ваш email</p>
									<?php echo $form->textField($modellogin,'nameadmin',array('class'=>'input_f_logon form-control input-lg','value'=>'')); ?>
								</div>
								<div class="div_inp_login">
									<p style="margin:13px 0 5px;">Пароль</p>
									<?php echo $form->passwordField($modellogin,'passadmin',array('class'=>'input_f_logon form-control input-lg','value'=>'')); ?>
								</div>
							</div><p style="margin:13px 0 5px;">Код проверки</p>
                        <div class="row">
                            <div class="col-xs-5">
                                <?php echo $form->textField($modellogin,'verifyCode',array('class'=>'form-control input-lg','value'=>'')); ?>
                            </div>
                            <div class="col-xs-5">
                                <?php $this->widget('CCaptcha', [
                                    'clickableImage' => true,
                                    'showRefreshButton' => false,
                                ])?>
                            </div>
                        </div>
                        <br>
							<?php echo CHtml::submitButton('ВОЙТИ',array('class'=>'btn btn-primary btn-confirmContact','name'=>'btnLoginAdmin')); ?>
							<?php $this->endWidget(); ?>
					</div>
					<br>
					<a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/remind">Восстановить забытый пароль</a>

				</div>
			</div>
	</div>
<?php } else { ?>

	<div id="notice_width_scrin" style="display:none;">Для корректного отображения, поверните устройство в горизонтальное положение<br><br><button id="btn_notice_width_scrin" type="button" class="btn btn-primary">OK</button></div>
	<!-- показ новостей -->
	<?php
	$type_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']);
	//if( $type_user->type == 'partner' || $type_user->type == 'teacher' || $type_user->type == 'admin' ) {
		if( $news = NewsAr::model()->findAll() ) {
			foreach ($news as $new) {
				//if( time() >= $new->date_from && time() <= $new->date_to ) {
					if( strpos($new->id_who_show, ";".Yii::app()->session['login_user'].";") !== false) {//echo 'DA'; ?>
						<div class="block_news">
							<p class="close_block_news">X</p>
							<p><?=$new->text;?></p>
							<p class="open_new"><a href="<?=$this->createAbsoluteUrl('/').'/lk/news?id_news='.$new->id;?>">Подробнее...</a></p>
						</div>
					<?php }
				//}
			}
		}
	//}
	?>

    <!-- лого -->
    <div id="head"></div>
	<!-- выведем рейтинг -->
	<div id="info_user">
        <?php
        $user_rating = 0;

        $connection = Yii::app()->db;

        $sql = 'SELECT SUM(`rating`) FROM `jobs` WHERE `id_user`=:user_id';

        $command = $connection->createCommand($sql);
        $command->bindValue(":user_id", $type_user->id, PDO::PARAM_INT);
        $user_rating = (float) $command->queryScalar();

//        var_dump($user_rating); die;

        $count_jobs_summ = 0;
        $count_jobs1 = 0;
        $count_jobs2 = 0;
        $count_jobs3 = 0;
        $count_jobs4 = 0;
        $count_jobs5 = 0;
        $count_jobs6 = 0;
        $count_jobs7 = 0;
        $count_jobs8 = 0;
        ?>
		<?php if( $jobs = JobsAr::model()->findAll("id_user=".Yii::app()->session['login_user']) ) {
			$count_jobs1 = 0; $count_jobs2 = 0; $count_jobs3 = 0;$count_jobs4 = 0;$count_jobs5 = 0;$count_jobs6 = 0;$count_jobs7 = 0;$count_jobs8 = 0; $count_jobs_summ = 0;
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Набор цифр на Абакусе'");
			//foreach($jobs as $job) { $count_jobs1 = $count_jobs1 + $job->rating; }
            $count_jobs1 = count($jobs);
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Определение числа с карты Абакус'");
            $count_jobs2 = count($jobs);
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Примеры на сложение / вычитание'");
            $count_jobs3 = count($jobs);
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Примеры на умножение'");
            $count_jobs4 = count($jobs);
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Примеры на деление'");
            $count_jobs5 = count($jobs);
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Bemoreclever'");
            $count_jobs6 = count($jobs);
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Bottomofabac'");
            $count_jobs7 = count($jobs);
			$jobs = JobsAr::model()->findAllBySql("SELECT * FROM `jobs` where id_user=".Yii::app()->session['login_user']." AND name='Mentalmap'");
            $count_jobs8 = count($jobs);
			$count_jobs_summ = $count_jobs1 + $count_jobs2 + $count_jobs3 + $count_jobs4 + $count_jobs5 + $count_jobs6 + $count_jobs7 + $count_jobs8;
		} ?>
		<p><?php $user = UsersAr::model()->findByPk(Yii::app()->session['login_user']); echo $user->fio; ?></p>
<!--		<p>Кол-во --><?//=$count_jobs1;?><!-- / --><?//=$count_jobs2;?><!-- / --><?//=$count_jobs3;?><!-- / --><?//=$count_jobs4;?><!-- / --><?//=$count_jobs5;?><!-- / --><?//=$count_jobs6;?><!-- / --><?//=$count_jobs7;?><!-- / --><?//=$count_jobs8;?><!-- = --><?//=$count_jobs_summ;?><!--</p>-->
		<p class="text-success">Рейтинг: <?= round($user_rating, 1) ?></p>
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/index">Панель управления</a>
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/site/logout">Выход</a>
	</div>
    <div style="clear: both;"></div>

    <!--==== uvedomlenie ===-->
    <div class="container" id="warning_delay" style="display: none; position: fixed; top: 50px;">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert" style="width: auto;">
                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    При данных настройках скорости, функция произношения цифр не работает
                </div>
            </div>
        </div>
    </div>

	<!-- ====== ЗАДАНИЯ ====== -->
	<?php if( !isset($_GET["jobSetting"]) && !isset($_GET["job"]) ) { ?>
		<h1>Тренажеры</h1>
		<a id="my_nav_middle" href="/jobs/training/?jobSetting=numbers">Набрать цифру</a>
		<a id="my_nav_middle" href="/jobs/training/?jobSetting=map">Определить цифру</a>
		<a id="my_nav_middle" href="/jobs/training/?jobSetting=shulteNumbers">Be more clever</a>
		<a id="my_nav_middle" href="/jobs/training/?jobSetting=shulteAbacus">Bottom of Abac</a>
		<a id="my_nav_middle" href="/jobs/training/?jobSetting=MentalMap">Mental Map</a>
		<a id="my_nav_middle" href="/jobs/training/?job=examples">Решение примеров</a>
		<a id="my_nav_middle" href="/simulator/settings?id_simulators=9"><strong>Smarty</strong> <span class="label label-danger">New</span></a>
	<?php
	} else { ?>
		<a style="position:absolute;top:5px;" id="a_home" href="/jobs/training"></a>
		
		<?php if(isset($_GET['job']) && $_GET["job"] == 'examples') { ?>
			<h1>Тренажеры - Решение примеров</h1>
			<a id="my_nav_middle" href="/jobs/training/?jobSetting=slalom" style="font-size: 0.9em">Сложение/вычитание</a>
			<a id="my_nav_middle" href="/jobs/training/?jobSetting=multiply">Умножение</a>
			<a id="my_nav_middle" href="/jobs/training/?jobSetting=division">Деление</a>
		<?php }
		
		elseif($_GET["jobSetting"] == 'numbers') { ?>
			<h1>Настройки тренажера - Набор чисел</h1>
			<div id="number_job_from_index" data-val="1"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			<!--
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=1" class="tab_content_a">Однозначные цифры</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=2" class="tab_content_a">Двухзначные цифры</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=3" class="tab_content_a">Трехзначные цифры</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=4" class="tab_content_a">Четырехзначные цифры</a>-->
		<?php }
		
		elseif($_GET["jobSetting"] == 'map') { ?>
			<h1>Настройки тренажера - Определить цифру</h1>
			<div id="number_job_from_index" data-val="2"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			
			<!--<p id="btn_settingsOpen" class="tab_content_a">Настройки</p>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=2" class="tab_content_a">Начать</a>-->
			<!--<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=1" class="tab_content_a">1 спица</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=2" class="tab_content_a">2 спицы</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=3" class="tab_content_a">3 спицы</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=4" class="tab_content_a">4 спицы</a>-->
		<?php }
		
		elseif($_GET["jobSetting"] == 'shulteNumbers') { ?>
			<h1>Настройки тренажера - Be more clever</h1>
			<div id="number_job_from_index" data-val="6"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			<!--<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=1" class="tab_content_a">Однозначные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=2" class="tab_content_a">Двухзначные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=3" class="tab_content_a">Трехзначные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=4" class="tab_content_a">Четырехзначные</a>-->
		<?php }
		
		elseif($_GET["jobSetting"] == 'shulteAbacus') { ?>
			<h1>Настройки тренажера - Bottom of Abac</h1>
			<div id="number_job_from_index" data-val="7"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			<!--<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=1" class="tab_content_a">1 спица</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=2" class="tab_content_a">2 спицы</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=3" class="tab_content_a">3 спицы</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=4" class="tab_content_a">4 спицы</a>-->
		<?php }
		
		elseif($_GET["jobSetting"] == 'MentalMap') { ?>
			<h1>Настройки тренажера - Mental Map</h1>
			<div id="number_job_from_index" data-val="8"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			<div id="laws" style="display:none;"><?php require_once(Yii::app()->basePath.'/views/site/rules.php'); ?></div>
			<!--<p id="btn_settingsOpen" class="tab_content_a">Настройки</p>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/mentalmap" class="tab_content_a">Начать</a>-->
		<?php }
		
		elseif($_GET["jobSetting"] == 'slalom') { ?>
			<h1>Настройки тренажера - Сложение / вычитание</h1>
			<div id="number_job_from_index" data-val="3"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			<div id="laws" style="display:none;"><?php require_once(Yii::app()->basePath.'/views/site/rules.php'); ?></div>
			<!--
			<h2 style="margin:10px;padding:0;font-size:23px;">Однозначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=4" class="tab_content_a tab_content_a_job3">четырехрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=5" class="tab_content_a tab_content_a_job3">пятирядные</a>
			<h2 style="margin:10px;padding:0;font-size:23px;">Двухзначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=4" class="tab_content_a tab_content_a_job3">четырехрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=5" class="tab_content_a tab_content_a_job3">пятирядные</a>
			<h2 style="margin:10px;padding:0;font-size:23px;">Трехзначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=4" class="tab_content_a tab_content_a_job3">четырехрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=5" class="tab_content_a tab_content_a_job3">пятирядные</a>-->
		<?php }
		
		elseif($_GET["jobSetting"] == 'multiply') { ?>
			<h1>Настройки тренажера - Умножение</h1>
			<div id="number_job_from_index" data-val="4"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			<div id="laws" style="display:none;"><?php require_once(Yii::app()->basePath.'/views/site/rules.php'); ?></div>
			<!--<h2 style="margin:10px;padding:0;font-size:23px;">Однозначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=1&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=1&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			<h2 style="margin:10px;padding:0;font-size:23px;">Двухзначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=2&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=2&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			<h2 style="margin:10px;padding:0;font-size:23px;">Трехзначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=3&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=3&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>-->
		<?php }
		
		elseif($_GET["jobSetting"] == 'division') { ?>
			<h1>Настройки тренажера - Деление</h1>
			<div id="number_job_from_index" data-val="5"></div>
			<div id="settings_built-in"><?php require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
			<div id="laws" style="display:none;"><?php require_once(Yii::app()->basePath.'/views/site/rules.php'); ?></div>
			<!--
			<h2 style="margin:10px;padding:0;font-size:23px;">Двухзначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=2&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=2&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			<h2 style="margin:10px;padding:0;font-size:23px;">Трехзначные</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=3&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=3&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>-->
		<?php }
		
		else $this->redirect(Yii::app()->homeUrl); 
	} ?>

	<!--
	<div style="margin:0px 0 0 20px;">
		<ul class="nav nav-tabs my_nav">
			<li><a id="my_nav_top" data-toggle="tab" href="#home">Набрать цифру</a></li>
			<li><a id="my_nav_middle" data-toggle="tab" href="#menu1">Определить цифру</a></li>
			<li><a id="my_nav_middle" data-toggle="tab" href="#menu2">Решение примеров</a></li>
			<li><a id="my_nav_middle" data-toggle="tab" href="#menu3">Умножение</a></li>
			<li><a id="my_nav_middle" data-toggle="tab" href="#menu4">Деление</a></li>
			<li><a id="my_nav_middle" data-toggle="tab" href="#menu5">Bemoreclever</a></li>
			<li><a id="my_nav_middle" data-toggle="tab" href="#menu6">Bottomofabac</a></li>
			<li><a id="my_nav_bottom" data-toggle="tab" href="#menu7">Mentalmap</a></li>
		</ul>

		<div class="tab-content" style="margin-left:300px;">
			<div id="home" class="tab-pane fade in active">
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=1" class="tab_content_a">Однозначные цифры</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=2" class="tab_content_a">Двухзначные цифры</a><br>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=3" class="tab_content_a">Трехзначные цифры</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers?lev=4" class="tab_content_a">Четырехзначные цифры</a>
			</div>
			<div id="menu1" class="tab-pane fade">
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=1" class="tab_content_a">1 спица</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=2" class="tab_content_a">2 спицы</a><br>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=3" class="tab_content_a">3 спицы</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/map?lev=4" class="tab_content_a">4 спицы</a>
			</div>
			<div id="menu2" class="tab-pane fade">
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Однозначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=4" class="tab_content_a tab_content_a_job3">четырехрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=1&rows=5" class="tab_content_a tab_content_a_job3">пятирядные</a>
				<br>
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Двухзначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=4" class="tab_content_a tab_content_a_job3">четырехрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=2&rows=5" class="tab_content_a tab_content_a_job3">пятирядные</a>
				<br>
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Трехзначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=4" class="tab_content_a tab_content_a_job3">четырехрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/slalom?lev=3&rows=5" class="tab_content_a tab_content_a_job3">пятирядные</a>
			</div>
			<div id="menu3" class="tab-pane fade">
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Однозначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=1&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=1&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
				<br>
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Двухзначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=2&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=2&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
				<br>
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Трехзначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=3&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/multiply?lev=3&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			</div>
			<div id="menu4" class="tab-pane fade">
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Двухзначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=2&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=2&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
				<br>
				<h2 style="margin:0 0 8px 0;padding:0;font-size:23px;color: #ffffff;">Трехзначные</h2>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=3&rows=2" class="tab_content_a tab_content_a_job3">двухрядные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/division?lev=3&rows=3" class="tab_content_a tab_content_a_job3">трехрядные</a>
			</div>
			<div id="menu5" class="tab-pane fade">
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=1" class="tab_content_a">Однозначные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=2" class="tab_content_a">Двухзначные</a><br>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=3" class="tab_content_a">Трехзначные</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteNumbers?lev=4" class="tab_content_a">Четырехзначные</a>
			</div>
			<div id="menu6" class="tab-pane fade">
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=1" class="tab_content_a">1 спица</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=2" class="tab_content_a">2 спицы</a><br>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=3" class="tab_content_a">3 спицы</a>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/shulteAbacus?lev=4" class="tab_content_a">4 спицы</a>
			</div>
			<div id="menu7" class="tab-pane fade">
				<div id="settings" style="display:none;"><?php //require_once(Yii::app()->basePath.'/views/site/settings.php'); ?></div>
				<p id="btn_settingsOpen" class="tab_content_a">Настройки</p>
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/mentalmap" class="tab_content_a">Начать</a>
			</div>
		</div>
	</div>
	-->
	

<?php } ?>