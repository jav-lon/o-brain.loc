<?php

$count_orders = 0;

if (
        Yii::app()->session['login_user'] != ''
        && ( $user = UsersAr::model()->findByPk(Yii::app()->session['login_user']) )
        && $user->type == 'admin'
) {

    $connection=Yii::app()->db;

    $sql = "SELECT COUNT(*) FROM `order` WHERE `status`='new' AND `is_deleted`=0";

    $command = $connection->createCommand($sql);

    $count_orders = (int) $command->queryScalar();
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Абакус - Панель управления</title>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/css/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/css/timeline.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/css/morris.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/pa/sidebar.css" rel="stylesheet" type="text/css">
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/js/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
		.clearboth {clear:both;}
	</style>
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Yii::app()->request->baseUrl; ?>/lk/index">Панель управления - На главную</a>
        </div>
        <div id="navbar1" class="collapse navbar-collapse">

            <!-- Top Navigation: Left Menu -->
            <ul class="nav navbar-nav navbar-left">
                <li><a href="/jobs/training"><i class="fa fa-home fa-fw"></i> Начать решение</a></li>
                <?php
                if ( Yii::app()->session['login_user'] != '' ) { //polzovatel ne gost
                    $login_user = UsersAr::model()->findByPk(Yii::app()->session['login_user']); // dannie o polzovatelya
                }
                ?>
                <?php if ( Yii::app()->session['login_user'] != '' ): //polzovatel ne gost ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Курс <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/olimp">Пройти</a></li>
                            <?php if ( $login_user->type == 'admin' ): //tolko admin mogut sozdat kurs ?>
                                <li><a href="/crud/olimp">Создать</a></li>
                            <?php endif; ?>
                            <li role="separator" class="divider"></li>
                            <li><a href="/result?type=course">Результаты</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Олимпиада <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/olympiad/list">Пройти</a></li>
                            <?php if ( $login_user->type == 'admin' || $login_user->type == 'teacher' ): ?>
                                <li><a href="/olympiad">Создать</a></li>
                            <?php endif; ?>
                            <li role="separator" class="divider"></li>
                            <li><a href="/result?type=olympiad">Результаты</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Пакеты <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <!--Vsem dostupno eti knopki-->
                            <li>
                                <a href="/package/franchise"><i class="fa fa-external-link fa-fw"></i> Франчайзинговый пакет</a>
                            </li>
                            <li>
                                <a href="/package/partner"><i class="fa fa-external-link fa-fw"></i> Партнерский пакет</a>
                            </li>
                            <?php if( $login_user->type == 'admin' ): //tolko admin mogut sozdat paket?>
                                <li>
                                    <div class="divider"></div>
                                </li>
                                <li>
                                    <a href="/package"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> Создать пакет</a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php if ( $login_user->type == 'admin'  || $login_user->type == 'teacher'): //tolko admin mogut sozdat kurs ?>
                        <li><a href="/smarty-manager"><i class="fa fa-book fa-fw"></i> Создать Smarty</a></li>
                    <?php endif; ?>

                <?php endif; ?>
            </ul>

            <!-- Top Navigation: Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <?php if ( Yii::app()->session['login_user'] == '' ): ?>
                    <li><a href="<?= Yii::app()->request->baseUrl; ?>/lk/">Вход</a></li>
                    <li><a href="<?= Yii::app()->request->baseUrl; ?>/lk/registration">Регистрация</a></li>
                <?php else: ?>
                    <li><a href="<?= Yii::app()->request->baseUrl; ?>/lk/logout">Выход (<?= $login_user->fio; ?>)</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>

<div id="wrapper">

    <div class="overlay"></div>

    <?php if ( Yii::app()->session['login_user'] != '' ): ?>
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand">
                <a href="#">
                    Abacus Open
                </a>
            </li>
            <?php
            if( $user->type == 'admin' || $user->type == 'partner' ) { ?>
                <?php if( $user->type == 'admin' ): ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/partners">Партнеры</a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/teachers" >Преподаватели</a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/students">Ученики</a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/reports">Отчеты</a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/shop">
                        Заказы
                        <?php if ($user->type == 'admin' && $count_orders > 0): ?>
                            <span class="badge"><?= $count_orders ?></span>
                        <?php endif; ?>
                    </a>
                </li>
                <?php if( $user->type == 'admin' ) { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/rules">Примеры Mental Map</a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/site-settings">Настройка сайта</a>
                    </li>
                <?php } ?>
            <?php } ?>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/changePassword">Сменить пароль</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/records">Рекорды</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/news">Новости</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/journal">Журнал</a>
            </li>
            <?php if( $user->type != 'admin' && $user->type != 'partner' ) { ?>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/profile">Профиль</a>
                </li>
            <?php } ?>
            <?php if( $user->type != 'student' ) { ?>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/filter">Фильтр</a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/sendEmail">Рассылка писем</a>
                </li>
            <?php } ?>
            <?php if( $user->type == 'student' ) { ?>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/payment?user=<?=$user->id;?>">Оплата</a>
                </li>
            <?php } ?>
            <?php //if( $type_user->type == 'teacher' ) { ?>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/lk/index">Упражнения, результаты, истории посещений</a>
            </li>
            <!--<li>
                            <a href="<?php /*echo Yii::app()->request->baseUrl; */?>/result/smarty-user-results"><i class="fa fa-bar-chart"></i> Результаты Smarty</a>
                        </li>-->
            <?php //} ?>
        </ul>
    </nav>
    <?php endif; ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php if ( Yii::app()->session['login_user'] != '' ): ?>
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
                <span class="hamb-middle"></span>
                <span class="hamb-bottom"></span>
            </button>
        <?php endif; ?>

        <div class="container">
            <div class="row">
                <!-- ... Your content goes here ... -->
                <?php echo $content; ?>
            </div>
        </div>
    </div>
	<div id="id_user" data-val="<?=Yii::app()->session['login_user'];?>"></div>
	
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/js/jquery.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/js/metisMenu.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/js/startmin.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/pa/sidebar.js"></script>
<script>
	$(document).ready(function() {
		
		//$('.datepicker').datepicker();

        // dlya registratsii, esli polzovatel uchenik otkrivaem blok ID teacher, esli net zakrivaem
        $('#registration-form input').on('change', function() {
            var formInputVal = $('input[name="RegistrationForm[type]"]:checked', '#registration-form').val();
            if (formInputVal === 'teacher') {
                $("#id_prepod").attr("style", "display:none");
            }
            if (formInputVal === 'student') {
                $("#id_prepod").attr("style", "display:block");
            }
        });
		
		// выделение\снятие всех пользователей в Рассылке писем
		$('#btn_select_all').click(function(){
			$(".usersForSendEmail").prop("checked", true);
		});
		$('#btn_unselect_all').click(function(){
			$(".usersForSendEmail").prop("checked", false);
		});
		
		// =========== Примеры mentalMap ===========
		// добавим строки дополнительных примеров
		var count_primers = 1;
		for (var j = 1; j < 50; j++) {
			count_primers++;
			$add_ex_primer = '<div id="'+count_primers+'" style="display:none;">';
				for (var i = 1; i <= 6; i++) {
					$add_ex_primer = $add_ex_primer + '<div class="col-lg-1"><p style="margin:13px 0 5px;">'+i+' число</p><input class="form-control ex_value" value="" name="'+count_primers+'ex_value_'+i+'" type="text"></div>';
				}
				$add_ex_primer = $add_ex_primer + '<div class="col-lg-1"><p style="margin:13px 0 5px;">Ответ</p><input class="form-control ex_answer" value="" name="'+count_primers+'ex_answer" type="text"></div><div class="clearfix"> </div>';
			$add_ex_primer = $add_ex_primer + '</div>';
			$("#ex_wrap_primers").append($add_ex_primer);
		}
		// вычисление ответа в добавлении примеров в Mental Map
		$(".ex_value").on('input', function() {
			var raschet = 0;
			id_wrap = $(this).parent().parent().attr('id');
			
			$("#"+id_wrap+">div>.ex_value").each(function(){
				
				if( $(this).val()[0] == '-' && $(this).val().length > 1 ) {
					raschet = raschet - parseInt($(this).val().substr(1));
				}
				else if( $(this).val()[0] == '*' && $(this).val().length > 1 ) {
					raschet = raschet * parseInt($(this).val().substr(1));
				}
				else if( $(this).val()[0] == '/' && $(this).val().length > 1 ) {
					raschet = raschet / parseInt($(this).val().substr(1));
				}
				else if( $(this).val().length > 0 ) {
					raschet = raschet + parseInt($(this).val());
				}
			});
			$("#"+id_wrap+">div>.ex_answer").val(raschet);	//console.log( 'ex_value_1 - ' + $("input[name=ex_value_1]").val());
		});
		// показать следующую строчку примера
		id_display = 1;
		$('#add_ex_primer').click(function(){
			id_display++;
			$("#"+id_display).attr("style", "display:block");
		});
		// открыть/закрыть блок примеров
		$('.title_example').click(function(){
			$(this).next().toggle();
		});
		// =========== end Примеры mentalMap ===========
		// журнал посещаемости
		$('.byl').click(function(){
			$(this).attr("style", "border:1px solid #c0c0c0"); $(this).attr("data-val", "1"); 
			$(this).next().attr("style", "border:none"); $(this).next().attr("data-val", "0"); 
		});
		$('.nebyl').click(function(){
			$(this).attr("style", "border:1px solid #c0c0c0"); $(this).attr("data-val", "1"); 
			$(this).prev().attr("style", "border:none"); $(this).prev().attr("data-val", "0"); 
		});
		$('.added').click(function(){
			var id = $(this).attr("id");
			attend = $("#body_"+id+" > [data-val='1']").attr('id');
			if(attend == 'byl') {attend = 1; attendHTML = '<div class="byl attendAdded"></div>';} else {attend = 0; attendHTML = '<div class="nebyl attendAdded"></div>';}
			//dateParts = $("#data_"+id).val().split('.'); //dd.mm.yyyy
			//data = dateParts;
			//data = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0]);
			//data = data.getTime();
			$.post("/css/addItemJournal.php", {id_user: id, data: $("#data_"+id).val(), attended: attend});
			$('<div class="blockJournal"><div class="blockJournalTitle">'+$("#data_"+id).val()+'</div><div class="blockJournalBody">'+attendHTML+'</div></div>').insertBefore($(this).parent().parent());
		});
		
	});
</script>
</body>
</html>