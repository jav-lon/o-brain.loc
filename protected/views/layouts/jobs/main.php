<?php
/* @var $this Controller */
?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">-->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">-->
    <link href="https://fonts.googleapis.com/css?family=Neucha&amp;subset=cyrillic" rel="stylesheet">

	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/css/bootstrap.min.css" rel="stylesheet">
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/js/jquery.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jui/jquery-ui.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jui/touch_punch.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/adminka/js/bootstrap.min.js"></script>
	
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/my_css.css" rel="stylesheet">
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib/howler.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jtlib.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/my_js.js"></script>
	<!--<script src="<?php /*echo Yii::app()->request->baseUrl; */?>/css/howler/howler.js"></script>-->

    <style>
        body{
            font-family: 'Neucha', cursive;
            font-size: 22px;
        }
    </style>

	<title>Obrain Abacus Open</title>
</head>

<?php 
if ( Yii::app()->session['login_user'] != '' ) {
	//$num_fon = rand (1, 2);
	//if( ($_SERVER['REQUEST_URI'] == '/') || ($_SERVER['REQUEST_URI'] == '/site/index') ) $body_background = "style='background-image: url(/images/fon".$num_fon.".png);background-position: center top;'";
	//else $body_background = ""; 
	//if( (strpos($_SERVER['REQUEST_URI'], 'numbers') !== false) || (strpos($_SERVER['REQUEST_URI'], 'map') !== false) || (strpos($_SERVER['REQUEST_URI'], 'slalom') !== false) )
	$body_background = "style='background-color:#e3e3e3;'";
	//echo $_SERVER['REQUEST_URI'];
} else $body_background = ""; ?>

<body <?=$body_background;?>>

<!--<body>-->
	<?php if ( Yii::app()->session['login_user'] == '' ) { ?>
		<header>
			<div class="container container_header">
				<!--<p class="name_portal">Know Abacus</p> -->
				<!--
				<div class="bs-example">
					<ul class="nav nav-pills">
						<li class="active"><a href="<?php echo Yii::app()->homeUrl; ?>" style="padding:4px 15px;">Главная</a></li>
						<li class="dropdown">
							<a style="padding:4px 15px;" class="dropdown-toggle" data-toggle="dropdown" href="#">Задания <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/jobs/numbers">Набрать цифру</a></li>
								<li><a href="#">Определить цифру</a></li>
								<li><a href="#">Решение примеров</a></li>
							</ul>
						</li>
					</ul>
				</div>
				-->
			</div>
		</header>
	<?php } ?>
	
		<div id="id_user" data-val="<?=Yii::app()->session['login_user'];?>"></div>
	<!--<div class="container" id="page">-->
        <div class="countdown-wrapper">
            <div class="countdown-block">
                <span id="countdownText">

                </span>
            </div>
        </div>
		<p id="Excellent" style="display:none;">Excellent!</p>
		<p id="Very_nice" style="display:none;">Very nice</p>
		<p id="Well_done" style="display:none;">Well done</p>
		<p id="Try_more" style="display:none;">Try more</p>
		
		<p id="used_rules"></p>
		
		<?php echo $content; ?>

		<div class="clear"></div>
	<!--</div>-->

	<footer>
			
	</footer>
	
</body>
</html>
