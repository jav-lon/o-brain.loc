/**
 * Created by Javlonbek on 19.08.2018.
 */


$(document).ready(function() {


    // =========== Примеры Mentalmap ===========
    // добавим строки дополнительных примеров

    // параметры
    var exampleRows = 2;
    var exampleQuantity = 1;
    var htmlExample = '';
    var wrapExamples = document.getElementById('wrap_examples');

    /**
     * Generator examples
     * @param params
     * @return Object
     */
    function generateExamples(params) {
        const PLUS_AND_MINUS = 3;

        var _rows = params.rows || 2;
        var _quantity = params.quantity || 1;
        var _oneDigits = params.oneDigits;
        var _twoDigits = params.twoDigits;
        var _expression = PLUS_AND_MINUS;

        var _minNumber = 1;
        var _maxNumber = 9;

        var _examples = {
            "ex": [],
            "ans": []
        };

        for (var i = 0; i < _quantity; i++) {
            _examples.ex[i] = [];
            var _sumOneExample = 0;

            for (var j = 0; j < _rows; j++) {
                var _exampleNumber = generateNumber();

                switch (expressionStrategy(_sumOneExample, _expression, _exampleNumber)) {
                    case '+':
                        _sumOneExample += _exampleNumber;
                        _exampleNumber += '';
                        if (j > 0) _exampleNumber = "+" + _exampleNumber;
                        break;
                    case '-':
                        _sumOneExample -= _exampleNumber;
                        _exampleNumber += '';
                        if (j > 0) _exampleNumber = "-" + _exampleNumber;
                        break;
                }

                _examples.ex[i].push(_exampleNumber);
            }
            _examples.ans.push(_sumOneExample);
        }

        function generateNumber() {
            var _number;

            if (_oneDigits === true && _twoDigits === true) {
                _number = getRandomInRange(1, 99);
            }
            else if (_oneDigits === true) {
                _number = getRandomInRange(1, 9);
            }
            else if (_twoDigits === true) {
                _number = getRandomInRange(10, 99);
            } else {
                _number = getRandomInRange(1, 99);
            }
            return _number;
        }
        
        function expressionStrategy(sum, expression, exNumber) {

            if ( expression === PLUS_AND_MINUS) {
                var _tmpExpression = getRandomInRange(1, 2);

                exNumber = _tmpExpression === 1 ? exNumber : -exNumber;

                if (sum + exNumber <= 0) {
                    return '+';
                }

                return exNumber > 0 ? '+' : '-';
            }
        }

        return _examples;
    }

    /* funksiya dlya raschet summa */
    var eventSumma = function() {
        // console.log('input')
        var sum = 0;
        id_wrap = $(this).parent().parent().attr('id');

        $("#"+id_wrap+">div>.ex_value").each(function(){

            if( $(this).val()[0] == '-' && $(this).val().length > 1 ) {
                sum = sum - parseInt($(this).val().substr(1));
            }
            else if( $(this).val()[0] == '*' && $(this).val().length > 1 ) {
                sum = sum * parseInt($(this).val().substr(1));
            }
            else if( $(this).val()[0] == '/' && $(this).val().length > 1 ) {
                sum = sum / parseInt($(this).val().substr(1));
            }
            else if( $(this).val().length > 0 ) {
                sum = sum + parseInt($(this).val());
            }
        });
        $("#"+id_wrap+">div>.ex_answer").val(sum);	//console.log( 'ex_value_1 - ' + $("input[name=ex_value_1]").val());
    };

    var renderFunc = function () {

        console.log('touch');
        exampleRows = isNaN(parseInt($('#creatementalmapform-rows').val())) ? 2 : parseInt($('#creatementalmapform-rows').val());
        exampleQuantity = isNaN(parseInt($('#creatementalmapform-quantity').val())) ? 1 : parseInt($('#creatementalmapform-quantity').val());
        // console.log(exampleRows + ' ' + exampleQuantity);

        while (wrapExamples.firstChild) {
            wrapExamples.removeChild(wrapExamples.firstChild);
        }

        // name="'+j+'ex_value_'+i+'"
        // name="'+j+'ex_answer"

        htmlExample = '';
        for (var j = 1; j <= exampleQuantity; j++) {
            htmlExample += '<div id="'+j+'" class="col-sm-12">';
            for (var i = 1; i <= exampleRows; i++) {
                if ( i == 1 ) {
                    htmlExample += '<div class="col-sm-2" style="margin-top: 40px"><strong>' + j +'-пример</strong></div>'
                }
                htmlExample += '<div class="col-sm-1">';
                htmlExample += '<p style="margin:13px 0 5px;">'+i+' чис.</p>';
                htmlExample +='<input class="form-control ex_value input-lg" value="" name="CreateMentalmapForm[examples]['+j+'][example][]" type="text">'
                    + '</div>';
            }
            htmlExample +=    '<div class="col-sm-2">'
                + '<p style="margin:13px 0 5px;">Ответ:</p>'
                + '<input readonly class="form-control ex_answer input-lg" value="" name="CreateMentalmapForm[examples]['+j+'][answer]" type="text">'
                + '</div>';
            htmlExample += '</div>';
        }
        wrapExamples.innerHTML = htmlExample;


        // вычисление ответа в добавлении примеров в Mentalmap
        $(".ex_value").on('input', eventSumma); // veshaem obrabotchik sobitiy dlya dinamicheskiy dobavlenniy elementam

    };


    //dlya nachalniy render, odin raz rabotaet i v nachale
    renderFunc();

    /* esli najmit + ili - v forme*/
    //$('#smartyForm').on('touchspin.on.startspin', renderFunc);
    $('[data-krajee-touchspin]').on('change', renderFunc);

    // vizivaem sobitiya
    $('#mentalmapForm').trigger('touchspin.on.startspin');
    // =========== end Примеры Mentalmap ===========


    /*** Generatsiya primeri ***/
    var generateButton = document.getElementById('generate_button');

    generateButton.onclick = function (ev) {
        //params
        var _rows = +document.getElementById('creatementalmapform-rows').value;
        var _quantity = +document.getElementById('creatementalmapform-quantity').value;
        var oneDigits = document.getElementById('onedigits').checked;
        var twoDigits = document.getElementById('twodigits').checked;

        var _generatedExamples = generateExamples({
            rows: _rows,
            quantity: _quantity,
            oneDigits: oneDigits,
            twoDigits: twoDigits
        });

        // var sum = 0;
        // var id_wrap = $(".ex_value").parent().parent().attr('id');
        //
        // var countExamples = getCountExamples();
        // var countRows = getCountRowsExamples();

        for (var i = 0; i < _quantity; i++) {
            $("#"+(i + 1)+">div>.ex_value").each(function(index){
                $(this).val(_generatedExamples.ex[i][index])
            });
            $("#"+(i + 1)+">div>.ex_answer").val(_generatedExamples.ans[i] + '');	//console.log( 'ex_value_1 - ' + $("input[name=ex_value_1]").val());
        }

        console.log(_generatedExamples);


    };

    function getRandomInRange(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

});

