window.addEventListener('DOMContentLoaded', function (e) {
     var deleteReportButton = document.getElementById('delete_report_button');

     deleteReportButton.onclick = function (ev) {
         var confirmValue = confirm("Вы уверены, что хотите удалить этот отчет?");

         if (!confirmValue) {
             ev.preventDefault();
         }
     };
});