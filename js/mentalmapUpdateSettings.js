/**
 * Created by Javlonbek on 19.08.2018.
 */


$(document).ready(function() {


    // =========== Примеры Smarty ===========
    // добавим строки дополнительных примеров

    // DOM elementi
    var mentalmapRows = $('#mentalmap_rows').attr('data-val');
    var mentalmapQuantity = $('#mentalmap_quantity').attr('data-val');
    var mentalmap_examples = JSON.parse($('#mentalmap_examples').attr('data-val'));
    console.log(mentalmap_examples);

    // параметры
    var exampleRows = mentalmapRows;
    var exampleQuantity = mentalmapQuantity;
    var htmlExample = '';
    var wrapExamples = document.getElementById('wrap_examples');

    /* funksiya dlya raschet summa */
    var eventSumma = function() {
        var sum = 0;
        id_wrap = $(this).parent().parent().attr('id');

        $("#"+id_wrap+">div>.ex_value").each(function(){

            if( $(this).val()[0] == '-' && $(this).val().length > 1 ) {
                sum = sum - parseInt($(this).val().substr(1));
            }
            else if( $(this).val()[0] == '*' && $(this).val().length > 1 ) {
                sum = sum * parseInt($(this).val().substr(1));
            }
            else if( $(this).val()[0] == '/' && $(this).val().length > 1 ) {
                sum = sum / parseInt($(this).val().substr(1));
            }
            else if( $(this).val().length > 0 ) {
                sum = sum + parseInt($(this).val());
            }
        });
        $("#"+id_wrap+">div>.ex_answer").val(sum);	//console.log( 'ex_value_1 - ' + $("input[name=ex_value_1]").val());
    };

    var renderFunc = function () {

        exampleRows = isNaN(parseInt($('#creatementalmapform-rows').val())) ? 2 : parseInt($('#creatementalmapform-rows').val());
        exampleQuantity = isNaN(parseInt($('#creatementalmapform-quantity').val())) ? 1 : parseInt($('#creatementalmapform-quantity').val());

        while (wrapExamples.firstChild) {
            wrapExamples.removeChild(wrapExamples.firstChild);
        }

        htmlExample = '';
        for (var j = 0; j < exampleQuantity; j++) {
            htmlExample += '<div id="'+(j+1)+'" class="col-md-12">';
            for (var i = 1; i <= exampleRows; i++) {
                if ( i === 1 ) {
                    htmlExample += '<div class="col-md-2" style="margin-top: 40px"><strong>' + (j+1) +'-пример</strong></div>'
                }
                htmlExample += '<div class="col-md-1">';
                htmlExample += '<p style="margin:13px 0 5px;">'+i+' чис.</p>';
                if ( (j+1) <= mentalmapQuantity && i <= mentalmapRows ) {
                    htmlExample += '<input class="form-control ex_value input-lg" name="CreateMentalmapForm[examples]['+j+'][example]['+(i-1)+']" value="'+mentalmap_examples[j]['example'][i-1]+'" type="text">';
                } else {
                    htmlExample += '<input class="form-control ex_value input-lg" name="CreateMentalmapForm[examples]['+j+'][example]['+(i-1)+']" type="text">';
                }
                htmlExample += '</div>';
            }
            htmlExample += '<div class="col-md-2">';
            htmlExample += '<p style="margin:13px 0 5px;">Ответ:</p>';
            if ( (j+1) <= mentalmapQuantity ) {
                htmlExample += '<input readonly class="form-control ex_answer input-lg" name="CreateMentalmapForm[examples]['+j+'][answer]" value="'+mentalmap_examples[j]['answer']+'" type="text">'
            } else {
                htmlExample += '<input readonly class="form-control ex_answer input-lg" name="CreateMentalmapForm[examples]['+j+'][answer]" type="text">'
            }
            htmlExample += '</div>';
            htmlExample += '</div>';
        }
        wrapExamples.innerHTML = htmlExample;


        // вычисление ответа в добавлении примеров в Smarty
        $(".ex_value").on('input', eventSumma); // veshaem obrabotchik sobitiy dlya dinamicheskiy dobavlenniy elementam

    };


    //dlya nachalniy render, odin raz rabotaet i v nachale
    renderFunc();

    /* esli najmit + ili - v forme*/
    //$('#smartyForm').on('touchspin.on.startspin', renderFunc);
    $('[data-krajee-touchspin]').on('change', renderFunc);

    // vizivaem sobitiya
    $('#mentalmapForm').trigger('touchspin.on.startspin');
    // =========== end Примеры Mentalmap ===========

});

