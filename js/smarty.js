/**
 * Created by Javlonbek on 21.08.2018.
 */

/**
 * Js fayl dlya trenajor Smarty
 */

$(document).ready(function () {
    /**
     * send parameters
     */
    var resultTime = 0; // milliseconds
    var resultErrors = 0;
    var resultTrueAnswers = 0;
    var resultSimulatorId = 9;

    /**
     * parametri
     */
    var timeId;
    var timeStart;
    var timeEnd;
    var id_smarty = $('#id_smarty').attr('data-val');
    var examplesAnswers = JSON.parse($('#examplesAnswers').attr('data-val'));
    var examplesQuestionsAnswers = [];
    examplesQuestionsAnswers[0] = examplesAnswers;
    examplesQuestionsAnswers[1] = [];
    var countInputs = [];
    var trueAnswers = [];
    for (var i = 0; i < examplesAnswers.length; i++) {
        countInputs.push(0);
        trueAnswers.push(null);
    }
    var countTrueAnswers = 0;
    var countErrors = 0;

    /**
     * nastroyki otobrajeniya potrachenniy vremya
     * @type {Intl.DateTimeFormat}
     */
    var formatter = new Intl.DateTimeFormat("ru", {
        minute: "numeric",
        second: "numeric"
    });

    /**
     * elementi DOM
     */
    var durationElem = $('#durationElem'); //document.getElementById('durationElem');
    var smartySendButton = $('#smartySendButton');
    var smartyTable = $('#smartyTable'); //osnovnoy smarty tablitsa

    var parseDuration = function () {
        var dateObj = new Date(Date.now() - timeStart);

        durationElem.text(formatter.format(dateObj));
    };

    var loadAnswer = function() {
        var inputValue = $(this).val();
        var inputId = $(this).attr('data-answerid');

        examplesQuestionsAnswers[1][inputId] = +inputValue;
        console.log(examplesQuestionsAnswers);
        if ( inputValue == examplesAnswers[inputId]) {
            if (++countInputs[inputId] <= 2 ) {
                trueAnswers[inputId] = true;
                countTrueAnswers++;
                //console.log('true: ' + countTrueAnswers);
            }
            $(this).parent().removeClass('has-error');
            $(this).parent().addClass('has-success');
        } else {
            if (++countInputs[inputId] > 1 ) {
                countErrors++;
                console.log('error: ' + countErrors);
            }

            $(this).parent().addClass('has-success');
            $(this).parent().addClass('has-error');
        }
    };

    var sendResult = function (e) {
        // ne vse primeri resheno
        if (countInputs.some(function (value) {
                return value === 0;
            })) {

            $(smartySendButton).popover({
                content: 'Нужно решить все примеры'
            });
            $(smartySendButton).popover('show');
            setTimeout(function () {
                $(smartySendButton).popover('hide');
            }, 3000);

            return false;
        }
        timeEnd = Date.now();
        resultTime = timeEnd - timeStart;
        resultTrueAnswers = countTrueAnswers;
        //resultErrors = countErrors;
        /* izmenil rezultal */
        var cnt = 0;
        trueAnswers.forEach(function (item, i, arr) {
            if (item === true) {
                cnt++;
            }
        });
        resultErrors = trueAnswers.length - cnt;

        clearInterval(timeId);
        $('.example-answer').attr('readonly', 'true');
        $(smartySendButton).css('display', 'none');
        $('.progress').css('display', 'block');

        var course_type = $('#course_type').attr('data-val');
        var id_user = $("#id_user").attr('data-val');
        console.log(course_type);
        if (course_type == 'simulator') {
            console.log('simulator');
            $.ajax({
                url: "/simulator/save-result",
                method: "POST",
                data: {
                    "time": resultTime,
                    "count_true_answers": resultTrueAnswers,
                    "count_errors": resultErrors,
                    'simulator_id': resultSimulatorId,
                    "id_smarty": id_smarty,
                    "qa": examplesQuestionsAnswers
                },
                success: function (data) {
                    //alert(data);
                    window.location.href = data;
                },
                error: function (data) {
                    alert("Возникла ошибка!");
                }
            });
        } else if (course_type === 'olimp') {
            console.log('olimp');
            $.post("/olimp/exercise-save-result", {
                    id_user: id_user,
                    id_exercise: parseInt($("#id_exercise").attr('data-val')),
                    number_job: 9,
                    time_job: resultTime,
                    errors: resultErrors,
                    rating: 'no',
                    arr_answers: examplesQuestionsAnswers
                },
                function (data) {
                    //alert(JSON.stringify(data));
                    window.location.href = '/olimp/result?id_exercise=' + parseInt($("#id_exercise").attr('data-val'));
                }
            );
        } else if ( course_type === 'olympiad' ) {
            console.log(course_type);
            $.ajax({
                url: "/olympiad/save-result",
                method: "POST",
                data: {
                    "id_user": id_user,
                    "id_exercise": parseInt($("#id_exercise").attr('data-val')), // olympiad_exercise
                    "time": resultTime,
                    "count_true_answers": resultTrueAnswers,
                    "count_errors": resultErrors,
                    'simulator_id': resultSimulatorId,
                    "id_smarty": id_smarty,
                    "qa": examplesQuestionsAnswers
                },
                success: function (data) {
                    //alert(data);
                    window.location.href = data;
                },
                error: function (data) {
                    alert("Возникла ошибка!");
                }
            });
        }
    };

    // Сделать клавишу Enter как Tab
    $(function() {
        var a = $("input");
        a.each(function(c, b) {
            b = $(b);
            var d = c + 1 == a.length ? a.eq(0) : a.eq(c + 1);
            b.keydown(function(a) {
                if (13 == a.which || 9 == a.which) a.preventDefault(), d.select(), d.focus()
            }) }) });

    /**
     * nachalo funksional Smarty
     */

    $('#startModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });


    // posle coundown rabotaet sistema
    //$(document).on('jt.countdown.end', function () {
    document.addEventListener('jt.countdown.end', function () {
        $('#startModal').modal('hide');
        smartyTable.css('display', 'block');

        timeStart = Date.now();

        //otobrajenie proshedshie vremya
        timeId = setInterval(parseDuration, 1000);

        // polzovatel vvodil otvet
        $('.example-answer').on('change', loadAnswer);

        // polzovatel najal knopka 'Otpravit'
        smartySendButton.on('click', sendResult);
    });

    jtSimulator.renderCountdown({
        delay: 900,
        items: ['3', '2', '1', 'START'],
        node: '#countdownText', // jquery selector
        sounds: {
            countdown: ['/sounds/countdown/countdown_beep.wav'],
            start: ['/sounds/countdown/start.mp3']
        }
    });

});
