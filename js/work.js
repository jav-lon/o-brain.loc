$(document).ready(function() {

	if (screen.width < 640) $("#notice_width_scrin").attr("style", "display:block");
	$('#btn_notice_width_scrin').click(function(){
		$("#notice_width_scrin").attr("style", "display:none");
	});


	function zvuk() {
		if(zvuk_on) plZvuk = new Audio('/images/Zvuk_ok.wav').play();
	}
	
/* передвижение косточек */
$('#bead_up_1').draggable({
        axis: "y",
		containment: [29,84,159,129],  //"parent",   /*snap: "#www",  snapMode: "inner",   snapTolerance: 10*/
		drag: function( event, ui) {
        },
		stop: function( event, ui) {
			if( (ui.offset.top != 84) && (ui.offset.top != 129) ) 
			if( $(this).hasClass("on") ) $(this).animate({ top: '74', }, 300, function() { zvuk(); /*Зак аним*/ }); else $(this).animate({ top: '29', }, 300, function() { zvuk(); /*Зак аним*/ });
		
            if( (ui.offset.top == 129) && (!$(this).hasClass("on")) ) { itogo = itogo + parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			if( (ui.offset.top == 84) && ($(this).hasClass("on")) ) { itogo = itogo - parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			$("#itogo").html(itogo);
        }
});
$('#bead_up_2').draggable({
        axis: "y",
		containment: [153,84,283,129],
		drag: function( event, ui) {
        },
		stop: function( event, ui) {
			if( (ui.offset.top != 84) && (ui.offset.top != 129) ) 
			if( $(this).hasClass("on") ) $(this).animate({ top: '74', }, 300, function() { zvuk();/*Зак аним*/ }); else $(this).animate({ top: '29', }, 300, function() { zvuk();/*Зак аним*/ });
			
			if( (ui.offset.top == 129) && (!$(this).hasClass("on")) ) { itogo = itogo + parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			if( (ui.offset.top == 84) && ($(this).hasClass("on")) ) { itogo = itogo - parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			$("#itogo").html(itogo);
        }
});
$('#bead_up_3').draggable({
        axis: "y",
		containment: [296,84,426,129],
		drag: function( event, ui) {
        },
		stop: function( event, ui) {
			if( (ui.offset.top != 84) && (ui.offset.top != 129) ) 
			if( $(this).hasClass("on") ) $(this).animate({ top: '74', }, 300, function() { zvuk();/*Зак аним*/ }); else $(this).animate({ top: '29', }, 300, function() { zvuk();/*Зак аним*/ });
			
			if( (ui.offset.top == 129) && (!$(this).hasClass("on")) ) { itogo = itogo + parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			if( (ui.offset.top == 84) && ($(this).hasClass("on")) ) { itogo = itogo - parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			$("#itogo").html(itogo);
        }
});	
$('#bead_up_4').draggable({
        axis: "y",
		containment: [439,84,569,129],
		drag: function( event, ui) {
        },
		stop: function( event, ui) {
			if( (ui.offset.top != 84) && (ui.offset.top != 129) ) 
			if( $(this).hasClass("on") ) $(this).animate({ top: '74', }, 300, function() { zvuk();/*Зак аним*/ }); else $(this).animate({ top: '29', }, 300, function() { zvuk();/*Зак аним*/ });
		
			if( (ui.offset.top == 129) && (!$(this).hasClass("on")) ) { itogo = itogo + parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			if( (ui.offset.top == 84) && ($(this).hasClass("on")) ) { itogo = itogo - parseInt($(this).attr('data-val')); $(this).toggleClass( "on" ); zvuk(); runJob(); }
			$("#itogo").html(itogo);
        }
});	
	
$('#bead_1_1').draggable({
        axis: "y",
		containment: [29,203,159,248],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_2_1").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_3_1").filter('.on').offset( { top:ui.offset.top + 90 } );
				$("#bead_4_1").filter('.on').offset( { top:ui.offset.top + 135 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 203) && (ui.offset.top != 248) ) 
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '148', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_1").filter('.on').animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_1").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_1").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_1").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_1").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_1").filter('.on').animate({ top: '328', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 203) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo + parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			if( (ui.offset.top == 248) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_2_1").hasClass("on") ) { $("#bead_2_1").toggleClass( "on" ).offset({top:293});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_3_1").hasClass("on") ) { $("#bead_3_1").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_1").hasClass("on") ) { $("#bead_4_1").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_1_2').draggable({
        axis: "y",
		containment: [153,203,283,248],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_2_2").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_3_2").filter('.on').offset( { top:ui.offset.top + 90 } );
				$("#bead_4_2").filter('.on').offset( { top:ui.offset.top + 135 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 203) && (ui.offset.top != 248) ) 
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '148', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_2").filter('.on').animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_2").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_2").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_2").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_2").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_2").filter('.on').animate({ top: '328', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 203) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob();
			}
			if( (ui.offset.top == 248) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_2_2").hasClass("on") ) { $("#bead_2_2").toggleClass( "on" ).offset({top:293});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_3_2").hasClass("on") ) { $("#bead_3_2").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_2").hasClass("on") ) { $("#bead_4_2").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_1_3').draggable({
        axis: "y",
		containment: [296,203,426,248],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_2_3").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_3_3").filter('.on').offset( { top:ui.offset.top + 90 } );
				$("#bead_4_3").filter('.on').offset( { top:ui.offset.top + 135 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 203) && (ui.offset.top != 248) ) 
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '148', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_3").filter('.on').animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_3").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_3").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_3").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_3").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_3").filter('.on').animate({ top: '328', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 203) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob();
			}
			if( (ui.offset.top == 248) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_2_3").hasClass("on") ) { $("#bead_2_3").toggleClass( "on" ).offset({top:293});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_3_3").hasClass("on") ) { $("#bead_3_3").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_3").hasClass("on") ) { $("#bead_4_3").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_1_4').draggable({
        axis: "y",
		containment: [439,203,569,248],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_2_4").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_3_4").filter('.on').offset( { top:ui.offset.top + 90 } );
				$("#bead_4_4").filter('.on').offset( { top:ui.offset.top + 135 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 203) && (ui.offset.top != 248) ) 
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '148', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_4").filter('.on').animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_4").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_4").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_2_4").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_3_4").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_4").filter('.on').animate({ top: '328', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 203) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob();
			}
			if( (ui.offset.top == 248) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_2_4").hasClass("on") ) { $("#bead_2_4").toggleClass( "on" ).offset({top:293});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_3_4").hasClass("on") ) { $("#bead_3_4").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_4").hasClass("on") ) { $("#bead_4_4").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});

$('#bead_2_1').draggable({
        axis: "y",
		containment: [29,248,159,293],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_3_1").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_4_1").filter('.on').offset( { top:ui.offset.top + 90 } );
			} else {
				if( !$("#bead_1_1").hasClass("on") ) $("#bead_1_1").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 248) && (ui.offset.top != 293) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_3_1").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_1").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_1").hasClass("on") ) $("#bead_1_1").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 248) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_1").hasClass("on") ) { $("#bead_1_1").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob();
			}
			if( (ui.offset.top == 293) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_3_1").hasClass("on") ) { $("#bead_3_1").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_1").hasClass("on") ) { $("#bead_4_1").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_2_2').draggable({
        axis: "y",
		containment: [153,248,283,293],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_3_2").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_4_2").filter('.on').offset( { top:ui.offset.top + 90 } );
			} else {
				if( !$("#bead_1_2").hasClass("on") ) $("#bead_1_2").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 248) && (ui.offset.top != 293) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_3_2").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_2").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_2").hasClass("on") ) $("#bead_1_2").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 248) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_2").hasClass("on") ) { $("#bead_1_2").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob();
			}
			if( (ui.offset.top == 293) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_3_2").hasClass("on") ) { $("#bead_3_2").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_2").hasClass("on") ) { $("#bead_4_2").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_2_3').draggable({
        axis: "y",
		containment: [296,248,426,293],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_3_3").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_4_3").filter('.on').offset( { top:ui.offset.top + 90 } );
			} else {
				if( !$("#bead_1_3").hasClass("on") ) $("#bead_1_3").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 248) && (ui.offset.top != 293) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_3_3").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_3").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_3").hasClass("on") ) $("#bead_1_3").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 248) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_3").hasClass("on") ) { $("#bead_1_3").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob();
			}
			if( (ui.offset.top == 293) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_3_3").hasClass("on") ) { $("#bead_3_3").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_3").hasClass("on") ) { $("#bead_4_3").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_2_4').draggable({
        axis: "y",
		containment: [439,248,569,293],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_3_4").filter('.on').offset( { top:ui.offset.top + 45 } );
				$("#bead_4_4").filter('.on').offset( { top:ui.offset.top + 90 } );
			} else {
				if( !$("#bead_1_4").hasClass("on") ) $("#bead_1_4").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 248) && (ui.offset.top != 293) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '193', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_3_4").filter('.on').animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				$("#bead_4_4").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_4").hasClass("on") ) $("#bead_1_4").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 248) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_4").hasClass("on") ) { $("#bead_1_4").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob();
			}
			if( (ui.offset.top == 293) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_3_4").hasClass("on") ) { $("#bead_3_4").toggleClass( "on" ).offset({top:338});	itogo = itogo - parseInt($(this).attr('data-val')); }
				if( $("#bead_4_4").hasClass("on") ) { $("#bead_4_4").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob();
			}
			$("#itogo").html(itogo);
        }
});

$('#bead_3_1').draggable({
        axis: "y",
		containment: [29,293,159,338],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_4_1").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_1").hasClass("on") ) $("#bead_1_1").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_2_1").hasClass("on") ) $("#bead_2_1").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 293) && (ui.offset.top != 338) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_4_1").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_1").hasClass("on") ) $("#bead_1_1").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_1").hasClass("on") ) $("#bead_2_1").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 293) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_1").hasClass("on") ) { $("#bead_1_1").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_1").hasClass("on") ) { $("#bead_2_1").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 338) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_4_1").hasClass("on") ) { $("#bead_4_1").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_3_2').draggable({
        axis: "y",
		containment: [153,293,283,338],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_4_2").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_2").hasClass("on") ) $("#bead_1_2").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_2_2").hasClass("on") ) $("#bead_2_2").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 293) && (ui.offset.top != 338) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_4_2").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_2").hasClass("on") ) $("#bead_1_2").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_2").hasClass("on") ) $("#bead_2_2").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 293) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_2").hasClass("on") ) { $("#bead_1_2").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_2").hasClass("on") ) { $("#bead_2_2").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 338) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_4_2").hasClass("on") ) { $("#bead_4_2").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_3_3').draggable({
        axis: "y",
		containment: [296,293,426,338],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_4_3").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_3").hasClass("on") ) $("#bead_1_3").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_2_3").hasClass("on") ) $("#bead_2_3").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 293) && (ui.offset.top != 338) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_4_3").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_3").hasClass("on") ) $("#bead_1_3").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_3").hasClass("on") ) $("#bead_2_3").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 293) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_3").hasClass("on") ) { $("#bead_1_3").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_3").hasClass("on") ) { $("#bead_2_3").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 338) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_4_3").hasClass("on") ) { $("#bead_4_3").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_3_4').draggable({
        axis: "y",
		containment: [439,293,569,338],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				$("#bead_4_4").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_4").hasClass("on") ) $("#bead_1_4").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_2_4").hasClass("on") ) $("#bead_2_4").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 293) && (ui.offset.top != 338) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '238', }, 300, function() { zvuk();/*Зак аним*/ });
				$("#bead_4_4").filter('.on').animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			} else {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_4").hasClass("on") ) $("#bead_1_4").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_4").hasClass("on") ) $("#bead_2_4").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 293) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_4").hasClass("on") ) { $("#bead_1_4").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_4").hasClass("on") ) { $("#bead_2_4").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 338) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( $("#bead_4_4").hasClass("on") ) { $("#bead_4_4").toggleClass( "on" ).offset({top:383});	itogo = itogo - parseInt($(this).attr('data-val')); }
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});

$('#bead_4_1').draggable({
        axis: "y",
		containment: [29,338,159,383],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				//$("#bead_4_1").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_1").hasClass("on") ) $("#bead_1_1").offset( { top:ui.offset.top - 135 } );
				if( !$("#bead_2_1").hasClass("on") ) $("#bead_2_1").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_3_1").hasClass("on") ) $("#bead_3_1").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 338) && (ui.offset.top != 383) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
			} else {
				$(this).animate({ top: '328', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_1").hasClass("on") ) $("#bead_1_1").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_1").hasClass("on") ) $("#bead_2_1").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_3_1").hasClass("on") ) $("#bead_3_1").animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 338) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_1").hasClass("on") ) { $("#bead_1_1").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_1").hasClass("on") ) { $("#bead_2_1").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_3_1").hasClass("on") ) { $("#bead_3_1").toggleClass( "on" ).offset({top:293}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 383) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_4_2').draggable({
        axis: "y",
		containment: [153,338,283,383],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				//$("#bead_4_1").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_2").hasClass("on") ) $("#bead_1_2").offset( { top:ui.offset.top - 135 } );
				if( !$("#bead_2_2").hasClass("on") ) $("#bead_2_2").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_3_2").hasClass("on") ) $("#bead_3_2").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 338) && (ui.offset.top != 383) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
			} else {
				$(this).animate({ top: '328', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_2").hasClass("on") ) $("#bead_1_2").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_2").hasClass("on") ) $("#bead_2_2").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_3_2").hasClass("on") ) $("#bead_3_2").animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 338) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_2").hasClass("on") ) { $("#bead_1_2").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_2").hasClass("on") ) { $("#bead_2_2").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_3_2").hasClass("on") ) { $("#bead_3_2").toggleClass( "on" ).offset({top:293}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 383) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_4_3').draggable({
        axis: "y",
		containment: [296,338,426,383],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				//$("#bead_4_1").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_3").hasClass("on") ) $("#bead_1_3").offset( { top:ui.offset.top - 135 } );
				if( !$("#bead_2_3").hasClass("on") ) $("#bead_2_3").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_3_3").hasClass("on") ) $("#bead_3_3").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 338) && (ui.offset.top != 383) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
			} else {
				$(this).animate({ top: '328', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_3").hasClass("on") ) $("#bead_1_3").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_3").hasClass("on") ) $("#bead_2_3").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_3_3").hasClass("on") ) $("#bead_3_3").animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 338) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_3").hasClass("on") ) { $("#bead_1_3").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_3").hasClass("on") ) { $("#bead_2_3").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_3_3").hasClass("on") ) { $("#bead_3_3").toggleClass( "on" ).offset({top:293}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 383) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});
$('#bead_4_4').draggable({
        axis: "y",
		containment: [439,338,569,383],
		drag: function( event, ui) {
			if( $(this).hasClass("on") ) {
				//$("#bead_4_1").filter('.on').offset( { top:ui.offset.top + 45 } );
			} else {
				if( !$("#bead_1_4").hasClass("on") ) $("#bead_1_4").offset( { top:ui.offset.top - 135 } );
				if( !$("#bead_2_4").hasClass("on") ) $("#bead_2_4").offset( { top:ui.offset.top - 90 } );
				if( !$("#bead_3_4").hasClass("on") ) $("#bead_3_4").offset( { top:ui.offset.top - 45 } );
			}
        },
		stop: function( event, ui) {
			// ROW1 (animate 148,193  offset 203,248) ROW2 (animate 193,238  offset 248,293)  ROW3 (animate 238,283  offset 293,338)  ROW4 (animate 283,328  offset 338,383)
			if( (ui.offset.top != 338) && (ui.offset.top != 383) )
			if( $(this).hasClass("on") ) {
				$(this).animate({ top: '283', }, 300, function() { zvuk();/*Зак аним*/ });
			} else {
				$(this).animate({ top: '328', }, 300, function() { zvuk();/*Зак аним*/ });
				if( !$("#bead_1_4").hasClass("on") ) $("#bead_1_4").animate({ top: '193', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_2_4").hasClass("on") ) $("#bead_2_4").animate({ top: '238', }, 300, function() { /*Зак аним*/ });
				if( !$("#bead_3_4").hasClass("on") ) $("#bead_3_4").animate({ top: '283', }, 300, function() { /*Зак аним*/ });
			}
		
			if( (ui.offset.top == 338) && (!$(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				if( !$("#bead_1_4").hasClass("on") ) { $("#bead_1_4").toggleClass( "on" ).offset({top:203}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_2_4").hasClass("on") ) { $("#bead_2_4").toggleClass( "on" ).offset({top:248}); itogo = itogo + parseInt($(this).attr('data-val')); }
				if( !$("#bead_3_4").hasClass("on") ) { $("#bead_3_4").toggleClass( "on" ).offset({top:293}); itogo = itogo + parseInt($(this).attr('data-val')); }
				itogo = itogo + parseInt($(this).attr('data-val')); 
				zvuk(); runJob(); 
			}
			if( (ui.offset.top == 383) && ($(this).hasClass("on")) ) {
				$(this).toggleClass( "on" );
				itogo = itogo - parseInt($(this).attr('data-val'));
				zvuk(); runJob(); 
			}
			$("#itogo").html(itogo);
        }
});
	
	/* начальные установки */
	var id_user = $("#id_user").attr('data-val');
	var level = parseInt($("#level").attr('data-val'));
	var rows = parseInt($("#rows").attr('data-val')); var rows_for_calculate_rating = rows;
	var operation = 0;
	var delay = 1000;
	var zvuk_on = true;
	var errors = 0; var errors_for_rate = 0;
	var arr_answers = {}; var arr_answers_count = 0;
	var count_job = 0;
	var count_true_job = 0;
 	var number_job = parseInt($("#number_job").attr('data-val')); 	
	//if(!number_job_from_index) number_job_from_index = number_job;
	var time_begin = parseInt(new Date().getTime()/1000);
	var values_for_job3 = [];
	var count_part_for_coloring_green = 1; // для закраски правильных ответов в примерах в 3 задании
	var time_out = 0;
	var timeout_id;
	var timerId; var timerIdDeactivated = false;
	var source_number = 0;
	var selectNumberForShulteNumbers = 0; var selectNumberForShulteAbacus = 0;
	var itogo = 0;   $("#itogo").html(itogo);
	var voice = parseInt($("#voice").attr('data-val'));
	var sound_num = ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99','100'];
	var rating = 0;

    /*==dlya 2, 8 trenajorov==*/
    var isNextButton = false; // update_sparrow
    var isAnswerButton = false; // update_sparrow

    /* DOM elementi */
    var nextNumberButton = $("#next_number");
	
	/* кнопка вкл / выкл щелчков */
	$('#btn_zvuk').click(function(){
		$(this).toggleClass( "zvuk_off" );
		if(zvuk_on) zvuk_on = false; else zvuk_on = true;
	});
	
	function nextdetailedJobForAnswer() {
		arr_answers_count = arr_answers_count + 1;
		arr_answers[arr_answers_count] = {};
		arr_answers[arr_answers_count]['nomer_job'] = arr_answers_count;
        arr_answers[arr_answers_count]['answer'] = 'нет ответа'; // update_sparrow
    }
	function addDetailedJob(param) {
		if(param == 'initialization') {
			nextdetailedJobForAnswer();
			arr_answers[arr_answers_count]['question'] = source_number;
			console.log( '========================= initialization. arr_answers[arr_answers_count][question] - ' + arr_answers[arr_answers_count]['question'] );
		}
		if(param == 'from_nextJob') {
			if( (number_job == 1) || (number_job == 2) || (number_job == 3) || (number_job == 4) || (number_job == 5) || (number_job == 6) || (number_job == 7) || (number_job == 8) ) {
				//if( source_number == itogo ) {
					//arr_answers[arr_answers_count]['answer'] = itogo;
					//console.log( 'arr_answers[arr_answers_count][answer] - ' + arr_answers[arr_answers_count]['answer'] );
				//} else {
					//arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT';
					console.log( 'arr_answers[arr_answers_count][answer] - ' + arr_answers[arr_answers_count]['answer'] );
				//}
			}
			nextdetailedJobForAnswer();	arr_answers[arr_answers_count]['question'] = source_number;
			console.log( '========================= initialization. arr_answers[arr_answers_count][question] - ' + arr_answers[arr_answers_count]['question'] );
		}
	}
	
	/* определим уровень сложности и генерацию числа */
	var min_in = 0; var max_in = 0;
	if( parseInt($("#level").attr('data-val')) == 1 ) { min_in = 2; max_in = 9; }
	if( parseInt($("#level").attr('data-val')) == 2 ) { min_in = 10; max_in = 99; }
	if( parseInt($("#level").attr('data-val')) == 3 ) { min_in = 100; max_in = 999; }
	if( parseInt($("#level").attr('data-val')) == 4 ) { min_in = 1000; max_in = 9999; }
	function getRandomInRange(min, max) {
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	/* вывод сообщения и отправка результата */
	function outMessageAndSendResult() {
		$("#source_number").attr("style", "color:#8AE595");
		var time_end = parseInt(new Date().getTime()/1000); 
		var time_job = time_end - time_begin;
		//if( time_out < 1 ) $("#Try_more").attr("style", "display:block"); // to do. начисляется ли рейтинг, если время вышло?
		/*else {
			if( errors < 2 ) $("#Excellent").attr("style", "display:block");
			if( (errors >= 2) && (errors <= 3) ) $("#Very_nice").attr("style", "display:block");
			if( (errors >= 4) && (errors <= 5) ) $("#Well_done").attr("style", "display:block");
			if( errors > 5 ) $("#Try_more").attr("style", "display:block");
		}*/
		// подсчет рейтинга
		if(errors == 0) errors = 1; else errors_for_rate = errors; // на 0 не делится
		if( number_job == 1 ) {
			rating = count_job / errors; rating = rating.toFixed(1);
		}
		if( (number_job == 2) || (number_job == 8) ) {
			rating = (count_job / errors) * 1,3; rating = rating.toFixed(1);
		}
		if( (number_job == 3) || (number_job == 4) || (number_job == 5) ) {
			if(rows_for_calculate_rating == 1) { rating = (count_job / errors) * 1,2; rating = rating.toFixed(1); }
			if(rows_for_calculate_rating == 2) { rating = (count_job / errors) * 1,3; rating = rating.toFixed(1); }
			if(rows_for_calculate_rating == 3) { rating = (count_job / errors) * 1,4; rating = rating.toFixed(1); }
			if(rows_for_calculate_rating == 4) { rating = (count_job / errors) * 1,6; rating = rating.toFixed(1); }
			if(rows_for_calculate_rating == 5) { rating = (count_job / errors) * 1,7; rating = rating.toFixed(1); }
			if(rows_for_calculate_rating == 6) { rating = (count_job / errors) * 1,8; rating = rating.toFixed(1); }
		}
		if( (number_job == 6) || (number_job == 7) ) {
			if( (errors > 5) || (time_out < 1) ) { rating = 0; }
			else {
				if( parseInt($("#level").attr('data-val')) == 1 ) rating = 5;
				if( parseInt($("#level").attr('data-val')) == 2 ) rating = 6;
				if( parseInt($("#level").attr('data-val')) == 3 ) rating = 7;
				if( parseInt($("#level").attr('data-val')) == 4 ) rating = 8;
			}
		}

		/*$.post("/css/completedJob.php", {
			id_user: id_user,
			number_job: number_job,
			time_job: time_job,
			errors: errors_for_rate,
			rating: rating,
			arr_answers: arr_answers
		});*/

        var arr_answers_errors_count = 0
        for (var key in arr_answers) {
            if (arr_answers[key]['answer'] != arr_answers[key]['question']) {
                arr_answers_errors_count++;
            }
        }

        var result_count_errors = arr_answers_errors_count + (20 - arr_answers_count);

		console.log('count_job: ' + count_job + ' true_job: ' + count_true_job);
        var course_type = $('#course_type').attr('data-val');
		if ( course_type == 'course' ) {

            // отправка результата
            $.post("/course/exercise-save-result", {
					id_user: id_user,
					id_exercise: parseInt($("#id_exercise").attr('data-val')),
					number_job: number_job,
					time_job: time_job,
					errors: result_count_errors,
					rating: rating,
					arr_answers: arr_answers
				},
				function (data) {
					//alert(JSON.stringify(data));
					window.location.href = '/course/exercise-result?id_exercise=' + parseInt($("#id_exercise").attr('data-val'));
				}
        	);
		} else if (course_type == 'olimp') {
            // отправка результата
            $.post("/olimp/exercise-save-result", {
                    id_user: id_user,
                    id_exercise: parseInt($("#id_exercise").attr('data-val')),
                    number_job: number_job,
                    time_job: time_job,
                    errors: result_count_errors,
                    rating: rating,
                    arr_answers: arr_answers
                },
                function (data) {
                    //alert(JSON.stringify(data));
                    window.location.href = '/olimp/result?id_exercise=' + parseInt($("#id_exercise").attr('data-val'));
                }
            );
		} else {
			alert('Error');
		}

        $("#loading").attr("style", "display:block");

		$("#source_number").html('');
		$("#next_number").attr("style", "display:none");
		$("#answer_number").attr("style", "display:none");
		//$("#repeat_job").attr("style", "display:block");
		$("#back_time").attr("style", "display:none");
		$("#j2_input_number").attr("disabled", true); // для 2 задания (карта), чтоб не вводили дальше цифры
		$("#abacus").attr("style", "visibility:hidden");
		$("#shulte").attr("style", "visibility:hidden");
	}
	
	/* обратный отсчет времени */
	function resetTimeOut() {
		if( !isNaN(number_job) ) { // убеждаемся что находимся в одном из заданий
			$("#back_time").attr("style", "color:#000000");
			timerIdDeactivated = false;
			timerId = setInterval(function() {
				time_out = time_out - 1;
				if(time_out == 10) $("#back_time").attr("style", "color:#FF6666");
				$("#back_time").html(time_out);
				/* milliseconds */
        /*$.each([ 10,9,8,7,6,5,4,3,2,1 ], function( index, value ) {
            milliseconds = setInterval(function() {
              $("#back_time").html(time_out+','+value);
              //console.log( index + ": " + value );
            }, 100);
        });*/
			}, 1000);
			for_outMessage = setTimeout(function() {
				if(timerIdDeactivated == false) { clearInterval(timerId); outMessageAndSendResult(); }
			}, (time_out * 1000) + 1 );
		}
	}
	
	/* кнопка сброса Абакуса */
	$('#reset_btn').click(function(){
		zvuk(); resetAbacus(0);
	});
	/* сброс Абакуса */
	function resetAbacus(del) {
		//setTimeout(function () {
			$('.row_up').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top - 45}; });
			$('.row1').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			$('.row2').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			$('.row3').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			$('.row4').filter('.on').removeClass("on").offset(function(i,val){ return {top:val.top + 45}; });
			itogo = 0; $("#itogo").html(itogo);
		//}, del); // время в мс
	}
	
	function generatePrimer() {
		values_for_job3 = []; parts_for_job8 = []; znak_operat = []; znak_operat.push('beg'); r_element = ''; //tmp_oper3 = '';
		rows_source = parseInt($("#rows").attr('data-val'));
		if(operation == 2 ||operation == 4 || operation == 6) { // умножение и (или) деление
			if(rows_source > 2) rows_source = 2; // максимум 2 ряда (это также регулируется в окне настроек упражнения)
			if(level == 1) { min_in = 10; max_in = 99; } // минимум 2 значные, для умножения и (или) деления
		}
		rows = rows_source;
		if( number_job == 3 || operation == 1 || operation == 3 || operation == 5 ) {primer_summ = 0;} else {primer_summ = 1;}  primer_text = '';  count_part = 1; 
		if(operation == 6) { if( (Math.random() * (2 - 0) + 2) != 1 ) job8_umnojenie = true; else job8_umnojenie = false; }
		while (rows) { // при i, равном 0, значение в скобках будет false и цикл остановится
			
			if( number_job == 3 || operation == 1 || operation == 3 || operation == 5 ) { // сложение / вычитание
				part = getRandomInRange(min_in, max_in);
				if(operation == 1) { // задание 8: сложение
					primer_summ = primer_summ + part; znak_operat.push(' + ');
				} else if(operation == 3) { // задание 8: вычитание
					if( rows == rows_source ) { // цикл первый раз
						part = getRandomInRange( parseInt( (max_in / rows_source) * (rows_source - 1) ), max_in );
						primer_summ = part; znak_operat.push(' - ');
					} else {
						part = getRandomInRange( min_in, parseInt(max_in / rows_source) );
						primer_summ = primer_summ - part; znak_operat.push(' - ');
					}
				} else { // все остальное
					if( (Math.random() * (2 - 0) + 2) == 1 ) added = true; else added = false;
					if(added) {
						primer_summ = primer_summ + part; znak_operat.push(' + ');
					} else {
						if( (primer_summ - part) < 0 ) {
							primer_summ = primer_summ + part; znak_operat.push(' + ');
						} else {
							primer_summ = primer_summ - part; znak_operat.push(' - ');
						}
					}
				}
				values_for_job3.push(primer_summ);
				primer_text = primer_text + znak_operat[znak_operat.length-1] + '<span id="count_part'+count_part+'">' + String(part) + '</span><br>';	//primer_text = primer_text + ' + <span id="count_part'+count_part+'">' + String(part) + '</span><br>';
				parts_for_job8.push(part);
			
			} else if ( number_job == 4 || operation == 2 || ((operation == 6) && (job8_umnojenie == true)) ) { // умножение
				part = getRandomInRange(min_in, max_in);	//console.log( 'part: ' + part );
				primer_summ = primer_summ * part;
				values_for_job3.push(primer_summ);
				primer_text = primer_text + ' * <span id="count_part'+count_part+'">' + String(part) + '</span><br>';
				parts_for_job8.push(part); znak_operat.push(' * ');
				
			} else if( number_job == 5 || operation == 4 || ((operation == 6) && (job8_umnojenie == false)) ) { // деление
				var simple_numbers = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997];
				var push_ok = false;
				if( rows == rows_source ) { // первый цикл 
					while (!push_ok) {
						part = getRandomInRange(min_in, max_in);
						if( simple_numbers.indexOf(part) == -1 ) push_ok = true;
					}
					primer_summ = part;
					console.log( 'первый цикл. part: ' + part );
				} else {
					str_values_for_job3 = values_for_job3[values_for_job3.length-1]; // берем последний элемент из массива
					if(rows == 2) { // ПРЕДпоследний цикл
						part = 1;
						while (!push_ok) {
							part++;
							if( str_values_for_job3 % part == 0 ) if ( simple_numbers.indexOf(str_values_for_job3 / part) == -1 ) push_ok = true;
						}
						console.log( 'ПРЕДпоследний цикл. делим на - ' + part );
					} else { // последний цикл ( rows == 1 )
						while (!push_ok) {
							part = getRandomInRange(2, str_values_for_job3 - 1);
							if( str_values_for_job3 % part == 0) push_ok = true;
						}
						console.log( 'ПОСЛЕДНИЙ цикл. делим на - ' + part );
					}
					primer_summ = str_values_for_job3 / part;
				}
				values_for_job3.push(primer_summ);
				primer_text = primer_text + ' : <span id="count_part'+count_part+'">' + String(part) + '</span><br>';
				parts_for_job8.push(part); znak_operat.push(' : ');
			}
			rows--; count_part++;
		}
		source_number = primer_summ;
		if(voice != 2) $("#source_number").html(primer_text.substr(3)); else $("#source_number").html('?');
		// для 8 задания
        //if(number_job == 8) $("#wrap_job8").html(primer_text.substr(3));
		if(operation > 0 || rules == 2) {
			// 2|+|2|+|2|+|2|+|2=10&20|/|2|-|5|*|3=15
			// 2|+|2|+|2|+|2|+|2=10#2 закон&20|/|2|-|5|*|3=15#4 закон
			if(rules == 2 && id_rules !== '') {
				examples = id_rules.split('&');
				part = getRandomInRange(0, examples.length-1); // случайный элемент из массива с примерами
				example_full_withRules = examples[part].split('#');
				usedRules = example_full_withRules[1];
				if(rules == 2) $("#used_rules").html(usedRules);
				example_full = example_full_withRules[0].split('=');
				example_answer = example_full[1]; source_number = example_answer; // сначала возьмем ответ из случайного примера
				example_body_pak = example_full[0]; // сам пример
				example_body = example_body_pak.split('|'); // разобьем пример на составляющие
				// разнесем разобранный пример по массивам
				parts_for_job8 = []; znak_operat = []; znak_operat.push('beg'); znak_operat.push('beg'); primer_text = ''; count_part = 1; 
				example_body.forEach(function(element, i){
					if(element == '+' || element == '-' || element == '*' || element == '/') {
						znak_operat.push(' '+element+' ');
						primer_text = primer_text + ' '+element+' ';
					} else {
						parts_for_job8.push(element);
						primer_text = primer_text + '<span id="count_part'+count_part+'">' + String(element) + '</span><br>'; count_part++;
					}
				});
				if(voice != 2) $("#source_number").html(primer_text);
				//console.log( '================== ' );console.log( 'usedRules: ' + usedRules );console.log( 'examples[0]: ' + examples[0] );console.log( 'example_body_pak: ' + example_body_pak );
				//console.log( 'source_number: ' + source_number );console.log( 'example_answer: ' + example_answer );console.log( 'последний эл parts_for_job8: ' + parts_for_job8[parts_for_job8.length-1] );	console.log( 'последний эл znak_operat: ' + znak_operat[znak_operat.length-1] );
				console.log( '2' );
			}
			console.log( '1' );
		}
		if(voice != 3) {
			if(voice == 2) show_depend_voice = false; else show_depend_voice = true; // voice == 2 - только голос
			if(level < 3) delay2 = 2000; else delay2 = delay;
			parts_for_job8.forEach(function(element, i){
			   setTimeout(function(){
				  if(level < 3) {
					  delay = 800; // вместо 0,8
					  if(i > 1) {
						if(znak_operat[i] == ' + ') { if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/plus.opus'] }); plZvuk.play();} }
						if(znak_operat[i] == ' - ') { if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/minus.opus'] }); plZvuk.play();} }
						if(znak_operat[i] == ' * ') { if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/multiply.opus'] }); plZvuk.play();} }
						if(znak_operat[i] == ' : ' || znak_operat[i] == ' / ') { if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/divide.opus'] }); plZvuk.play();} }
						setTimeout(function(){
              if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/'+sound_num[element]+'.opus'] }); plZvuk.play();}
						}, delay);
					  } else {
              if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/'+sound_num[element]+'.opus'] }); plZvuk.play();}
					  }
				  }
                   if(show_depend_voice) r_element = element; else r_element = '?';
				  //if(show_depend_voice) {
					  if(i > 1) {
						  if(znak_operat[i] == ' + ' || znak_operat[i] == ' * ') style_color = 'color:red;'; else style_color = 'color:blue;';
						  complete_element = '<span class="znak_operat">'+znak_operat[i]+'</span><span style="'+style_color+'">'+r_element+'</span>';
					  } else complete_element = '<span style="color:red;">'+r_element+'</span>';
					  $("#wrap_job8").html(complete_element);
					  $("#wrap_job8").fadeIn(0).delay(delay).fadeOut(0);
				  //} else $("#wrap_job8").attr("style", "display:none");
			   }, (delay2) * ++i);
			});
		} else {
			if(number_job == 8) {
                if(level < 3) delay2 = 2000; else delay2 = delay;
                parts_for_job8.forEach(function(element, i){
                    setTimeout(function(){
                        if(i > 1) {
                            if(znak_operat[i] == ' + ' || znak_operat[i] == ' * ') style_color = 'color:red;'; else style_color = 'color:blue;';
                            complete_element = '<span class="znak_operat">'+znak_operat[i]+'</span><span style="'+style_color+'">'+element+'</span>';
                        } else complete_element = '<span style="color:red;">'+element+'</span>';
                        $("#wrap_job8").html(complete_element);
                        $("#wrap_job8").fadeIn(0).delay(delay).fadeOut(0);
                    }, (delay2) * ++i);
                });
			}
		}
	}
		
	/*= задание 6 =*/
	function generateShulteNumbers() {
		var i; arr_numbers = [];
		for (i = 1; i < 26; i++) {
		  if(getRandomInRange(1, 2) == 1) bold = 'bold'; else bold = 'normal';
		  colors = ['#000','#996600','#FF6633','#990000','#FF6699','#993366','#FF33FF','#9999FF','#3333CC','#006666','#00CC99','#339933','#666633','#999999'];
		  familys = ['Arial','Comic Sans MS','Courier New','Georgia','Impact','Lucida Console','Microsoft Sans Serif','Tahoma','Times New Roman','Trebuchet MS','Verdana'];
		  rand_number = getRandomInRange(min_in, max_in); arr_numbers.push(rand_number);
		  $("#"+i).html('<span style="font-size:'+getRandomInRange(20, 50)+'px; font-weight:'+bold+';font-family:'+familys[getRandomInRange(0, 10)]+';color:'+colors[getRandomInRange(0, 13)]+';">'+rand_number+'</span>');
		}
		source_number = getRandomInRange(min_in, max_in);
		while(arr_numbers.indexOf(source_number) == -1) {
			source_number = getRandomInRange(min_in, max_in);
		}
		// оставим source_number в одном экземпляре
		var once_exist = false; if(source_number == max_in) source_number_tmp = source_number - 1; else source_number_tmp = source_number + 1;
		for(i = 0; i < arr_numbers.length; i++) {
			//if(arr_numbers[i] == source_number) if(once_exist == false) once_exist = true; else { idNumb=i+1; $("#"+idNumb).text(source_number_tmp); }
            if(arr_numbers[i] == source_number) if(once_exist == false) once_exist = true; else { idNumb=i+1; $("#"+idNumb).html('<span style="font-size:'+getRandomInRange(20, 50)+'px; font-weight:'+bold+';font-family:'+familys[getRandomInRange(0, 10)]+';color:'+colors[getRandomInRange(0, 13)]+';">'+source_number_tmp+'</span>'); } // update_sparrow
        }
		if(voice !== 2) $("#source_number").html(source_number); else $("#source_number").html('?');
	}
	/*= задание 7 =*/
	function generateShulteAbacus() {
		var i; arr_numbers = [];
		for (i = 0; i < 9; i++) { rand_number = getRandomInRange(min_in, max_in); arr_numbers.push(rand_number); }
		source_number = getRandomInRange(min_in, max_in);
		while(arr_numbers.indexOf(source_number) == -1) {
			source_number = getRandomInRange(min_in, max_in);
		}
		// оставим source_number в одном экземпляре
		var once_exist = false; if(source_number == max_in) source_number_tmp = source_number - 1; else source_number_tmp = source_number + 1;
		for(i = 0; i < arr_numbers.length; i++) {
			if(arr_numbers[i] == source_number) if(once_exist == false) once_exist = true; else arr_numbers[i] = source_number_tmp;
		}
		for (i = 0; i < 9; i++) {
			add_html = '';
			rand_number_str = arr_numbers[i].toString();
			for (j = 0; j < rand_number_str.length; j++) {
				add_html = add_html + '<img src="/images/'+rand_number_str[j]+'.png" />';
				console.log(rand_number_str[j]);
			}
			idNumb=i+1;
			$("#"+idNumb).attr("data-val", arr_numbers[i]);
			$("#"+idNumb).html(add_html);
		}
		if(voice !== 2) $("#source_number").html(source_number);
	}
	/*= клик по таблице в задании 6 и 7 =*/
	$('#shulteNumbers>div>table>tbody>tr>td').click(function(){
		if( number_job == 6 ) {	selectNumberForShulteNumbers = $(this).children().text(); }
		else { selectNumberForShulteNumbers = $(this).children().attr('data-val'); }
        isNextButton = false; // update_sparrow
        /* Job = 8 MentalMap*/
        isAnswerButton = false; // update_sparrow
		nextJob();
	});

	
	/* === инициализация заданий === */
	// 1 - numbers, 2 - map, 3 - slalom, 4 - multiply 5 - division, 6 - shulteNumbers, 7 - shulteAbacus, 8 - mentalMap
	if( number_job == 1 ) {
		source_number = getRandomInRange(min_in, max_in);
		// 1 - с голосом 2 - только голос 3 - без голоса
		if(voice !== 2) $("#source_number").html(source_number);
		if(voice != 3 && level < 3) {
      if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/'+sound_num[source_number]+'.opus'] }); plZvuk.play();}
		}
		time_out = 151;
	}
	if( number_job == 2 ) { time_out = 221; delay = parseInt($("#delay").attr('data-val')); /*console.log( 'delay - ' + delay );*/ }

    /* === инициализация 2 задания === */
    if( number_job == 2 ) {
        /* для создания карты ( 2 задание ) */
        function createMap(source_number, col) {
            if(source_number == 1) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
            if(source_number == 2) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
            if(source_number == 3) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
            if(source_number == 4) {  $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row4').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
            if(source_number == 5) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; });  }
            if(source_number == 6) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
            if(source_number == 7) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
            if(source_number == 8) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
            if(source_number == 9) {  $('.row_up').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top + 45}; }); $('.row1').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row2').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row3').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; }); $('.row4').filter('.col'+col).addClass("on").offset(function(i,val){ return {top:val.top - 45}; });  }
        }
        function generateMap(source_number, level) {
            if(level == 1) { createMap(source_number, 4); }
            if(level == 2) { source_number = String(source_number); createMap( source_number[1], 4); createMap( source_number[0], 3); }
            if(level == 3) { source_number = String(source_number);	createMap( source_number[2], 4); createMap( source_number[1], 3); createMap( source_number[0], 2); }
            if(level == 4) { source_number = String(source_number);	createMap( source_number[3], 4); createMap( source_number[2], 3); createMap( source_number[1], 2); createMap( source_number[0], 1);	}
        }
        /* сгенерим число первоначально */
        var source_number = getRandomInRange(min_in, max_in);
        /* и первоначально сгенерируем map abacus */
        generateMap(source_number, level);
        /* и спрячем карту */
        timeout_id = setTimeout(function () {
            $("#abacus").attr("style", "visibility:hidden");
            console.log('hidden1');
        }, delay);
    }
	if( (number_job == 3) || (number_job == 4) || (number_job == 5) ) {
		if(rows_for_calculate_rating == 1) time_out = 181;
		if(rows_for_calculate_rating == 2) time_out = 221;
		if(rows_for_calculate_rating == 3) time_out = 241;
		if(rows_for_calculate_rating == 4) time_out = 261;
		if(rows_for_calculate_rating == 5) time_out = 281;
		var rules = parseInt($("#rules").attr('data-val')); var id_rules = $("#id_rules").attr('data-val');
		generatePrimer();
	}
	if( number_job == 6 ) { time_out = 120; generateShulteNumbers(); }
	if( number_job == 7 ) { time_out = 140; generateShulteAbacus(); }
	if( number_job == 6 || number_job == 7) {
		if(voice != 3 && level < 3) {
      if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/'+sound_num[source_number]+'.opus'] }); plZvuk.play();}
		}
	}
	if( number_job == 8 ) {
	    time_out = 221;
	    operation = parseInt($("#operation").attr('data-val'));
		var rules = parseInt($("#rules").attr('data-val'));
		var id_rules = $("#id_rules").attr('data-val');
		delay = parseInt($("#delay").attr('data-val'));
		generatePrimer();
	}
	addDetailedJob('initialization');
	resetTimeOut();


	/* кнопка Следующая */
	$('#next_number').click(function(){
        nextNumberButton.attr("disabled", true); // delaem knopka neaktivnim
        isNextButton = true; //update_sparrow
        /*Job = 8 MentalMap*/
        isAnswerButton = false; // update_sparrow

        $("#j2_input_number").popover('hide');
		nextJob();
	});
	/* для 8 задания (mental Map) */
	$('#answer_number').click(function(){
		$("#wrap_job8").attr("style", "display:block");	console.log( 'source_number это ' + source_number );
		$("#wrap_job8").html('<p class="true_answer">true answer:</p><span style="color:green;">'+source_number+'</span>');

        isAnswerButton = true; // update_sparrow
        isNextButton = false; // update_sparrow
        $("#j2_input_number").popover('hide');
		setTimeout(function () {
			nextJob();
		}, 600);
	});


    //popover
    $("#j2_input_number").popover({
        content: 'Введите ответ', //'Вы ввели недопустимый символ, введите ответ заново'
        placement: 'top',
        trigger: 'manual'
    });
	/* для 2 и 8 заданий */
	$("#form_next_number").submit(function() {

        /*== Polzovatel doljen vvedit otvet ==*/
        var userAnswer = $("#j2_input_number").val();


        if (userAnswer == '') {
            $("#j2_input_number").popover('show');
            $("#j2_input_number").val('');
            return false;
        }
        isNextButton = false; //update_sparrow
        isAnswerButton = false; // update_sparrow
        $("#j2_input_number").popover('hide');
		nextJob();
		return false;
	});

	/* === NEXT job === */
	function nextJob() {
		count_job = count_job + 1;  $("#count_job").html(count_job);
		if( count_job == 20 ) {
			clearInterval(timerId); // для исключения повторной отправки результатов, по истечении времени (когда остаемся на странице после завершения заданий)
			timerIdDeactivated = true;
			if( (number_job == 6) || (number_job == 7) ) {
                if( source_number != selectNumberForShulteNumbers ) errors = errors + 1; $("#errors").html(errors); // update_sparrow
                if (isNextButton)  arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT';
                else arr_answers[arr_answers_count]['answer'] = selectNumberForShulteNumbers;
            } else if( (number_job == 2) || (number_job == 8) ) {

                if( source_number != parseInt($("#j2_input_number").val()) ) errors = errors + 1; $("#errors").html(errors); // update_sparrow
                if (isNextButton) {
                    arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT';
                } else if (isAnswerButton) {
                    arr_answers[arr_answers_count]['answer'] = 'кнопка ANSWER';
                } else {
                    arr_answers[arr_answers_count]['answer'] = parseInt($("#j2_input_number").val());
                }
                //console.log('number_job: ' + '2|8');
            } else {
                if( source_number == itogo && !isNextButton) {
                    arr_answers[arr_answers_count]['answer'] = itogo;
                    console.log('source_number == itogo' + ' itogo:' + itogo);
                } else {
                    errors = errors + 1; $("#errors").html(errors); // update_sparrow
                    arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT';
                    console.log('knopka NEXT');
                }
			}
			outMessageAndSendResult();
		} else {
			//следующее задание
			// набрать цифру НА абакусе
			if( number_job == 1 ) {
				if( source_number == itogo ) { $("#source_number").attr("style", "color:#8AE595");	setTimeout(function () { $("#source_number").attr("style", "color:#000000"); }, 1600); }
				else { errors = errors + 1; $("#errors").html(errors); }
				// log
				if( source_number == itogo ) arr_answers[arr_answers_count]['answer'] = itogo; else arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT';
				/* сгенерим следующее число */
				setTimeout(function () {
					resetAbacus();
					source_number = getRandomInRange(min_in, max_in); 
					// 1 - с голосом 2 - только голос 3 - без голоса
					if(voice !== 2) $("#source_number").html(source_number);
					if(voice != 3 && level < 3) {
            if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/'+sound_num[source_number]+'.opus'] }); plZvuk.play();}
					}
					addDetailedJob('from_nextJob');

                    nextNumberButton.attr("disabled", false); // delaem knopka aktivnim
				}, 1600);
			}
			// сложение, умножение, деление
			if( (number_job == 3) || (number_job == 4) || (number_job == 5) ) {
				if( source_number == itogo && !isNextButton) { $("#source_number").attr("style", "color:#8AE595");	setTimeout(function () { $("#source_number").attr("style", "color:#000000"); }, 1600); }
				else { errors = errors + 1; $("#errors").html(errors); }
				// log
				if( source_number == itogo && !isNextButton) arr_answers[arr_answers_count]['answer'] = itogo; else arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT';
				isNextButton = false;
				/* сгенерим следующее число */
				setTimeout(function () {
					resetAbacus();
					generatePrimer(); count_part_for_coloring_green = 1;
					addDetailedJob('from_nextJob');
                    nextNumberButton.attr('disabled', false);				// sdealaem knopka aktivnim
                }, 1600);
			}
			// ввести число в поле ввода С абакуса
			if( (number_job == 2) || (number_job == 8) ) {
				// проверка числа из Инпута
				if( source_number != parseInt($("#j2_input_number").val()) ) {
					$("#j2_input_number").attr("style", "color:#FF6666");
					errors = errors + 1; $("#errors").html(errors);
					//if(!parseInt($("#j2_input_number").val())) arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT'; else arr_answers[arr_answers_count]['answer'] = parseInt($("#j2_input_number").val());
				} else {
					$("#j2_input_number").attr("style", "color:#8AE595");
					//arr_answers[arr_answers_count]['answer'] = source_number;
				}
                // update_sparrow
                if (isNextButton) {
                    arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT';
                } else if (isAnswerButton) {
                    arr_answers[arr_answers_count]['answer'] = 'кнопка ANSWER';
                } else {
                    arr_answers[arr_answers_count]['answer'] = parseInt($("#j2_input_number").val());
                }

				if(number_job == 2) {
					clearTimeout(timeout_id);  									// сброс демонстрации абакуса
					source_number = getRandomInRange(min_in, max_in);			// сгенерим следующее число
					resetAbacus();												// сброс абакуса, без задержки
					generateMap(source_number, level);							// и сразу перегенерим его
					addDetailedJob('from_nextJob');
					setTimeout(function () {
						$("#j2_input_number").attr("style", "color:#000000"); 	// черный цвет цифрам
						$("#j2_input_number").val(''); 							// очистим инпут
						$("#abacus").attr("style", "visibility:none");  		// покажем абакус
                        nextNumberButton.attr('disabled', false);				// sdealaem knopka aktivnim
					}, 600);
					// прячем абакус
					timeout_id = setTimeout(function () {
						$("#abacus").attr("style", "visibility:hidden");
                        console.log("hidden");
					}, delay + 600);
				} else {
					//addDetailedJob('from_nextJob');
					setTimeout(function () {
						$("#j2_input_number").attr("style", "color:#000000"); 	// черный цвет цифрам
						$("#j2_input_number").val(''); 							// очистим инпут
                        nextNumberButton.attr('disabled', false);				// sdealaem knopka aktivnim
                    }, 600);
					generatePrimer();
                    addDetailedJob('from_nextJob'); // update_sparrow
                }
			}
			// найти НА абакусе сгенерированное число
			if( (number_job == 6) || (number_job == 7) ) {
				if( source_number == selectNumberForShulteNumbers ) { $("#source_number").attr("style", "color:#8AE595"); zvuk(); } 
				else { errors = errors + 1; $("#errors").html(errors); $("#source_number").attr("style", "color:#FF6666"); }
				setTimeout(function () { $("#source_number").attr("style", "color:#000000"); }, 1600); // вернем черный цвет числу

                if (isNextButton)  arr_answers[arr_answers_count]['answer'] = 'кнопка NEXT'; // update_sparrow
                else arr_answers[arr_answers_count]['answer'] = selectNumberForShulteNumbers; // update_sparrow

				// сгенерим следующее число
				setTimeout(function () {
					if(number_job == 6) generateShulteNumbers(); else generateShulteAbacus();
					if(voice != 3 && level < 3) {
            if(zvuk_on) {plZvuk = new Howl({ src: ['/sounds/'+sound_num[source_number]+'.opus'] }); plZvuk.play();}
					}
					addDetailedJob('from_nextJob');
                    nextNumberButton.attr('disabled', false);				// sdealaem knopka aktivnim
                }, 1600);
			}
		}
	}
	
	/* ====== отработка заданий ====== */
	function runJob() {
		if( number_job == 1 ) {
			/* если при очередном передвижении кости угадали число */
			if( source_number == itogo ) {
				//arr_answers[arr_answers_count]['answer'] = itogo;
				nextJob();
			}
		}
		if( (number_job == 3) || (number_job == 4) || (number_job == 5) ) {
			if( rules !== 2 && values_for_job3[0] == itogo ) {
				if(number_job == 3) { $("#count_part"+count_part_for_coloring_green).attr("style", "color:green");  count_part_for_coloring_green++; } // только для + / -
				values_for_job3.splice(0, 1);
				if (values_for_job3.length == 0) {
					//arr_answers[arr_answers_count]['answer'] = 'Пример решен';
					nextJob();
				}
			} else if ( source_number == itogo ) {
				nextJob();
			}
		}
	}
	

});