var jtSimulator = (function() {

    var version = 1.0;

    //events
    var eventCountdownEnd = document.createEvent('Event');
    eventCountdownEnd.initEvent('jt.countdown.end', true, true);

    function assignDefaults() {  }

    return {
        defaults: function() {  },
        renderCountdown: function (params) {
            if ( typeof (params) !== 'object') return;

            var colors = ['red', 'orange', 'green', 'darkblue'];

            var node = params.node;
            var renderFunction = params.renderFunction || function (node, delay) {
                $(node).fadeIn(100).delay(delay - 200).fadeOut(100);
            };
            var startIterator = 800;
            var iterator = startIterator;

            var countdownVoice;
            var startVoice;
            if (params.sounds && params.sounds.countdown && params.sounds.start) {
                countdownVoice = playSound(params.sounds.countdown, true);
                startVoice = playSound(params.sounds.start, true);
            }

            params.items.forEach(function (item, i, arr) {
                setTimeout(function () {
                    var elem = '<span style="color: ' + colors[i] + ';">' + item + '</span>';
                    $(node).html(elem);

                    //play sound
                    if (arr.length - 1 !== i) {
                        if (countdownVoice) countdownVoice.play();
                    } else {
                        if (startVoice) startVoice.play();
                    }

                    renderFunction(node, params.delay);
                }, iterator);
                iterator += params.delay;


                if (arr.length - 1 === i) {
                    setTimeout(function () {
                        document.dispatchEvent(eventCountdownEnd);
                    }, params.delay * (i + 1) + startIterator);
                }
            });

            function playSound(src, noPlay) {
                var sound = new Howl({
                    src: src,
                    volume: 0.8
                });
                if (noPlay === undefined) sound.play();

                return sound;
            }

        }
    }

})();