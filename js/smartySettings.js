/**
 * Created by Javlonbek on 19.08.2018.
 */


$(document).ready(function() {


    // =========== Примеры Smarty ===========
    // добавим строки дополнительных примеров

    // параметры
    var exampleRows = 2;
    var exampleQuantity = 1;
    var htmlExample = '';
    var wrapExamples = document.getElementById('wrap_examples');

    /* rows and quantity */
    var rowsInput = $('#createsmartyform-rows');
    var quantityInput = $('#createsmartyform-quantity');

    /* funksiya dlya raschet summa */
    var eventSumma = function() {
        console.log('input');
        var sum = 0;
        var id_wrap = $(this).parent().parent().attr('id');

        $("#"+id_wrap+">div>.ex_value").each(function(){

            if( $(this).val()[0] == '-' && $(this).val().length > 1 ) {
                sum = sum - parseInt($(this).val().substr(1));
            }
            else if( $(this).val()[0] == '*' && $(this).val().length > 1 ) {
                sum = sum * parseInt($(this).val().substr(1));
            }
            else if( $(this).val()[0] == '/' && $(this).val().length > 1 ) {
                sum = sum / parseInt($(this).val().substr(1));
            }
            else if( $(this).val().length > 0 ) {
                sum = sum + parseInt($(this).val());
            }
        });
        $("#"+id_wrap+">div>.ex_answer").val(sum);	//console.log( 'ex_value_1 - ' + $("input[name=ex_value_1]").val());
    };

    var renderFunc = function () {

        console.log('touch');
        exampleRows = getCountRowsExamples(); //isNaN(parseInt($('#createsmartyform-rows').val())) ? 2 : parseInt($('#createsmartyform-rows').val());
        exampleQuantity = getCountExamples(); //isNaN(parseInt($('#createsmartyform-quantity').val())) ? 1 : parseInt($('#createsmartyform-quantity').val());
        console.log(exampleRows + ' ' + exampleQuantity);

        while (wrapExamples.firstChild) {
            wrapExamples.removeChild(wrapExamples.firstChild);
        }

        // name="'+j+'ex_value_'+i+'"
        // name="'+j+'ex_answer"

        htmlExample = '';
        for (var j = 1; j <= exampleQuantity; j++) {
            htmlExample += '<div id="'+j+'" class="col-md-12">';
            for (var i = 1; i <= exampleRows; i++) {
                if ( i == 1 ) {
                    htmlExample += '<div class="col-md-2" style="margin-top: 40px"><strong>' + j +'-пример</strong></div>'
                }
                htmlExample += '<div class="col-md-1">';
                htmlExample += '<p style="margin:13px 0 5px;">'+i+' чис.</p>';
                htmlExample +='<input class="form-control ex_value input-lg" value="" name="CreateSmartyForm[examples]['+j+'][example][]" type="text">'
                    + '</div>';
            }
            htmlExample +=    '<div class="col-md-2">'
                + '<p style="margin:13px 0 5px;">Ответ:</p>'
                + '<input readonly class="form-control ex_answer input-lg" value="" name="CreateSmartyForm[examples]['+j+'][answer]" type="text">'
                + '</div>';
            htmlExample += '</div>';
        }
        wrapExamples.innerHTML = htmlExample;


        // вычисление ответа в добавлении примеров в Smarty
        $(".ex_value").on('input', eventSumma); // veshaem obrabotchik sobitiy dlya dinamicheskiy dobavlenniy elementam

        //test
        // generateNumbersForTables();

    };


    //dlya nachalniy render, odin raz rabotaet i v nachale
    renderFunc();

    /* esli najmit + ili - v forme*/
    //$('#smartyForm').on('touchspin.on.startspin', renderFunc);
    $('[data-krajee-touchspin]').on('change', renderFunc);

    // vizivaem sobitiya
    $('#smartyForm').trigger('touchspin.on.startspin');

    $('#smarty_save_button').on('click', validationExamples);

    // rows input value
    function getCountRowsExamples()
    {
        return isNaN(parseInt(rowsInput.val())) ? 2 : parseInt(rowsInput.val());
    }

    // quantity input value
    function getCountExamples()
    {
        return isNaN(parseInt(quantityInput.val())) ? 1 : parseInt(quantityInput.val());
    }

    function generateNumbersForTables() {
        var sum = 0;
        var id_wrap = $(".ex_value").parent().parent().attr('id');

        var countExamples = getCountExamples();
        var countRows = getCountRowsExamples();

        for (var i = 1; i <= countExamples; i++) {
            $("#"+i+">div>.ex_value").each(function(){
                $(this).val('1')
            });
            $("#"+i+">div>.ex_answer").val(countRows);	//console.log( 'ex_value_1 - ' + $("input[name=ex_value_1]").val());
        }
    }

    //examples validation
    function  validationExamples() {
        var countExamples = getCountExamples();
        var countRows = getCountRowsExamples();

        var isValid = true;
        for (var i = 1; i <= countExamples; i++) {
            $("#"+i+">div>.ex_value").each(function(){
                var val = $(this).val();
                console.log('type:', typeof val, 'value:', val);
                if (val.length == 0) isValid = false;
            });
            //$("#"+i+">div>.ex_answer").val(countRows);	//console.log( 'ex_value_1 - ' + $("input[name=ex_value_1]").val());
            var answerVal = $("#"+i+">div>.ex_answer").val();
            console.log('type:', typeof answerVal, 'value:', answerVal);
            if (answerVal.length == 0 || answerVal == 'NaN') isValid = false;

        }

        if (!isValid) {
            var alertBlock = document.querySelector('#alert-block .alert');

            alertBlock.innerHTML = 'Сохранение невозможно. Заполните все ячейки.';
            alertBlock.style.display = 'block';

            setTimeout(function () {
                alertBlock.style.display = 'none';
            }, 3000);
            return false;
        }
    }



    // =========== end Примеры Smarty ===========

});

