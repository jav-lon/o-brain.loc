/* ==== events ==== */
var saveSuccessEvent = document.createEvent('Event');
saveSuccessEvent.initEvent('jt.save-settings.success', true, true);

var saveErrorEvent = document.createEvent('Event');
saveErrorEvent.initEvent('jt.save-settings.error', true, true);

var exerciseEndEvent = document.createEvent('Event');
exerciseEndEvent.initEvent('jt.exercise.end', true, true);

isMentalMapExercise = true;
minDelayForVoice = 1900;

//params
var alertMessages = {
    exerciseInput: 'Выберите задание!',
    saveSettingsError: 'Ошибка!',
    mentalmapInputRequired: "Введите ответ",
    delayMinValue: "При данных настройках скорости, функция произношения цифр не работает"
};

// settings params
var m_delay = minDelayForVoice;
var m_voice = 3;
var m_voice_lang = 'ru-RU';

var m_parts_for_job8_array = [];
var m_znak_operat_array = [];
var m_count_examples = 0;
var m_answers_examples = [];
var m_id_user;

var m_resultValues;
var m_preloader_DOM;

$(document).ready(function(){

    //DOM elements
    var voiceLangDOMContainer = $('#m_voice_lang_column');
    var countdownDOMContainer = $('#m_countdown');
    var settingsDOMContainer = $('#m_settings_form');
    var mentalmapDOMContainer = $('#m_mentalmap_container');
    var sendButton = $('#m_send_button');
    var preloader = m_preloader_DOM =  $('#m_preloader');
    var resultDOMContainer = $('#m_result_bg');
    var newExerciseBtn = $('#new_exercise_btn');

    //result DOM elements
    var resultTrueAnswersElem = $('#m_cnt_true_answer_render');
    var resultCountErrorsElem = $('#m_cnt_error_answer_render');
    var resultTimeElem = $('#m_time_render');

    // input DOM elements
    var exerciseInput = $('select[name=m_exercise_elem]');
    var delayInput = $('#m_delay_range_elem');
    var voiceInput = $('#m_voice_elem');
    var voiceLangInput = $('input[name=m_voice_lang]');

    // form inputs value
    var inputExerciseValue;

    // init materialize select
    $('select').formSelect();

    //izmnenenie po range
    delayInput.change(function () {
        if (voiceInput.prop('checked') === true && $(this).val() < m_delay/1000) {
            $(this).val(m_delay/1000);
            M.Toast.dismissAll();
            M.toast({html: alertMessages.delayMinValue});
        }
    });

    // izmenenie po ozvuchke
    voiceInput.change(function () {
        if (this.checked) { // ozvuchka vkluchen
            voiceLangDOMContainer.addClass('scale-in');
            if(delayInput.val() < m_delay/1000) {
                delayInput.val(m_delay/1000);
                M.Toast.dismissAll();
                M.toast({html: alertMessages.delayMinValue});
            }
        } else { // ozvuchka viklyuchen
            voiceLangDOMContainer.removeClass('scale-in');
        }
    });

    //klick po knopke "Nachat"
    sendButton.click(function (e) {

        var selectValue = exerciseInput.val();

        // 1. proverayem forma
        if ( selectValue === null) {
            // vivedem uvedomlenie
            M.Toast.dismissAll();
            M.toast({html: '<i class="material-icons">error_outline</i> ' + alertMessages.exerciseInput});
        } else {
            M.Toast.dismissAll();
            preloader.addClass('scale-in'); // otkrivaem preloader
            settingsDOMContainer.hide(); // skrivaem settings

            //init settings value
            m_delay = parseInt( +delayInput.val() * 1000 );
            m_voice = voiceInput.prop('checked') === true ? 2 : 3;
            m_voice_lang = voiceLangInput.filter(':checked').val();

            //alert('m_delay, m_voice, m_voice_lang ==> ' + m_delay + ',' + m_voice + ',' + m_voice_lang);

            sendSettings(selectValue); // otpravim nastroyki na server
        }

        return false;
    });

    // uspeshno
    document.addEventListener('jt.save-settings.success', function(e) {
        preloader.removeClass('scale-in'); // skrivaem preloader
        countdownDOMContainer.removeClass('hide'); // otkrivaem countdown
        mentalmapDOMContainer.removeClass('hide'); // otkrivaem mentalmap container

        jtSimulator.renderCountdown({
            delay: 900,
            items: ['3', '2', '1', 'START'],
            node: '#m_countdownText',
            sounds: {
                countdown: ['/sounds/countdown/countdown_beep.wav'],
                start: ['/sounds/countdown/start.mp3']
            },
            renderFunction: function (node, delay) {
                $(node).addClass('scale-in');
                setTimeout(function () {
                    $(node).removeClass('scale-in');
                }, delay-100);
            }
        });
    });

    // oshibka
    document.addEventListener('jt.save-setings.error', function (e) {
        settingsDOMContainer.show() // skrivaem settings
        preloader.removeClass('scale-in'); // zakrivaem preloader

        M.toast({html: '<i class="material-icons">error_outline</i> ' + alertMessages.saveSettingsError});
    });

    //exercise ended
    document.addEventListener('jt.exercise.end', function (evt) {
        resultTrueAnswersElem.text(m_resultValues.true_answers);
        resultCountErrorsElem.text(m_resultValues.errors);
        resultTimeElem.text(m_resultValues.time);

        mentalmapDOMContainer.hide();
        preloader.removeClass('scale-in');
        resultDOMContainer.addClass('scale-in');

        setTimeout(function () {
            renderStars();
        }, 500);

        setTimeout(function () {
            addPulseForButton();
        }, 3000);
    });

    function sendSettings(selectValue) {
        // otpravim nastroyki na server
        $.ajax({
            url: "/simulator/get-exercise-mental-map",
            method: 'POST',
            data: {"id_mentalmap": selectValue},
            success: function (data) {
                //alert(JSON.stringify(data));
                m_id_user = data.id_user;
                initExamples(data);
                //alert('m_parts_for_job8_array => ' + m_parts_for_job8_array + ' m_znak_operat_array => ' + m_znak_operat_array + ' m_answers_examples => ' + m_answers_examples);
                document.dispatchEvent(saveSuccessEvent);
            },
            error: function (data) {
                //alert(JSON.stringify(data));
                document.dispatchEvent(saveErrorEvent);
            }
        });

    }
    //renderStars();

    function renderStars() {
        var startDOMContainer = $('#m_stars .scale-transition');

        var iterator = 100;
        var countStars = getCountStars();
        startDOMContainer.each(function () {
            var self = this;
            var bindFunc = bindFuncForTimeout.bind(this, self, countStars--);
            setTimeout(bindFunc, iterator);


            iterator += 200;
        });

        function getCountStars() {
            var countStars = 1;

            switch (m_resultValues.errors) {
                case 0: countStars = 5; break;
                case 1: countStars = 4; break;
                case 2: countStars = 3; break;
                case 3: countStars = 2; break;
            }

            return countStars;
        }
    }


    function addPulseForButton() {
        newExerciseBtn.addClass('pulse');
    }
    
    function initExamples(data) {
        var count = data.count;
        var examples = data.exercise.examples;
        var answers = data.exercise.answers;

        var operations;
        var parts;
        examples.forEach(function (ex, i, array) {
            operations = [];
            parts = [];
           ex.forEach(function (item, j) {
               console.log('item: '+item);
                if (!isNumeric(+item[0])) {
                    operations.push(" " + item[0] + " ");
                    parts.push(item.slice(1));
                } else {
                    operations.push(' ');
                    parts.push(item.slice(0));
                }
           })
            console.log('m_parts_for_job8_array: ' + JSON.stringify(m_parts_for_job8_array));
            console.log('m_znak_operat_array: ' + JSON.stringify(m_znak_operat_array));
            m_parts_for_job8_array[i] = parts;
            m_znak_operat_array[i] = operations;

        });
        m_answers_examples = answers;
        m_count_examples = count;


    }

    //bind
    function bindFuncForTimeout(self, countStars) {
        if (countStars > 0) {
            $(self).addClass('active-star');
            $(self).removeClass('disabled-star');
        }
        $(self).removeClass('scale-out');
    }
});

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}