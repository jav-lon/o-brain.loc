$(document).ajaxComplete(function() {

	//console.log('ajax');

	/* ========= НАСТРОЙКИ ========= */
	// ------ при загрузке страницы упражнения, первоначальная установка настроек ------
	if( $('input[name=rules]:checked').val() == 2 ) {  // если С правилами, сложность, рядность и вид задания - блокируем
		$("input[name=level]").each(function(){	$(this).attr("disabled", true); });
		$("input[name=rows]").each(function(){ $(this).attr("disabled", true); });
		$("input[name=operation]").each(function(){	$(this).attr("disabled", true); });
	} else {
		$("input[name=level]").each(function(){	$(this).attr("disabled", false); });
		$("input[name=rows]").each(function(){ $(this).attr("disabled", false); });
		$("input[name=operation]").each(function(){	$(this).attr("disabled", false); });
		$("#btn_rulesOpen").attr("style", "display:none"); // кнопка Выбрать правила
	}
	//console.log($('input[name=operation]:checked').val());
	// если выбрано С правилами, сложность, рядность и вид задания - блокируем
	if( $('input[name="Settings[operation]"]:checked').val() == 2 || $('input[name="Settings[operation]"]:checked').val() == 4 || $('input[name="Settings[operation]"]:checked').val() == 6 ) {
		$("input[name='Settings[rows]']").each(function(){ $(this).prop("checked", false); $(this).attr("disabled", true); });
		$("input[name='Settings[rows]']").filter("[value=2]").attr("disabled", false);
		$("input[name='Settings[rows]']").filter("[value=2]").prop("checked", true);
	}
	var number_job_from_index = parseInt($("#number_job_from_index").attr('data-val'));
	if(number_job_from_index == 8) {  // для MentalMap: если С голосом, задержку блокируем
		if( $('input[name="Settings[voice]"]:checked').val() !== 3 ) {
			$("input[name='Settings[delay]']").filter("[value=800]").prop("checked", true);
			$("input[name='Settings[delay]']").filter(':not(:checked)').each(function(){	$(this).attr("disabled", true);	});
		}
	}
	// ------ действия в настройках ------
	$(document).on('click','input[name="Settings[operation]"]', function(){ // клик по Умножение, Деление и Умножение и Деление. Оставляем Рядность только 2
		if( ($(this).val() == 2) || ($(this).val() == 4) || ($(this).val() == 6) ) {
			$("input[name='Settings[rows]']").each(function(){ $(this).prop("checked", false); $(this).attr("disabled", true); });
			$("input[name='Settings[rows]']").filter("[value=2]").attr("disabled", false);
			$("input[name='Settings[rows]']").filter("[value=2]").prop("checked", true);
		} else {
			$("input[name='Settings[rows]']").each(function(){ $(this).attr("disabled", false); });
		}
	});
	$(document).on('click', 'input[name="Settings[voice]"]', function(){
		if( ($(this).val() == 1) || ($(this).val() == 2) ) {
			$("input[name='Settings[delay]']").filter("[value=800]").prop("checked", true);
			$("input[name='Settings[delay]']").filter(':not(:checked)').each(function(){	$(this).attr("disabled", true);	});
		} else {
			$("input[name='Settings[delay]']").each(function(){	$(this).attr("disabled", false);	});
		}
	});
	$(document).on('click', 'input[name="Settings[rules]"]', function(){  // при клике на правила: если С правилами, сложность и вид задания - блокируем
		if( $(this).val() == 1 ) {
			$("input[name='Settings[level]']").each(function(){	$(this).attr("disabled", false); });
			$("input[name='Settings[rows]']").each(function(){ $(this).attr("disabled", false); });
			$("input[name='Settings[operation]']").each(function(){	$(this).attr("disabled", false); });
		} else {
			$("input[name='Settings[level]']").filter(':not(:checked)').each(function(){	$(this).attr("disabled", true); });
			$("input[name='Settings[rows]']").filter(':not(:checked)').each(function(){ $(this).attr("disabled", true); });
			$("input[name='Settings[operation]']").filter(':not(:checked)').each(function(){	$(this).attr("disabled", true); });
		}
	});
	// ------ сброс настроек ------
	/*$('#btn_settingsReset').click(function(){
		$("input[name=level]").each(function(){	$(this).attr("disabled", false); });
		$("input[name=rows]").each(function(){ $(this).attr("disabled", false); });
		$("input[name=operation]").each(function(){	$(this).attr("disabled", false); });
		$("input[name=level]").each(function(){	$(this).prop("checked", false); });
		$("input[name=level]").filter("[value=1]").prop("checked", true);
		$("input[name=rows]").each(function(){	$(this).prop("checked", false); });
		$("input[name=rows]").filter("[value=2]").prop("checked", true);
		$("input[name=delay]").filter("[value=800]").prop("checked", true);
		$("input[name=voice]").filter("[value=1]").prop("checked", true);
		$("input[name=operation]").filter("[value=1]").prop("checked", true);
		$("input[name=rules]").filter("[value=1]").prop("checked", true);
		$("#btn_rulesOpen").attr("style", "display:none");
	});*/
	// --- отобразить кнопку выбора правил ---
	$(document).on('click', 'input[name="Settings[rules]"]', function(){
		if( $(this).val() == 2 ) {
			$("#btn_rulesOpen").attr("style", "display:inline");
		} else {
			$("#btn_rulesOpen").attr("style", "display:none");
		}
	});
	// --- открыть окно правил ---
	$('body').on('click', '#btn_rulesOpen', function(){
		$("#laws").attr("style", "display:block");
	});
	// --- закрыть окно правил ---
    $('body').on('click', '#btn_rulesSave', function(){
		$("#laws").attr("style", "display:none");
	});
	// ------ сохранение настроек ------
	$('#btn_settingsSave').click(function(){
		if(number_job == 1) {}
		if(number_job == 2) {}
		if(number_job == 3) {}
		if(number_job == 4) {}
		if(number_job == 5) {}
		if(number_job == 6) {}
		if(number_job == 7) {}
		if(number_job == 8) {}
			var str_rules = ''; str_levels = ''; str_rows = '';
			// соберем выбранные законы (правила)
			if($(".btn_settingsOpen_from_job").length) { str_rules = 'from_job'; } // если сохраняем "из задания", правила недоступны...
			else {
				$(".checkboxs_rules:checked").each(function(){ str_rules = str_rules + this.value + ';'; });
			}
			$("input[name=rows]:checked").each(function(){ str_rows = str_rows + this.value + ';'; });
			$("input[name=level]:checked").each(function(){ str_levels = str_levels + this.value + ';'; });
			
			if(number_job_from_index == 1 || number_job_from_index == 6 || number_job_from_index == 7) $.post("/css/saveSettings.php", {id_user: id_user, id_job: number_job_from_index, level: str_levels, rows: '0', operation: 0, voice: $('input[name=voice]:checked').val(), rules: 0, delay: 0,str_rules:'no_used'});
			if(number_job_from_index == 2) $.post("/css/saveSettings.php", {id_user: id_user, id_job: number_job_from_index, level: str_levels, rows: '0', operation: 0, voice: 0, rules: 0, delay: $('input[name=delay]:checked').val(),str_rules:'no_used'});
			if(number_job_from_index == 3) $.post("/css/saveSettings.php", {id_user: id_user, id_job: number_job_from_index, level: str_levels, rows: str_rows, operation: 0, voice: $('input[name=voice]:checked').val(), rules: $('input[name=rules]:checked').val(), delay: 0,str_rules:str_rules});
			if(number_job_from_index == 4 || number_job_from_index == 5) $.post("/css/saveSettings.php", {id_user: id_user, id_job: number_job_from_index, level: str_levels, rows: '2', operation: 0, voice: $('input[name=voice]:checked').val(), rules: $('input[name=rules]:checked').val(), delay: 0,str_rules:str_rules});
			if(number_job_from_index == 8) $.post("/css/saveSettings.php", {id_user: id_user, id_job: number_job_from_index, level: str_levels, rows: str_rows, operation: $('input[name=operation]:checked').val(),voice: $('input[name=voice]:checked').val(),rules: $('input[name=rules]:checked').val(),delay: $('input[name=delay]:checked').val(),str_rules:str_rules});
			// покажем процесс отправки
			$("#loading").attr("style", "display:block");
			setTimeout(function() {
				$("#loading").attr("style", "display:none");
				if($(".btn_settingsOpen_from_job").length) {
					$("#settings").attr("style", "display:none");
					if( time_out > 1 ) { resetTimeOut(); } // продолжим отсчет	//	console.log( 'закрыли из окна настроке ');
				} else {
					if(number_job_from_index == 8) window.location.href = "/jobs/mentalmap";
					else if(number_job_from_index == 1) window.location.href = "/jobs/numbers";
					else if(number_job_from_index == 2) window.location.href = "/jobs/map";
					else if(number_job_from_index == 3) window.location.href = "/jobs/slalom";
					else if(number_job_from_index == 4) window.location.href = "/jobs/multiply";
					else if(number_job_from_index == 5) window.location.href = "/jobs/division";
					else if(number_job_from_index == 6) window.location.href = "/jobs/shulteNumbers";
					else if(number_job_from_index == 7) window.location.href = "/jobs/shulteAbacus";
				}
			}, 2000 );
	});
	/* ======= END Окно настройки ======== */
	

});